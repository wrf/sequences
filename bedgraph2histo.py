#!/usr/bin/env python
#
# info for bedtools found here
# http://bedtools.readthedocs.org/en/latest/content/tools/genomecov.html
# v1.1 2019-11-04
# v1.2 2022-04-29 add catch for scientific notation in bedgraph

'''bedgraph2histo.py  last modified 2022-12-02
    starting from a bedgraph
    make table (for histograms) of coverage vs number of bases

bedgraph2histo.py -b genome.bg > genome_cov.histo.tab

    output appears as:
coverage    basecount

    generate the bedgraph from the read mappings with bedtools
    https://github.com/arq5x/bedtools2

bedtools genomecov -ibam genomic_bowtie2.sorted.bam -bga -g contigs.fa > genomic_bedtools_genomecov.bg

    using optional -c FILE
    total counts per contig are written to file, appearing as:
contig001   3302461

'''

import sys
import argparse
import time
from collections import defaultdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--bedgraph', help=".bg format file from bedtools")
	parser.add_argument('-m','--maximum', help="only report coverage values up to maximum, i.e. to ignore coverage greater than -m in case of repeats, transposons, etc.", type=int)
	parser.add_argument('-p','--percentage', action="store_true", help="output as percentage instead of raw count")
	parser.add_argument('-c','--contig-counts', help="optional filename to write contig counts to this file for debugging")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	totalcount = 0
	histocount = defaultdict(int)
	contigcount = defaultdict(int)
	sys.stderr.write("# Parsing bedgraph input {}  {}\n".format(args.bedgraph, time.asctime()) )
	for line in open(args.bedgraph,'r').readlines():
		line = line.strip()
		# bedgraph in format of:
		# contig_1	1815	1892	0
		lsplits = line.split("\t")
		contigname, startpos, endpos, coverage = lsplits[0:4]
		span = int(endpos)-int(startpos)
		totalcount += span
		contigcount[contigname] += span
		try:
			coverage = int(coverage)
		except ValueError: # caused by invalid literal for int() with base 10: '1.18157e+06'
			coverage = int(float(coverage))
		histocount[coverage] += span
	sys.stderr.write("# Counted {} total bases  {}\n".format(totalcount, time.asctime()) )

	highestcov = max(histocount.keys())
	if args.maximum:
		highestcov = min(args.maximum, highestcov)
	for i in range(highestcov + 1):
		if args.percentage:
			wayout.write("{}\t{:.6f}\n".format(i, histocount[i]*100.0/totalcount) )
		else:
			wayout.write("{}\t{}\n".format(i, histocount[i]) )

	if args.contig_counts:
		sys.stderr.write("# Writing contig counts to {}  {}\n".format(args.contig_counts, time.asctime()) )
		with open(args.contig_counts, 'w') as cc:
			for k,v in sorted(contigcount.items(), key=lambda x: x[0]):
				cc.write( "{}\t{}\n".format(k, v) )

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
