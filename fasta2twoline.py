#! /usr/bin/env python
#
# fasta2twoline.py 2016-09-29
# python3 update 2022-10-20

'''convert padded fasta to two line fasta format:

fasta2twoline.py seqs.fasta > twoline_seqs.fasta
'''

import sys

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	fastafile = sys.argv[1]
	seq = ""
	sys.stderr.write( "# Reading {}\n".format( fastafile ) )
	for line in open(fastafile,'r'):
		line = line.strip()
		if line:
			if line[0]==">":
				if seq:
					print(seq, file=sys.stdout)
					seq = ""
				print(line, file=sys.stdout)
			else:
				seq += line
	else: # print last line
		if seq: # if any sequence was collected for last entry
			print(seq, file=sys.stdout)
	sys.stderr.write( "# Done converting\n" )
