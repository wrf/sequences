#!/usr/bin/env python
#
# compare_transcript_versions.py v1 2020-05-19

'''compare_transcript_versions.py  last modified 2020-05-21

compare_transcript_versions.py -q transcripts.fasta -s models.fasta -t 4 > summary.txt

blastn -query nveGenes.vienna130208.fasta -db Nemve1.FilteredModels1_for_jbrowse.fasta -outfmt 6 -evalue 1e-5 -num_threads 4 -out nveGenes.vienna130208.v_Nemve1.blastn.tab

compare_transcript_versions.py -q nveGenes.vienna130208.fasta -s Nemve1.FilteredModels1_for_jbrowse.fasta -b nveGenes.vienna130208.v_Nemve1.blastn.tab

'''

import sys
import time
import gzip
import argparse
from collections import defaultdict, namedtuple
from Bio import SeqIO


example_blast = """NVE3	t196074	100.000	762	0	0	497	1258	1055	1816	0.0	1408
NVE3	t196074	100.000	494	0	0	1	494	328	821	0.0	913
NVE3	t196074	97.546	163	2	2	1289	1450	2150	2311	7.44e-74	278
NVE3	t219232	98.988	494	5	0	1	494	328	821	0.0	885
NVE3	t215415	94.906	530	8	6	493	1014	1355	1873	0.0	811
NVE3	t215415	98.291	234	4	0	261	494	644	877	7.03e-114	411
NVE3	t215415	97.653	213	5	0	1	213	435	647	1.54e-100	366
NVE3	t215415	96.373	193	5	2	1259	1450	3334	3525	1.58e-85	316
NVE3	t215415	98.837	86	1	0	1173	1258	1871	1956	1.31e-36	154
NVE3	t247996	97.765	358	8	0	901	1258	3040	3397	3.86e-176	617
NVE3	t247996	98.113	212	4	0	283	494	1939	2150	1.19e-101	370
NVE3	t247996	97.279	147	2	1	520	664	2786	2932	5.83e-65	248
NVE3	t247996	91.743	109	3	1	765	867	2931	3039	2.19e-34	147
NVE3	t248755	98.559	347	5	0	148	494	781	1127	4.99e-175	614
NVE3	t248755	94.595	370	5	3	493	854	1746	2108	2.37e-158	558
NVE3	t222672	89.831	177	4	1	1259	1435	882	1044	5.92e-55	215
NVE4	t196075	100.000	595	0	0	143	737	1943	2537	0.0	1099
NVE4	t218335	94.025	636	13	2	147	781	1	612	0.0	941
NVE4	t213522	97.978	544	7	2	239	781	531	1071	0.0	941
NVE4	t213522	98.156	488	5	2	291	777	46	530	0.0	848
NVE4	t213522	100.000	37	0	0	162	198	9	45	2.48e-11	69.4
NVE4	t247720	98.529	408	5	1	375	781	1	408	0.0	719
NVE5	t43493	99.189	493	1	2	569	1058	1	493	0.0	885
NVE5	t55155	98.851	174	2	0	567	740	1	174	5.56e-84	311
NVE6	t176880	100.000	1188	0	0	137	1324	1	1188	0.0	2194
NVE6	t219233	99.436	532	3	0	791	1322	1	532	0.0	966
NVE6	t219233	99.647	283	1	0	1401	1683	533	815	6.48e-146	518
NVE7	t176880	98.933	375	2	2	87	460	1187	1560	0.0	669
NVE8	t219223	94.498	618	30	2	574	1187	1042	1659	0.0	950
NVE8	t219223	97.887	284	6	0	292	575	701	984	2.08e-138	492
NVE8	t225065	94.337	618	31	2	574	1187	1287	1904	0.0	944
NVE8	t225065	96.127	284	11	0	292	575	946	1229	4.53e-130	464
NVE9	t157632	100.000	1076	0	0	34	1109	1	1076	0.0	1988
NVE9	t8845	100.000	230	0	0	831	1060	1	230	2.71e-118	425
NVE9	t5543	94.340	212	12	0	849	1060	1	212	2.83e-88	326
NVE9	t5443	93.868	212	13	0	849	1060	1	212	1.32e-86	320
NVE10	t176889	99.950	1988	0	1	1389	3375	1333	3320	0.0	3664
NVE10	t176889	97.961	1275	2	5	32	1297	1	1260	0.0	2189
NVE10	t176889	100.000	188	0	0	3834	4021	3320	3507	2.01e-94	348
NVE10	t176889	98.540	137	2	0	1257	1393	1082	1218	9.75e-63	243
NVE10	t176889	92.157	51	3	1	1256	1306	1288	1337	4.87e-11	71.3
NVE10	t176889	90.000	50	4	1	1110	1159	1289	1337	8.15e-09	63.9
NVE10	t961	100.000	472	0	0	1393	1864	506	977	0.0	872
NVE10	t961	97.624	463	9	2	784	1245	3	464	0.0	793
NVE10	t961	98.462	130	2	0	1263	1392	335	464	7.59e-59	230
NVE10	t221554	98.571	140	2	0	641	780	25	164	2.10e-64	248
NVE11	t211896	93.827	162	10	0	23	184	717	878	5.11e-64	244
NVE11	t211896	96.800	125	4	0	179	303	941	1065	1.86e-53	209
NVE12	t196084	100.000	964	0	0	176	1139	220	1183	0.0	1781
"""

BlastHit = namedtuple("BlastHit", ["subject", "bits", "intervals"] )
# 

def make_seq_length_dict(sequencefile):
	'''from a fasta file, return a dict where key is seqid and value is int of length'''
	sys.stderr.write("# Parsing target sequences from {}  {}\n".format(sequencefile, time.asctime() ) )
	lengthdict = {}
	for seqrec in SeqIO.parse(sequencefile,'fasta'):
		lengthdict[seqrec.id] = len(seqrec.seq)
	sys.stderr.write("# Found {} sequences  {}\n".format(len(lengthdict), time.asctime() ) )
	return lengthdict

def parse_blast_results(blastfile, identitycutoff, evaluecutoff):
	'''from the tabular blast results, return two dicts, each where keys are sequence IDs and values are tuples of the targets and intervals'''

	linecounter = 0
	identRemovals = 0
	evalueRemovals = 0

	# track bitscores and intervals for all queries and subjects
	q_best_bits = defaultdict( lambda: defaultdict(int) )
	q_best_intervals = defaultdict( lambda: defaultdict(list) )
	s_best_bits = defaultdict( lambda: defaultdict(int) )
	s_best_intervals = defaultdict( lambda: defaultdict(list) )

	if blastfile.rsplit('.',1)[-1]=="gz": # autodetect gzip format
		opentype = gzip.open
		sys.stderr.write("# Starting BLAST parsing on {} as gzipped  {}\n".format(blastfile, time.asctime() ) )
	else: # otherwise assume normal open for fasta format
		opentype = open
		sys.stderr.write("# Starting BLAST parsing on {}  {}\n".format(blastfile, time.asctime() ) )

	# parse blast results
	for line in opentype(blastfile, 'r'):
		line = line.strip()
		if not line or line[0]=="#": # skip comment lines
			continue # also catch for empty line, which would cause IndexError
		linecounter += 1
		#qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore		
		lsplits = line.split("\t")

		qseqid = lsplits[0]
		sseqid = lsplits[1]
		# do all removals before any real counting
		identity = float(lsplits[2])
		evalue = float(lsplits[10])
		bitscore = float(lsplits[11])

		qstart = int(lsplits[6])
		qend = int(lsplits[7])
		if qend < qstart: # if blast hits are inverted to the transcript sense, reverse them
			qstart = int(lsplits[7])
			qend = int(lsplits[6])
		query_bounds = (qstart, qend)
		sstart = int(lsplits[8])
		send = int(lsplits[9])
		if send < sstart: # if blast hits are inverted to the transcript sense, reverse them
			sstart = int(lsplits[9])
			send = int(lsplits[8])
		subject_bounds = (sstart, send)

		# filter low quality matches
		if identity < identitycutoff: # skip domains that are too short
			identRemovals += 1
			continue
		if evalue >= evaluecutoff: # skip domains with bad evalue
			evalueRemovals += 1
			continue

		# sum up bitscores and intervals
		q_best_bits[qseqid][sseqid] += bitscore
		q_best_intervals[qseqid][sseqid].append(query_bounds)
		s_best_bits[sseqid][qseqid] += bitscore
		s_best_intervals[sseqid][qseqid].append(subject_bounds)

	sys.stderr.write("# Counted {} lines  {}\n".format(linecounter, time.asctime() ) )
	if identRemovals:
		sys.stderr.write("# Removed {} hits with identity below {}\n".format( identRemovals, identitycutoff ) )
	if evalueRemovals:
		sys.stderr.write("# Removed {} hits with evalue above {}\n".format( evalueRemovals, evaluecutoff ) )

	# collate hits where key is query ID, value is list of tuples
	# tuples contain subject ID, sum of bitscores to that hit, and list of intervals
	q_hits_dict = defaultdict(list)
	s_hits_dict = defaultdict(list)

	# first for queries
	for qseqid in q_best_intervals.keys():
		for sseqid in q_best_intervals[qseqid].keys():
			qbh = BlastHit(subject = sseqid, 
					bits = q_best_bits[qseqid][sseqid], 
					intervals = q_best_intervals[qseqid][sseqid] )
			q_hits_dict[qseqid].append(qbh)
	# then reciprocally for subjects
	for sseqid in s_best_intervals.keys():
		for qseqid in s_best_intervals[sseqid].keys():
			sbh = BlastHit(subject = qseqid, 
					bits = s_best_bits[sseqid][qseqid], 
					intervals = s_best_intervals[sseqid][qseqid] )
			s_hits_dict[sseqid].append(sbh)
	
	sys.stderr.write("# Collated hits for {} queries and {} subjects\n".format( len(q_hits_dict), len(s_hits_dict) ) )

	return q_hits_dict, s_hits_dict

def combine_intervals(rangelist):
	'''convert list of tuples to non redundant invervals'''
	# sl = [(1,10), (1,6), (15,20), (1,10), (19,29), (30,35), (6,13), (40,48), (15,21), (42,50)]
	nrintervallist = []
	srtrangelist = sorted(rangelist) # sort list now, to only do this once
	interval = srtrangelist[0] # need to start with first time, which will be the same for the first bounds
	for bounds in srtrangelist:
		# since it is sorted bounds[0] should always be >= interval[0]
		if bounds[0] > interval[1]+1: # if the next interval starts past the end of the first + 1
			nrintervallist.append(interval) # add to the nr list, and continue with the next
			interval = bounds
		else:
			if bounds[1] > interval[1]: # bounds[1] <= interval[1] means do not extend
				interval = (interval[0], bounds[1]) # otherwise extend the interval
	else: # append last interval
		nrintervallist.append(interval)
	# should return [(1, 13), (15, 35), (40, 50)]
	return nrintervallist

def get_coverage_sum(intervals):
	'''for list of intervals, return the sum of the spans'''
	coverage_sum = 0
	for interval in intervals:
		span = interval[1] - interval[0] + 1
		coverage_sum += span
	return coverage_sum

def remove_base_intervals(intervals, keptbase_intervals):
	'''from two lists of intervals, remove all of A in B, and return a list of intervals '''
	# remove_base_intervals( [[2,10],[20,30]], [[1,30]] )
	# should return [ [1,1], [11,19] ]
	updated_intervals = keptbase_intervals
	for interval in sorted(intervals):
		for i, kept_interval in enumerate(updated_intervals):
			if interval[0] > kept_interval[1]: # ignore
				pass
			elif interval[1] < kept_interval[0]: # ignore
				pass
			elif interval[0] >= kept_interval[0]: # if intervals overlap at all
				# remove this interval
				updated_intervals.pop(i)
				# then check how much is left, and create a new interval on one or both sides
				if interval[0] > kept_interval[0]:
					split1 = [ kept_interval[0], interval[0]-1 ]
					updated_intervals.append(split1)
				if interval[1] < kept_interval[1]:
					split2 = [ interval[1]+1, kept_interval[1] ]
					updated_intervals.append(split2)
			elif interval[1] < kept_interval[1]: # if they overlap only on the 3pr side
				updated_intervals.pop(i)
				split1 = [ interval[1]+1, kept_interval[1] ]
				updated_intervals.append(split1)
			# after new intervals are added, sort the list again, and move to the next interval
			updated_intervals.sort()
	return updated_intervals

def get_cross_blast_coverage(qs_intervals, query_len_dict, num_targets, MIN_TX_CUTOFF=100, BASE_CUTOFF=50):
	'''from the dict of lists, report stats on coverage'''
	total_gene_count = 0
	total_bases = 0
	covered_bases = 0
	missing_bases = 0

	# track genes with any hit
	genes_w_hits = defaultdict(list)
	# track genes with more than 1 hit
	fused_queries = defaultdict(list)
	# for queries with multiple hits, track the subjects too
	fused_subjects = defaultdict(list)
	# track difference in bases between tx and covered intervals for tx with a single hit
	truncated_hits = {} # key is query ID, value is list of remaining bases and subject

	# start from length, to include all sequences, even those without hits
	for query in query_len_dict.keys():
		total_gene_count += 1
		# reset counts for this gene
		covered_tx = []
		coverage_sum = 0
		# get parameters of this query
		query_len = query_len_dict.get(query)
		hit_list = qs_intervals.get(query,[])
		# set up query structure to then subtract from it
		remaining_bases = query_len # reset base count, then subtract for each hit
		keptbase_intervals = [[1,query_len]] # make a starting interval of the whole transcript

		# sort through hits
		for blasthit in sorted(hit_list, key=lambda x: x.bits, reverse=True):
			covered_intervals = combine_intervals(blasthit.intervals)
			coverage_sum += get_coverage_sum(covered_intervals)
			keptbase_intervals = remove_base_intervals(covered_intervals, keptbase_intervals)
			kept_bases = get_coverage_sum(keptbase_intervals)
			bases_covered_by_tx = remaining_bases - kept_bases

			# meaning greater than Nbp were covered by this blast hit
			if bases_covered_by_tx > MIN_TX_CUTOFF: # so count this tx
				covered_tx.append(blasthit.subject)
			remaining_bases = kept_bases

			# if fewer than N bases are left, stop anyway
			if remaining_bases < BASE_CUTOFF:
				remaining_bases = get_coverage_sum(keptbase_intervals)
				break
		else: # meaning has run out of hits
			remaining_bases = get_coverage_sum(keptbase_intervals)

		# get sums for this query
		total_bases += query_len
		missing_bases += remaining_bases
		query_total_covered = query_len - remaining_bases
		covered_bases += query_total_covered
		if len(covered_tx) > 0: # at least one hit for this gene at all
			genes_w_hits[query] = list(covered_tx)
			if len(covered_tx) > 1: # meaning two or more long transcripts hit this gene
				fused_queries[query] = list(covered_tx)
				# create the reversed dictionary, key is subject and value is list of queries
				for subject in covered_tx:
					fused_subjects[subject].append(query)
			else: # meaning == 1
				if len(covered_intervals) > 1: # meaning gene or tx is split
					truncated_hits[query] = [genes_w_hits[query][0], remaining_bases]

	# report overall totals
	sys.stdout.write("Total bases: {}\n".format(total_bases))
	genes_w_hits_count = len(genes_w_hits)
	sys.stdout.write("Transcripts with blast hits: {} out of {} total ({:.2f}%)\n".format(genes_w_hits_count, total_gene_count, genes_w_hits_count*100.0/total_gene_count))
	sys.stdout.write("Bases covered by other dataset: {} ({:.2f}%)\n".format(covered_bases, covered_bases*100.0/total_bases))
	#sys.stdout.write("Bases unique to this dataset: {} ({:.2f}%)\n".format(missing_bases, missing_bases*100.0/total_bases))
	sys.stdout.write("Transcripts covered by multiple hits longer than {}bp (possible fusions): {} ({:.2f}%)\n".format(MIN_TX_CUTOFF, len(fused_queries), len(fused_queries)*100.0/total_gene_count))
	sys.stdout.write("Number of targets hitting fused transcripts: {} ({:.2f}%)\n".format( len(fused_subjects), len(fused_subjects)*100.0/num_targets))

	return truncated_hits

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-q','--query-transcripts', help="query transcripts, or version 1", required=True)
	parser.add_argument('-s','--subject-transcripts', help="subject transcripts, or version 2", required=True)
	parser.add_argument('-b','--blast-output', help="tabular blast output")
	parser.add_argument('-e','--evalue', type=float, default=1e-50, help="evalue cutoff threshold [1e-50]")
	parser.add_argument('-L','--length-report', help="name of optional output file of difference between query and target lengths")
	parser.add_argument('-i','--min-identity', type=float, default=95.0, help="identity percent cutoff [95.0]")
	parser.add_argument('-m','--min-transcript-length', type=int, default=100, metavar="Nbp", help="minimum length of covered bases to count transcript as a hit")
	parser.add_argument('-r','--remaining-length', type=int, default=50, metavar="Nbp", help="skip any remaining hits if transcript length is below N [50]")
	parser.add_argument('-t','--threads', type=int, default=1, help="number of threads for blastn")
	parser.add_argument('-E','--exclude', help="file of list of bad contigs")
	args = parser.parse_args(argv)

	# read files and make length dictionaries
	query_file = args.query_transcripts
	query_len_dict = make_seq_length_dict(query_file)
	subject_file = args.subject_transcripts
	subject_len_dict = make_seq_length_dict(subject_file)

	# read blast results, and parse into hits
	qs_intervals, sq_intervals = parse_blast_results(args.blast_output, args.min_identity, args.evalue)

	# sort through hits and count the covered bases
	remaining_bases_by_hit = {}
	sys.stdout.write("# Sorting intervals for {}\n".format(args.query_transcripts) )
	cross_coverage = get_cross_blast_coverage(qs_intervals, query_len_dict, len(subject_len_dict), args.min_transcript_length, args.remaining_length)
	remaining_bases_by_hit.update(cross_coverage)
	sys.stdout.write("# Sorting intervals for {}\n".format(args.subject_transcripts) )
	cross_coverage = get_cross_blast_coverage(sq_intervals, subject_len_dict, len(query_len_dict), args.min_transcript_length, args.remaining_length)
	remaining_bases_by_hit.update(cross_coverage)

	if args.length_report:
		sys.stdout.write("# Writing seqs with largest difference between query and subject length to {}\n".format(args.length_report) )
		with open(args.length_report, 'w') as lr:
			for query, rlist in sorted(remaining_bases_by_hit.items(), key=lambda x: x[1][1], reverse=True):
				lr_line = "{}\t{}\t{}\n".format(query, rlist[0], rlist[1])
				lr.write(lr_line)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
