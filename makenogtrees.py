#!/usr/bin/env python
#
# v1.0 taking tabular sequence and NOG file, make append alignments and make trees

'''
makenogtrees.py v1.1 2016-01-14

    usage:
makenogtrees.py -i meNOG_by_transcript.txt -s eggnog4.species_list.txt -l meNOG_raw_algs/ -f peptides.fasta -C -v > nog_commands.sh

    set CPUs for MAFFT using -p, where default is 1
    all processes are run sequentially

    download eggnog4.species_list from:
    http://eggnogdb.embl.de/download/eggnog_4.1/eggnog4.species_list.txt

    generate a tabular sequence to NOG file using:
parsehmmtables.py -i *.meNOG.tab > meNOG_by_transcript.txt
'''

import sys
import os
import argparse
import time
import re
import glob
import subprocess
from collections import defaultdict
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

# set path of FastTree binary
MAFFT = "~/mafft-7.182-with-extensions/bin/mafft"
FASTTREEMP = "~/FastTree/FastTreeMP"
#
# annotations file in format of:
# TaxonomicLevel | GroupName | ProteinCount | SpeciesCount | COGFunctionalCategory | ConsensusFunctionalDescription
# meNOG	ENOG410VUKR	2	2	S	Sushi domain (SCR repeat)
def get_annotations(annotationfile):
	annotationdict = {}
	for line in open(annotationfile,'r'):
		line = line.strip()
		if line and not line[0]=="#": # remove empty and comment lines
			lsplits = line.split("\t")
			nog = lsplits[1]
			description = lsplits[5]
			annotationdict[nog] = description
	return annotationdict

# NOG species file in format of:
# species name	tax id	core/periphery/adherent	source	source version
# Sinorhizobium fredii NGR234	394	periphery	Refseq	not available

# OrthoDB file odb9_species.tab in format of:
# NCBI tax_id	scientific_name	genes	OGs	mapping_type
# 394	Sinorhizobium fredii NGR234	6185	18011	C
def get_species_dict(speciesfile, dbformat):
	speciesdict = {} # keys are numbers, values are formatted species names
	for line in open(speciesfile,'r'):
		line = line.strip()
		if line and not line[0]=="#": # remove empty and comment lines
			lsplits = line.split("\t")
			if dbformat=="nog" or dbformat=="NOG":
				taxid = lsplits[1]
				rawspecies = lsplits[0]
			else:
				taxid = lsplits[0]
				rawspecies = lsplits[1]
			resp = re.search("(\w+) (\w+)", rawspecies)
			try:
				formatspecies = "{}_{}".format(resp.group(1), resp.group(2))
			except AttributeError: # for weird ones where it does not match
				formatspecies = rawspecies.replace(" ","_").replace("(","").replace(")","")
			speciesdict[taxid] = formatspecies
	return speciesdict

# alignments in format of:
# >30608.ENSMICP00000013527
# -----------------------------------MRL--------LGA-
# ------------AAAAA-L--RPRP-LPQV-----PVAL-----GWQ-G-
# ------------------------SGVIP-NEKI-RN-IGISAHIDSGKT
# TLTERXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

def mafft_base_command(alignfile, threadcount, opvalue="2.0", epvalue="0.1"):
	newalign = "{}.aln".format( os.path.splitext(alignfile)[0] )
	mafftargs = [MAFFT, "--auto", "--reorder", "--quiet", "--op", opvalue, "--ep", epvalue, "--thread", threadcount, alignfile]
	mafftcommand = "{} > {}".format( ' '.join(mafftargs), newalign)
	return mafftcommand, newalign

def fasttree_base_command(alignfile, threadcount):
	treefile = "{}.tree".format(alignfile)
	ftargs = [FASTTREEMP, "-quiet", alignfile]
	ftcommand = "{} > {}".format( ' '.join(ftargs), treefile)
	return ftcommand, treefile

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="file of sequences and domains/protein families", required=True)
	parser.add_argument('-a','--annotations', help="annotations tsv file")
	parser.add_argument('-l','--alignments', nargs="*", help="alignment files or directory")
	parser.add_argument('-s','--species-list', help="NOG species list, for replacing nubmers with species")
	parser.add_argument('-t','--temp', help="directory for temporary files ['temp/']", default="temp/")
	parser.add_argument('-c','--completeness', type=float, help="skip NOGs whose alignments have this fraction of gaps [0.8]", default=0.8)
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff [1e-30]", default=1e-30)
	parser.add_argument('-f','--fasta', help="fasta reference file, whatever contains the base names", required=True)
	parser.add_argument('-F','--db-format', default="nog", help="database format, as nog or orthodb [nog]")
	parser.add_argument('-m','--min-taxa', type=int, help="minimum number of allowed taxa [20]", default=20)
	parser.add_argument('-p','--processors', metavar='N', help="number of processors [1]", default="1")
	parser.add_argument('-C','--commands', action="store_true", help="run commands during operation")
	parser.add_argument('-g','--gaps', action="store_true", help="retain existing gaps")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	startclock=time.asctime()
	starttime= time.time()

	tempdir = os.path.abspath(args.temp)
	if os.path.isdir(tempdir):
		print >> sys.stderr, "The directory {0} already exists, and will be used".format(tempdir)
	else:
		print >> sys.stderr, "Making the directory {0}".format(tempdir)
		os.mkdir(tempdir)

	if args.species_list:
		speciesdict = get_species_dict(args.species_list, args.db_format)

	print >> sys.stderr, "# Parsing fasta file {}".format(args.fasta), time.asctime()
	reffastaDict = {}
	for seqrec in SeqIO.parse(args.fasta, 'fasta'):
		splitid = seqrec.id.split("|")[0]
		reffastaDict[splitid] = seqrec
	print >> sys.stderr, "# Reference fasta contained {} sequences".format(len(reffastaDict) ), time.asctime()

	print >> sys.stderr, "# Linking files to NOGs", time.asctime()
	if os.path.isdir(args.alignments[0]):
		globstring = "{}*".format(args.alignments[0])
		alignfiles = glob.glob(globstring)
	elif os.path.isfile(args.alignments[0]):
		alignfiles = args.alignments
	else:
		print >> sys.stderr, "ERROR: Unknown alignment files, check directory path or file names, exiting", time.asctime()
		sys.exit()
	# files in format of:
	# meNOG.ENOG410VKNC.meta_raw.fa      meNOG.ENOG410W38D.meta_raw.fa      meNOG.ENOG410WJUE.meta_raw.fa
	alignfilebynog = {}
	for af in alignfiles:
		nog = af.split(".")[1] # from meNOG.ENOG410VKNC.meta_raw.fa should take ENOG410VKNC
		alignfilebynog[nog] = af
	print >> sys.stderr, "# Found {} alignments".format(len(alignfilebynog) ), time.asctime()

	print >> sys.stderr, "# Parsing sequence to NOG index {}".format(args.input), time.asctime()
	seqsbynog = defaultdict(list)
	evaluesbynogbyseq = defaultdict(dict)
	seqcount = 0
	for line in open(args.input, 'r'):
		lsplits = line.strip().split("\t")
		seqid = lsplits[0]
		nogmatches = lsplits[1]
		if not nogmatches=="No_matches":
			seqcount += 1
			for nm in nogmatches.split("|"):
				nog, ev = nm.split("_",1)
				seqsbynog[nog].append(seqid)
				evaluesbynogbyseq[seqid][nog] = ev
	print >> sys.stderr, "# NOG index contains {} NOGs for {} sequences".format(len(seqsbynog), seqcount ), time.asctime()

	mfcomcount = 0
	ftcomcount = 0
	print >> sys.stderr, "# Making new alignment files in {}".format(tempdir), time.asctime()
	for nog, seqlist in seqsbynog.iteritems():
		af = str(alignfilebynog.get(nog, "") )
		if af: # if somehow the alignment is not found, then move on
			if args.verbose:
				print >> sys.stderr, "# Making new alignment and tree for {}".format(nog), time.asctime()
			alignplusseqs = os.path.join(tempdir, os.path.basename(af) )
			gapcnt, poscnt = 0,0
			speciescounter = defaultdict(int) # count total number of species and sequences per species, for filtering
			with open(alignplusseqs, 'w') as na: # make new fasta file "na" that is not yet aligned
				for line in open(af,'r'):
					line = line.strip()
					if line[0]==">":
						speciesid, protid = line[1:].split(".",1) # needed for species counting and fasta header renaming
						speciescounter[speciesid] += 1
						if args.species_list:
							newid = ">{}.{}".format(speciesdict[speciesid], protid)
							print >> na, newid
						else:
							print >> na, line
					else: ### TODO flag sequences that are all gaps (too short) or no gaps (too long and misannotations)
						gapcnt += line.count("-")
						poscnt += len(line)
						if args.gaps: # keep existing gaps if desired
							print >> na, line
						else:
							print >> na, line.replace("-","")
				for seq in set(seqlist):
					seqidplusev = "{}|{}".format(seq, evaluesbynogbyseq[seq][nog] )
					try:
						evseqrecord = SeqRecord(reffastaDict[seq].seq)
					except KeyError: # for cases of | separation from TransDecoder where key is not found
						evseqrecord = SeqRecord(reffastaDict[ seq.split("|")[0] ].seq)
					evseqrecord.id = seqidplusev
					evseqrecord.description = ''
					na.write(evseqrecord.format("fasta"))
			# skip alignments that are mostly gaps, as they are probably bad orthologs
			if float(gapcnt)/poscnt >= args.completeness: # check for 80% gaps or more
				print >> sys.stderr, "{} is {:2f}% gaps, skipping".format(nog, float(gapcnt)/poscnt*100)
			elif len(speciescounter) < args.min_taxa: # remove alignments with fewer than 20 species
				print >> sys.stderr, "{} has only {} taxa, skipping".format(nog, len(speciescounter) )
			else: # unaligned file is already generated but not aligned or treed
				mafftcommand,alignfile = mafft_base_command(alignplusseqs, args.processors)
				print >> sys.stdout, mafftcommand
				mfcomcount += 1
				if args.commands:
					subprocess.call(mafftcommand, shell=True)
				fasttreecommand,treefile = fasttree_base_command(alignfile, args.processors)
				print >> sys.stdout, fasttreecommand	
				ftcomcount += 1
				if args.commands:
					subprocess.call(fasttreecommand, shell=True)
				if args.verbose:
					if mfcomcount%100==0:
						print >> sys.stderr, mfcomcount, "{:.1f} minutes".format((time.time()-starttime)/60), time.asctime()
		else:
			if args.verbose:
				print >> sys.stderr, "No alignment for {}".format(nog), time.asctime()

	print >> sys.stderr, "{} mafft commands".format(mfcomcount), time.asctime()
	print >> sys.stderr, "{} fasttree commands".format(mfcomcount), time.asctime()
	print >> sys.stderr, "Processed completed in {:.1f} minutes".format((time.time()-starttime)/60)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
