#! /usr/bin/env python
#
# v2.0 changed to accept xml or existing blasttable and parse
#       then recreate proteins as predicted from blast hits
# v1.0 creates blasttable from xml file, using xml parser
#       but cannot retrieve no-match sequences from xml

usage="""
blastxmltotable.py v2

blastxmltotable.py -x blastresult.xml > resultstable.txt

or

blastxmltotable.py -B blasttable.txt -t -q queryfile.fasta
"""

import sys
import argparse
import time
import os
from Bio import SeqIO
from Bio.Blast.Applications import NcbitblastxCommandline
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord

from blastplustable import get_basicseq, frame_to_orfs, get_matchseq, get_subjctlength, get_trans_inframe, get_transseq, get_align_piece

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-x','--xml', help="xml file")
	parser.add_argument('-B','--blasttable', help="existing blasttable file to reparse")
	parser.add_argument('-q','--query', help="query file")
	parser.add_argument('-d','--db', help="database file - default: swissprot", default="uniprot_sprot.fasta")
	parser.add_argument('-D','--directory', help="folder containing all blast DBs, default read from ~/.ncbirc")
	parser.add_argument('-o','--output-tag', help="label to append to files - default: sp", default="sp")
	parser.add_argument('-s','--search-tag', help="label to index blast hits - default: sp", default="sp")
	parser.add_argument('-S','--use-search', action="store_true", help="use search string, instead of accession number")
	parser.add_argument('-H','--no-header', action="store_false", help="omit header line in output table")
	parser.add_argument('-M','--no-match-file', action="store_false", help="do not write hits and nomatch output files")
	parser.add_argument('-t','--translate', action="store_true", help="based on the alignment, print a translation (use with blastx)")
	parser.add_argument('-a','--alignment-cutoff', metavar='N', type=int, help="minimum number of alignments - default: 1", default=1)
	parser.add_argument('-c','--code', type=int, help="genetic code number - default: 1", default='1')
	parser.add_argument('-i','--identities', action="store_true", help="report identites as number rather than percentage")
	args = parser.parse_args(argv)

	blast_file = args.blasttable

	starttime= time.time()
	print >> sys.stderr, '# Parsing %s at:' % (blast_file), time.asctime()

	## start main
	recnum = 0
	nomatch = 0

	fastabasename = os.path.splitext(os.path.abspath(blast_file))[0]
	if args.no_match_file:
		hitsoutputfilename = "%s_%s_hits.fasta" % (fastabasename, args.output_tag)
		hitsoutfile = open(hitsoutputfilename, 'w')
		nomatchoutputfilename = "%s_%s_nom.fasta" % (fastabasename, args.output_tag)
		nomatchoutfile = open(nomatchoutputfilename, 'w')
	# file for translated proteins if using blastx
	if args.translate:
		protcount = 0
		nonsensecount = 0
		transoutputfilename = "%s_%s_prots.fasta" % (fastabasename, args.output_tag)

	if args.blasttable:
		query_seq_dict = SeqIO.to_dict(SeqIO.parse(args.query,'fasta'))
		blasthitdict = {}
		blastprogram = "blastx"
		with open(args.blasttable,'r') as bt, open(transoutputfilename, 'w') as transoutfile:
			for l in bt:
				if l.split("\t")[0] == "Resulttitle":
					continue
				rt, ev, bt, na, rq, ac, ms, fr, tl, ip, al, ql = l.rstrip().split('\t')
				sl = tl
				if args.translate:
					# only translate each seq once, so disregard if either returns 1
					if not blasthitdict.get(rt,0) and not blasthitdict.get(rq,0):
				### TODO should add some exception other than blastx and tblastn 04-04-2014
						if blastprogram == "blastx":
							blasthitdict[rq]=1
							transframe = int(fr)
							untransseq = get_basicseq(query_seq_dict,rq)
						elif blastprogram == "tblastn":
							blasthitdict[rt]=1
							transframe = fr
							untransseq = get_basicseq(target_db_dict,rt)

						# get translated sequence by frame hit, and break into orfs by stop codons
						transprot = get_trans_inframe(untransseq.seq, transframe, ms, args.code)
						untransseq.seq = transprot
						if len(transprot) < sl:
							nonsensecount += 1
							print >> sys.stderr, "CHECK FOR NONSENSE: %s" % untransseq.id
						protcount += 1
						transoutfile.write("%s" % untransseq.format("fasta"))

	# print the header line for the table
	if args.no_header:
		print >> wayout, "\t".join(["Resulttitle", "Evalue", "Bit_Score", "Num_Align", "Query", "Accession", "Matched", "Frame", "Subject Length", "Identity %", "Align_length", "Query Length"])
 
	# if using in xml mode, mostly copied from blastplustable
	### TODO finish this part
	if args.xml:
		with open(args.xml,'rU') as result_handle:
			for record in NCBIXML.parse(result_handle):
				recnum+=1
				blastprogram = record.application
				# originally used try because if there is no match, just skip the record
				# there was minimal change in time for using len, but was better for debugging
				if len(record.alignments) >= args.alignment_cutoff:
					# print hits here, so each hit is printed only once 2013-10-25
					if args.no_match_file:
						hitsoutfile.write("%s" % get_basicseq(query_seq_dict,record.query).format("fasta"))
					# print each alignment, for all hits with more than args.alignment_cutoff number of alignments v6.1
					for x,alignment in enumerate(record.alignments):
						rt = alignment.hit_def
						ev = record.descriptions[x].e
						bt = record.descriptions[x].bits #this is the normalized score, bit-score
						#sc = record.descriptions[x].score #this is the raw score
						al = alignment.hsps[0].align_length
						# this is not the same as alignment.length
						rq = record.query
						if args.use_search:
							try:
								ac = re.search(searchstring,record.descriptions[x].title).group(1)
							# for cases where NoneType is flagged, possibly due to database errors
							except AttributeError:
								ac = alignment.accession
						else:
							ac = alignment.accession
						#if blast_db == 'nr' or blast_db == 'nt':
						#	ac = record.alignments[0].accession

						# get matching sequence for table
						if blastprogram == "blastn":
							ms = get_matchseq(alignment.hsps[0].match, True, record.alignments[x].hsps[0].query)
						else:
							ms = get_matchseq(alignment.hsps[0].match, False, None)

						# number of alignments
						na = len(record.alignments)

						# to calculate query coverage as on ncbi
						# appears to be query length / subject length
						ql = record.query_length

						# get subject length, which is not the same as length of the full subject hit
						sl = get_subjctlength(alignment.hsps[0].sbjct_end, alignment.hsps[0].sbjct_start, blastprogram)
						# subject gaps is a count of - in the subject, which is equal to hsps.align_length - sl
						sg = al - sl
						# also note that al = sl + subject side gaps
						# hsps[0].gaps includes both sides

						# to calculate identity % as on ncbi
						qi = alignment.hsps[0].identities
						# intuitively the identity percentage is calculated as identities/query length
						if args.identities:
							ip = qi
						else:
							#ip = qi/float(ql)*100
							# however on ncbi it is calculated as identities/alignment length, which is the same as subject end - start + 1 + subject gaps
							ip = qi/float(al)*100

						# to correct for cases where db is not a text file
						if get_db_dict:
							tl = len(get_basicseq(target_db_dict,rt).seq)
						else:
							tl = sl

						# choose query frame based on program used
						qf = alignment.hsps[0].frame[0]
						sf = alignment.hsps[0].frame[1]
						if blastprogram == "blastx" or blastprogram == "tblastx":
							fr = qf
						elif blastprogram == "tblastn":
							fr = sf
						else:
							fr = 0

						# this does not use join() because of the ints and floats
						print >> wayout, "%s\t%s\t%s\t%d\t%s\t%s\t%s\t%s\t%d\t%.2f\t%d\t%d" % (rt, ev, bt, na, rq, ac, ms, fr, tl, ip, al, ql)

						# if '-t' is enabled, gets the alignment and frame information
						# translates that frame, then gets the piece with the alignment, and writes to file
						if args.translate:
							# only translate each seq once, so disregard if either returns 1
							if not blasthitdict.get(rt,0) and not blasthitdict.get(rq,0):
						### TODO should add some exception other than blastx and tblastn 04-04-2014
								if blastprogram == "blastx":
									blasthitdict[rq]=1
									transframe = qf
									untransseq = get_basicseq(query_seq_dict,rq)
								elif blastprogram == "tblastn":
									blasthitdict[rt]=1
									transframe = sf
									untransseq = get_basicseq(target_db_dict,rt)

								# get translated sequence by frame hit, and break into orfs by stop codons
								transprot = get_trans_inframe(untransseq.seq, transframe, ms, args.code)

								# tries to force M-containing ORFs, otherwise use all ORFs TODO
								#if metorfs:
								#	fragmentlengths = [len(fragment) for fragment in metorfs]
								#	longestfragment = metorfs[fragmentlengths.index(max(fragmentlengths))]
								#else:
								#	fragmentlengths = [len(fragment) for fragment in orfs]
								#	longestfragment = orfs[fragmentlengths.index(max(fragmentlengths))]
								untransseq.seq = transprot
								if len(transprot) < sl:
									nonsensecount += 1
									print >> sys.stderr, "CHECK FOR NONSENSE: %s" % untransseq.id
								protcount += 1
								transoutfile.write("%s" % untransseq.format("fasta"))
				else:
					# no match output
					nomatch+=1
					if args.no_match_file:
						nomatchoutfile.write("%s" % get_basicseq(query_seq_dict,record.query).format("fasta"))

	print >> sys.stderr, '# Finished parsing at:', time.asctime()
	print >> sys.stderr, "# Processed %d records in %.1f minutes" % (recnum, (time.time()-starttime)/60)

	hitsoutfile.close()
	nomatchoutfile.close()
	print >> sys.stderr, "#", nomatch, "records had no match, written to", nomatchoutputfilename

"""
	###########################################################
	# contents of a record in records:

['__doc__', '__init__', '__module__', 'alignments', 'application', 'blast_cutoff', 
	'database', 'database_length', 'database_letters', 'database_name', 
	'database_sequences', 'date', 'descriptions', 'dropoff_1st_pass', 
	'effective_database_length', 'effective_hsp_length', 'effective_query_length', 
	'effective_search_space', 'effective_search_space_used', 'expect', 'filter', 
	'frameshift', 'gap_penalties', 'gap_trigger', 'gap_x_dropoff', 'gap_x_dropoff_final', 
	'gapped', 'hsps_gapped', 'hsps_no_gap', 'hsps_prelim_gapped', 'hsps_prelim_gapped_attemped', 
	'ka_params', 'ka_params_gap', 'matrix', 'multiple_alignment', 'num_good_extends', 'num_hits', 
	'num_letters_in_database', 'num_seqs_better_e', 'num_sequences', 'num_sequences_in_database', 
	'posted_date', 'query', 'query_id', 'query_length', 'query_letters', 'reference', 'sc_match', 
	'sc_mismatch', 'threshold', 'version', 'window_size']

	# contents of a description in a record
	dir(r.descriptions[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'bits', 'e', 
	'num_alignments', 'score', 'title']

	# contents of an alignment in a record
	dir(r.alignments[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'hit_def', 
	'hit_id', 'hsps', 'length', 'title']

	dir(r.alignments[0].hsps[0])
	['__doc__', '__init__', '__module__', '__str__', 'align_length', 'bits', 'expect', 'frame', 
	'gaps', 'identities', 'match', 'num_alignments', 'positives', 'query', 'query_end', 
	'query_start', 'sbjct', 'sbjct_end', 'sbjct_start', 'score', 'strand']
"""

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
