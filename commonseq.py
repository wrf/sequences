#! /usr/bin/env python2
#
# commonseq.py
# v3.2 fixed output ordering to match inputs
# v3.1 added minimum tree length for 10% decrease in memory usage 2015-08-14
# v3.0 allows stdin and stdout for pipeline use 2015-08-13
# v2.3 added exact match option 2015-06-26
# v2.2 changed verbose command, added removed 2014-10-13
# v2.1 clarified usage info 2014-10-09
# v2.0 added bigstring search 2013-11-14
# v1.1 added option to specify sequence type, or autodetect
#      and corrected bug of reverse complement of proteins 2013/08/13
#
"""commonseq.py last modified 2015-09-22

WARNING  DEPRICATED, AS BIO TRIE IS NO LONGER SUPPORTED

################################################################################
program to keep shortest common substrings between two sets of sequences

    To remove redundant subsequences from just one file:
commonseq.py -1 seqs1.fasta -o seqs1_nonredundant.fasta
    Or can be piped into another program or file:
commonseq.py -1 seqs1.fasta > seqs1_nonredundant.fasta

    To keep only common subsequences between two files:
commonseq.py -1 seqs1.fasta -2 seqs2.fasta -o common_sequences.fasta

    To get sequences from stdin, could be translator, etc.
cat seqs*.fasta | commonseq -1 - > common_sequences.fasta

  # THIS MIGHT USE A LOT OF MEMORY, AROUND 1GB / 2MEGABASES #
    Use -e to search for only exact matches (and about 20x LESS MEMORY)
    This includes reverse complements but not subsequences

    For memory saving mode, though easily 5-10x longer runtime
commonseq.py -1 seqs1.fasta -2 seqs2.fasta -o common_sequences.fasta -m s

    Use -v to display progress update at each 1000 sequences
"""

import sys
import time
import argparse
from collections import OrderedDict
from Bio.Seq import Seq
from Bio import SeqIO
#from Bio import trie
#from Bio import triefind

def get_alphabet(seqstring):
	dnapercent=sum([seqstring.count(x) for x in "ACTG"])/float(len(seqstring))
	if dnapercent >= 0.8:
		print >> sys.stderr, "# Auto-detected DNA", time.asctime()
		return 'n'
	else:
		print >> sys.stderr, "# Auto-detected protein", time.asctime()
		return 'p'

def get_seq_list(filename):
	seqlist = list(SeqIO.parse(filename, 'fasta'))
	return seqlist

def build_and_search(reclist1, reclist2, seqtype, searchmethod, doexact, doverbose, searchfilename):
	print >> sys.stderr, "Generating tree", time.asctime()
	seqmin = min(len(r.seq) for r in reclist1)
	searchtree = trie.trie()
	populate_tree(searchtree, reclist2, doexact, seqmin)
	print >> sys.stderr, "Tree contains {} substrings".format(len(searchtree)), time.asctime()
	# for every sequence in file 1, search through the tree from file 2 for fragments
	# append those that are found as fragment to the list of allseqs
	print >> sys.stderr, "Searching in %s for fragments" % (searchfilename), time.asctime()
	# no need for garbage collection as searchtree should be deleted when the function closes
	commonseqs = search_for_seqs(searchtree, reclist1, seqtype, searchmethod, doverbose, doexact)
	print >> sys.stderr, "Found {} substrings".format(len(commonseqs)), time.asctime()
	return commonseqs

def populate_tree(tree, records, doexact, seqmin):
	if doexact:
		for r in records:
			tree[str(r.seq)] = len(r.seq)
	else:
		for r in records:
			# populate based on shortest sequence in the other list
			for i in xrange(len(r.seq) - seqmin + 1):
				tree[str(r.seq)[i:]] = True

def search_for_seqs(seqstructure, records, seqtype, method, verbose, doexact):
	foundseqs = list()
	for n,record in enumerate(records):
		# verbose enables a timer for every 1000 sequences
		if verbose:
			if n and not(n%1000):
				sys.stderr.write(".")
			if n and not(n%10000):
				print >> sys.stderr, n, time.asctime()
		recstring = str(record.seq)
		# for nucleotides, also do check the reverse complement
		if seqtype == 'n':
			recstrrev = str(record.seq.reverse_complement())
			if method == 't':
				if doexact: # only exact matching forward or reverse
					sensetrue = seqstructure.has_key(recstring)
					rctrue = seqstructure.has_key(recstrrev)
				else: # allow prefix and subsequence matching
					sensetrue = seqstructure.has_prefix(recstring)
					rctrue = seqstructure.has_prefix(recstrrev)
			else:
				sensetrue = (recstring in seqstructure)
				rctrue = (recstrrev in seqstructure)
		# otherwise for proteins, the rctrue is always false and thus ignored
		else:
			if method == 't':
				if doexact:
					sensetrue = seqstructure.has_key(recstring)
				else:
					sensetrue = seqstructure.has_prefix(recstring)
			else:
				sensetrue = (recstring in seqstructure)
			rctrue = False
		if sensetrue or rctrue:
			# if record.seq is a substring of tree, add record to the final list
			foundseqs.append(record)
	return foundseqs

def make_megastring(records):
	megastring = " ".join([str(s.seq) for s in records])
	return megastring

def main(argv):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-1','--file1', help="fasta format file, or stdin", type=argparse.FileType('r'), default="-" )
	parser.add_argument('-2','--file2', help="fasta format file")
	parser.add_argument('-o','--output', help="destination file for common seqs, default written to stdout", type=argparse.FileType("w"), default=sys.stdout)
	parser.add_argument('-t','--type', choices='pna', help="sequence type: p (proteins), n (nucleotide), or a (auto)", default='a')
	parser.add_argument('-m','--method', choices='st', help="sorting method: t (tree), s (string) - default: t", default='t')
	parser.add_argument('-e','--exact', action="store_true", help="only search for exact matches (uses less memory)")
	parser.add_argument('-r','--removed', action="store_true", help="print sequence names as they are removed")
	parser.add_argument('-v','--verbose', action="store_true", help="give more info, including timestamp")
	args = parser.parse_args(argv)

	allseqs = []

	# must be read as lists in order to do cross-wise comparison
	print >> sys.stderr, "Reading sequences", time.asctime()
	recs1 = get_seq_list(args.file1)
	print >> sys.stderr, "Read %d sequences from %s" % (len(recs1), args.file1.name)
	# !!! will get error if list length is zero, IndexError: list index out of range

	# if undeclared, autodetect sequence type, because nucleotides are searched with reverse complement as well
	seqtype = args.type
	if args.type=='a':
		print >> sys.stderr, "Detecting sequence type", time.asctime()
		seqtype = get_alphabet(str(recs1[0].seq))

	# if a second file is present, generate the second tree, otherwise just remove redundant fragments
	if args.file2:
		recs2 = get_seq_list(args.file2)
		print >> sys.stderr, "Read %d sequences from %s" % (len(recs2), args.file2)

		if args.method == 't':
			allseqs.extend(build_and_search(recs1, recs2, seqtype, args.method, args.exact, args.verbose, args.file1.name))
			allseqs.extend(build_and_search(recs2, recs1, seqtype, args.method, args.exact, args.verbose, args.file2))
		else: # implies method 's'
			print >> sys.stderr, "Generating megastrings", time.asctime()
			rec1megastring = make_megastring(recs1)
			rec2megastring = make_megastring(recs2)

			print >> sys.stderr, "Searching in %s for fragments" % (args.file1.name), time.asctime()
			allseqs.extend(search_for_seqs(rec2megastring, recs1, seqtype, args.method, args.verbose))

			print >> sys.stderr, "Searching in %s for fragments" % (args.file2), time.asctime()
			allseqs.extend(search_for_seqs(rec1megastring, recs2, seqtype, args.method, args.verbose))

		# begin operation of removing redundant sequences in allseqs, as duplicates are likely to exist
		print >> sys.stderr, "Removing redundant fragments", time.asctime()
	else:
		# if only one file is specified, allseqs is just the list of sequences from file 1
		allseqs = recs1
		print >> sys.stderr, "Removing redundant fragments in %s" % (args.file1.name), time.asctime()

	# this is a OrderedDict, from the order of the file, for writing out common seqs in the same order and removing doubles - sets are unordered so cannot be used
	outseqs = OrderedDict([(s.id,True) for s in allseqs])

	print >> sys.stderr, "Sorting sequences", time.asctime()
	# sequences are sorted so that longest sequences are processed first
	# to prevent the case where a short sequence is a substring of a long one
	# this is necessary for single files as well
	allseqs.sort(key=len, reverse=True) # this works since SeqRecords have a __len__ method
	allseqd = {}

	if args.method == 't':
		# quickly find the min here
		allseqmin = min(len(r.seq) for r in allseqs)
		# initialize tree and sequence dictionary for output
		alltree = trie.trie()

		# uses enumerate to allow for counting
		print >> sys.stderr, "Generating tree", time.asctime()
		for n,aseq in enumerate(allseqs):
			# verbose enables a timer for every 1000 sequences
			if args.verbose:
				if n and not(n%1000):
					sys.stderr.write(".")
				if n and not(n%10000):
					print >> sys.stderr, n, time.asctime()
			allstring = str(aseq.seq)
			if seqtype == 'n':
				allstrrev = str(aseq.seq.reverse_complement())
				if args.exact:
					senseabsent = alltree.has_key(allstring)
					rcabsent = alltree.has_key(allstrrev)
				else:
					senseabsent = alltree.has_prefix(allstring)
					rcabsent = alltree.has_prefix(allstrrev)
			else:
				if args.exact:
					senseabsent = alltree.has_key(allstring)
				else:
					senseabsent = alltree.has_prefix(allstring)
				rcabsent = False
			if not senseabsent and not rcabsent:
				if args.exact:
					alltree[str(aseq.seq)] = len(aseq.seq)
				else:
					# there is no need to generate suffices for sequences shorter than min
					# must be +1 since otherwise would call xrange(0) for shortest sequence
					for i in xrange(len(aseq.seq) - allseqmin + 1):
						alltree[str(aseq.seq)[i:]] = True
				allseqd[aseq.id] = aseq
			else:
				if args.removed:
					print >> sys.stderr, aseq.id
		# test for generation of substrings, number shown after last ... in verbose mode
		if args.verbose:
			print >> sys.stderr, "{} substrings".format( len(alltree.values() ) )

	else: # meaning method 's'
		# create megastring to build with each added seq
		allmegastring = ''

		# uses enumerate to allow for counting
		print >> sys.stderr, "Generating megastring", time.asctime()
		for n,aseq in enumerate(allseqs):
			# verbose enables a timer for every 1000 sequences
			if args.verbose:
				if n and not(n%1000):
					sys.stderr.write(".")
				if n and not(n%10000):
					print >> sys.stderr, n, time.asctime()
			allstring = str(aseq.seq)
			if seqtype == 'n':
				allstrrev = str(aseq.seq.reverse_complement())
				senseabsent = (allstring in allmegastring)
				rcabsent = (allstrrev in allmegastring)
			else:
				senseabsent = (allstring in allmegastring)
				rcabsent = False
			if not senseabsent and not rcabsent:
				allmegastring += " "+allstring
				allseqd[aseq.id] = aseq
			else:
				if args.removed:
					print >> sys.stderr, aseq.id

	# module to preserve order of contigs when written
	print >> sys.stderr, "Writing unique sequences", time.asctime()
	outseqcount = 0
	for osid in outseqs:
		# iterates for outseqs and not allseqd
		try:
			args.output.write(allseqd[osid].format('fasta'))
			outseqcount += 1
		# assumes that if it is not in allseqd, it was removed
		except:
			if args.removed:
				print >> sys.stderr, "# excluded", osid

	print >> sys.stderr, "Wrote {} sequences to {}".format(outseqcount, args.output.name), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:])
