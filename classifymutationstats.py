#!/usr/bin/env python
#
# classifymutationstats.py v1.0 2015-09-17
#
# mutation notation from:
# http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1867422/
# http://www.hgvs.org/mutnomen/recs.html

'''classifymutationstats.py -i vcf.mutations.tab > transcript_dNdS.tab

    set -d to the delimiter that separates genes from transcripts
    contig123.246_01 -d should be _
    this assumes that transcript is always the last field
    long runtimes may be caused by gene parsing problems

    tabular STDOUT consists of:
  transcript  dS   dN-pos   dN-null   dN-neg  dN/dS  dN/dS%   mutations

    dN/dS calculated as (dN-null + dN-neg) / dS
    thus positives are ignored, but can be included with -p
'''

import sys
import argparse
import time
from collections import defaultdict

def map_blosum(blosumfile):
	'''convert BLOSUM file to dictionary of dictionaries for each amino acid change'''
	blosummatrix = {}
	print >> sys.stderr, "# Reading BLOSUM matrix from {}".format(blosumfile), time.asctime()
	for line in open(blosumfile,'r').readlines():
		if line[0]=="#": # skip comment lines
			continue
		if line[0]==" ":
			aaorder = [line[i:i+3].strip() for i in xrange(1,len(line[1:-1]),3)]
		else:
			aa = line[0]
			mutvals = [int(line[i:i+3].strip()) for i in xrange(1,len(line[1:-1]),3)]
			blosummatrix[aa] = dict(zip(aaorder,mutvals))
	return blosummatrix

def print_mutation_types(mutdict):
	'''print sorted output of syn and nonsyn mutation counts in descending order'''
	mutationtotal = sum(mutdict.values())
	print >> sys.stderr, "# Found {} unique mutations".format(mutationtotal), time.asctime()
	for k,v in sorted(mutdict.iteritems(), key=lambda x: x[1], reverse=True):
		if v:
			print >> sys.stderr, "{}\t{}\t{:.3f}%".format(k,v, v*100.0/mutationtotal)

def print_nonsyn_changes(mutdict, blosummatrix):
	'''print most common amino acid substitutions in descending order'''
	for k,v in sorted(mutdict.iteritems(), key=lambda x: x[1], reverse=True):
		if blosummatrix:
			print >> sys.stderr, "{}\t{}\t{}".format(k,v, blosummatrix[k[0]][k[1]])
		else:
			print >> sys.stderr, "{}\t{}".format(k,v)

def print_transcript_dnds(transMutCountsdict, transMutListdict, getpositives, wayout):
	'''print dN/dS tabular information for each transcript'''
	selectioncount = 0
	print >> sys.stderr, "# Counting dN/dS ratios for {} transcripts".format(len(transMutCountsdict)), time.asctime()
	### TODO figure out the best way to sort these things
	#for k,v in sorted(transMutCountsdict.items(), key=lambda x: int(x[0].split(args.delimiter)[1]) ):
	for k,v in sorted(transMutCountsdict.items(), key=lambda x: x[1]["non-"], reverse=True ):
		dN = v["non-"] + v["non0"]
		if getpositives: # in positive mode add those here
			dN += v["non+"]
		try: # allow for dS value of zero
			dnds = dN * 1.0 /v["syn"]
		except ZeroDivisionError:
			dnds = 1.0
		if dnds > 1.0:
			selectioncount += 1
		mutationlist = ",".join(transMutListdict[k]) if transMutListdict[k] else "-"
		print >> wayout, "{}\t{}\t{}\t{}\t{}\t{}/{}\t{:.3f}\t{}".format(k, v["syn"], v["non+"], v["non0"], v["non-"], dN, v["syn"], dnds, mutationlist)
	print >> sys.stderr, "# Found {} transcripts with dN/dS greater than 1".format(selectioncount), time.asctime()

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="vcf mutation stats", required=True)
	parser.add_argument('-B','--blosum', help="BLOSUM matrix file")
	parser.add_argument('-d','--delimiter', default=".", help="delimiter of genes from transcript names [.]")
	parser.add_argument('-n','--negative', action="store_true", help="restrict nonsynonymous counts to negative exchanges")
	parser.add_argument('-p','--positive', action="store_true", help="include positive nonsynonymous mutations in dN/dS value")
	args = parser.parse_args(argv)

	genepositions = defaultdict(list) # dict by gene names with mutations, to avoid double counting

	scoredict = {"p0-syn":0, "p1-syn":0, "p2-syn":0, "p0-non-pos":0, "p0-non-nul":0, "p0-non-neg":0, "p1-non-pos":0, "p1-non-nul":0, "p1-non-neg":0, "p2-non-pos":0, "p2-non-nul":0, "p2-non-neg":0}
	nonsyndict = defaultdict(int)

	# variables for keeping track of individual transcript dN/dS values
	tscoretypes = ["syn","non+","non0","non-"]
	tscoredefs = [0,0,0,0]
	transcriptScoreDict = defaultdict(lambda: dict(zip(tscoretypes, tscoredefs)))

	blosummatrix = map_blosum(args.blosum) if args.blosum else None

	transNonNegDict = defaultdict(list) # for counting transcripts with negative mutations

	print >> sys.stderr, "# Parsing mutations {}".format(args.input), time.asctime()
	for line in open(args.input,'r').readlines():
		lsplits = line.strip().split("\t")
		pos = lsplits[2]
		transcript = lsplits[1]
		gene = transcript.rsplit(args.delimiter,1)[0]
		phase = lsplits[7]
		ns = lsplits[13]
		if ns == "Non":
			if len(lsplits) > 14: # assume BLOSUM info is included
				exchangeval = int(lsplits[14])
			elif blosummatrix:
				exchangeval = blosummatrix[refaa][altaa]
			else:
				exchangeval = None
			if exchangeval is not None:
				if exchangeval>0:
					transcriptScoreDict[transcript]["non+"] += 1
					sign = "pos"
				elif exchangeval<0:
					transcriptScoreDict[transcript]["non-"] += 1
					sign = "neg"
					mutationkey = "{0}{2}{1}".format(*lsplits[10:13]) # in standard protein notation, like S45V
					transNonNegDict[transcript].append(mutationkey) # count transcripts with negative mutations
				else:
					transcriptScoreDict[transcript]["non0"] += 1
					sign = "nul"
			phasekey = "p{}-non-{}".format(phase, sign)
			nonsynkey = "{}{}".format(*sorted(lsplits[10:12])) # two letters are sorted, since direction of mutation is not known
		else: # assume Syn
			phasekey = "p{}-syn".format(phase)
			transcriptScoreDict[transcript]["syn"] += 1
		if pos not in genepositions[gene]: # assume encountered for the first time, thus ignoring splice variants
			scoredict[phasekey] += 1
			genepositions[gene].append(pos)
			if ns == "Non":
				if args.negative:
					if exchangeval<0:
						nonsyndict[nonsynkey] += 1
				else:
					nonsyndict[nonsynkey] += 1

	### PRINT SUMMARY TO STDERR
	print_mutation_types(scoredict)
	print >> sys.stderr, "# {} transcripts contain negative mutations".format(len(transNonNegDict))
	print_nonsyn_changes(nonsyndict, blosummatrix)

	### PRINT TRANSCRIPT STATS TO STDOUT
	print_transcript_dnds(transcriptScoreDict, transNonNegDict, args.positive, wayout)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
