#!/usr/bin/env python
#
# removealleles.py
#
# v1 script to sort and remove alleles from transcriptomic data

import sys
import os
import argparse
import time
from collections import defaultdict
from Bio import SeqIO

usage="""
removealleles.py -b blast_results.txt -f sequences.fasta
"""

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-b','--blast', help="blast results file")
	parser.add_argument('-f','--fasta', help="fasta input file")
	parser.add_argument('-g','--histogram', action="store_true", help="show histogram of percent identites")
	parser.add_argument('-i','--identity-cutoff', type=int, help="percent identity cutoff for allelic variants: 90", default=90)
	parser.add_argument('-l','--length-cutoff', type=float, help="percent identity cutoff for allelic variants: 0.97", default=0.97)
	args = parser.parse_args(argv)

	# counter for number of lines, and strand flips
	counter = {"linecounter":0, "selfhits":0, "otheridentical":0, "samelength":0, "checkallele":0, "goodidentity":0 }
	histocounter = defaultdict(int)

	print >> sys.stderr, "Building seq dictionary from %s" % (args.fasta), time.asctime()
	fastadict = SeqIO.to_dict(SeqIO.parse(args.fasta, 'fasta'))

	print >> sys.stderr, "Starting BLAST parsing on %s" % (args.blast), time.asctime()
	for line in open(args.blast, 'r'):
		counter["linecounter"] += 1
		qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = line.rstrip().split("\t")
		if qseqid == sseqid:
			counter["selfhits"] += 1
		else:
			histocounter["%.1f" % (float(pident))] += 1
			if int(float(pident)) == 100:
				counter["otheridentical"] += 1
			if int(float(pident)) >= args.identity_cutoff:
				counter["goodidentity"] += 1
				qlen = len(fastadict[qseqid].seq)
				slen = len(fastadict[sseqid].seq)
				alength = float(qlen)/slen
				if alength == 1:
					counter["samelength"] += 1
				elif args.length_cutoff < alength < 1/args.length_cutoff:
					counter["checkallele"] += 1
					wayout.write(fastadict[qseqid].format("fasta"))
					wayout.write(fastadict[sseqid].format("fasta"))


	print >> sys.stderr, "Final counts of hits", time.asctime()
	for k,v in counter.iteritems():
		print >> sys.stderr, k, v
	if args.histogram:
		print >> sys.stderr, "Histogram of percent identities excluding self hits", time.asctime()
		for k in sorted(histocounter.keys()):
			print >> sys.stderr, k, histocounter[k]


if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
