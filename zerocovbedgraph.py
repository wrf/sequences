#!/usr/bin/env python
#
# info for bedtools found here
# http://bedtools.readthedocs.org/en/latest/content/tools/genomecov.html

'''zerocovbedgraph.py   last modified 2015-11-19
analyze zero coverage regions from bedgraph

zerocovbedgraph.py -b genomic_bedtools_genomecov.bg -c contigs.fasta > zerocov.bg.stats

    generate the bedgraph from the read mappings with:
samtools view -b genomic_bowtie2.sorted.bam | bedtools genomecov -ibam stdin -bga -g contigs.fa > genomic_bedtools_genomecov.bg

	zero cov regions can be sorted with:
grep -w 0$ genomic_bedtools_genomecov.bg > genomic_bedtools_genomecov.only0.bg

    final output is tabular containing:
contig  start   end     coverage contiglength bases;transcripts spanning the gap

Start: is number of zero cov regions at the first base of a contig
End: is number of zero cov regions at the last base of a contig
Bridges: zero cov regions that are mostly or entirely Ns or ns
Errors: zero cov region spans normal base composition (Total minus above 3)
Trans-no-bridge: number of transcripts spanning error regions
Trans-with-bridge: number of transcripts spanning a N or n bridge
'''

import sys
import argparse
import time
import re
from collections import Counter,defaultdict
from Bio import SeqIO

def make_transcript_index(transcriptgtf):
	'''from gtf file, makes dict of dicts where keys are tuples of ranges and values are transcript names'''
	transbyScaffold = defaultdict(dict)
	transcount = 0
	commentlines = 0
	print >> sys.stderr, "# Reading transcripts", time.asctime()
	for line in open(transcriptgtf).readlines():
		line = line.strip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				lsplits = line.split("\t")
				scaffold = lsplits[0]
				feature = lsplits[2]
				if feature=="transcript":
					transcount += 1
					boundaries = (int(lsplits[3]), int(lsplits[4]) ) # tuple of ints
					attributes = lsplits[8]
					transid = re.search('transcript_id "([\w.-]+)";', attributes).group(1)
					transbyScaffold[scaffold][boundaries] = transid
	print >> sys.stderr, "# Counted {} comment lines".format(commentlines), time.asctime()
	print >> sys.stderr, "# Counted {} transcripts for {} scaffolds".format(transcount, len(transbyScaffold)), time.asctime()
	return transbyScaffold

def index_from_sam(samfile):
	'''from sam file, make dict of dicts where keys are tuples of ranges and values are read names'''
	matesbyscaffold = defaultdict(dict)
	matecounts = 0
	commentlines = 0
	print >> sys.stderr, "# Reading matepair/long reads from {}".format(samfile), time.asctime()
	for line in open(samfile).readlines():
		line = line.strip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				lsplits = line.split("\t")
				readid = lsplits[0]
				scaffold = lsplits[2]
				# SAM format always is forward strand
				startpos = int(lsplits[3]) - 1 # SAM starts at 1, bg starts at 0
				cigar = lsplits[5]
				# zip makes tuples of ('1', 'M'), getting total length of all cases not 'I', since insertions are not positions in the reference genome
				length = sum(int(x[0]) for x in zip(re.findall("\d+",cigar),re.findall("\D",cigar)) if x[1]!="I")
				endpos = startpos + length - 1 # for length to match, must subtract one base from the end
				boundaries = (startpos, endpos)
				matesbyscaffold[scaffold][boundaries] = readid
				matecounts += 1
	print >> sys.stderr, "# Counted {} comment lines".format(commentlines), time.asctime()
	print >> sys.stderr, "# Counted {} matepairs for {} scaffolds".format(matecounts, len(matesbyscaffold)), time.asctime()
	return matesbyscaffold

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--bedgraph', help=".bg format output")
	parser.add_argument('-c','--contigs', help="genomic contigs in fasta format")
	parser.add_argument('-g','--gtf', help="transcriptome in gtf format")
	parser.add_argument('-m','--matepairs', help="long reads or mate pairs to check for bridging")
	parser.add_argument('-S','--sam', action="store_true", help="matepair mappings are in SAM format")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	print >> sys.stderr, "# Reading contigs file {}".format(args.contigs), time.asctime()
	seqdict = SeqIO.to_dict(SeqIO.parse(args.contigs,'fasta'))

	if args.gtf:
		transbyScaffold = make_transcript_index(args.gtf) # list of tuples of transcript boundaries
	if args.matepairs:
		if args.sam: # get mate pair mappings from SAM file
			matesbyScaffold = index_from_sam(args.matepairs)
		else: # otherwise assume it is gtf like transcripts
			matesbyScaffold = make_transcript_index(args.matepairs) # list of tuples of transcript boundaries

	print >> sys.stderr, "# Parsing bg input {}".format(args.bedgraph), time.asctime()
	totalzerocov = 0
	startofcontig = 0
	endofcontig = 0
	scaffoldbridges = 0
	transnobridge = 0
	transwithbridge = 0
	matesbridging = 0

	contigset = set() # to keep track of names of contigs, to count how many have problems

	errorbases = 0 # sum of bases defined as error
	for line in open(args.bedgraph,'r').readlines():
		line = line.strip()
		# bedgraph in format of:
		# contig_1	1815	1892	0
		contigname, startpos, endpos, coverage = line.split("\t")
		contigset.add(contigname)
		if not coverage=="0": # this can be filtered with grep, but is otherwise checked here
			continue
		totalzerocov += 1
		# check if zero coverage area is beginning or end of the contig
		if startpos=="0":
			startofcontig += 1
			continue
		# set start and end to ints for hereafter
		startpos, endpos = int(startpos), int(endpos)
		seqlength = len(seqdict[contigname].seq)
		if endpos==seqlength:
			endofcontig += 1
			continue
		# check if it is a scaffold bridge containing all Ns
		substring = seqdict[contigname].seq[startpos:endpos]
		c = Counter(substring)
		mc = c.most_common(1)[0][0] # should be most common letter, as first list item, first tuple item
		isbridge = False
		if mc=="N" or mc=="n": # check if zero cov region was introduced by scaffolding
			isbridge = True
			scaffoldbridges += 1
		else: # otherwise keep track of how much this is
			errorbases += endpos - startpos
		counterstring = ",".join(["{}:{}".format(k,v) for k,v in c.items() ] )
		# check if region is within a transcript
		if args.gtf:
			transspans = []
			for k,v in transbyScaffold[contigname].iteritems():
				if k[0] <= startpos and k[1] >= endpos:
					transspans.append(v)
			if transspans:
				if isbridge: # means that transcript effectively bridged two contigs
					transwithbridge += 1
				else: # transcript spans region of no coverage that is not Ns
					transnobridge += 1
			spanstring = ";{}".format(",".join(transspans) )
		else: # if no GTF, then there are no transcripts to span the gaps
			spanstring = ""
		if args.matepairs: # check if region is within a long read
			#matespans = [] ### TODO maybe print these as they come up, for change for mates vs long reads
			for k,v in matesbyScaffold[contigname].iteritems():
				if k[0] >= startpos and k[1] <= endpos: # check if span is within a mate pair or long read
					#matespans.append(v)
					matesbridging += 1
		print >> wayout, "{}\t{}\t{}{}".format(line, seqlength, counterstring, spanstring)
	print >> sys.stderr, "# Counted {} contigs with zero coverage regions".format(len(contigset) ), time.asctime()
	errorsum = totalzerocov-startofcontig-endofcontig-scaffoldbridges
	print >> sys.stderr, "Total:{}, Start:{}, End:{}, Bridges:{}, Errors:{}, Trans-no-bridge:{}, Trans-with-bridge:{}, Mates-bridging:{}".format(totalzerocov, startofcontig, endofcontig, scaffoldbridges, errorsum, transnobridge, transwithbridge, matesbridging)
	print >> sys.stderr, "# Counted {} total bases in possible error regions".format(errorbases), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
