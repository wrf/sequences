#!/usr/bin/env python

'''spadescontigstocovgc.py  last updated 2022-04-07
  directly convert contigs into stats table from the contig headers

spadescontigstocovgc.py contigs.fasta > contigs.stats.tab
    or contigs.fa  from velvetg
    or contig.fa or scaffold.fa  from platanus
    or final.contigs.fa  from MEGAHIT

    output as 6-column tabular of:
contigID  contig number  length  coverage  0.GC  #Ns

    headers specifically labeled as, for downstream scripts:
scaffold	number	length	coverage	GC	gaps

    use -H to print histogram of coverage vs. mass instead of contig stats
    default max coverage set to 1000

    the output can be used directly into contig_gc_coverage.R
Rscript ~/git/lavaLampPlot/contig_gc_coverage.R contigs.stats.tab
'''

import sys
import re
from collections import defaultdict
from Bio import SeqIO

def get_re(seqrec):
	try:
		node, nodelen, coverage = re.search("NODE_(\d+)_length_(\d+)_cov_([\d.]+)", seqrec.id).groups()
		return node, nodelen, coverage
	except AttributeError: # for no groups, because re failed, for platanus scaffolds
		pass
	try: # should work for platanus
		node, nodelen, coverage = re.search("seq(\d+)_len(\d+)_cov([\d.]+)", seqrec.id).groups()
		return node, nodelen, coverage
	except AttributeError: # for gapClosed platanus, which does not have length
		pass
	try:
		node, nodelen, coverage = re.search("scaffold(\d+)_len(\d+)_cov([\d.]+)", seqrec.id).groups()
		return node, nodelen, coverage
	except AttributeError: # for gapClosed platanus, which does not have length
		pass
	try: # change order for megahit
		node, coverage, nodelen = re.search("k\d+_(\d+) flag=\d+ multi=([\d.]+) len=(\d+)", seqrec.description).groups()
		return node, nodelen, coverage
	except AttributeError: #
		pass
	try: # >MAG_(paired)_trimmed_(paired)_contig_1_[cov=6]
		node, coverage = re.search("contig_(\d+)_\[cov=([\d.]+)\]", seqrec.id).groups()
		return node, 0, coverage
	except AttributeError: #
		pass
	try:
		node, coverage = re.search("scaffold(\d+)_cov([\d.]+)", seqrec.id).groups()
		return node, 0, coverage
	except AttributeError: # no idea
		pass
	return 0, 0, None

def get_cov_from_desc(seqdesc):
	try:
		coverage = re.search("Average coverage: ([\d.]+)", seqdesc).group(1)
		return coverage
	except AttributeError: # no idea
		pass

def clean_id(seqid):
	seqid_clean = seqid.replace("(paired)_","")
	return seqid_clean

if len(sys.argv) < 2:
	sys.exit( __doc__ )
else:
	contig_count = 0
	covbasehisto = defaultdict(int)
	contigsfile = sys.argv[1]
	if len(sys.argv)>2 and sys.argv[2]=="-H":
		dohisto = True
	else:
		dohisto = False
		sys.stdout.write( "scaffold\tnumber\tlength\tcoverage\tGC\tgaps\n" )
	sys.stderr.write( "Parsing {}\n".format(contigsfile) )
	for seqrec in SeqIO.parse(contigsfile, 'fasta'):
		contig_count += 1
		# NODE_1_length_100374_cov_28.1597_ID_1
		calclen = len(seqrec.seq)
		node, nodelen, coverage = get_re(seqrec)
	#	print >> sys.stderr, seqrec, node, nodelen, coverage
		if nodelen==0:
			nodelen = calclen
		if coverage is None and seqrec.description: # for cases where coverage is written in description
			coverage = get_cov_from_desc(seqrec.description)
		covbasehisto[int( float(coverage) )] += calclen
		if not calclen==int(nodelen): # check if measured length is different than node length
			sys.stderr.write( "WARNING: {} length {} not equal to measured {}bp\n".format(seqrec.id, nodelen, calclen) )
		gc = (seqrec.seq.count("G") + seqrec.seq.count("C"))*100.0/calclen
		ns = seqrec.seq.count("N") + seqrec.seq.count("n")

		seqid = clean_id(seqrec.id)
		if not dohisto:
			sys.stdout.write( "{}\t{}\t{}\t{}\t{:.3f}\t{}\n".format(seqid, node, calclen, coverage, gc, ns) )
	sys.stderr.write( "Done parsing {} contigs\n".format(contig_count) )
	if dohisto:
		#for k in sorted(covbasehisto.keys()):
		for k in xrange( min(1000, max(covbasehisto.keys() ) ) ):
			sys.stdout.write( "{} {}\n".format( k, covbasehisto[k] ) )

