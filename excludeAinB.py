#!/usr/bin/env python
# v1.1 2021-11-10
# v1.2 2023-04-05 bug fixes
# v1.21 2023-06-19 report total count

"""excludeAinB.py - v1.21 2023-06-19
  reads the names of sequences to exclude, then reads the sequences from a file
  and prints out the sequences whose names are not in the exclusion list

  EXAMPLE USAGE:

excludeAinB.py excludelist.txt seq.fasta > outputfile.fasta

  excludelist.txt has each sequence to exclude on a different line, such as:
contig123
contig234

  To print names which are found, use -v (verbose)
  To only print seqs and no stderr, use -q (quiet)
"""

import os
import sys
import argparse
from Bio import SeqIO

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('query_name', help="file of names or single search term")
	parser.add_argument('search_file', default = '-', help="fasta format files")
	parser.add_argument('-f','--fasta', action="store_true", help="assume query is normal fasta file, i.e. including sequence")
	parser.add_argument('--format', default="fasta", help="read format [default: fasta]")
	parser.add_argument('-m','--moleculo', action="store_true", help="separate moleculo headers at ':'")
	parser.add_argument('-q','--quiet', action="store_true", help="quiet, will not print stderr messages")
	parser.add_argument('-s','--split', nargs="?", const=' ', metavar="C", help="split query line at C, default is space")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose, print names which are not found")
	args = parser.parse_args(argv)

	# query can be file name or a string
	query_name = args.query_name
	# subject must be a fasta format file

	# detect file, and read lines into a dictionary of excludable terms
	query_dict = {}
	if os.path.isfile(query_name):
		for line in open(query_name, 'rt'):
			searchline = line.strip()
			if not searchline: # skip blank lines
				continue
			if args.fasta and searchline[0] != ">": # if fasta format, skip non-header lines
				continue
			if searchline[0]==">":
				searchline = searchline[1:].strip()
			if args.split:
				searchline = searchline.split(args.split)[0]
			query_dict[searchline] = True
	# otherwise just give one term to the dictionary
	else:
		query_dict[query_name] = True

	seq_count = 0
	not_found_count = 0
	exclude_count = 0
	if not args.quiet:
		sys.stderr.write( "# Found {} terms to exclude\n".format(len(query_dict)) )
		sys.stderr.write( "# Reading sequences from {}\n".format(args.search_file) )
	for seq_record in SeqIO.parse(open(args.search_file,'rt'), args.format):
		seq_count += 1
		seqid = seq_record.id
		if args.moleculo:
			seqid = seqid.split(":")[0]

		if query_dict.get(seqid, False): # meaning exclude sequence
			exclude_count += 1
			if args.verbose:
				sys.stderr.write( "Removing sequence: {}\n".format(seq_record.id) )
		else: # meaning keep sequence
			wayout.write(seq_record.format(args.format))
			not_found_count += 1

	if not args.quiet:
		sys.stderr.write( "# Excluded {} names out of {} sequences from A in B\n".format(exclude_count, seq_count) )
		sys.stderr.write( "# Wrote {} sequences\n".format(not_found_count) )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
