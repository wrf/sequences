#! /usr/bin/env python
# 
# This could easily be modified to run with other blast programs, or to use 
# a different program depending on a run-time parameter.
#
# v1.8 - added argument for blast type 27/03/2013
# v1.7 - removed hits/nom files, as they are unused 06/mar/2013
# v1.61- updated help information 01/feb/2013
# v1.6 - changed return to integrate with assemblerblaster 02/oct/2012
# v1.5 - updated for blast 2.2.26 09/aug/2012
# v1.4 - rearranged code blocks, corrected output 9/jun/2012
# v1.3 - added output of full length sequences with option -f
# v1.2 - 21/may/2012
# v1.1 - 14/may/2012
# ver. 1 - 29 Mar 2012
#
# email wfrancis at mbari dot org with questions or comments

"""
for nearly all cases use:

kogblaster.py -f -d dbfile

dbfile is generated from assembled contigs using makeblastdb

expects query and descriptions files to be in default locations
or specified with -q and -D, respectively

generates by default:
results file (detailed), summary file of total kog matches,
lists of hit and miss KOGs as NAME_hits.fasta and NAME_nom.fasta
a blasttable of the full results as NAME_blasttable.txt
additionally:
use -f to collect the best-hit proteins as NAME_full_seqs.faa

use -h to see all options
"""

import sys
import argparse

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-q','--query', help="query file", default="/home/biolum/cogdata/Core_genes_Parra.fta")
	parser.add_argument('-d','--db', help="database file", default="-")
	parser.add_argument('-b','--use-blasttable', metavar="BLASTTABLE", help="blasttable file, otherwise takes part of db name")
	parser.add_argument('-B','--no-blast', action="store_false", help="skip the blaststep")
	parser.add_argument('-f','--fullseqs', action="store_true", help="output a file of only full length proteins")
	parser.add_argument('-D','--descriptions', help="use description file", default="/home/biolum/cogdata/Core_genes_Parra_desc.txt")
	parser.add_argument('-p','--processors', help="number of processors", default='16')
	parser.add_argument('-s','--sizes', action="store_true", help="use fixed size cutoffs for full length proteins")
	parser.add_argument('-t','--type', help="type of blast - blastn...", default='tblastn')
	args = parser.parse_args(argv)

	import time
	import re
	import os.path
	import cStringIO
	from Bio import SeqIO
	from Bio.Blast.Applications import NcbiblastxCommandline
	from Bio.Blast import NCBIXML
	from Bio.Seq import Seq
	from Bio.Alphabet import IUPAC
	from Bio.SeqRecord import SeqRecord

	blast_file = os.path.abspath(args.query.strip())

	# establish default blasting parameters for single commandline run
	ProgramName = 'tblastn'

	blast_db = args.db
	db_path = os.path.expanduser('~/ncbi-blast-2.2.26+/db')
	mainquery = args.query
	maxnum = '50'
	threadcount = args.processors

	# the blast command
	# blasts each protein in the query file (KOGs) against the database of assembled contigs
	Blast_Command = NcbiblastxCommandline(cmd= ProgramName,query=blast_file, db=blast_db,evalue=0.000001, outfmt=5, num_threads=threadcount)
	# max_target_seqs = maxnum,num_alignments=maxnum,num_descriptions=maxnum)

	## start main
	recnum = 0
	nomatch = 0

	print >> sys.stderr, '# Reading sequences: ', time.asctime()
	seq_dictionary = SeqIO.to_dict(SeqIO.parse(blast_file, "fasta"))
	db_dictionary = SeqIO.to_dict(SeqIO.parse(os.path.join(db_path,blast_db), "fasta"))
	seq_records = list(SeqIO.parse(blast_file, "fasta"))
	#seqreclength = float(len(seq_records))
	#seq_sizes = [len(rec.seq) for rec in seq_records]
	prot_kog_pairs = [rec.id.split("___") for rec in seq_records]
	#prot_kog_pairs = []
	kog_list = []
	# adds kog if not already in list
	[kog_list.append(prot_kog[1]) for prot_kog in prot_kog_pairs if not kog_list.count(prot_kog[1])]
	kog_sizes = {}
	kog_hits = {}
	# each line in description file appears as:
	# [J] KOG0002 60s ribosomal protein L39
	# from header lines in ftp://ftp.ncbi.nih.gov/pub/COG/KOG/kog
	if os.path.exists(args.descriptions):
		kog_descs = {}
		for line in open(args.descriptions, 'r'):
			kog_num_desc = line.split("KOG")[1]
			descriptions = kog_num_desc.split(" ",1)
			kogid = "KOG"+descriptions[0]
			kog_descs[kogid] = descriptions[1]
	for kog in kog_list:
		kog_sizes[kog] = []
		kog_hits[kog] = []
	for rec in seq_records:
		kog_sizes[rec.id.split("___")[1]].append(len(rec.seq))

	print >> sys.stderr, '# Finished reading',str(len(seq_records)),'sequences: ', time.asctime()

	#print blast_db, btableoutputfilename
	if args.no_blast:
		btableoutputfilename = os.path.splitext(os.path.basename(blast_db))[0] + "_blasttable.txt"
		btableoutfile = open(btableoutputfilename, 'w')
	else:
		if args.use_blasttable:
			blast_db = os.path.splitext(os.path.basename(args.use_blasttable))[0].rsplit("_",1)[0]
			#btableoutputfilename = os.path.splitext(os.path.basename(args.use_blasttable))[0].rsplit("_",1)[0] + "_blasttable.txt"
		btableoutputfilename = os.path.splitext(os.path.basename(blast_db))[0] + "_blasttable.txt"
		btableoutfile = open(btableoutputfilename, 'rU')

	# human-readable format results file, containing kog and information about hits and alignment
	# to pull unique identifier numbers for the results from kog_results files by:
	# grep \\$ FILES | cut -d "$" -f 2 | cut -d " " -f 1 > output
	resultsfilename = os.path.splitext(os.path.basename(blast_db))[0] + "_kog_results.txt"
	resultsoutfile = open(resultsfilename, 'w')
	# summary of the results which are displayed as standard error
	summaryfilename = os.path.splitext(os.path.basename(blast_db))[0] + "_kog_summary.txt"
	summaryoutfile = open(summaryfilename, 'w')

	if args.fullseqs:
		seqsfilename = os.path.splitext(os.path.basename(blast_db))[0] + "_full_seqs.faa"
		seqsoutfile = open(seqsfilename, 'w')

	startclock=time.asctime()
	starttime= time.time()
	if args.no_blast:

		###hitsoutputfilename = os.path.splitext(os.path.abspath(blast_file))[0] + "_hits.fasta"
		#hitsoutputfilename = os.path.splitext(os.path.basename(blast_db))[0] + "_hits.fasta"
		#hitsoutfile = open(hitsoutputfilename, 'w')
		###nomatchoutputfilename = os.path.splitext(os.path.abspath(blast_file))[0] + "_nom.fasta"
		#nomatchoutputfilename = os.path.splitext(os.path.basename(blast_db))[0] + "_nom.fasta"
		#nomatchoutfile = open(nomatchoutputfilename, 'w')

		print >> sys.stderr, '# Started BLAST on file',blast_file,'at: ' + startclock
		print >> sys.stderr, 'COMMAND:', Blast_Command

		print >> btableoutfile, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % ("Resulttitle", "Evalue", "Align_length", "Query Length", "Fraction Covered", "Query", "Accession", "Matched", "Num_Align", "Frame") 

		# creates handle from standard output of blast command
		result_handle = cStringIO.StringIO(Blast_Command()[0])

		print >> sys.stderr, '# Started BLAST: ', startclock
		print >> sys.stderr, '# Finished BLAST: ', time.asctime()
		print >> sys.stderr, "# BLASTed %d records in %.1f minutes" % (len(db_dictionary), (time.time()-starttime)/60)

		twotime=time.time()
		if result_handle:
			for record in NCBIXML.parse(result_handle):
				recnum+=1
				if len(record.alignments):
					# all of the results are returned
					for x,alignment in enumerate(record.alignments):
						rt = record.alignments[x].hit_def
						ev = record.descriptions[x].e
						al = record.alignments[x].hsps[0].align_length
						ql = record.query_length
						fc = float(al)/float(ql)
						rq = record.query
						ac = record.alignments[x].accession
						ms = record.alignments[x].hsps[0].match.replace(' ','.')
						na = str(x+1)+"/"+str(len(record.alignments))
						qf = str(record.alignments[x].hsps[0].frame)

						# hit data is stored in kog_hits dictionary
						kog = record.query.split("___")[1]
						outline = [rt, ev, al, ql, fc, rq, ac, ms, na, qf]
						kog_hits[kog].append(outline)

						# generates blasttable file
						print >> btableoutfile, "\t".join([str(x) for x in outline]) 
					#try:
						#hitsoutfile.write("%s" % seq_dictionary[record.query].format("fasta"))
					#except:
						#hitsoutfile.write("%s" % seq_dictionary[record.query.split(" ")[0]].format("fasta"))
				else:
					nomatch+=1
					#try:
						#nomatchoutfile.write("%s" % seq_dictionary[record.query].format("fasta"))
					#except:
						#nomatchoutfile.write("%s" % seq_dictionary[record.query.split(" ")[0]].format("fasta"))
			result_handle.close()
		else:
			print >> sys.stderr, "ERROR - no handle"
		#hitsoutfile.close()
		#nomatchoutfile.close()
		print >> sys.stderr, '# Finished parsing records: ', time.asctime()
		print >> sys.stderr, "# Processed %d records in %.1f minutes" % (recnum, (time.time()-twotime)/60)
		print >> sys.stderr, "# %d records had hits" % ((recnum-nomatch))
		print >> sys.stderr, "# %d records had no match" % (nomatch)
	# if not blasting, then expects a blasttable and generates kog_hits dictionary from file
	else:
		linecount = 0
		for line in btableoutfile:
			if linecount:
				outline = line.split("\t")
				kog = outline[5].split("___")[1]
				kog_hits[kog].append(outline)
			linecount+=1

	btableoutfile.close()
	print >> sys.stderr, '# Processing %d kogs: ' % (len(kog_hits)), time.asctime()

	# this section processes kog_hits dictionary

	# for number of organisms, thus sequences per kog
	kogmax = 6
	# where defined as:
	# unambig for cases where best hit in a kog is within size range, and has start and stop
	# transoffsize for cases where best hit is within size, but translated is offsize
	# notrans for cases where best hit is within size, but lacks stop or start
	# offsize for where no top hits are within size
	# allsame for where all top hits are to same transcript
	# allzero for where all evalues are zero
	# discord for where there are hits but dont match an above case
	# nohits for where that kog has no hits at all
	koghits = 0
	perfects = 0
	notrans = 0
	anyalign = 0
	transoffsize = 0
	offsize = 0
	allsame = 0
	allzero = 0
	nohits = 0

	nonsenseprot = 0
	longprot = 0
	shortalign = 0
	possibleshort = 0
	trueshortseq = 0

	for kog in sorted(kog_hits.keys()):
		if kog_descs:
			print >> resultsoutfile, "#"+kog, kog_descs[kog].rstrip()
		else:
			print >> resultsoutfile, "#"+kog	
		if kog_hits[kog]:
			koghits+=1
			#          rt     evalue      al len   quer len  query   frame                              match
			realqfs = [[x[0],float(x[1]),int(x[2]),int(x[3]),x[5],int(x[9].rstrip()[1:-1].split(",")[1]),x[7]] for x in kog_hits[kog]]
			rt_list = [x[0] for x in realqfs]
#			#if all([x == rt_list[0] for x in rt_list]):
			if len(list(set(rt_list))) == 1:
				allsame+=1
				final_rt = rt_list[0]
			# generates list of translated ORFs from blast results
			pre_orf_list = []
			# pulls the sequence from the db_dictionary
			for i,rt in enumerate(rt_list):
				try:
					rt_sequence = db_dictionary[rt.strip()].seq
				except KeyError:
					rt_sequence = db_dictionary[rt.split(" ")[0].strip()].seq
				rt_frame = realqfs[i][5]
				# gets longest piece of the alignment
				align_frags = realqfs[i][6].replace('+','.').split('.')
				align_lens = [len(afrag) for afrag in align_frags]
				align_piece = align_frags[align_lens.index(max(align_lens))]
				if rt_frame < 0:
					transseq = rt_sequence.reverse_complement()[(abs(rt_frame)-1):].translate()
				else:
					transseq = rt_sequence[(rt_frame-1):].translate()
				preorfs = transseq.split('*')
				# generates orfs list if preorf contains the alignment, to restrict frame
				orfs = [orf for orf in preorfs if align_piece in orf]
				orflength = len(orfs)
				if orflength:
					fragmentlengths = [len(fragment) for fragment in orfs]
					#fragdiffs = [abs(1-float(x)/realqfs[i][3]) for x in fragmentlengths]
					#longestfragment = orfs[fragdiffs.index(min(fragdiffs))]
					longestfragment = orfs[fragmentlengths.index(max(fragmentlengths))]
					pre_orf_list.append((longestfragment,i))
			# kog size range is generated here
			kog_min_size = min(kog_sizes[kog])
			kog_max_size = max(kog_sizes[kog])
			if args.sizes:
				size_upper_var = 1.3
				size_low_var = 0.95
				size_floor_var = 0.9
			else:
				size_upper_var = float(kog_max_size+1)/kog_min_size
				size_low_var = float(kog_min_size)/(sorted(kog_sizes[kog])[-2])
				size_floor_var = float(kog_min_size)/(kog_max_size+1)
			# counts for cases where e value is zero for all hits
			ev_list = [x[1] for x in realqfs]
			if not any(ev_list):
				allzero+=1
			ev_orf_list = []
			# if any preorfs exist, then search for M in those preorfs
			if pre_orf_list:
				rt_orf_list = [[orf[0][orf[0].find('M'):],orf[1]] for orf in pre_orf_list if orf[0].count('M')]
				if rt_orf_list:
					ev_orf_list = [realqfs[x[1]][1] for x in rt_orf_list]
					best_ev_index = ev_orf_list.index(min(ev_orf_list))
					final_rt = rt_list[rt_orf_list[best_ev_index][1]]+"___"+kog
					best_sequence = SeqRecord(rt_orf_list[best_ev_index][0], id=final_rt, description=final_rt)
					best_query = realqfs[rt_orf_list[best_ev_index][1]][4]
					best_ev_match = realqfs[rt_orf_list[best_ev_index][1]][6]
					best_ev_al = realqfs[rt_orf_list[best_ev_index][1]][2]
					best_ev_ql = realqfs[rt_orf_list[best_ev_index][1]][3]
					best_ev_frame = realqfs[rt_orf_list[best_ev_index][1]][5]
					if (size_upper_var*kog_max_size > len(best_sequence) >= size_low_var*kog_min_size) and ((len(best_sequence) > 1.4*best_ev_al) or (len(best_sequence) < 0.9*best_ev_al)):
						shortalign+=1
						print >> resultsoutfile, "$2 Low-confidence-Match"
					elif size_upper_var*kog_max_size > len(best_sequence) >= size_low_var*kog_min_size:
						perfects+=1
						print >> resultsoutfile, "$1 Positive-Match"
					elif size_low_var*kog_min_size > len(best_sequence) >= size_floor_var*kog_min_size:
						possibleshort+=1
						print >> resultsoutfile, "$64 Possible-correct-ORF"
					elif len(best_sequence) >= size_upper_var*kog_max_size:
					#elif len(best_sequence) >= 1.3*best_ev_ql:
						longprot+=1
						print >> resultsoutfile, "$32 Long-ORF"
					elif (len(best_sequence) < size_floor_var*best_ev_al) and (len(best_sequence) < (size_floor_var*kog_min_size)):
						nonsenseprot+=1
						print >> resultsoutfile, "$16 Nonsense-ORF"
					elif (len(best_sequence) < (size_floor_var*kog_min_size)):
						trueshortseq+=1
						print >> resultsoutfile, "$8 Short-ORF"
					#if (abs(1-(len(best_sequence)/float(best_ev_al))) <= 0.1) and best_ev_al < 0.9*best_ev_ql:
					# so if P:244 A:232 Q:752, then |1-(244/232)| < 1-(708/753) and 232 < (708/753*708)
					# 1-0.9508 < 1-0.9414 and 232 < 666.57
					# 0.04918 < 0.05851 and True, thus True
					# but if P:168 A:130 Q:190, then 1-(168/130) < 1-(190/198) and 130 < 190/198*190
					# |1-1.2923| < 1-0.9595 and 130 < 182.32
					# 0.2923 > 0.0404 and True, thus False
					#elif (abs(1-(len(best_sequence)/float(best_ev_al))) <= (1.0-size_floor_var) and best_ev_al < (size_floor_var*kog_min_size)):
						#trueshortseq+=1
						#print >> resultsoutfile, "$8 Short-ORF"
					#elif 0.95*best_ev_ql > len(best_sequence) >= 0.9*best_ev_ql:
					#elif size_low_var*kog_min_size > len(best_sequence) >= size_floor_var*kog_min_size:

					# line shows protein length (P), alignment length (A), query length (Q), e value, and frame
					print >> resultsoutfile, "P:%d A:%d Q:%d E:%.3e F:%d" % (len(best_sequence), best_ev_al, best_ev_ql, ev_orf_list[best_ev_index], best_ev_frame)
				else:
					ev_orf_list = [realqfs[x[1]][1] for x in pre_orf_list]
					best_ev_index = ev_orf_list.index(min(ev_orf_list))
					final_rt = rt_list[pre_orf_list[best_ev_index][1]]+"___"+kog
					best_sequence = SeqRecord(pre_orf_list[best_ev_index][0], id=final_rt, description=final_rt)
					best_query = realqfs[pre_orf_list[best_ev_index][1]][4]
					best_ev_match = realqfs[pre_orf_list[best_ev_index][1]][6]
					best_ev_al = realqfs[pre_orf_list[best_ev_index][1]][2]
					best_ev_ql = realqfs[pre_orf_list[best_ev_index][1]][3]
					best_ev_frame = realqfs[pre_orf_list[best_ev_index][1]][5]
					transoffsize+=1
					print >> resultsoutfile, "$4 Incomplete-ORF"
					print >> resultsoutfile, "P:%d A:%d Q:%d E:%.3e F:%d" % (len(best_sequence), best_ev_al, best_ev_ql, ev_orf_list[best_ev_index], best_ev_frame)
				resultsoutfile.write("%s" % best_sequence.format("fasta"))
				print >> resultsoutfile, best_ev_match
				resultsoutfile.write("%s" % seq_dictionary[best_query].format("fasta"))
				if args.fullseqs:
					seqsoutfile.write("%s" % best_sequence.format("fasta"))
			else:
				print >> resultsoutfile, "$128 No-ORFs-Detected"
				print >> resultsoutfile, "P:0 A:0 Q:1 E:0 F:0"
				notrans+=1
			al_ok = [x[2] for x in realqfs if x[2] >= min(kog_sizes[kog])]
			if al_ok:
				anyalign+=1
			else:
				offsize+=1
		# if no hits were found for a kog
		else:
			print >> resultsoutfile, "$256 No-Match-Found"
			print >> resultsoutfile, "P:0 A:0 Q:1 E:0 F:0"
			nohits+=1

#		outline = (rt, ev, al, ql, fc, rq, ac, ms, na, qf, kog)

	for outmode in [sys.stderr,summaryoutfile]:
		print >> outmode, "# %d kogs had at least one hit" % (koghits)
		print >> outmode, "Of those:"
		print >> outmode, "# %d kogs had at least one alignment within expected size" % (anyalign)
		print >> outmode, "# %d kogs had no alignments within expected size" % (offsize)
		print >> outmode, "Of those:"
		print >> outmode, "# %d kogs had full-length matches ($1)" % (perfects)
		print >> outmode, "# %d kogs had the best alignment much shorter than the protein or query ($2)" % (shortalign)
		print >> outmode, "# %d kogs had no predicted orfs within expected size ($4)" % (transoffsize)
		print >> outmode, "# %d kogs had the protein and alignment shorter than the query ($8)" % (trueshortseq)
		print >> outmode, "# %d kogs had the best protein much shorter than the best alignment - possible nonsense ($16)" % (nonsenseprot)
		print >> outmode, "# %d kogs had the protein much longer than the query ($32)" % (longprot)
		print >> outmode, "# %d kogs had the best protein slightly shorter than the query ($64)" % (possibleshort)
		print >> outmode, "# %d kogs had sequences without Met or stop codons ($128)" % (notrans)
		print >> outmode, "# %d kogs had all hits to the same transcript" % (allsame)
		print >> outmode, "# %d kogs had all hits with evalue of 0" % (allzero)
		print >> outmode, "# %d kogs had no hits ($256)" % (nohits)

	summaryoutfile.close()
	resultsoutfile.close()
	if args.fullseqs:
		seqsoutfile.close()
	# keep this to integrate into pipelines
	# the nomatch output from one search can be used as input for the next search
	return resultsfilename

"""
	###########################################################
	# contents of a record in records:

['__doc__', '__init__', '__module__', 'alignments', 'application', 'blast_cutoff', 
	'database', 'database_length', 'database_letters', 'database_name', 
	'database_sequences', 'date', 'descriptions', 'dropoff_1st_pass', 
	'effective_database_length', 'effective_hsp_length', 'effective_query_length', 
	'effective_search_space', 'effective_search_space_used', 'expect', 'filter', 
	'frameshift', 'gap_penalties', 'gap_trigger', 'gap_x_dropoff', 'gap_x_dropoff_final', 
	'gapped', 'hsps_gapped', 'hsps_no_gap', 'hsps_prelim_gapped', 'hsps_prelim_gapped_attemped', 
	'ka_params', 'ka_params_gap', 'matrix', 'multiple_alignment', 'num_good_extends', 'num_hits', 
	'num_letters_in_database', 'num_seqs_better_e', 'num_sequences', 'num_sequences_in_database', 
	'posted_date', 'query', 'query_id', 'query_length', 'query_letters', 'reference', 'sc_match', 
	'sc_mismatch', 'threshold', 'version', 'window_size']

	# contents of a description in a record
	dir(r.descriptions[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'bits', 'e', 
	'num_alignments', 'score', 'title']

	# contents of an alignment in a record
	dir(r.alignments[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'hit_def', 
	'hit_id', 'hsps', 'length', 'title']

	dir(r.alignments[0].hsps[0])
	['__doc__', '__init__', '__module__', '__str__', 'align_length', 'bits', 'expect', 'frame', 
	'gaps', 'identities', 'match', 'num_alignments', 'positives', 'query', 'query_end', 
	'query_start', 'sbjct', 'sbjct_end', 'sbjct_start', 'score', 'strand']

	put_full_orfs = [x for x in rt_orf_list if size_upper_var*kog_max_size > len(x[0]) >= size_low_var*kog_min_size]
	if put_full_orfs:
		ev_orf_list = [realqfs[x[1]][1] for x in put_full_orfs]
		best_ev_index = ev_orf_list.index(min(ev_orf_list))
		final_rt = rt_list[put_full_orfs[best_ev_index][1]]
		best_sequence = SeqRecord(put_full_orfs[best_ev_index][0], id=final_rt, description=final_rt)
		best_query = realqfs[put_full_orfs[best_ev_index][1]][4]
		best_ev_match = realqfs[put_full_orfs[best_ev_index][1]][6]
		best_ev_al = realqfs[put_full_orfs[best_ev_index][1]][2]
		best_ev_ql = realqfs[put_full_orfs[best_ev_index][1]][3]
		best_ev_frame = realqfs[put_full_orfs[best_ev_index][1]][5]
		if len(best_sequence) > 1.4*best_ev_al:
			shortalign+=1
			print >> resultsoutfile, "$2 Low-confidence-Match"
		else:
			perfects+=1
			print >> resultsoutfile, "$1 Putative-Match"

"""

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

