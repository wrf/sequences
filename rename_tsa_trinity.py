#!/usr/bin/env python

# last modified 2018-04-05

'''
rename_tsa_trinity.py GELM01.1.fsa_nt > GELM01.renamed.fasta

    or, can accept gzipped:

rename_tsa_trinity.py GELM01.1.fsa_nt.gz > GELM01.renamed.fasta
'''

import sys
import gzip

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	fastafile = sys.argv[1]
	if fastafile.rsplit('.',1)[-1]=="gz": # autodetect gzip format
		opentype = gzip.open
	else: # otherwise assume normal open
		opentype = open
	for line in opentype(fastafile,'r'):
		line = line.strip()
		if line[0]==">":
			headersplits = line.split()
			newheader = ">{}_{}_{}".format( *headersplits[2:5] )
			newheader = newheader.replace("TRINITY_","")
			print >> sys.stdout, newheader
		else:
			print >> sys.stdout, line
