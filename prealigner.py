#!/usr/bin/env python
#
# v2.41 warning if search file is not found 2019-02-14
# v2.4 added .pal support 2017-08-09
# v2.3 added option to display all output to stdout 2016-10-03
# v2.2 fixed bits bug for multiple hits as it stored one value 2014/oct/10
# v2.1 fixed bits error when there are no hits 2014/sep/30
# v2.0 changed output options to display bitscore with evalue 2014/aug/15
# v1.9 updated for blast 2.2.29+ 2014-03-05
# v1.8 updated for blast 2.2.28+ 2013/jul/10
# v1.7 added option for blast program 2013/jun/20
# v1.6 added reporting of evalues, removed blasttable indexing 2013/jun/03
# v1.5 corrected bug with path names 01/nov/2012
# v1.4 updated for blast 2.2.26+ 07/aug/2012
# v1.3 added feature to blast a query list against fasta sequences 13/mar/2012
# v1.2 searches multiple words provided by internal list
# v1.1 integrates biopython SeqIO to handle fasta sequences

"""prealigner.py  last updated 2022-01-18

    -s search is a fasta file of proteins to search for in subject_file
prealigner.py -s query_prots.fasta transcriptome.fasta

    for protein databases, change -b to blastp
prealigner.py -s query_prots.fasta -b blastp translated_ests.fasta

    a hit results summary is printed to stderr
    the fasta sequence of that contig is written to a file

    the input file must have a formatted blast database using makeblastdb
    or be a .nal or .pal file, using the full path
prealigner.py -s query_prots.fasta all_est_dbs.nal
"""

import sys
import os
import argparse
import re
import time
from io import StringIO
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastxCommandline
from Bio.Blast import NCBIXML
from Bio.Seq import Seq

def get_dbs_from_pal(palfile):
	'''extract database list from pal file and return list of absolute-paths'''
	paldir = os.path.dirname(palfile)
	for line in open(palfile,'r'):
		if line[0:6]=="DBLIST":
			rawdblist = line[7:].strip().replace('"','').split(" ")
	dblist = [os.path.join(paldir,l) for l in rawdblist]
	return dblist

def main(argv):
	if not len(argv):
		argv.append("-h")
	#prealigner_seqs = os.path.expanduser("~/ncbi-blast-2.2.29+/db/prealigner_seqs_original.fasta")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('subject_file', help="fasta format file that has a blast database", default = '-')
	parser.add_argument('-o','--output-dir', help='output directory for files', default="./")
	parser.add_argument('-s','--search', help="query fasta file of sequences", required=True)
	parser.add_argument('-b','--blast', help="blast program [default: tblastn]", default="tblastn")
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff blast search - default: 1e-5", default=0.00001)
	parser.add_argument('-m','--max-seqs', type=int, help="maximum number of records per blast search [default: 100]", default=100)
	parser.add_argument('-O','--stdout', action='store_true', help="write all matches to STDOUT")
	parser.add_argument('-Q','--add-query', action='store_true', help="include query sequence in output file, but only if there are hits")
	parser.add_argument('-R','--no-rename', action='store_true', help="do not rename sequences in output files")
	parser.add_argument('--RD', action='store_true', help="no rename, and instead put hit stats in description")
	parser.add_argument('-t','--threads', type=int, help="number of threads for blast [default: 1]", default=1)
	parser.add_argument('-v','--verbose', action='store_true', help="verbose output")
	args = parser.parse_args(argv)

	if not os.path.isfile(args.search):
		sys.exit("ERROR: CANNOT FIND FILE -s {}".format(args.search))

	# firstly, make dictionary of subject sequences
	seq_dictionary = {} # empty dict
	if args.subject_file[-4:]==".pal" or args.subject_file[-4:]==".nal": # if suffix is .pal or .nal
		for blastdb in get_dbs_from_pal(args.subject_file):
			# actually check if the file is there before trying to import
			if os.path.exists(blastdb) and os.path.isfile(blastdb):
				sys.stderr.write("# Reading sequences from {} : {}\n".format(blastdb, time.asctime() ) )
				seq_dictionary.update(SeqIO.to_dict(SeqIO.parse(blastdb, "fasta")))
			else: # print warning and continue
				sys.stderr.write("# WARNING: CANNOT FIND {} , SKIPPING\n".format(blastdb, time.asctime() ) )
		commanddb = args.subject_file[:-4] # chop off pal or nal suffix
	else: # otherwise is a normal fasta file
		sys.stderr.write("# Reading sequences from {} :  {}\n".format(args.subject_file, time.asctime() ) )
		seq_dictionary.update( SeqIO.to_dict(SeqIO.parse(args.subject_file, "fasta")) )
		commanddb = args.subject_file # db name is subject file
	sys.stderr.write("# Finished reading {} sequences:  {}\n".format(len(seq_dictionary), time.asctime() ) )

	# auto increase max-seqs if total is very high
	if len(seq_dictionary) > 500000 and args.max_seqs < 200:
		args.max_seqs = 200

	# make blast command
	Blast_Command = NcbiblastxCommandline(cmd=args.blast, query="-", db=commanddb, evalue=args.evalue, outfmt=5, num_threads=args.threads, max_target_seqs = args.max_seqs)
	# num_descriptions removed due to incompatability in blast-2.2.26+

	prealignedlist = []
	# for each sequence in the query file, blast them one by one against the db
	for seqrec in SeqIO.parse(args.search, "fasta"):
		# blank list to be filled with tuples, if there are any hits
		contig_list = []
		reverse_count = 0
		query_seq = seqrec.format("fasta")
		sys.stderr.write( "# {} search for {} in {} :  {}\n".format(args.blast, seqrec.id, args.subject_file, time.asctime() ) )
		# this is the blast step
		result_handle = StringIO(Blast_Command(stdin=query_seq)[0])
		for record in NCBIXML.parse(result_handle):
			sys.stderr.write( "# Parsing output for {} :  {}\n".format(seqrec.id, time.asctime() ) )
			# iterate through alignments to collect evalue and bitscore
			for i,alm in enumerate(record.alignments):
				# tuple only appended if there are hits
				contig_list.append((alm.hit_def, alm.hsps[0].frame[1], alm.hsps[0].expect, record.descriptions[i].bits))
				# corrected to display accession in verbose mode
				if args.verbose:
					# though accession is not so useful as it is just a number
					print( alm.accession , file=sys.stderr )
			sys.stderr.write( "# {} hits found for {} :  {}\n".format( len(contig_list), seqrec.id , time.asctime() ) )

		word = seqrec.id
		if len(contig_list): # if there are no alignments, then next steps will be skipped
			if args.stdout:
				outfile = sys.stdout
				prealigner_file = None
			else:
				prealigner_name = "{}_{}.fasta".format(os.path.splitext(os.path.basename(args.subject_file))[0], word.replace("|","_") )
				prealigner_file = os.path.join(args.output_dir, prealigner_name)
				outfile = open (prealigner_file, 'w') # if there are hits, create a file to output the hits
			if args.add_query: # if set, add the query sequence to the fasta file
				outfile.write( seqrec.format("fasta") )
			for (matchcontig,frame,evalue,bitscore) in contig_list:
				try:
					outseq = seq_dictionary[matchcontig]
				except KeyError: 
					try: # check to see if the key was split at the first space
						outseq = seq_dictionary[matchcontig.split(" ")[0]]
					except KeyError: # otherwise ignore the sequence
						print( "# ERROR CANNOT FIND {}".format(matchcontig) , file=sys.stderr )
				if frame < 0: # frame should be 1,2,3 or -1,-2,-3
					outseq.seq = outseq.seq.reverse_complement()
					reverse_count += 1

				# change ID or description to include hit information
				if args.RD:
					outseq.description = "hitby={} e={:.1e} bits={:.1f}".format(seqrec.id, evalue, bitscore)
				else: # removes description so it will not appear as duplicate of seqid
					outseq.description = ""
					if not args.no_rename:
					# add the bitscore or evalue to the fasta name in the output file
						outseq.id += "__hitby={}_e={:.1e}_bits={:.1f}".format(seqrec.id, evalue, bitscore)

				# write each seq to file
				outfile.write("{}".format(outseq.format("fasta") ) )
			if not args.stdout:
				outfile.close()
				sys.stderr.write( "# Matches written to {} :  {}\n".format(prealigner_file, time.asctime() ) )
			if reverse_count: # if proteins, or no reverse compliments, ignore
				sys.stderr.write( "# {} reverse compliments for {} :  {}\n".format(reverse_count, seqrec.id, time.asctime() ) )
			prealignedlist.append(prealigner_file)
	return prealignedlist

if __name__ == "__main__":
	main(sys.argv[1:])

