#!/usr/bin/env python
# v0.1 2015-09-25
# v0.2 2024-12-09

'''
samAinB.py v0.2 2024-12-09

    can take stdin for BAM files, and allow reads as .gz
samtools view hits.bam | samAinB.py -c keepcontigs -i - | samtools view -bT genome.fa - > filtered_hits.bam

    for tab as a delimiter, used -d $'\\t' with SINGLE QUOTES
'''

import sys
import argparse
import time

def get_contig_list(contigfile, delimiter):
	print >> sys.stderr, "# Reading contigs file {}".format(contigfile), time.asctime()
	if delimiter and delimiter=="\\t": # in case the instructions are disregarded and "\t" is used
		delimiter = "\t"
	contigdict = {}
	example = ""
	for line in open(contigfile,'r'):
		line = line.rstrip()
		if delimiter:
			line = line.split(delimiter)[0]
		if line[0] == ">":
			line = line[1:]
		contigdict[line] = True
		if not example:
			example = line
			print >> sys.stderr, "Contigs from file parsed as: {}".format(example)
	print >> sys.stderr, "# Found {} contigs".format(len(contigdict) ), time.asctime()
	return contigdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', type = argparse.FileType('r'), default = '-', help="sam or bam file")
	parser.add_argument('-c','--contigs', help="list of contigs, genomic or transcriptomic")
	parser.add_argument('-d','--delimiter', help="delimiter of contig name, such as ',' or ' ' from csv or table")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	contigdict = get_contig_list(args.contigs, args.delimiter) if args.contigs else None

	headercount = 0
	linecounter = 0
	print >> sys.stderr, "# Parsing input from {}".format(args.input.name), time.asctime()
	for line in args.input.readlines():
		if line[0]=="@":
			headercount += 1
			wayout.write(line)
		elif line.split('\t')[2] in contigdict:
			linecounter += 1
			wayout.write(line)
	print >> sys.stderr, "# Counted {} header lines".format(headercount), time.asctime()
	print >> sys.stderr, "# Wrote {} SAM lines".format(linecounter), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
