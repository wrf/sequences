#!/usr/bin/env python

'''align_pos_to_fasta.py  last modified 2020-05-06

    based on an alignment, extract AAs at positions and append them to fasta headers

align_pos_to_fasta.py -a ace2_mammal_sequences.aln -s 44,48,51,95,369 > ace2_mammal_sequences.w_sites.aln
'''

import sys
import argparse
from Bio import SeqIO

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-a','--alignment', help="alignment in fasta format")
	parser.add_argument('-s','--sites', help="comma separate list of sites, like '75,79,150'")
	parser.add_argument('-i','--keep-index', action="store_true", help="sites given are already indexed for python (meaning minus 1)")
	parser.add_argument('--delimiter', help="separator for fasta header and sites [_]")
	args = parser.parse_args(argv)

	sites = map(int,args.sites.split(","))
	if not args.keep_index:
		sites = [i-1 for i in sites]

	seqcounter = 0
	sys.stderr.write("# Reading {}, keeping {} positions\n".format(args.alignment, len(sites) ) )
	for seqrec in SeqIO.parse(args.alignment, "fasta"):
		seqsites = [seqrec.seq[s] for s in sites]
		posstring = "-".join(seqsites)
		seqrec.id = "{}{}{}".format( seqrec.id, args.delimiter, posstring )
		seqrec.description = ""
		wayout.write( seqrec.format("fasta") )
		seqcounter += 1
	sys.stderr.write("# Counted {} sequences\n".format(seqcounter) )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
