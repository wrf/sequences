#!/usr/bin/env python

'''
rename_select_fasta.py renamed_vector.tab seqs.fasta > seqs_w_renames.fasta

renamed_vector.tab is any text in 2 columns per line as:
old_name    New_name
'''

import sys

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	renamedict = {} # key is first column old ID, value is new ID
	print >> sys.stderr, "# Reading rename vector from {}".format( sys.argv[1] )
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		if line:
			old_name, new_name = line.split("\t")
			renamedict[old_name] = new_name
	print >> sys.stderr, "# Found {} name changes".format( len(renamedict) )

	idcounter = 0
	changecounter = 0
	missingcounter = 0
	print >> sys.stderr, "# Reading fasta file {}".format( sys.argv[2] )
	for line in open(sys.argv[2],'r'):
		line = line.strip()
		if line[0]==">":
			idcounter += 1
			seqid = line[1:].split()[0]
			if seqid in renamedict:
				newline = line.replace(seqid, renamedict[seqid])
				changecounter += 1
				print >> sys.stdout, newline
			else: # ID not found
				missingcounter += 1
				print >> sys.stdout, line
		else: # is not a header line
			print >> sys.stdout, line
	print >> sys.stderr, "# Changed {} out of {} IDs".format( changecounter, idcounter )
	if missingcounter:
		print >> sys.stderr, "# Could not change {} sequences".format( missingcounter )
