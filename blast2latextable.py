#!/usr/bin/env python
#

'''blast2latextable.py copied_blast.txt

    copy text of hits from NCBI BLAST to a file, looks like:

Select seq gb|KF309179.1|
	Rhodobacter sp. WS22 16S ribosomal RNA gene, partial sequence
	848 	848 	100% 	0.0	100% 	KF309179.1

    reformats and prints to STDOUT
'''

import sys
import argparse

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input', help="text format copied from online BLAST")
	args = parser.parse_args(argv)

	headerline = "Description & Max score & Total score & Query Coverage & Percent Identity & Accession \\\\ \hline"
	print >> sys.stderr, headerline
	for line in open(args.input,'r').readlines():
		line = line.strip() # this removes leading tab
		if line.find("Select")==0: # if line is Select seq gb|...
			continue
		linestart = line[1]
		try:
			firstletter = int(linestart)
		except ValueError:
			firstletter = linestart
		if type(firstletter) is str:
			# multiple descriptions can be separated by ;
			description = line.split(";")[0].replace(", partial sequence","")
		elif type(firstletter) is int:
			scoresplits = line.replace("\t\t","\t").split("\t")
			stripsplits = [x.strip() for x in scoresplits]
			scoreline = " & ".join(stripsplits[0:3]+stripsplits[4:])
			scoreline = scoreline.replace("%","\%")
			print >> wayout, "{} & {} \\\\".format(description, scoreline)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
