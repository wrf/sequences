#!/usr/bin/env python
#
# gtfAinB.py v1.0 2015-09-21
# renamed to getgtfgenes.py 2020-02-27
# python3 update 2022-10-25
# v1.3 2023-05-14
# v1.4 2023-06-19 add GenBank GFF mode

'''getgtfgenes.py last modified v1.4 2023-06-19

    to print only entries where the gene name is in list_of_genes
    gene name or transcript ID is in the 9th GTF column

getgtfgenes.py list_of_genes transcripts.gtf > kept_genes.gtf

    list_of_genes gene names or fasta names are split at first space
'''

import sys
import re
import os
import gzip
import time
import argparse

def make_query_dict(query_file, isfasta, delimiter=' '):
	'''take file name of queries to keep, return a dict where key is gene or transcript ID and value is True'''
	query_dict = {}
	linecounter = 0
	sys.stderr.write("# Reading queries from {}  {}\n".format(query_file, time.asctime() ) )
	for line in open(query_file,'r'):
		line = line.strip()
		if line: # should remove blank or empty lines
			linecounter += 1
			if isfasta:
				if line[0]==">": # get query names from fasta headers
					geneid = line[1:].split(delimiter)[0]
					query_dict[geneid] = True
			else:
				geneid = line.split(delimiter)[0]
				query_dict[geneid] = True
	sys.stderr.write("# Read {} lines for {} entries  {}\n".format(linecounter, len(query_dict) , time.asctime() ) )
	return query_dict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('query_name', help="file of names or gene IDs")
	parser.add_argument('gtf_file', nargs="*", help="gtf format file, can be .gz")
	parser.add_argument('-e','--exclude', action="store_true", help="exclude entries match the query")
	parser.add_argument('-f','--fasta', action="store_true", help="assume query is normal fasta file")
	parser.add_argument('-t','--by-gene', nargs="?", const="gene", default="transcript", help="search by gene_id instead of transcript_id (for Stringtie)")
	parser.add_argument('--genbank-gff', help="use presets when proteins and GFF files are from GenBank", action="store_true")
	args = parser.parse_args(argv)

	query_dict = make_query_dict(args.query_name, args.fasta)

	file_list = [n for n in args.gtf_file if os.path.isfile(n)]

	allowed_features = ["gene", "mRNA", "transcript", "exon", "CDS"]
	toplevel_features = ["mRNA", "transcript"]

	for gtffile in file_list:
		commentlines = 0
		linecounter = 0
		writecount = 0
		transcounter = 0
		keeptrans = 0
		ignoredfeatures = 0

		if gtffile.rsplit('.',1)[-1]=="gz": # autodetect gzip format
			opentype = gzip.open
			sys.stderr.write("# Searching features from gzipped {}  {}\n".format(gtffile, time.asctime() ) )
		else: # otherwise assume normal open for GTF format
			opentype = open
			sys.stderr.write("# Searching features from {}  {}\n".format(gtffile, time.asctime() ) )
		for line in opentype(gtffile,'rt'):
			line = line.strip()
			if line: # ignore empty lines
				if line[0]=="#": # count comment lines, just in case
					commentlines += 1
				else:
					linecounter += 1

					lsplits = line.split("\t")
					scaffold = lsplits[0]
					feature = lsplits[2]
					geneid = None
					parentid = None

					if feature not in allowed_features: # any other features may cause problems later
						ignoredfeatures += 1
						continue

					if feature in toplevel_features :
						transcounter += 1

					attributes = lsplits[8]

					if attributes.find("ID") > -1: # indicates gff3 format
						geneid = re.search('ID=([\w.|-]+)', attributes).group(1)
					elif attributes.find("gene_id") > -1 or attributes.find("transcript_id") > -1: # indicates gtf format
						geneid = re.search('{}_id "([\w.|-]+)"'.format(args.by_gene), attributes).group(1)

					if attributes.find("Parent") > -1: # indicates gff3 format
						parentid = re.search('Parent=([\w.|-]+)', attributes).group(1)

					# ID=cds-XP_054755137.1;Parent=rna-XM_054899162.1;Dbxref=GeneID:129261115,GenBank:XP_054755137.1;Name=XP_054755137.1;gbkey=CDS;gene=LOC129261115;product=protocadherin Fat 1-like isoform X2;protein_id=XP_054755137.1
					if args.genbank_gff and attributes.find("protein_id") > -1:
						geneid = re.search("protein_id=([\w.|-]+)", attributes).group(1)

					if geneid is None: # if no gene is given, use the parent ID
						if feature=="exon" or feature=="CDS": # split at transcript ID in case top level is not the parent
							geneid = parentid.rsplit(".t",1)[0]
						else:
							geneid = parentid

					if query_dict.get(geneid, False) or query_dict.get(parentid, False) or (args.exclude and not query_dict.get(geneid, False) ):
						writecount += 1
						if feature in toplevel_features:
							keeptrans += 1
						print( line, file = wayout)

		sys.stderr.write("# Counted {} lines for {} transcripts  {}\n".format(linecounter, transcounter, time.asctime() ) )
		sys.stderr.write("# Wrote {} lines, with {} top-level features\n".format(writecount, keeptrans) )


if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
