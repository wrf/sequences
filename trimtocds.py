#! /usr/bin/env python
#
# v2.1 fixed translation bug due to using incorrect frame 2014-11-25
# v2.0 functionalized for ease of use with other programs 2014-04-17
# v1.1 added trim to methionine function 2013-11-01
# v1.0 2013-10-08

# program to trim nucleotide sequences to the coding sequence
# or trim proteins to the first methionine
# requires both nucleotides and the translated proteins
# for example, these could be from genome models

"""trimtocds v2.2  last modified 2022-03-10

trimtocds.py -n nucls.fasta -p translated.fasta -o output.fasta
  this should take appx 5 minutes for a set of 200k proteins

  accepts protein names as, and will output as:
>seq123_removethis
  to match nucleotide by removing last term:
>seq123
  "_" can be changed to any character or string with -d

  or can trim proteins to first methionine:
trimtocds.py -p protswithmet.fasta -o output.fasta
"""

import sys
import time
import argparse
from collections import deque
from Bio.Seq import Seq
from Bio import SeqIO


def get_six_frame(seq_record,code):
	"""the 6-frame translation and returns a list of all proteins from six frames"""
	dna_seq = seq_record.seq
	prot_seqs = []
	intronphase = deque([None,-2,-1])
	# some reason there is a problem subsetting 0:0 if entire seq should be translated
	intronphase.rotate(len(dna_seq) % 3)
	prot_seqs.append(dna_seq[0:intronphase[0]].translate(table=code))
	prot_seqs.append(dna_seq[1:intronphase[1]].translate(table=code))
	prot_seqs.append(dna_seq[2:intronphase[2]].translate(table=code))
	prot_seqs.append(dna_seq.reverse_complement()[0:intronphase[0]].translate(table=code))
	prot_seqs.append(dna_seq.reverse_complement()[1:intronphase[1]].translate(table=code))
	prot_seqs.append(dna_seq.reverse_complement()[2:intronphase[2]].translate(table=code))
	return prot_seqs

def get_basesequence(seqtotrim,frame):
	"""get reverse complement for frames 3 to 5"""
	if frame > 2:
		# change the offset to 0-2 rather than 3-5
		frame = frame-3
		baseseq = str(seqtotrim.reverse_complement())
	else:
		baseseq = str(seqtotrim)
	# and return the nucleotide sequence
	return baseseq, frame

def get_matching_frames(nuclseq, seqid, sequence, geneticcode):
	"""use the sequence from the nucleotide dictionary and translate in all 6 frames"""
	allframes = get_six_frame(nuclseq, geneticcode)
	# keep only the frame that contains the target protein, as well as the frame, in a tuple
	matchingframes = [(x,i) for i,x in enumerate(allframes) if sequence in x]
	# returns a list of tuples of full frame Seq object and number of frame
	return matchingframes

def trim_nucleotides(untrimmedseq, protstring, workingprot, frame):
	"""find the protein string in the full sequence, and calculate the start and end positions"""
	protstart = workingprot.find(protstring)
	protend = protstart+len(protstring)
	# calculate the positions in the string for the protein
	startpos = frame + (protstart*3)
	endpos = frame + (protend*3)
	# using the calculated break positions, make the trimmed nucleotide sequence for alignments, etc.
	outseq = untrimmedseq[startpos:endpos]
	# returns string of trimmed sequence
	return outseq

def trim_protein(fullprot):
	"""trim protein at first Methionine"""
	mposit = str(fullprot).find("M")
	if mposit > -1:
		return fullprot[mposit:]
	else:
		return 0

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-n','--nucleotides', help='nucleotides to be trimmed')
	parser.add_argument('-p','--proteins', help='translated proteins')
	parser.add_argument('-o','--output', help='optional output file name, default is stdout',  type=argparse.FileType("w"), default=wayout)
	parser.add_argument('-c','--code', type=int, default=1, help="use alternate genetic code")
	parser.add_argument('-d','--delimiter', default="_", help='split protein names [_]')
	parser.add_argument('-v', '--verbose', action='store_true', help="display more output")
	args = parser.parse_args(argv)

	# protein counter
	seqcount = 0 # total in protein file
	protcount = 0 # proteins with nucl match
	trimcount = 0 # final trimmed

	# MAIN LOOP
	if args.nucleotides: # if this is present, then trim nucleotides to protein coding sequence
		print( "# Reading nucleotides from {}  {}".format(args.nucleotides, time.asctime() ) , file=sys.stderr )
		nucldict = SeqIO.to_dict(SeqIO.parse(args.nucleotides,'fasta'))

		print( "# Found {} transcripts, now trimming transcripts to CDS  {}".format( len(nucldict), time.asctime() ) , file=sys.stderr )
		for seqrec in SeqIO.parse(args.proteins,'fasta'):
			seqcount += 1
			# for programs that have appended tags, remove those from the name
			# by default, looks for __ such as sequenceID__frame
			shortseqid = seqrec.id.rsplit(args.delimiter,1)[0]
			# get the translated protein frame which matches the protein query
			try:
				keepframes = get_matching_frames(nucldict[shortseqid], seqrec.id, seqrec.seq, args.code)
				protcount += 1
			except KeyError: # meaning cannot find protein ID
				print( "WARNING: CANNOT FIND NUCL MATCH FOR PROTEIN {}    CHECK -d".format(shortseqid) , file=sys.stderr )
				keepframes = []
			if len(keepframes)==1:
				trimcount += 1
				# get the original untrimmed nucleotide sequence or reverse complement
				# must store new frame as well for cases where frame is 3 to 5 (-1 to -3)
				untrimmedseq, useframe = get_basesequence(nucldict[shortseqid].seq, keepframes[0][1])
				# get the trimmed seq
				outseq = trim_nucleotides(untrimmedseq, str(seqrec.seq), keepframes[0][0], useframe)
				# reassign the sequence the nucleotide sequence as a Seq object
				seqrec.seq = Seq(outseq)
				# write the sequence to file as fasta
				args.output.write(seqrec.format("fasta"))
			elif len(keepframes)>1: # will not write CDS
				print( "WARNING: {} ORFS IN {} OF LENGTH {}".format( len(keepframes), seqrec.id, len(seqrec.seq) ) , file=sys.stderr )
			else: # will not write CDS
				print( "WARNING: NO PROTS FOUND FOR PROTEIN {}    CHECK -c".format(shortseqid) , file=sys.stderr )
		if seqcount:
			print( "# Found {} transcripts for {} proteins  {}".format(seqcount, protcount, time.asctime() ) , file=sys.stderr )
		if trimcount:
			print( "# Trimmed {} transcripts".format( trimcount ) , file=sys.stderr )
		else:
			print( "# WARNING: NO TX TRIMMED  CHECK -d OR -c" , file=sys.stderr )

	else: # otherwise trim translations to first methionine
		print( "# Trimming proteins to methionine  {}".format( time.asctime() ) , file=sys.stderr )
		nometcount = 0
		for seqrec in SeqIO.parse(args.proteins,'fasta'):
			seqcount += 1
			mtrimmedprot = trim_protein(seqrec.seq)
			if mtrimmedprot:
				seqrec.seq = mtrimmedprot
			else:
				print( "ERROR: NO M FOUND IN {}".format(seqrec.id) , file=sys.stderr )
				nometcount += 1
			args.output.write(seqrec.format("fasta"))
		print( "# Trimmed {} proteins  {}".format(seqcount, time.asctime() ) , file=sys.stderr )
		print( "# {} possible nonsense proteins  {}".format(nometcount, time.asctime() ) , file=sys.stderr )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
