#!/usr/bin/env python
#
# raworthodb2ogs.py v1 2017-01-02

'''
raworthodb2ogs.py v1.0  last modified 2017-01-09
convert raw OrthoDB data into proteins by ortholog group, instead of species

    usage, for example from all FASTA files from metazoa, from odb9v1_metazoa_fasta.tar.gz
    files are automatically named by OG, like EOG091G1ZB7.fasta

raworthodb2ogs.py -f *.fs -s odb9v1_species.tab -g odb9v1_OG2genes.tab -o odb9v1_OGs.tab

    to filter odb9v1_OG2genes.tab by relevant taxa from -f, use -G
    this retains only entries for taxa from -f, for future use
raworthodb2ogs.py -f *.fs -g odb9v1_OG2genes.tab -G > odb9v1_OG2genes.metazoa.tab

    for re-running on the same data with the filtered set:
raworthodb2ogs.py -f *.fs -s odb9v1_species.tab -g odb9v1_OG2genes.metazoa.tab -o odb9v1_OGs.tab

    BioPython library is required
'''

import sys
import os
import argparse
import time
import re
from collections import defaultdict
from Bio import SeqIO

def parse_ogs_to_genes(ogsgenesfile, keptspecies, outdirectory, doprint, keptogs, verbose=False):
	'''read in tabular information and return dict where key is gene ID and value is ortholog number'''
	# odb9_OG2genes.tab
	# 1.	OG unique id
	# 2.	Ortho DB unique gene id
	ogtogenesdict = {} # keys are protein IDs, values are ortholog numbers
	ogdict = {}
	linecounter = 0
	doublecounts = 0
	print >> sys.stderr, "# Reading orthologs-genes list from {}".format(ogsgenesfile), time.asctime()
	for line in open(ogsgenesfile,'r'):
		line = line.rstrip()
		if line and not line[0]=="#": # remove empty and comment lines
			linecounter += 1
			lsplits = line.split("\t")
			ognumber = lsplits[0]
			proteinid = lsplits[1]
			speciesid = proteinid.split(":")[0]
			if keptspecies.get(speciesid, False): # only consider species in the set
				if keptogs.get(ognumber, False): # only consider prepicked OGs
					if ogtogenesdict.get(proteinid, False) and verbose:
						doublecounts += 1
						print >> sys.stderr, "# WARNING: {} overwrites existing value {} for {}".format(ognumber, ogtogenesdict.get(proteinid), proteinid)
					ogtogenesdict[proteinid] = ognumber
					ogdict[ognumber] = "{}.fasta".format(os.path.join(outdirectory,ognumber))
					if doprint: # print filtered lines
						print >> sys.stdout, line
	print >> sys.stderr, "# Counted {} lines and kept ortholog IDs for {} genes".format(linecounter, len(ogtogenesdict)), time.asctime()
	if doublecounts:
		print >> sys.stderr, "# {} proteins are in multiple OGs".format(doublecounts), time.asctime()
	return ogtogenesdict, ogdict

def parse_orthologs(orthologsfile, levels):
	'''read in tabular information and return dict where key is ortholog ID and value is OG description'''
	# odb9_OGs.tab
	# 1.	OG unique id (not stable between releases)
	# 2.	level tax_id on which the cluster was built
	# 3.	OG name (the group's most common gene name)
	keptogs = {}
	linecounter = 0
	print >> sys.stderr, "# Reading ortholog list from {}".format(orthologsfile), time.asctime()
	for line in open(orthologsfile,'r'):
		line = line.rstrip()
		if line and not line[0]=="#": # remove empty and comment lines
			linecounter += 1
			lsplits = line.split("\t")
			ognumber = lsplits[0]
			level = lsplits[1]
			if levels.get(level,False):
				description = lsplits[2]
				keptogs[ognumber] = description
	print >> sys.stderr, "# Counted {} lines and kept {} OGs".format(linecounter, len(keptogs)), time.asctime()
	return keptogs

def parse_species(speciesfile, keptspecies):
	'''read in tabular information and return dict where key is ID number and value is species name'''
	# odb9_species.tab
	# 1.	NCBI tax_id
	# 2.	scientific name
	# 3.	total count of genes in this species
	# 4.	total count of OGs it participates
	# 5.	mapping type, clustered(C) or mapped(M)
	speciesdict = {} # keys are numbers, values are formatted species names
	linecounter = 0
	print >> sys.stderr, "# Reading species list from {}".format(speciesfile), time.asctime()
	for line in open(speciesfile,'r'):
		line = line.rstrip()
		if line and not line[0]=="#": # remove empty and comment lines
			linecounter += 1
			lsplits = line.split("\t")
			taxid = lsplits[0]
			rawspecies = lsplits[1]
			if keptspecies.get(taxid, False):
				resp = re.search("(\w+) (\w+)", rawspecies)
				try:
					formatspecies = "{}_{}".format(resp.group(1), resp.group(2))
				except AttributeError: # for weird ones where it does not match
					formatspecies = rawspecies.replace(" ","_").replace("(","").replace(")","")
				speciesdict[taxid] = formatspecies
	print >> sys.stderr, "# Counted {} lines and kept {} species".format(linecounter, len(speciesdict)), time.asctime()
	return speciesdict

def keep_species(fastalist):
	'''read in tabular information and return dict where key is ID number and value is species name'''
	keepdict = {}
	for entry in fastalist:
		idnum = entry.split(".")[0]
		keepdict[idnum] = True
	print >> sys.stderr, "# Keeping proteins for {} species".format(len(keepdict)), time.asctime()
	return keepdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-a','--annotations', help="ortholog annotations tsv file")
	parser.add_argument('-d','--directory', help="directory for ortholog FASTA files ['ogs/']", default="ogs/")
	parser.add_argument('-f','--fasta', nargs="*", help="fasta reference files")
	parser.add_argument('-g','--ortholog-genes', help="ortholog-to-genes tabular file")
	parser.add_argument('-G','--output-orthologs', action="store_true", help="output ortholog-to-genes information for kept species to stdout")
	parser.add_argument('-o','--orthologs', help="ortholog names tabular file")
	parser.add_argument('-s','--species-list', help="OG species list, for replacing numbers with species")
	parser.add_argument('-t','--taxa', default="33208", help="taxa levels [33208]")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	startclock=time.asctime()
	starttime= time.time()

	keptspeciesdict = keep_species(args.fasta)
	speciesnamedict = parse_species(args.species_list, keptspeciesdict) if args.species_list else {}
	levelsdict = dict([[l, True] for l in args.taxa.split(",")])
	keptogsdict = parse_orthologs(args.orthologs, levelsdict) if args.orthologs else {}
	ogtogenedict, ogdict = parse_ogs_to_genes(args.ortholog_genes, keptspeciesdict, args.directory, args.output_orthologs, keptogsdict)

	if os.path.isdir(args.directory):
		print >> sys.stderr, "# Using existing directory {}".format(args.directory), time.asctime()
	else:
		print >> sys.stderr, "# Making directory {}".format(args.directory), time.asctime()
		os.mkdir(args.directory)

	protcounts = defaultdict(int)
	goodcounts = defaultdict(int)
	badcounts = defaultdict(int)
	for speciesprots in args.fasta:
		speciesid = speciesprots.split(".")[0]
		print >> sys.stderr, "# Reading prots from {}".format(speciesprots), time.asctime()
		for seqrec in SeqIO.parse(speciesprots,"fasta"):
			protcounts[speciesid] += 1
			ogID = ogtogenedict.get(seqrec.id, None)
			if ogID:
				goodcounts[speciesid] += 1
				with open(ogdict[ogID],'a') as og:
					og.write(seqrec.format("fasta"))
			else:
				badcounts[speciesid] += 1
				if args.verbose:
					print >> sys.stderr, "# WARNING: cannot find OG for {}".format(seqrec.id)
		print >> sys.stderr, "# Found {} prots from {}".format(protcounts[speciesid], speciesprots), time.asctime()
	print >> sys.stderr, "# Process completed in {:.1f} minutes".format((time.time()-starttime)/60)
	print >> sys.stderr, "# Sorted {} total proteins for {} species".format(sum(protcounts.values()),len(args.fasta)), time.asctime()
	print >> sys.stderr, "# Count not find OGs for {} proteins".format(sum(badcounts.values())), time.asctime()
	print >> sys.stderr, "NCBI-ID\tSPECIES\tTOTAL\tWITH-OG\tNO-OG\tPERCENT"
	for k,v in sorted(badcounts.items(), key=lambda x: x[1], reverse=True):
		print >> sys.stderr, "{}\t{}\t{}\t{}\t{}\t{}".format(k, speciesnamedict.get(k,"None"), protcounts[k], goodcounts[k], v, v*100.0/protcounts[k])

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
