#! /usr/bin/env python
#
# v1.0 remove_identical_seqs.py  created 2022-01-10
# because commonseq.py is depricated, due to the Bio.Trie library no longer being supported
# v1.1 2023-06-26 try except for proteins
# v1.2 filetype changed to rt instead of rU 2024-12-09

'''remove_identical_seqs.py  last modified 2024-12-09

    remove_identical_seqs.py sequences.fasta > nr_sequences.fasta

  can take stdin, writes to stdout

    cat seqs1.fa seqs2.fa | remove_identical_seqs.py - > nr_seqs.fasta
'''

import sys
import time
import argparse
from Bio.Seq import Seq
from Bio import SeqIO

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('r'), default = '-', help="fasta format file")
	args = parser.parse_args(argv)
	
	input_file = args.input_file

	record_counter = 0
	write_counter = 0
	rc_counter = 0
	base_total = 0

	already_seen_seqs = {} # key is sequence, value is True
	
	for seq_record in SeqIO.parse(input_file, "fasta"):
		record_counter += 1
		base_total += len(seq_record.seq)
		if str(seq_record.seq) in already_seen_seqs:
			continue

		try:
			seq_record.rc = seq_record.seq.reverse_complement()
			if str(seq_record.rc) in already_seen_seqs:
				rc_counter += 1
				continue
		except ValueError: # meaning probably protein, so no RC, skip
			pass

		already_seen_seqs[str(seq_record.seq)] = True
		write_counter += 1
		wayout.write(seq_record.format("fasta"))

	print("# Read {} sequences, wrote {}".format(record_counter, write_counter), file=sys.stderr )
	if rc_counter:
		print("# {} matched a reverse complement".format(rc_counter), file=sys.stderr )
	print("# Total counted bases: {}".format(base_total), file=sys.stderr )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

