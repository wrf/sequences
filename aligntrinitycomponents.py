#!/usr/bin/env python
#
# aligntrinitycomponents.py v1 2014-11-27

'''
aligntrinitycomponents.py -o sample_x_alignments trinity.fa

this calls mafft-einsi, which might take a long time
'''

import sys
import os
import time
import argparse
from Bio import SeqIO
from collections import defaultdict
import subprocess
import multiprocessing

def parse_components(seqdict):
	componentdict = defaultdict(list)
	for k in seqdict.keys():
		comp, seq = seqdict[k].id.rsplit("_",1)
		componentdict[comp].append(seqdict[k].id)
	# returns a dictionary where keys are components and values are sequences
	return componentdict

def generate_split_files(components, sequences, outdir):
	filelist = []
	for k in sorted(components, key=lambda k: len(components[k])):
		outfilename = os.path.join(outdir,"{}.fasta".format(k))
		with open(outfilename,'w') as sf:
			for s in components[k]:
				sf.write(sequences[s].format('fasta'))
		# pointless to align single sequence, so ignore those with only one
		if len(components[k]) > 1:
			if len(components[k]) < 300:
				filelist.append(outfilename)
			else:
				print >> sys.stderr, "%d VARIANTS IN %s" % (len(components[k]), k)
		else:
			print >> sys.stderr, "SINGLE SEQUENCE IN %s" % k
	return filelist

def run_mafft(filelist, outdir, threads):
	print >> sys.stderr, "Making mafft commands", time.asctime()
	mafftcommandfile = "mafft_commands_{}.sh".format(outdir)
	with open(mafftcommandfile,'w') as mcf:
		for f in filelist:
			outfilename = "{}.aln".format(os.path.splitext(f)[0])
			mafftcommand = "mafft-einsi --ep 0 {0} > {1}".format(f,outfilename)
			print >> mcf, mafftcommand
	print >> sys.stderr, "Running parallel mafft", time.asctime()
	parallelArgs = ['parallel', '--gnu', '-a', mafftcommandfile, '-j', threads, '--joblog', 'mafft_%s.log' % outdir]
	subprocess.call(parallelArgs)

def get_threads(threads):
	if threads:
		threadcount = threads
	else:
		print >> sys.stderr, "Detecting processors..."
		threadcount = str(multiprocessing.cpu_count())
	print >> sys.stderr, "Using %s threads" % (threadcount)
	return threadcount

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', help="fasta format file", default = '-')
	parser.add_argument('-o','--output-dir', help='output directory for files', default="./")
	parser.add_argument('-t','--threads', action='store_true', help="threads for mafft")
	parser.add_argument('-v','--verbose', action='store_true', help="verbose output")
	args = parser.parse_args(argv)

	threadcount = get_threads(args.threads)
	starttime = time.time()

	print >> sys.stderr, "Reading fasta sequences", time.asctime()
	seqdict = SeqIO.to_dict(SeqIO.parse(args.input_file,'fasta'))
	print >> sys.stderr, "Parsing Trinity components", time.asctime()
	components = parse_components(seqdict)
	print >> sys.stderr, "Splitting fasta sequences", time.asctime()
	splitfiles = generate_split_files(components, seqdict, args.output_dir)
	print >> sys.stderr, "Aligning components", time.asctime()
	run_mafft(splitfiles, args.output_dir, threadcount)

	print >> sys.stderr, "Process completed in %.1f minutes" % ( (time.time()-starttime)/60 )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
