#!/usr/bin/env python
# haplotypeblastn.py v1.0 created 2015-12-01

'''haplotypeblastn.py  last modified  2015-12-01
haplotypeblastn.py -b scaffolds.blastn.tab -g scaffolds.fa
'''


import sys
import argparse
import time
from collections import defaultdict
from Bio import SeqIO

def make_seq_length_dict(contigsfile):
	print >> sys.stderr, "# Parsing genomic contigs {}".format(contigsfile), time.asctime()
	lengthdict = {}
	for seqrec in SeqIO.parse(contigsfile,'fasta'):
		lengthdict[seqrec.id] = len(seqrec.seq)
	print >> sys.stderr, "# Found {} contigs".format(len(lengthdict)), time.asctime()
	return lengthdict

def parse_blastn(blastfile, scaflendict, identmin, lengthmin, coveragemin):
	blastmatches = defaultdict(dict)
	matchcount = 0
	selfmatches = 0
	tooshort = 0
	lowidentity = 0
	notcovered = 0
	keptmatches = 0
	print >> sys.stderr, "# Parsing {}".format(blastfile), time.asctime()
	for line in open(blastfile,'r').readlines():
		line = line.strip()
		if line: # skip empty lines
			matchcount += 1
			lsplits = line.split("\t")
			querycontig = lsplits[0]
			subjectcontig = lsplits[1]
			if querycontig==subjectcontig: # skip all self matches
				selfmatches += 1
				continue
			pident = float(lsplits[2])
			if pident < identmin:
				lowidentity += 1
				continue
			allength = float(lsplits[3])
			if allength < lengthmin:
				tooshort += 1
				continue
			blastcoverage = allength/scaflendict[querycontig]
			if blastcoverage < coveragemin:
				notcovered += 1
				continue
			print >> sys.stderr, "{} to {} {}% for {} out of {} vs {}".format(querycontig, subjectcontig, pident, allength, scaflendict[querycontig], scaflendict[subjectcontig])
			keptmatches += 1
			print line
	print >> sys.stderr, "# Counted {} matches".format(matchcount), time.asctime()
	print >> sys.stderr, "# Removed {} self matches".format(selfmatches), time.asctime()
	print >> sys.stderr, "# Removed {} below {}% identity".format(lowidentity, identmin), time.asctime()
	print >> sys.stderr, "# Removed {} alignments shorter than {}bp".format(tooshort, lengthmin), time.asctime()
	print >> sys.stderr, "# Removed {} alignments with less than {}% coverage".format(notcovered, coveragemin), time.asctime()
	print >> sys.stderr, "# Kept {} matches".format(keptmatches), time.asctime()

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--blastn', help="tabular blastn results")
	parser.add_argument('-g','--genome', help="genomic contigs as fasta")
	parser.add_argument('-o','--output', help="optional output name, otherwise uses input name as base")
	parser.add_argument('-c','--coverage', default=0.8, type=int, help="minimum alignment coverage [0.8]")
	parser.add_argument('-i','--identity', default=75.0, type=float, help="minimum identity percentage to consider as haplotypes [75.0]")
	parser.add_argument('-l','--length', default=500, type=int, help="minimum alignment length [500]")
	args = parser.parse_args(argv)

	scaffoldlengths = make_seq_length_dict(args.genome)
	parse_blastn(args.blastn, scaffoldlengths, args.identity, args.length, args.coverage)







if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
