#!/usr/bin/env python
#
# getmethionine.py v1 2013-09-27

import sys
from Bio import SeqIO

usage='''
get sequences that do not start with methionine

getmethionine.py proteins.fasta > non-m-seqs.fasta
'''

def main(argv, wayout):
	if len(argv) < 2:
		print usage
		sys.exit()

	inputfile = argv[1]

	readcount = 0
	writecount = 0

	for seqrec in SeqIO.parse(inputfile,'fasta'):
		readcount += 1
		if seqrec.seq[0] != "M":
			writecount += 1
			wayout.write("%s" % seqrec.format("fasta"))

	print >> sys.stderr, "read %d sequences" % readcount
	print >> sys.stderr, "wrote %d sequences" % writecount


if __name__ == "__main__":
	main(sys.argv, sys.stdout)
