#!/usr/bin/env python
#
# random_lines_from_file.py  created 2021-03-30

'''random_lines_from_file.py  last modified 2021-03-30
    randomly extract lines from large data files, or other text files

    take 1 in 20 lines
random_lines_from_file.py -i data.csv -p 0.05 > short_data.csv

    take 1 in 10 lines, up to 5000
random_lines_from_file.py -i data.csv -l 5000 -p 0.1 > data.5000lines.csv

'''

import sys
import argparse
import time
import gzip
import random

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="input file, in any text format")
	parser.add_argument('-l','--lines', metavar="N", type=int, help="keep N lines, and will stop printing after N")
	parser.add_argument('-t','--total', metavar="N", type=int, help="total number of lines, if known (-p percent will be ignored)")
	parser.add_argument('-p','--percent', metavar="0.N", type=float, help="keep on average N percent of lines, 0.1 would be appx 1 in 10 lines")
	parser.add_argument('-C','--comments', help="print any comment lines, but do not count to total, otherwise skip comment lines")
	parser.add_argument('-H','--header', help="has header line, meaning print first line")
	args = parser.parse_args(argv)


	linecounter = 0
	writecounter = 0
	commentcounter = 0
	has_found_header = 0

	if args.total:
		sys.stderr.write("# Randomly taking {} out of {} lines\n".format( args.lines, args.total ) )
	else:
		if args.lines:
			sys.stderr.write("# Randomly taking {:.2f}% of lines, stopping at {}\n".format( args.percent * 100.0, args.lines ) )
		else:
			sys.stderr.write("# Randomly taking {:.2f}% of lines\n".format( args.percent * 100.0 ) )

	inputfilename = args.input
	if inputfilename.rsplit('.',1)[-1]=="gz": # autodetect gzip format
		opentype = gzip.open
		sys.stderr.write("# Reading from {} as gzipped  {}\n".format(inputfilename, time.asctime() ) )
	else: # otherwise assume normal open for fasta format
		opentype = open
		sys.stderr.write("# Reading from {}  {}\n".format(inputfilename, time.asctime() ) )

	for line in opentype(inputfilename,'rt'):
		if args.header and linecounter==0:
			sys.stdout.write(line)
		linecounter += 1
		if line[0]=="#":
			commentcounter += 1
			if args.comments:
				sys.stdout.write(line)
				continue
		
		if args.total: # meaning calculate differently if a total line count is known
			pass

		else: # meaning use percent until either target line count is hit or file ends
			if args.lines:
				if writecounter >= args.lines:
					continue
			random_float = random.random()
			if random_float <= args.percent:
				sys.stdout.write(line)
				writecounter += 1
	writefraction = 100.0*writecounter/linecounter

	if commentcounter:
		sys.stderr.write("# Printed {} comment lines\n".format(writecounter, linecounter) )
	if has_found_header:
		sys.stderr.write("# Wrote {} lines (excluding header line) out of {} ({:.2f}%)  {}\n".format(writecounter, linecounter, time.asctime() ) )
	else:
		sys.stderr.write("# Wrote {} lines out of {} ({:.2f}%)  {}\n".format(writecounter, linecounter, writefraction, time.asctime() ) )

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
