#! /usr/bin/env python
#
# v1.0 protein repeat detector, taking parts of sizecutter
# v1.1 removed relaxed option, added skips 2015-03-19
# added catch for empty sequences 2015-03-25
# v1.2 python3 update 2022-04-28
# v1.3 add minimum length option 2022-10-19
# v1.31 filetype changed to r instead of rU 2024-12-09

'''
protein_repeat_finder.py v1.31 - last modified 2024-12-09

  flags proteins for unusual amino acid content
  operation should take around 10-30 seconds for normal gene models

protein_repeat_finder.py prot_sequences.fasta > flagged_IDs.txt

  OUTPUT:
  tabular output is in the form of:

12345  g2468   543     Rep:E-11 Rep:D-7 Rep:CV-22

  indicating protein #12345
  protein ID g2468
  length 543
  with repeats of E (11 in a row), etc.

    to print out the sequence for each flagged prot, use -s

protein_repeat_finder.py -s prot_sequences.fasta > flagged_proteins.txt

#7000 Rep:E-11 Rep:D-7 Rep:CV-22
>jgi|Monbr1|33067|estExt_fgenesh2_pg.C_150203
MVCVCVCVCVCVCVCVCVCVCVCVSSNCQTEITQLLTTTILTTNGFYHNSSFAAPISMAK
KKKGGRRQDDDWDDDKVPDPVAAAAAAAGSNDDAADTPAPANAGGKKKGGKKGKGKGKGK
KGDDDDEWAEAEAKVLNKADDAAKDESEDEPEGAATAPPRKQKQKKGKGGGFGALVLDDE
DEDDNASDAGSDAGETEKPLGLAPVKVPAKKNKGGKKGKGAKGKKSEPEPEEEPEEEPEA
AEAAAADDVDDNDNAYDDDGENNGDDNASDNDNDEDGDGDSDDDDDDDASGRRGKKKEKV
SRKARKKAAQQAKFQAEIAAREADGNFSVAQRANANDNDVLENATDINIDAFSIAARGKD
LFVNAQLKVAAGRRYGLIGPNGHGKTTLLKHIAERKLRFPANIDCLLCEQEVAANDLPAV
EAVLSSDVRRTELMQREKEINILIEKGNTEEALQKELNEVYAELEAIGAEAAEGRARKIL
AGLGFSAEMQDRPTKNFSGGWRMRVSLARALFMEPTLLMLDEPTNHLDLNAVIWLDHYLS
RWKKTLLIVSHDQDFLDNVCTDIVHLDNKKLYYYRGNYTSFKKMHVQKRREQEKAFEKQQ
KELKKLKQSGVTKAKAESQAKAKQDMKKKGNKKKGDDDEEEDGPELLERPMDYVVRFHFP
NPPELAPPILGIHDVSFRYNEKADWLFDGVDLGIDMNSRIAIVGNNGVGKSTLLKLLTGD
VNPTQGEITRNHRLRIGVYNQHSAEQLGKEESPSERLQRLFNLPYQECRKTLGQYGLASH
AHTIKMKDLSGGQKARVVFAELSLCAPDIIILDEPTNNLDIESIDALVKAINEYEGGVIL
VSHDARLILETDCELYECANRDCRRIDGDFNDYRDLVLERLENDDIVEVEGRVVDTGNHA
EEEEEEEEEEEVLF*

    options -c and -r disable composition and repeat searches
    appx. 90% of the run time is repeat searches

    ### NOTE some compositions and repeats are typical of real proteins
    for example:
  com:K or rep:R may be histones
  com:G may be collagen
'''

import sys
import argparse
import time
import re
from Bio import SeqIO

# copied from sizecutter.py
def get_longest_repeat(sequence, regexp):
	# this will allow for multi-character repeats, but takes 2x as long (v2.2)
	polyresults = regexp.finditer(str(sequence))
	# for multicharacter repeats, length with be number of repeats * length, so 'ACACAC' would return 6 rather than 3
	try:
		longestrepeat = max([l.end()-l.start() for l in polyresults])
		return longestrepeat
	# for cases where repeat does not occur once, and max of list fails
	except ValueError:
		return 0

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('r'), default = '-', help="fasta format file")
	parser.add_argument('-9', '--ninety', action='store_true', help="use 0.9 percentile vs 0.99 (this finds a lot of junk)")
	parser.add_argument('-c', '--composition', action='store_false', help="skip test for simple repeats")
	parser.add_argument('-m', '--minimum-length', type=int, default=1, help="only report proteins with length greater than m , default: 1")
	parser.add_argument('-r', '--repeats', action='store_false', help="skip test for composition bias")
	parser.add_argument('-d', '--dipeptide', action='store_true', help="use longer cutoffs for dipeptide repeats")
	parser.add_argument('-s', '--sequences', action='store_true', help="print full sequences, rather than just ID")
	args = parser.parse_args(argv)

	# if both tests are skipped, then exit
	if not args.repeats and not args.composition:
		sys.exit( "# NO CHECKS ARE SELECTED, REMOVE EITHER -c OR -r  {}\nexiting".format( time.asctime() ) )

	#aa_list = "ANQILVKRHFYWDESTCMGP"   # this is not called anywhere


	# strict dictionary values (99th percentile of SwissProt, except C-composition and N- and Q-repeats)
	#else:
	if args.composition:
		sys.stderr.write( "# Run test for compositional bias  {}\n".format( time.asctime() ) )
		compd = {"A":21.49,"N":15.36,"Q":13.92,"I":16.13,"L":20.43, "V":15.25,"K":21.54,"R":24.00,"H":8.96,"F":14.63, "Y":10.00,"W":7.69,"D":13.04,"E":17.74,"S":17.60, "T":13.33,"C":15.0,"M":10.00,"G":20.00,"P":18.18 , "X":0.0 }
		# if searching for X, add the last value to the dictionary
		flagcomp = {}
	if args.repeats:
		sys.stderr.write( "# Run test for simple amino acid repeats  {}\n".format( time.asctime() ) )
		# large number of sequences have a single 'X', thus greater than 1 forms a true repeat
		repeatd = {"A":7,"N":10,"Q":10,"I":3,"L":5, "V":4,"K":5,"R":4,"H":4,"F":3, "Y":3,"W":2,"D":5,"E":8,"S":8, "T":5,"C":3,"M":3,"G":7,"P":7, "CV":4,"IY":4,"RA":4,"RE":4,"SL":4,"TH":4, "X":1}
		if args.dipeptide: # using the longer cutoff values for dipeptides
			sys.stderr.write( "# Using long cutoffs for dipeptide repeats  {}\n".format( time.asctime() ) )
			repeatd.update({"CV":6,"IY":6,"RA":6,"RE":6,"SL":6,"TH":6})
		doublerepeats = 0
		flagrepeats = {}
		# make regular expression objects for each repeat
		re_repeats = {}
		for key in repeatd.keys():
			re_repeats[key] = re.compile("({})+".format(key) )
	if args.minimum_length > 1:
		sys.stderr.write( "# Only keeping proteins of length {} or more\n".format( args.minimum_length ) )

	# relaxed dictionary values (90th percentile of SwissProt)
	if args.ninety:
		compd = {"A":15.0,"N":7.91,"Q":7.55,"I":10.89,"L":14.51, "V":11.0,"K":13.03,"R":11.48,"H":4.76,"F":7.69, "Y":5.79,"W":3.62,"D":8.38,"E":11.33,"S":10.87, "T":8.25,"C":9.70,"M":5.02,"G":11.43,"P":8.47, "X":0.0 }
		repeatd = {"A":4,"N":3,"Q":3,"I":3,"L":3, "V":3,"K":3,"R":3,"H":2,"F":2, "Y":2,"W":2,"D":3,"E":3,"S":3, "T":3,"C":2,"M":2,"G":3,"P":3, "CV":4,"IY":4,"RA":4,"RE":4,"SL":4,"TH":4, "X":1}

	seqcount = 0
	flagcount = 0
	failcount = 0

	# begin main operation
	sys.stderr.write( "# Parsing sequences on {}  {}\n".format( args.input_file.name, time.asctime() ) )
	for seq_record in SeqIO.parse(args.input_file, "fasta"):
		failone = False
		flaglist = []
		seqcount += 1
		seqlength = len(seq_record)

		# if the sequence is empty for some reason, skip it
		if not seqlength or seqlength < args.minimum_length:
			continue

		# start repeat checking
		if args.repeats:
			checkmode="Rep"
			# sorted so ordering in the output will be roughly alphabetical
			for k,i in sorted(re_repeats.items(), key=lambda x: len(x[0])):
				outvalue = get_longest_repeat(str(seq_record.seq), i)
				if outvalue > repeatd[k]:
					flagcount+=1
					flaglist.append( "{}:{}-{}".format(checkmode,k,outvalue) )
					flagrepeats[k]=flagrepeats.get(k,0)+1
					failone=True

		# start AA composition checking
		if args.composition:
			checkmode="Com"
			for k,i in sorted(compd.items()):
				outvalue = seq_record.seq.count(k)/float(seqlength)*100.0
				if outvalue > i+1:
					flagcount+=1
					flaglist.append( "{}:{}-{:.2f}".format(checkmode,k,outvalue) )
					flagcomp[k]=flagcomp.get(k,0)+1
					failone=True

		# if either test fails, flag the sequence
		if failone:
			failcount+=1
			flagoutstring = " ".join(flaglist)
			# this checks only once per protein if any double repeat was flagged
			if any(r in flagoutstring for r in ["CV","IY","RE","RA","SL","TH"]):
				doublerepeats += 1
			# in all cases write the seq number and all flags
			# and either write just the fasta header or the full sequence
			if args.sequences: # write flag info and then sequence underneath
				wayout.write( "#{} {}\n".format(seqcount, flagoutstring) )
				wayout.write(seq_record.format("fasta"))
			else: # table format
				wayout.write("{}\t{}\t{}\t{}\n".format(seqcount, seq_record.id, seqlength, flagoutstring))

	sys.stderr.write( "# Counted {} sequences:  {}\n".format(seqcount, time.asctime() ) )
	sys.stderr.write( "# Detected unusual content in {} ({:.2f}%) sequences:  {}\n".format(failcount, failcount*100.0/seqcount, time.asctime() ) )

	if args.composition:
		sys.stderr.write( "# Composition flags called:\n" )
		for k,i in sorted(flagcomp.items()):
			sys.stderr.write( "{0}\t{1}\n".format(k,i) )
		sys.stderr.write( "# Total including multiple for same sequence: {}\n".format( sum(flagcomp.values())) )

	if args.repeats:
		sys.stderr.write( "# Repeat flags called:\n" )
		doublecounts = 0
		# lambda function is needed to put double repeats last
		for k,i in sorted(flagrepeats.items(), key=lambda x: len(x[0])):
			sys.stderr.write( "{0}\t{1}\n".format(k,i) )
			if len(k) == 2:
				doublecounts += i
		sys.stderr.write( "# Total including multiple for same sequence: {}\n".format( sum(flagrepeats.values())) )
		sys.stderr.write( "# Total double repeats: {} (for {} proteins)\n".format(doublecounts, doublerepeats) )

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
