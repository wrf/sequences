#!/usr/bin/env python
#
# v2.2 added codon splitting 2014-12-17
# v2.1 fixed with bug in trimtocds 2014-11-25
# v2 functionalized and imports trimtocds 2014-04-17
# regapper.py v1 add gaps to nucleotide alignment from protein alignment

'''regapper.py v2.3  last updated 2022-03-10
add gaps to nucleotides based on some protein alignments

regapper.py -n master_database.fasta protein_align*.fa

  output files are renamed automatically as:
    protein_align_001.fa.nucalign
    protein_align_002.fa.nucalign
    etc...

  -n should be the database containing the all of nucleotide sequences
  for all of the alignments
  individual nucleotide sequences for each alignment do not need to be
  collected for separate calls to regapper.py

  use -d to split at end of ID if proteins contain extra appended info
  if nucleotide ID is >seq12345
  and protein ID is >seq12345_0
  use -d _

requires trimtocds.py
'''

import sys
import argparse
import os
import re
import time
from trimtocds import get_six_frame, get_basesequence, get_matching_frames, trim_nucleotides
from Bio import SeqIO
from Bio.Seq import Seq

def make_gapped_nucl(regex,baseseq,protstring):
	# regular expression is converted to list to allow for reverse indexing
	reresults = list(regex.finditer(protstring))
	# sequence is converted to list to allow list insert
	seqtolist = list(baseseq)
	offset = 0
	for reseq in reresults:
		# starting from the end of the list, by reverse=True
		# order must be reversed, otherwise positions would be disrupted by adding an additional list item
		# for each hit, insert the red and off escape per the positions from the regs
		# insert at position defined from regex plus the wrap buffer
		respan = (reseq.span()[1]-reseq.span()[0])*3
		seqtolist.insert((reseq.span()[0]*3)+offset, "-"*respan)
		# with each successive gap, extend the offset, plus one for each gap string added
		offset = offset - respan + 1
	# rejoin the list as a string
	rejoinedseq = ''.join(seqtolist)
	# returns string of gapped nucleotide sequence
	return rejoinedseq

def pop_positions(codonpos, targetstring):
	# this makes a list of booleans, eg. [True, True, False] multiplied by the number of codons
	codonposmap = [str(int(x)+1) in codonpos for x in "012"] * (len(targetstring)/3)
	# the true-false list is mapped to targetstring, keeping the letter if true
	newstring = ''.join(map(lambda k,v: v if k else '', codonposmap, targetstring))
	# the short string is returned
	return newstring

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('protein_file', nargs="*", default = '-', help="fasta format protein alignments")
	parser.add_argument('-c','--code', type=int, default=1, help="use alternate genetic code")
	parser.add_argument('-d','--delimiter', help="optional delimiter for protein IDs (uses rsplit)")
	parser.add_argument('-n','--nucleotides', help='nucleotide database to index and gap')
	parser.add_argument('-o','--output-name', default=".nucalign", help="output extension for automatic renaming [.nucalign]")
	parser.add_argument('-s','--subcodons', help='specify codon positions to retain [123]')
	args = parser.parse_args(argv)

	starttime= time.time()
	print( "# Importing dictionary from {}  {}".format(args.nucleotides, time.asctime() ), file=sys.stderr )
	nucldict = SeqIO.to_dict(SeqIO.parse(args.nucleotides,'fasta'))

	if args.subcodons:
		print( "# Extracting codon positions: {}  {}".format( args.subcodons, time.asctime() ), file=sys.stderr )

	dashre = re.compile("-+")
	filecounter = 0

	for prot_file in args.protein_file:
		filecounter += 1

		protcount = 0 # proteins with nucl match
		trimcount = 0 # final trimmed
		protfilename = os.path.basename(prot_file)
		print( "Reading file {}".format(protfilename), file=sys.stderr )
		outfilename = "{}{}".format(protfilename, args.output_name)
		print( "Writing to {}".format(outfilename), file=sys.stderr )
		with open(outfilename,'w') as outfile:
			for protseq in SeqIO.parse(prot_file,'fasta'):
				if args.delimiter:
					protid = protseq.id.rsplit(args.delimiter,1)[0]
				else:
					protid = protseq.id
				matchingnucl = nucldict[protid]
				degappedprot = str(protseq.seq).replace("-","")
				# get the frames that match the protein from the 6-frame-translated nucleotide
				try:
					protcount += 1
					keepframes = get_matching_frames(matchingnucl, protseq.id, degappedprot, args.code)
				except KeyError:
					print( "WARNING: CANNOT FIND NUCL MATCH FOR PROTEIN {}    CHECK -d".format(protid) , file=sys.stderr )
					keepframes = []
				if len(keepframes)==1:
					trimcount += 1
					# trim to the coding sequence
					untrimmedseq, useframe = get_basesequence(nucldict[protid].seq, keepframes[0][1])
					basecds = trim_nucleotides(untrimmedseq, degappedprot, keepframes[0][0], useframe)
					# add gaps based on the protein alignment
					outseq = make_gapped_nucl(dashre, basecds, str(protseq.seq))
					if args.subcodons:
						outseq = pop_positions(args.subcodons, outseq)
					matchingnucl.seq = Seq(outseq)
					# print to file
					outfile.write(matchingnucl.format('fasta'))
				elif len(keepframes) > 1:
					print( "WARNING: {} ORFS IN: {}".format( len(keepframes), protseq.id ), file=sys.stderr )
				else:
					print( "WARNING: NO PROTS FOUND: {}  CHECK -c\n{}".format( matchingnucl.id, degappedprot ), file=sys.stderr )
	print( "# Processed {} files in {:.1f} minutes".format( filecounter, (time.time()-starttime)/60 ), file=sys.stderr )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
