#!/usr/bin/env python
#
# getgtfscaffolds.py  v1.0 2020-02-27

'''getgtfscaffolds.py last modified 2020-02-27

    to print only entries where the scaffold name is in the list -s

getgtfscaffolds.py -g some_genes.gtf -s scaffold_list > filtered_genes.gtf

    GTF file can be .gz

getgtfscaffolds.py -g some_genes.gtf.gz -s scaffold_list | gzip > filtered_genes.gtf.gz
'''

import sys
import gzip
import argparse

def make_keep_dict(keepfile, delimiter=' '):
	'''take file name of list of scaffolds to keep, return a dict where key is scaffold and value is True'''
	keep_dict = {}
	linecounter = 0
	sys.stderr.write("# Reading list from {}\n".format(keepfile) )
	for line in open(keepfile,'r'):
		line = line.strip()
		if line: # should remove blank or empty lines
			linecounter += 1
			short_id = line.split(delimiter)[0]
			keep_dict[line] = True
	sys.stderr.write("# Read {} lines for {} entries\n".format(linecounter, len(keep_dict) ) )
	return keep_dict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-g', '--gtf-file', help="gtf format file, or any other tab delimited format where scaffold is the first column, can be .gz")
	parser.add_argument('-d', '--delimiter', default=' ', help="split scaffold names at the first space")
	parser.add_argument('-s', '--scaffolds', help="list of scaffolds to keep, one per line")
	args = parser.parse_args(argv)

	keep_dict = make_keep_dict(args.scaffolds)

	commentlines = 0
	linecounter = 0
	writecount = 0

	if args.gtf_file.rsplit('.',1)[-1]=="gz": # autodetect gzip format
		opentype = gzip.open
		sys.stderr.write("# Reading features from {} as gzipped\n".format(args.gtf_file) )
	else: # otherwise assume normal open for fasta format
		opentype = open
		sys.stderr.write("# Reading features from {}\n".format(args.gtf_file) )

	for line in opentype(args.gtf_file):
		line = line.strip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				linecounter += 1
				lsplits = line.split("\t")
				scaffold = lsplits[0]
				if keep_dict.get(scaffold, False):
					writecount += 1
					wayout.write("{}\n".format( line ) )
	sys.stderr.write("# Counted {} lines, {} comments\n".format(linecounter, commentlines) )
	sys.stderr.write("# Wrote {} lines\n".format(writecount) )


if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
