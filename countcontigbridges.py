#!/usr/bin/env python

'''
v0.1 2015-05-20
  count mapping of reads to find cases where contigs are bridged by paired reads
  unusual cases of trios occur, possibly due to incorrectly merged haplotypes

samtools view hits.bam | countcontigbridges.py -
'''

import sys
import argparse
import time
from collections import defaultdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('rU'), default = '-', help="sam or bam file")
	parser.add_argument('-c', '--cutoff', type=int, default=2, help="ignore bridges with fewer than N reads [2]")
	args = parser.parse_args(argv)

	bridgedict = defaultdict(dict)
	print >> sys.stderr, "Parsing input", time.asctime()
	for line in args.input_file:
		if line:
			lsplits = line.split("\t")
			if lsplits[6]=="*": # second read did not match
				continue
			elif lsplits[6]=="=": # second read hit the same contig
				continue
			else:
				if int(lsplits[1])<256:
					bridgedict[lsplits[2]][lsplits[6]] = bridgedict[lsplits[2]].get(lsplits[6],0) + 1
	print >> sys.stderr, "Finished parsing", time.asctime()
	bridgecounts = []
	for left in bridgedict.keys():
		for right in bridgedict[left].keys():
			bridgecounter = bridgedict[left][right]
			if bridgecounter >= args.cutoff:
				bridgecounts.append((left, right, bridgecounter))
	print >> sys.stderr, "Writing contigs with more than {} reads".format(args.cutoff), time.asctime()
	for x in sorted(bridgecounts, reverse=True, key=lambda x: x[2]):
		print >> sys.stdout, x[0], x[1], x[2]
	print >> sys.stderr, "Finished writing", time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
