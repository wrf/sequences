#! /usr/bin/env python

'''
appendblasthits.py v2.0 2015-06-11
read in blast table and append data from best hit to fasta names

appendblasthits.py blasttable.txt seqs.fasta > seqs.annotated.fasta

appendblasthits.py -b rnaseq.blastx.swissprot.tab -i targets.txt -s ', ,0,",1' -d ' ' -B ',.,0-2'
'''

import sys
import argparse
import re
from collections import defaultdict,namedtuple
from Bio import SeqIO

def parse_swissprot_header(hitstring):
	# hitstring can conveniently be taken from seq_record.description
	# for example
	# swissprotdict.next().description
	# 'sp|Q6GZX4|001R_FRG3G Putative transcription factor 001R OS=Frog virus 3 (isolate Goorha) GN=FV3-001R PE=4 SV=1'
	#
	# from the swissprot website http://www.uniprot.org/help/fasta-headers
	# fasta headers appear as:
	# >db|UniqueIdentifier|EntryName ProteinName OS=OrganismName[ GN=GeneName]PE=ProteinExistence SV=SequenceVersion
	#
	# Where:
	#
    # db is 'sp' for UniProtKB/Swiss-Prot and 'tr' for UniProtKB/TrEMBL.
    # UniqueIdentifier is the primary accession number of the UniProtKB entry.
    # EntryName is the entry name of the UniProtKB entry.
    # ProteinName is the recommended name of the UniProtKB entry as annotated in the RecName field. For UniProtKB/TrEMBL entries without a RecName field, the SubName field is used. In case of multiple SubNames, the first one is used. The 'precursor' attribute is excluded, 'Fragment' is included with the name if applicable.
    # OrganismName is the scientific name of the organism of the UniProtKB entry.
    # GeneName is the first gene name of the UniProtKB entry. If there is no gene name, OrderedLocusName or ORFname, the GN field is not listed.
    # ProteinExistence is the numerical value describing the evidence for the existence of the protein.
    # SequenceVersion is the version number of the sequence.
	#
	# Examples:
	#
	# >sp|Q8I6R7|ACN2_ACAGO Acanthoscurrin-2 (Fragment) OS=Acanthoscurria gomesiana GN=acantho2 PE=1 SV=1
	# or
	# sp|Q9GZU7|CTDS1_HUMAN Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1 OS=Homo sapiens GN=CT DSP1 PE=1 SV=1

	# assuming the format of "OS=Homo sapiens"
	organismre = "OS=(\w+) (\w+)"
	# extract genus and species as groups()
	try:
		osgroups = re.search(organismre,hitstring).groups()
	# in case of Nonetype for some reason, possibly weird names of bacteria or viruses
	# such as:
	# sp|P07572|POL_MPMV Pol polyprotein OS=Mason-Pfizer monkey virus GN=pol PE=3 SV=1
	except AttributeError:
		osgroups = re.search("OS=([\w-]+) (\w+)",hitstring).groups()
	# print in format of H.sapiens
	osname = "{}.{}".format(osgroups[0][0], osgroups[1])

	genedescRE = "(.+) OS="
	# split will take everything after the first space, so:
	# "Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1 OS=Homo sapiens GN=CT DSP1 PE=1 SV=1"
	nospnumber = hitstring.split(' ',1)[1]
	# this should pull everything before the species excluding " OS=", so:
	# "Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1"
	genedesc = re.search(genedescRE, nospnumber).groups()[0]
	# this should specifically remove the annotation "(Fragment)" for some proteins
	genedesc = genedesc.replace("(Fragment)","")
	# convert all remaining spaces to underscores, so:
	# Carboxy-terminal_domain_RNA_polymerase_II_polypeptide_A_small_phosphatase_1
	gdwithdashes = genedesc.replace(" ","_")
	# join finally with the species, so:
	# H.sapiens_Carboxy-terminal_domain_RNA_polymerase_II_polypeptide_A_small_phosphatase_1
	swissstring = "{}_{}".format(osname, gdwithdashes)
	return swissstring

def build_hitdict(blastfile, isplustable, evaluecutoff, blastsplit):
	linecounts = 0
	evaluecuts = 0
	blasthit_d = {}
	for line in open(blastfile,'r'):
		line = line.rstrip()
		linecounts += 1
		lsplits = line.split("\t")
		if isplustable:
			if lsplits[0]=="Resulttitle": # skip header line if present
				continue
		# rt, ev, bt, na, rq, ac, ms, fr, tl, ip, al, ql
			queryid = lsplits[4]
			subject = lsplits[0]
			evalue = lsplits[1]
			bitscore = lsplits[2]
			subjcov = lsplits[8]
			pid = lsplits[9]
		else:
		# qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore
			queryid = lsplits[0]
			subject = lsplits[1]
			evalue = lsplits[10]
			bitscore = lsplits[11].strip()
			subjcov = lsplits[3]
			pid = lsplits[2]
		# check evalue against static cutoff
			if float(evalue) > evaluecutoff:
				evaluecuts += 1
				continue
		# clean up 
		if blastsplit:
			queryid = line_to_seqid(queryid, blastsplit)
		# check if query is already in dictionary
		if not queryid in blasthit_d:
			blasthit_d[queryid] = [subject, evalue, bitscore, subjcov, pid]
	print >> sys.stderr, "Counted {} lines, removed {} for evalue".format(linecounts, evaluecuts)
	return blasthit_d

def get_hit_info(seq_header, blast_dict, blast_headers, delimiter, isswissprot):
	hit_info = blast_dict.get(seq_header,None)
	if hit_info==None:
		return ["None"] * len(blast_headers)
	else:
		if isswissprot:
			hit_info[0] = parse_swissprot_header(hit_info[0])
		return hit_info

def line_to_seqid(line, sepscheme):
	if sepscheme:
		# sepscheme is a short string such as ', ,0,:,1'
		# where ',' is the divider, ' ,0' is a split index, same with ':,1'
		# if a third value is present, separated by '-' then take the range
		# such as "|,1-3"
		sdiv = sepscheme[0]
		part = line
		# split sepscheme into pairs of separators and indeces, then iterate over the pairs
		for s,i in zip(*[iter(sepscheme[1:].split(sdiv))] * 2):
			# in case i contains two values separate by hyphen, assume those are a range
			ips = i.split("-")
			if len(ips)==2:
				# split at s, take the range, then rejoin by s
				part = s.join(part.split(s)[int(ips[0]):int(ips[1])])
			else:
				part = part.split(s)[int(i)]
		return part
	else:
		return line

def main(argv):
	if not argv:
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--blastfile', help="blast results, table, output format 6 or 7", required=True)
	parser.add_argument('-d',"--delimiter", help="output delimiter for hits ['| ']", default="| ")
	parser.add_argument('-D','--db', help="database from blasting in fasta format")
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff for taking results [1e-4]", default=1e-4)
	parser.add_argument('-i','--inputfile', help="input file, such as Trinity.fasta", required=True)
	parser.add_argument('-H','--no-header', action="store_false", help="omit header line in output table")
	parser.add_argument('-f','--fasta', action="store_true", help="target file is a fasta sequence file")
	parser.add_argument('-P','--plustable', action="store_true", help="blast results are blastplustable, not tabular blast")
	parser.add_argument('-s',"--ischeme", help="scheme for parsing input sequences, such as ', 0,:1'")
	parser.add_argument('-B',"--bscheme", help="scheme for parsing blast sequences, such as [',.,0']")
	parser.add_argument('-S','--swissprot', action="store_true", help="sequence hits are in SwissProt format")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	# make dictionary of blast hits
	print >> sys.stderr, "Reading blast hits from %s" % args.blastfile
	blast_dict = build_hitdict(args.blastfile, args.plustable, args.evalue, args.bscheme)
	print >> sys.stderr, "Counted {} sequences with blast hits".format(len(blast_dict) )

	#if args.renamed_fasta:
	#	print >> sys.stderr, "Reading name prefixes from %s" % args.renamed_fasta
		# renamed fasta dictionary for holding the prefixes
	#	rfd = make_renamed_fasta_dict(args.renamed_fasta, args.rsplit)
	### TODO merge these two, name lookup from renamed fasta, and name lookup from db
	if args.db:
		print >> sys.stderr, "Reading reference sequences from %s" % args.db
		reference_db = SeqIO.to_dict(SeqIO.parse(args.db,'fasta'))
		print >> sys.stderr, "Counted {} reference sequences".format(len(reference_db) )

	seqcounter = 0
	annocounter = 0
	blast_headers = ["Subject", "Evalue", "Bitscore", "SubjCov", "Pid"]
	#blastInfo = namedtuple("blast",blast_headers)

	# parse fasta file and write out hit if present
	if args.fasta:
		print >> sys.stderr, "Reannotating sequences from {}".format(args.inputfile)
		for line in open(args.inputfile,'r'):
			if line[0] == ">":
				seqcounter += 1
				line = line.rstrip()
				# remove the leading '>' and then search
				seq_header = line[1:]
				seqid = line_to_seqid(seq_header, args.ischeme)
				hit_info = get_hit_info(seqid, blast_dict, blast_headers, args.delimiter, args.swissprot)
				outputline = "{}{}{}".format(line, args.delimiter, hit_info)
				print >> sys.stdout, outputline
			else:
				# write all sequence lines back as they were
				sys.stdout.write(line)
	else:
		for line in open(args.inputfile,'r'):
			line = line.rstrip()
			seqcounter += 1
			if seqcounter==1: # skip header line
				if args.no_header:
					outputline = "{}{}{}".format(line, args.delimiter, args.delimiter.join(blast_headers) )
				else:
					continue
			else:
				seqid = line_to_seqid(line, args.ischeme)
				hit_info = get_hit_info(seqid, blast_dict, blast_headers, args.delimiter, args.swissprot)
				outputline = "{}{}{}".format(line, args.delimiter, args.delimiter.join(hit_info) )
			print >> sys.stdout, outputline

	print >> sys.stderr, "Counted %d sequences" % seqcounter
	print >> sys.stderr, "Reannotated %d sequences" % annocounter

if __name__ == "__main__":
	main(sys.argv[1:])
