#!/usr/bin/env python
#
# artificialreads.py v1.0 created 2015-11-24

'''
artificialreads.py v1.0 2015-12-10
    Generates artificial mate pair reads based on long reads
    minimum -m is the length of all reads, which are then offset by -t bases
    Thus a long read of 6000bp will contain read pairs spanning from
    bases 0 to 4000 through to bases 2000 to 6000, with uniform insert size

      |-l    -m    -l|-t|
      <--F        R--> for mate-pair reads with length -l and insert size -m
      <--  4000bp  -->
        <--  4000bp  -->  additionaly pairs are made offset by -t bases
          <--  4000bp  -->
            <--  4000bp  -->
              <--  4000bp  -->
      |         -M           |
    5'|--------6000bp--------|3'  across the entire length of the long read

    EXAMPLE USAGE:
artificialreads.py -i moleculo_reads_no_N.fastq -o all_moleculo_reads_fa_8000 -m 8000 -M 20000 -t 10

    output is same format as input, and can be optionally gzipped with -g
'''

# PE vs MP directions from Platanus
#    Inward-pair data (often called "Paired-end", accepted in options "-ip" or "-IP"):
#        FWD --->
#         5' -------------------- 3'
#         3' -------------------- 5'
#                            <--- REV
#    Outward-pair data (often called "Mate-pair", accepted in options "-op" or "-OP"):
#                            ---> REV
#         5' -------------------- 3'
#         3' -------------------- 5'
#        FWD <---

import sys
import argparse
import time
import gzip
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

def fail_n_fraction(sequence, seqlength, nlimit):
	'''returns boolean, where True if fraction of N in sequence is greater than nlimit'''
	ncount = sequence.count("N")
	toomanyNs = ncount*1.0/seqlength > nlimit
	return toomanyNs

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="single file of long reads")
	parser.add_argument('-g','--gzip', action="store_true", help="gzip output files")
	parser.add_argument('-o','--output', help="optional output name, otherwise uses input name as base")
	parser.add_argument('-f','--format', default="fastq", help="read format, default: fastq")
	parser.add_argument('-l','--length', default=150, type=int, help="length of artificial reads [150]")
	parser.add_argument('-m','--minimum', default=4000, type=int, help="mate insert size, and minimum length for long read [4000]")
	parser.add_argument('-M','--maximum', default=8000, type=int, help="maximum length for long read [8000]")
	parser.add_argument('-n','--nlimit', default=0.1, type=float, help="max fraction of N in either read [0.1]")
	parser.add_argument('-p','--paired-end', action="store_true", help="reads point inward to generate normal paired-end reads")
	parser.add_argument('-t','--tiling', default=5, type=int, help="make artificial reads every N bases [5]")
	args = parser.parse_args(argv)

	outputbase = args.output if args.output else args.input
	readtypetag = "pair" if args.paired_end else "mate"
	if args.gzip:
		print >> sys.stderr, "# Generating gzipped outputs", time.asctime()
		outputfile1 = "{}_{}1.{}.gz".format(outputbase, readtypetag, args.format)
		outputfile2 = "{}_{}2.{}.gz".format(outputbase, readtypetag, args.format)
		opentype = gzip.open
	else:
		outputfile1 = "{}_{}1.{}".format(outputbase, readtypetag, args.format)
		outputfile2 = "{}_{}2.{}".format(outputbase, readtypetag, args.format)
		opentype = open

	totalreads = 0
	lengthkept = 0
	writtenreads = 0
	nremovals = 0
	with opentype(outputfile1, 'w') as of1, opentype(outputfile2, 'w') as of2:
		print >> sys.stderr, "# Parsing {}".format(args.input), time.asctime()
		for n,seqrec in enumerate(SeqIO.parse(args.input, args.format)):
			totalreads += 1
			if n and not(n%1000):
				sys.stderr.write(".")
			if n and not(n%10000):
				print >> sys.stderr, n, time.asctime()
			seqlength = len(seqrec.seq)
			if args.minimum <= seqlength <= args.maximum:
				lengthkept += 1
				for i in xrange(0, seqlength-args.minimum+1, args.tiling):
					writtenreads += 1
					# if seqlength is 6000, xrange is 0, 6000-4000+1, 10
					# when i is 100, m is 4000, l is 150
					rvs = args.minimum+i-args.length # 4000+100-150 = 3950
					rve = args.minimum+i # 4000+100 = 4100
					fws = i # 100
					fwe = args.length+i # 150+100 = 250
					if args.paired_end:
						forwardseq = seqrec.seq[fws:fwe] # returns a Seq
						forwardid = "{}:{}".format(seqrec.id, i)
						forward_read = SeqRecord(forwardseq, id=forwardid, description="1:N:0:")
						reverseseq = seqrec.seq[rvs:rve].reverse_complement() # returns a Seq
						reverseid = "{}:{}".format(seqrec.id, i)
						reverse_read = SeqRecord(reverseseq, id=reverseid, description="2:N:0:")
						if fail_n_fraction(forwardseq, args.length, args.nlimit) or fail_n_fraction(reverseseq, args.length, args.nlimit):
							nremovals += 1
							continue # too many N's in one of the two reads
						if args.format=="fastq":
							forwardqual = seqrec.letter_annotations.get("phred_quality")[fws:fwe] # returns a list
							forward_read.letter_annotations["phred_quality"] = forwardqual
							reversequal = list(reversed(seqrec.letter_annotations.get("phred_quality")[rvs:rve]))
							reverse_read.letter_annotations["phred_quality"] = reversequal
						of1.write(forward_read.format(args.format))	
						of2.write(reverse_read.format(args.format))
					else: # only difference is forward read is reverse complemented and reverse read is 5-3
						reverseseq = seqrec.seq[rvs:rve] # returns a Seq
						reverseid = "{}:{}".format(seqrec.id, i)
						reverse_read = SeqRecord(reverseseq, id=reverseid, description="1:N:0:")
						forwardseq = seqrec.seq[fws:fwe].reverse_complement() # returns a Seq
						forwardid = "{}:{}".format(seqrec.id, i)
						forward_read = SeqRecord(forwardseq, id=forwardid, description="2:N:0:")
						if fail_n_fraction(forwardseq, args.length, args.nlimit) or fail_n_fraction(reverseseq, args.length, args.nlimit):
							nremovals += 1
							continue # too many N's in one of the two reads
						if args.format=="fastq":
							reversequal = seqrec.letter_annotations.get("phred_quality")[rvs:rve] # returns a list
							reverse_read.letter_annotations["phred_quality"] = reversequal
							forwardqual = list(reversed(seqrec.letter_annotations.get("phred_quality")[fws:fwe]))
							forward_read.letter_annotations["phred_quality"] = forwardqual
						of1.write(reverse_read.format(args.format))	
						of2.write(forward_read.format(args.format))
	print >> sys.stderr, "# Counted {} total long reads".format(totalreads), time.asctime()
	print >> sys.stderr, "# Counted {} within size range".format(lengthkept), time.asctime()
	print >> sys.stderr, "# Removed {} pairs due to N".format(nremovals), time.asctime()
	print >> sys.stderr, "# {} reads written to {}".format(writtenreads, outputfile1), time.asctime()
	print >> sys.stderr, "# {} reads written to {}".format(writtenreads, outputfile2), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
