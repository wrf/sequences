#! /usr/bin/env python
#
# created 2015-10-07

'''pairblast2besthits.py  last modified 2016-05-27

pairblast2besthits.py -q a_vs_b.blastx.tab -d b_vs_a.tblastn.tab > best_hits.tab

    output is a table of:
A-name  A-to-B-Identity  Length  Evalue  B-name  B-to-A-Identity  Length  Evalue
'''

import sys
import argparse
import time
from collections import namedtuple
from Bio import SeqIO

blastdat = namedtuple("blastdat", "sseqid pident alignlen evalue seqlen")

def make_seq_length_dict(sequencefile):
	print >> sys.stderr, "# Parsing target sequences from {}".format(sequencefile), time.asctime()
	lengthdict = {}
	for seqrec in SeqIO.parse(sequencefile,'fasta'):
		lengthdict[seqrec.id] = len(seqrec.seq)
	print >> sys.stderr, "# Found {} sequences".format(len(lengthdict)), time.asctime()
	return lengthdict

def parse_tabular_blast(blasttabfile, evaluecutoff, querydelimiter, dbdelimiter, querylengthdict):
	'''return dict where key is query seq ID and value is named tuple of sseqid, pident, alignlen, and evalue'''
	print >> sys.stderr, "# Parsing tabular blast output {}".format(blasttabfile), time.asctime()
	BlastbyTransdict = {}
	hitcount = 0
	evalueRemovals = 0
	for line in open(blasttabfile, 'r').readlines():
		hitcount += 1
		line = line.strip()
		lsplits = line.split("\t")
		# qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore
		queryseq = lsplits[0]
		querygene = queryseq.rsplit(querydelimiter,1)[0]
		ev = float(lsplits[10])
		# if no key, then return dummy blastdat with high evalue
		if ev < BlastbyTransdict.get(querygene, blastdat("","","",1.0,0) ).evalue:
			if ev > evaluecutoff:
				evalueRemovals += 1
				continue
			querylen = querylengthdict.get(queryseq, 1)
			subjectid = lsplits[1].rsplit(dbdelimiter,1)[0]
			# make tuple for output
			blastvalue = blastdat(sseqid=subjectid, pident=lsplits[2], alignlen=int(lsplits[3]), evalue=ev, seqlen=querylen)
			BlastbyTransdict[querygene] = blastvalue
	print >> sys.stderr, "# Found {} blast hits for {} sequences".format(hitcount, len(BlastbyTransdict)), time.asctime()
	print >> sys.stderr, "# Removed {} hits by evalue".format(evalueRemovals), time.asctime()
	return BlastbyTransdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-A','--query-fasta', help="query fasta file")
	parser.add_argument('-B','--db-fasta', help="database fasta file")
	parser.add_argument('-q','--query', help="tabular blast results of query vs DB from blastx", required=True)
	parser.add_argument('-d','--db', help="tabular blast results of DB vs query from tblastn", required=True)
	parser.add_argument('-Q','--query-delimiter', help="gene transcript separator for query [.]", default='.')
	parser.add_argument('-D','--db-delimiter', help="gene transcript separator for db [.]", default='.')
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff blast search [1e-5]", default=1e-5)
	parser.add_argument('-v','--verbose', action='store_true', help="verbose output")
	args = parser.parse_args(argv)

	querylendict = make_seq_length_dict(args.query_fasta) if args.query_fasta else {}
	dblendict = make_seq_length_dict(args.db_fasta) if args.db_fasta else {}

	# must change order of delimiters to parse hits correctly
	queryblasthits = parse_tabular_blast(args.query, args.evalue, args.query_delimiter, args.db_delimiter, querylendict)
	dbblasthits = parse_tabular_blast(args.db, args.evalue, args.db_delimiter, args.query_delimiter, dblendict)

	totalalign = 0
	totalidents = 0
	matchcounter = 0
	bestrecipmatches = 0
	for q, bd in queryblasthits.iteritems():
		matchcounter += 1
		# if query id is the top hit for sseqid
		reciphitdata = dbblasthits.get(bd.sseqid, blastdat(None, "","",1.0,0) )
		if q == reciphitdata.sseqid:
			bestrecipmatches += 1
			#totalalign += max([bd.alignlen, reciphitdata.alignlen])
			totalalign += bd.alignlen
			totalidents += int( (float(bd.pident)*bd.alignlen+1)/100 )
			outstring = "{}\t{}\t{}\t{:.2e}\t{}-{:.1f}\t{}\t{}\t{}\t{:.2e}\t{}".format(q, bd.pident, bd.alignlen, bd.evalue, bd.seqlen, bd.seqlen/3.0, bd.sseqid, reciphitdata.pident, reciphitdata.alignlen, reciphitdata.evalue, reciphitdata.seqlen)
			print >> wayout, outstring
	print >> sys.stderr, "# Counted {} total matches with {} best matches".format(matchcounter, bestrecipmatches), time.asctime()
	print >> sys.stderr, "# Counted {} total aligned amino acids with {} identites".format(totalalign, totalidents), time.asctime()
	print >> sys.stderr, "# Overall percent identity is {:.4f}".format(totalidents*1.0/totalalign), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
