#!/usr/bin/env python
#
# parse_swissprot_data.py  created 2017-10-06
# python3 update 2022-04-28
# add option to print each sequence 2022-12-27

'''count_alphabet.py  last modified 2022-12-27
    count all letters (nucl or prot) across all sequences in a fasta file

count_alphabet.py -i human_uniprot.fasta

    results appear like:
file	A	C	D	E	F	G	H	I	K	L	M	N	P	Q	R	S	TV	W	Y	X	-
human_uniprot.fasta	795785	260431	537217	804990	413875	744938	297731	491900	649158	1129056	241703	406694	715477	540274	639594	943272	607357	676698	138206	302068	1	0

'''

import sys
import argparse
import time
import gzip
from collections import Counter
from Bio import SeqIO

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument("-i","--input", nargs="*", help="nucl or prot sequences in fasta format, can be gzipped")
	parser.add_argument('-n','--nucleotides', action="store_true", help="sequences are nucleotides")
	parser.add_argument('-p','--preset', action="store_true", help="do not use preset alphabet, use only letters given in raw data")
	parser.add_argument('-s','--by-sequence', action="store_true", help="print output for each sequence")
	parser.add_argument("-t","--taxa", nargs="*", help="provide taxa names instead of filenames")
	args = parser.parse_args(argv)

	LETTERSET = list("ATCGNX-") if args.nucleotides else list("ACDEFGHIKLMNPQRSTVWYX-")

	if args.taxa:
		if len(args.taxa)!=len(args.input):
			for tfile, tname in zip(args.input, args.taxa):
				sys.stderr.write( "{}\t{}\n".format(tfile, tname) )
			raise ValueError("ERROR: number of taxa names ({}) does not match number of files ({}), exiting".format(len(args.taxa),len(args.input) ) )
		else:
			sys.stderr.write( "# Using {} pairs of files and taxon names\n".format(len(args.taxa)) )
			for tfile, tname in zip(args.input, args.taxa):
				sys.stderr.write( "{}\t{}\n".format(tfile, tname) )
		headerline = "file\ttaxon\t{}\n".format( "\t".join(LETTERSET) )
	else:
		headerline = "file\t{}\n".format( "\t".join(LETTERSET) )

	if not args.preset: # preset uses other output, ignore header
		sys.stdout.write( headerline )

	seqcounter = 0
	for i, fastafile in enumerate(args.input):
		final_letter_counts = Counter()

		if fastafile.rsplit('.',1)[-1]=="gz": # autodetect gzip format
			opentype = gzip.open
			sys.stderr.write( "# Opening {} as gzipped  {}\n".format(fastafile, time.asctime() ) )
		else: # otherwise assume normal open for fasta format
			opentype = open
			sys.stderr.write( "# Reading sequences from {}  {}\n".format(fastafile, time.asctime() ) )

		for seqrec in SeqIO.parse(opentype(fastafile,'r'), "fasta"):
			seqcounter += 1
			seq_letter_counts = Counter( str(seqrec.seq) )
			if args.by_sequence:
				lettersbyseq = "\t".join( [ str(seq_letter_counts[l]) for l in LETTERSET] )
				sys.stdout.write( "{}\t{}\n".format( seqrec.id, lettersbyseq ) )
			else:
				final_letter_counts += seq_letter_counts

		if not args.by_sequence:
			if args.preset:
				for k,v in final_letter_counts.iteritems():
					sys.stdout.write( "{}\t{}\n".format(k, v) )
			else:
				lettersbyfile = "\t".join( [ str(final_letter_counts[l]) for l in LETTERSET] )
				if args.taxa:
					sys.stdout.write( "{}\t{}\t{}\n".format( fastafile, args.taxa[i], lettersbyfile ) )
				else:
					sys.stdout.write( "{}\t{}\n".format( fastafile, lettersbyfile ) )
	sys.stderr.write( "# Counted {} sequences  {}\n".format( seqcounter, time.asctime() ) )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
