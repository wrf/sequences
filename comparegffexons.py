#!/usr/bin/env python
# comparegffexons.py
# v1.0 2015-06-01

'''
comparegffexons.py last modified 2016-05-12
    script to compare gtf/gff3 format transcripts to tblastn hits
    searching for blast hits which do not correspond to RNAseq-predicted exons

    EXAMPLE USAGE:

comparegffexons.py -t pasa_alignments.gtf stringtie.gtf -b ref_genes_tblastn_fmt6.tab -v > no_rna_seq_tblastn.txt

    OR GIVEN GENE PREDICTIONS, USE -G:
comparegffexons.py -t pasa_alignments.gtf stringtie.gtf -G augustus.gff > augustus_overlap.gff

    TO RETAIN ONLY THOSE WITHOUT OVERLAPS:
grep "Overlap=None" augustus_overlap.gff > augustus_no_overlap_genes.gff
'''

import sys
import os
import argparse
import time
from collections import defaultdict
from itertools import groupby
from numpy import histogram,arange

def exon_nums_to_set(exondict):
	for k,v in exondict.iteritems():
		exondict[k] = set(v)

def numbers_to_ranges(numberset):
	# makes groups of value-positions, where values in a row end up as the same group
	for i,l in groupby(enumerate(sorted(numberset)), lambda (x,y): y-x):
		l = list(l)
		# returns generator of first and last positions in the group
		yield l[0][1], l[-1][1]+1 # since these are base positions, they need to be offset for gff

def ranges_to_count(rangeDict):
	'''returns total number of intervals in rangeDict'''
	return sum(len(v) for v in rangeDict.itervalues())

def reduce_intervals(rangedict):
	'''take exon intervals as dict of list of intervals, remove redundant bases, generate new ranges as dict of lists where keys are scaffolds and lists are tuple intervals'''
	reducedranges = {} # dictionary of lists, where lists contain tuples of exon boundaries
	for scaffold,rangelist in rangedict.iteritems():
		baseset = set() # convert intervals to ranges, then to sets
		for interval in rangelist:
			baseset.update(set(range(*interval)))
		# baselist should now be a long list of values
		# on each scaffold, convert set to list of ranges
		reducedranges[scaffold] = list(numbers_to_ranges(baseset)) # numbers to ranges yields a generator
	return reducedranges

def combine_intervals(rangelist):
	'''convert list of tuples to non redundant invervals'''
	# sl = [(1,10), (1,6), (15,20), (1,10), (19,29), (30,35), (6,13), (40,48), (15,21), (42,50)]
	nrintervallist = []
	srtrangelist = sorted(rangelist) # sort list now, to only do this once
	interval = srtrangelist[0] # need to start with first time, which will be the same for the first bounds
	for bounds in srtrangelist:
		# since it is sorted bounds[0] should always be >= interval[0]
		if bounds[0] > interval[1]+1: # if the next interval starts past the end of the first + 1
			nrintervallist.append(interval) # add to the nr list, and continue with the next
			interval = bounds
		else:
			if bounds[1] > interval[1]: # bounds[1] <= interval[1] means do not extend
				interval = (interval[0], bounds[1]) # otherwise extend the interval
	else: # append last interval
		nrintervallist.append(interval)
	# should return [(3, 13), (15, 35), (40, 50)]
	return nrintervallist

def make_exon_ranges(exondict):
	rangeDict = {}
	for k,v in exondict.iteritems():
		exonranges = list(numbers_to_ranges(v))
		rangeDict[k] = exonranges
	return rangeDict		

def gtf_to_ranges(gtflist, excludedict):
	'''takes a list of gtf transcript files, and returns the non redundant exon occupancy intervals as a pair of dictionaries where keys are scaffolds and values are list of tuples of intervals'''
	forwardranges = defaultdict(list)
	reverseranges = defaultdict(list)
	transcriptExonCount = 0
	for transcriptFile in gtflist:
		print >> sys.stderr, "# Starting exon parsing on %s" % (transcriptFile), time.asctime()
		for line in open(transcriptFile,'r'):
			line = line.strip()
			if line and not line[0]=="#":
				lsplits = line.split("\t")
				contig = lsplits[0]
				if excludedict and excludedict.get(contig, False): # skip excluded contigs
					continue
				if lsplits[2]=="exon":
					transcriptExonCount += 1
					strand = lsplits[6]
					exonbounds = ( int(lsplits[3]), int(lsplits[4]) )
					if strand=="+":
						forwardranges[contig].append(exonbounds)
					else: # strand=="-":
						reverseranges[contig].append(exonbounds)
	print >> sys.stderr, "# Counted %d exons" % (transcriptExonCount), time.asctime()
	print >> sys.stderr, "# Removing redundant exons", time.asctime()
	forward_sets = {}
	reverse_sets = {}
	for contig,exons in forwardranges.iteritems():
		forward_sets[contig] = combine_intervals(exons)
	print >> sys.stderr, "# Counted %d forward exons" % (ranges_to_count(forward_sets)), time.asctime()
	for contig,exons in reverseranges.iteritems():
		reverse_sets[contig] = combine_intervals(exons)
	print >> sys.stderr, "# Counted %d reverse exons" % (ranges_to_count(reverse_sets)), time.asctime()
	return forward_sets, reverse_sets

def get_gene_overlaps(genebounds, exonlist):
	'''checks if bounds are within or spanning any exon on a contig, and return list of overlaps'''
	overlaps = [] # create list of all overlaps
	for exon in exonlist: # each exon is a bound
		if genebounds[0] <= exon[0] and genebounds[1] >= exon[1]: # gene is bigger than exon
			overlaps.append(exon)
		elif genebounds[0] >= exon[0] and genebounds[0] < exon[1]: # gene overlaps with exon at Nterm
			overlaps.append(exon)
		elif genebounds[1] > exon[0] and genebounds[1] <= exon[1]: # gene overlaps with exon at Cterm
			overlaps.append(exon)
	return overlaps

def assign_reduced_loci(reducedlocidict, bounddict, scoredict, strand, wayout):
	for scaffold, loci in reducedlocidict.iteritems():
		for locus in loci:
			maxscore = 0
			bestname = ""
			for b, name in bounddict[scaffold].iteritems():
				if b[0] >= locus[0] and b[1] <= locus[1]:
					if scoredict[name] > maxscore:
						maxscore = scoredict[name]
						bestname = name
			### TODO merge duplicates by name before printing
			outline = "{}\tmerged\tgene\t{}\t{}\t{}\t{}\t.\t{}".format(scaffold, locus[0], locus[1], maxscore, strand, bestname )
			print >> wayout, outline

def flag_blast_hit(scaffold_ranges, sstart, send):
	svals = [sstart, (sstart+send)/4, (sstart+send)/2, (sstart+send)*3/4, send]
	for srange in scaffold_ranges:
		for val in svals:
			if (srange[0] <= val <= srange[1]):
	#	for x in xrange(sstart, send):
	#		if srange[0] <= x <= srange[1]:
				return 0
	else:
		return 1

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--blast', help="blast results file")
	parser.add_argument('-G','--gene-predictions', help="gff of gene predictions from genewise or Augustus")
	parser.add_argument('--search-transcripts', action="store_true", help="allow searching for transcripts as well as genes")
	parser.add_argument('-t','--transcripts', nargs='*', help="query used for the blast, in gtf or gff3 format")
	parser.add_argument('-e','--evalue', type=float, default=0.1, help="evalue cutoff for blast filtering [1e-1]")
	parser.add_argument('-d','--db', help="database file use for the blast")
	parser.add_argument('-E','--exclude', help="list of contigs to exclude")
	parser.add_argument('-l','--blast-length', type=int, default=100, help="minimum hit overlap in bases [100]")
	parser.add_argument('-n','--non-overlapping', help="only write non-overlapping loci", action="store_true")
	parser.add_argument('-v','--verbose', help="verbose output", action="store_true")
	args = parser.parse_args(argv)

	if args.exclude:
		print >> sys.stderr, "# Reading exclusion list {}".format(args.exclude), time.asctime()
		exclusiondict = {}
		for term in open(args.exclude,'r'):
			term = term.strip()
			if term[0] == ">":
				term = term[1:]
			exclusiondict[term] = True
		print >> sys.stderr, "# Found {} contigs to exclude".format(len(exclusiondict) ), time.asctime()
	else:
		exclusiondict = None

	# GET MERGED EXON INFORMATION
	forward_exons, reverse_exons = gtf_to_ranges(args.transcripts, exclusiondict)

	# IF COMPARING TO GENE PREDICTIONS
	if args.gene_predictions:
		linecounter = 0
		genecounter = 0
		yesoverlap, noneoverlap = 0,0
		scoredict = {} # keep track of scores by gene name
		fbounddict = defaultdict(dict) # first key is scaffold,
		rbounddict = defaultdict(dict) # then gene bounds as key, and value is attributes
		forwardnonoverlap = defaultdict(list) # dict of interval lists by scaffold
		reversenonoverlap = defaultdict(list) # dict of interval lists by scaffold
		print >> sys.stderr, "# Reading gene predictions {}".format(args.gene_predictions), time.asctime()
		for line in open(args.gene_predictions,'r').readlines():
			line = line.strip()
			if line and not line[0]=="#": # ignore empty and comment lines
				linecounter += 1
				lsplits = line.split("\t")
				contig = lsplits[0]
				if exclusiondict and exclusiondict.get(contig,False):
					continue
				if lsplits[2]=="gene" or (args.search_transcripts and lsplits[2]=="transcript"):
					genecounter += 1
					score = float(lsplits[5])
					strand = lsplits[6]
					genebounds = ( int(lsplits[3]), int(lsplits[4]) )
					attributes = lsplits[8]
					# for use with non overlapping, add to dicts
					scoredict[attributes] = score # instead of geneid, keys are attributes
					if strand=="+":
						withinexons = get_gene_overlaps(genebounds, forward_exons.get(contig, []) )
						fbounddict[contig][genebounds] = attributes
					elif strand=="-":
						withinexons = get_gene_overlaps(genebounds, reverse_exons.get(contig, []) )
						rbounddict[contig][genebounds] = attributes
					else:
						print >> sys.stderr, "Cannot identify strand {} from:\n{}".format(strand,line)
					if withinexons: # if any exons match, format output like "4544-5059,5121-5227"
						overlapstring = ",".join(["{}-{}".format(*x) for x in withinexons])
						yesoverlap += 1
					else: # otherwise write None
						overlapstring = "None"
						noneoverlap += 1
						if strand=="+":
							forwardnonoverlap[contig].append(genebounds)
						elif strand=="-":
							reversenonoverlap[contig].append(genebounds)
					if not args.non_overlapping: # for normal output
						newattributes = "{};Overlap={}".format(attributes, overlapstring)
						outline = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t.\t{}".format(contig, lsplits[1], lsplits[2], genebounds[0], genebounds[1], int(bool(withinexons)), strand, newattributes )
						print >> wayout, outline
		print >> sys.stderr, "# Found {} genes for {} lines".format(genecounter, linecounter), time.asctime()
		print >> sys.stderr, "# {} genes with overlaps and {} without".format(yesoverlap, noneoverlap), time.asctime()

		### TO EXTRACT NON REDUNDANT LOCI ###
		if args.non_overlapping:
			print >> sys.stderr, "# Merging predicted loci", time.asctime()
			reducedforwardloci = reduce_intervals(forwardnonoverlap)
			print >> sys.stderr, "# Counted %d forward loci" % (ranges_to_count(reducedforwardloci)), time.asctime()
			reducedreversedloci = reduce_intervals(reversenonoverlap)
			print >> sys.stderr, "# Counted %d reverse loci" % (ranges_to_count(reducedreversedloci)), time.asctime()
			assign_reduced_loci(reducedforwardloci, fbounddict, scoredict, "+", wayout)
			assign_reduced_loci(reducedreversedloci, rbounddict, scoredict, "-", wayout)


	# IF PROCESSING BLAST INFORMATION
	if args.blast: ### TODO clean this up
		forward_blast = defaultdict(list)
		reverse_blast = defaultdict(list)
		forward_wms = defaultdict( lambda: defaultdict(list) )
		reverse_wms = defaultdict( lambda: defaultdict(list) )
		forward_nomatch = defaultdict(list)
		reverse_nomatch = defaultdict(list)
		#protswithhits = {}
		contigswithhits = {}
		blastCounter = 0
		print >> sys.stderr, "Starting BLAST parsing on %s" % (args.blast), time.asctime()
		for line in open(args.blast,'r'):
			line = line.strip()
			blastCounter += 1
			lsplits = line.split("\t")
			qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = lsplits
		#	protswithhits[qseqid] = True
			if args.exclude and exclusiondict.get(sseqid,False): # skip contigs from excluder list
				continue
			contigswithhits[sseqid] = True
			if sstart < send:
				forward_blast[sseqid].extend(range(int(sstart), int(send)))
				forward_wms[sseqid][qseqid].extend((int(sstart), int(send)))
			else:
				reverse_blast[sseqid].extend(range(int(send), int(sstart)))
				reverse_wms[sseqid][qseqid].extend((int(sstart), int(send)))
		print >> sys.stderr, "Counted %d blast hits" % (blastCounter), time.asctime()
		print >> sys.stderr, "Sorting blast hits by scaffold", time.asctime()
		exon_nums_to_set(forward_blast)
		exon_nums_to_set(reverse_blast)
		print >> sys.stderr, "Removing bases with exon coverage", time.asctime()
		for ck in contigswithhits.iterkeys():
			# remove all numbers in blast that are in exons
			forward_nomatch[ck] = set(forward_blast[ck]).difference(set(forward_exons[ck]))
			reverse_nomatch[ck] = set(reverse_blast[ck]).difference(set(reverse_exons[ck]))
		print >> sys.stderr, "Reorganizing bases", time.asctime()
		# regenerate sets and make ranges
		exon_nums_to_set(forward_nomatch)
		forward_blastsets = make_exon_ranges(forward_nomatch)
		exon_nums_to_set(reverse_nomatch)
		reverse_blastsets = make_exon_ranges(reverse_nomatch)
		# final counts
		longnomatch = 0
		shortnomatch = 0
		setlengths = []
		longsum = 0
		print >> sys.stderr, "Sorting blast non-matches by length", time.asctime()
		for ck in sorted(contigswithhits.iterkeys()):
			if args.exclude and exclusiondict.get(ck,False): # skip contigs from excluder list
				continue
			strandsymbol = "+"
			for nm in sorted(forward_blastsets[ck], key=lambda x : x[0]):
				# check the range of nm is greater than cutoff
				setlength = nm[1]-nm[0]+1
				if setlength >= args.blast_length:
					longnomatch+=1
					longsum += setlength
					if args.verbose:
						# generate a list of proteins whose start position is within nm
						ob = []
						for k,v in forward_wms[ck].iteritems():
							#for ss in v:
							#	if if nm[0] <= ss <= nm[1]:
							#		ob.append(k)
							#		break
							ob.extend(set([k for ss in v if nm[0] <= ss <= nm[1]]))
						print >> sys.stdout, ck, strandsymbol, nm, ob
				else:
					shortnomatch+=1
				setlengths.append(setlength)
			strandsymbol = "-"
			for nm in reverse_blastsets[ck]:
				setlength = nm[1]-nm[0]+1
				if setlength >= args.blast_length:
					longnomatch+=1
					longsum += setlength
					if args.verbose:
						ob = []
						for k,v in forward_wms[ck].iteritems():
							ob.extend(set([k for ss in v if nm[0] <= ss <= nm[1]]))
						print >> sys.stdout, ck, strandsymbol, nm, ob
				else:
					shortnomatch+=1
				setlengths.append(setlength)
		print >> sys.stderr, "Counted {} long nomatches of average {:.1f}bp".format(longnomatch, float(longsum)/longnomatch), time.asctime()
		print >> sys.stderr, "Counted {} short nomatches".format(shortnomatch), time.asctime()
		print >> sys.stderr, "Generating histogram of nomatches", time.asctime()
		bins = arange(0, max(setlengths)+10, 10)
		# histogram function generates a tuple of arrays, which must be zipped to iterate
		sizehist = histogram(setlengths,bins,(0, max(setlengths)+10))
		for x,y in zip(sizehist[0],sizehist[1]):
			print >> sys.stdout, "%d\t%d" % (x,y)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
