#! /usr/bin/env python
#
# v2.1 fixed zero division error for print table 2014-10-27
# v2 added feature to output fraction within each aa 2014-02-27
#
# makecodontable.py v1
# program to read in fasta file and output codon usage, either an aggregate
# or to output one line for each sequence to be parsed otherwise

'''make_codon_table.py  last modified 2022-01-02
    count codon frequency from nucleotide CDS fasta file

make_codon_table.py -n trimmed_seqs.fasta > codon_table.txt

    output is in form of:
  codon   amino acid   global codon frequency   frequency by AA (for degenerate codons)
ACA T 2.07 33.30
ACC T 0.96 15.43
ACG T 0.39 6.27
ACT T 2.80 45.00

    use -m 100 to make values as percent rather than per thousand

    use -a -s to print usage per sequence by amino acid rather than total

For most cases:
make_codon_table.py -n trimmed_seqs.fasta -m 100 > codons.all.txt
make_codon_table.py -n trimmed_seqs.fasta -m 100 -a -s > codons.byaa.txt
'''

import sys
import itertools
import argparse
import time
from collections import defaultdict
from Bio import SeqIO

def make_codon_dict(codonlist):
	# set values of all of them to zero, in case some codon is never used
	codondict = {c:0 for c in codonlist}
	return codondict

def print_table(cd, codoncount, wayout, multiplier, codonlist, transtable):
	# set the default to zero for each aa, in case some codon or aa is never used
	aacounttable = defaultdict(int)
	# then iterate over each codon-aa pair and add the value to a dictionary storing the sum for each aa
	for k,v in transtable.items():
		aacounttable[v] += cd[k]
	# finally print the table using the same itertools product to generate the codons
	for c in codonlist:
		# and divide by the total codon count, and then by the sum of each aa from aacounttable
		try:
			print( "{} {} {:.2f} {:.2f}".format(c, transtable[c], cd[c]/float(codoncount)*multiplier, cd[c]/float(aacounttable[transtable[c]])*multiplier) , file=wayout )
		except ZeroDivisionError:
			print( "{} {} 0.00 0.00".format(c, transtable[c]) , file=wayout )

def print_codon_line(cd, wayout, codonlist, byaminoacid, transtable):
	if byaminoacid:
		# set the default to zero for each aa, in case some codon or aa is never used
		aacounttable = defaultdict(int)
		# then iterate over each codon-aa pair and add the value to a dictionary storing the sum for each aa
		for k,v in transtable.items():
			aacounttable[v]+=cd[k]
		outaacounts = []
		for c in codonlist:
			try:
				outaacounts.append("{:.2f}".format( cd[c] / float(aacounttable[transtable[c]]) ) )
			except ZeroDivisionError:
				outaacounts.append("0.00")
		outstring = ' '.join(outaacounts)
	else:
		outstring = ' '.join([str(cd[c]) for c in codonlist])
	print( outstring , file=wayout )

def main(argv, wayout):
	#print argv
	if not len(argv):
		argv.append('-h')

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-n','--nucleotides', help='nucleotide fasta file')
	parser.add_argument('-a','--by-amino-acid', action='store_true', help="output percentage of usage within each amino acid, use with -s")
	parser.add_argument('-m','--multiplier', type=int, help='display as per quantity, default - 1000', default=1000)
	parser.add_argument('-s','--by-sequence', action='store_true', help="output codon counts for each sequence")
	parser.add_argument('-t','--totals', action='store_true', help="output codon totals rather than percentage")
	args = parser.parse_args(argv)
	
	codoncount = 0
	seqcount = 0
	miscounts = 0
	#nbases = 0

	# generate all combinations of 3 letters from ACTG, the order is normally random so they must be sorted
	codons = [''.join(x) for x in sorted(set(itertools.product("ACTG",repeat=3)))]
	codoncounter = make_codon_dict(codons)

	# establishes translation table so that each codon can be paired to an amino acid
	transtable = {
	"AAA":"K", "AAC":"N", "AAG":"K", "AAT":"N", "ACA":"T", "ACC":"T", "ACG":"T", "ACT":"T", 
	"AGA":"R", "AGC":"S", "AGG":"R", "AGT":"S", "ATA":"I", "ATC":"I", "ATG":"M", "ATT":"I", 
	"CAA":"Q", "CAC":"H", "CAG":"Q", "CAT":"H", "CCA":"P", "CCC":"P", "CCG":"P", "CCT":"P", 
	"CGA":"R", "CGC":"R", "CGG":"R", "CGT":"R", "CTA":"L", "CTC":"L", "CTG":"L", "CTT":"L", 
	"GAA":"E", "GAC":"D", "GAG":"E", "GAT":"D", "GCA":"A", "GCC":"A", "GCG":"A", "GCT":"A", 
	"GGA":"G", "GGC":"G", "GGG":"G", "GGT":"G", "GTA":"V", "GTC":"V", "GTG":"V", "GTT":"V", 
	"TAA":"*", "TAC":"Y", "TAG":"*", "TAT":"Y", "TCA":"S", "TCC":"S", "TCG":"S", "TCT":"S", 
	"TGA":"*", "TGC":"C", "TGG":"W", "TGT":"C", "TTA":"L", "TTC":"F", "TTG":"L", "TTT":"F" }

	sys.stderr.write("# Counting codons  {}\n".format(time.asctime() ) )
	for seqrec in SeqIO.parse(args.nucleotides,'fasta'):
		seqcount += 1
		# this will add up all of the codons to the counter and either finish or hit an error at any N's or an incomplete codon
		try:
			for x in range(0,len(seqrec.seq),3):
				codon = str(seqrec.seq[x:x+3])
				codoncounter[codon]+=1
				codoncount += 1
		# indicate that this sequence was not properly trimmed
		except KeyError:
			sys.stderr.write( "# ERROR: codon {} in {}\n".format(codon, seqrec.id) )
			miscounts += 1
		# for use with -s option, and possibly -a
		# print a single line of each codon separated by a space, then reset the counter for the next sequence
		if args.by_sequence:
			print_codon_line(codoncounter, wayout, codons, args.by_amino_acid, transtable)
			codoncounter = make_codon_dict(codons)

	sys.stderr.write( "# {} sequences with {} codons\n".format(seqcount,codoncount) )
	#sys.stderr.write( "Counted %d sequences with Ns" % (nbases)
	sys.stderr.write( "# Counted {} sequences with incomplete codons\n".format(miscounts) )

	if not args.by_sequence:
		# print the final table of the codon and the percentage of all codons and within each aa
		if args.totals:
			print_table(codoncounter, 1, wayout, 1, codons, transtable)
		else:
			print_table(codoncounter, codoncount, wayout, args.multiplier, codons, transtable)

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
