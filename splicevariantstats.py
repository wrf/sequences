#!/usr/bin/env python
#
# splicevariantstats.py v1.0 2015-07-31

'''
splicevariantstats.py v1.1 2016-07-04

splicevariantstats.py -i genes.gtf -o genes_splicestats.tab

SPLICE CATEGORIES INCLUDE:
Canonical       C       |====|-----|====|-------|====>
Single Exon     SE      |====>
Cassette Exon   Ca      |====|-----|====|--|=|--|====>
Skipped Exon    Sk      |====|------------------|====>
Alt-N-Term      AN  |==|-----------|====|-------|====>
Alt-C-Term      AC      |====|-----|====|--------------|====>
Alt Donor       Ad      |====|-----|======|-----|====>
Alt Acceptor    Aa      |====|---|======|-------|====>
Retained Intron IR      |===============|-------|====>
Intron Start    IS  |========|-----|====|-------|====>
Intron End      IE      |====|-----|====|-------|========>
Non-Canonical   NC           |====|-------|====>
No Variants     NV     no splice variants, thus canonical
No Var-Single   NS     single exon and no splice variants

TABULAR OUTPUT LOOKS LIKE:
genename    exoncount    description of each exon
avic1.1     5            0,1,2,3,4
avic1.2     4            0,IR1,2,3
avic1.3     6            0,1,2,Ca3,4,5
avic2.1     2            NV

    add the exon classification output with --exon-classes
    this generates a tabular output of:
scaffold  gene  startposition  endposition  class as above

    for the -T transdecoder option, generate the genome.gff3 file using:
    util/cdna_alignment_orf_to_genome_orf.pl in the TransDecoder folder
'''

#
import sys
import argparse
import time
import re
from collections import defaultdict,Counter
from itertools import chain
#

def non_overlapping_ranges(trbounds):
	'''returns boolean if any transcript in a gene does not overlap with any other transcript'''
	for i,trbound in enumerate(trbounds):
		boundlist = list(trbounds) # create new list to compare to all items except self
		boundlist.pop(i)
		for otherbound in boundlist:
			if otherbound[0] <= trbound[0] <= otherbound[1] or otherbound[0] <= trbound[1] <= otherbound[1]:
				break # if either value is within range of another
			if trbound[0] < otherbound[0] and trbound[1] > otherbound[1]:
				break # if one transcript is nested inside another
		else:
			return True
	return False

def determine_splicing(ExonbyTrbyGene, TrboundsbyGene, countsdict, fpkmdict, minfraction, cefraction, verbose, output, tdgff=None, debug=False):
	MINCANONICAL = 0.11 # mininum number of canonical exons to allow use of gene, otherwise flagged
	noncanonicals = 0 # count of possibly erroneous genes
	nonoverlaps = 0 # count of genes with non-overlapping transcripts
	nslengthcounter = 0 # count of total bases for NS exons
	if output:
		outfile = open(output,'w')
	if tdgff:
		pass ### TODO add this part
	exonclassbygene = defaultdict(dict) # keys are gene names then exon bounds, and exon type is value
	exonlengthdict = defaultdict(int) # for counting frequency of exon phase, where keys are 0 1 or 2
	intronlengthdict = defaultdict(int) # for counting retained intron phase, where keys are 0 1 or 2
	for gene,exbytr in sorted(ExonbyTrbyGene.iteritems(), key=lambda x: x[0]):
		numtrans = len(exbytr)
		if numtrans > 1 and non_overlapping_ranges(TrboundsbyGene[gene]):
			if debug: # only for printing special output
				print >> sys.stderr, "WARNING: {} contains non-overlapping transcripts {}".format(gene, sorted(TrboundsbyGene[gene]) )
			nonoverlaps += 1
		# SETTING UP EXON STATS FOR THIS GENE
		# must unpack the list of lists from values(), otherwise each iterable is a list
		exoncounts = get_exon_set( chain(*exbytr.values() ) ) # Counter of exons
		exon1 = exoncounts.iterkeys().next()
		doreverse = (exon1[0] > exon1[1])
		mainexons = get_canonical_sites(exoncounts, minfraction) # dict of exons as keys and True as value
		mainexnum = len(mainexons)
		if mainexnum <= MINCANONICAL*len(exoncounts) and numtrans > 2:
			if debug: # only for printing special output
				print >> sys.stderr, "WARNING: {} main exons are {:.4f}% of {} total exons in {} with {} transcripts".format(mainexnum, mainexnum*100.0/len(exoncounts), len(exoncounts), gene, numtrans)
			errorgene = True # ignore this gene for future calculations
			noncanonicals += 1
		else:
			errorgene = False

		if numtrans==2: # for cases with only two transcripts, the highest expressed is canonical
			try:
				for exon in exbytr[fpkmdict[gene][0]]: # declare all exons in this transcript as mainexons
					mainexons[exon] = True
			except KeyError: # if no expression is available, so fpkmdict is empty and key is not found
				pass ### TODO this might hit an error later anyway
		skippableexons = get_canonical_sites(exoncounts, numtrans/2.0) # dict of exons with %50+ coverage

		# MAIN LOOP
		# iterate by transcript then by exons, to count multiple cases in one transcript
		for tr, exons in exbytr.iteritems():
			excount = len(exons)
			transinfostring = "{}\t{}\t".format(tr,excount) # string for output file
			# RESET THESE FOR EACH TRANSCRIPT
			canonicalcount = 0
			hascassette, hasretintron, hasskipped = False, False, False

			# CHECK FOR ONE OF FOUR STRANGE CASES
			if numtrans==1:
				if excount==1:
					countsdict["no-variant-single-exon"]+=1
					transinfostring += "NS:"
					singleexonlength = abs(exons[0][0] - exons[0][1]) + 1
					nslengthcounter += singleexonlength
				else:
					countsdict["no-splice-variants"]+=1
					transinfostring += "NV:"
				if output:
					print >> outfile, transinfostring
				continue
			if verbose:
				print >> sys.stderr, "transcript {} with {} exons".format(tr, excount)
			lastexonindex = excount - 1
			if excount==1:
				countsdict["single-exon"] += 1
				transinfostring += "SE:"
				if output:
					print >> outfile, transinfostring
				continue
			if numtrans==2:
				try:
					highestfpkmtrans = fpkmdict[gene][0]
				except KeyError: # fpkmdict contains no values or gene is missing
					highestfpkmtrans = None # then declare both transcripts as non-canonical
				if highestfpkmtrans==tr: # for highest expression of two transcripts
					countsdict["canonical"] += 1
					transinfostring += "2TrC:"
					if output:
						print >> outfile, transinfostring
					continue # the other transcript of the pair should be treated as normal
				else:
					transinfostring += "2TrN:"
			if not len(mainexons): # for cases where every exon is different
				countsdict["non-canonical"] += 1
				transinfostring += "NC:"
				if output:
					print >> outfile, transinfostring
				continue

			# GO THROUGH EACH EXON TO DETERMINE SPLICING
			for i, exon in enumerate( sorted(exons, reverse=doreverse) ):
				exonlength = max(exon)+1-min(exon) # length of exon
				nmap, cmap = 0,0 # zero can be used since positions are always positive integers
				nmatch, cmatch = None, None # numbers of matching exon
				if not mainexons.get(exon, False): # skip canonical exons
					# nmap is the position of the main exon that is matched
					for j, me in enumerate( sorted(mainexons.keys(), reverse=doreverse ) ): # using main exons
						if exon[0] == me[0]:
							nmatch = j
							nmap = me[0]
						if exon[1] == me[1]:
							cmap = me[1]
							cmatch = j
					if verbose:
						print >> sys.stderr, "noncanonical exon {} {} {} {} {} {}".format(i, exon, nmap, cmap, nmatch, cmatch)
					if i==0: # for all cases where it is first exon, alt-N-terminal and intron-start
						if cmap and not nmap: # intron start
							countsdict["intron-start"] += 1
							transinfostring += "IS{},".format(i)
						elif not nmap and not cmap: # alt-N-terminal
							countsdict["alt-N-terminal"] += 1
							transinfostring += "AN{},".format(i)
						if nmap and not cmap: # alt N terminal donor
							countsdict["altdonor"] += 1
							transinfostring += "Ad{},".format(i)
					elif i==lastexonindex: # for all cases of last exon, alt-C-terminal and intron-end
						if nmap and not cmap: # intron end
							countsdict["intron-end"] += 1
							transinfostring += "IE{},".format(i)
						elif not nmap and not cmap: # alt-C-terminal
							countsdict["alt-C-terminal"] += 1
							transinfostring += "AC{},".format(i)
						elif cmap and not nmap: # alt-C-terminal acceptor
							countsdict["altacceptor"] += 1
							transinfostring += "Aa{},".format(i)
					else: # middle exons
						if nmap and not cmap: # and exon[1] < mecpos: # altdonor
							countsdict["altdonor"] += 1
							transinfostring += "Ad{},".format(i)
						elif cmap and not nmap: # and exon[1] < mecpos: # altdonor
							countsdict["altacceptor"] += 1
							transinfostring += "Aa{},".format(i)
						if not nmap and not cmap: # cassette exon
							countfraction = exoncounts[exon]*1.0/excount
							if nmatch==cmatch and countfraction<=cefraction: # cassettes are less than half used
								hascassette = True
								countsdict["cassette-exons"] += 1
								exonphase = exonlength%3
								exonlengthdict[exonphase] += 1
								if not errorgene: # only count non-erroneous genes
									transinfostring += "Ca{},".format(i)
									exonclassbygene[gene][exon] = "Ca"
					# CHECK FOR RETAINED INTRONS
					for j, me in enumerate( sorted(exoncounts.keys(), reverse=doreverse ) ): # using all exons
						if me==exon: # skip exon if it matches only self
							continue
						if exon[0] == me[0]:
							nmatch = j
							nmap = me[0]
							nirpos = me[1]
						if exon[1] == me[1]:
							cmap = me[1]
							cmatch = j
							cirpos = me[0]
					if nmap and cmap: # must match real exons that are not self
						if not nmatch==cmatch: # exons must be different numbers
							hasretintron = True
							bigpos = max(nirpos, cirpos)
							smallpos = min(nirpos, cirpos)
							intronlength = bigpos - smallpos - 1 # is really b-1 - s+1 - 1
							retainedintronbounds = (smallpos+1, bigpos-1)
							retintronphase = intronlength%3
							intronlengthdict[retintronphase] += 1
							countsdict["intron-retention"] += 1
							transinfostring += "IR{},".format(i)
							exonclassbygene[gene][retainedintronbounds] = "IR" # create new exon for IR
				else: # exon is a canonical exon
					canonicalcount += 1
					transinfostring += "{},".format(i)
				#	if not exonclassbygene[gene].get(exon, False): # only assign canonical if not already assigned
				#		exonclassbygene[gene][exon] = "C"
					if verbose:
						print >> sys.stderr, "canonical exon {} {}".format(i, exon)

			# CHECK FOR SKIPPED EXONS
			minpos = min(chain(*exons))
			maxpos = max(chain(*exons))
			seswithinrange = [e for e in skippableexons if min(e)>=minpos and max(e)<=maxpos]
			for sewr in sorted(seswithinrange): # sewr means skippable exon within range
				sewrn, sewrc = 0, 0
				for exon in exons:
					if sewr[0]==exon[0]:
						sewrn = exon[0]
					if sewr[1]==exon[1]:
						sewrc = exon[1]
				if not sewrn and not sewrc:
					if verbose:
						print >> sys.stderr, "exon skipped at {}".format(sewr)
					countsdict["skipped-exons"] += 1
					transinfostring += "Sk{}-{},".format(*sewr)
					hasskipped = True
					if not errorgene: # only count non-erroneous genes
						exonclassbygene[gene][sewr] = "Sk" # reassign exon as skipped or skippable
						exonphase = exonlength%3
						exonlengthdict[exonphase] += 1

			# FINAL EVALUATIONS
			if canonicalcount == excount and not hasskipped:
				countsdict["canonical"] += 1
			elif hasskipped:
				countsdict["skipped-transcripts"] += 1
			if hascassette:
				countsdict["cassette-transcripts"] += 1
			if hasretintron:
				countsdict["retained-intron-transcripts"] += 1
			if output:
				print >> outfile, transinfostring
	if output:
		outfile.close()
	for k,v in exonlengthdict.iteritems():
		print >> sys.stderr, "Skipped/cassette exons in phase {}\t{}".format(k,v)
	for k,v in intronlengthdict.iteritems():
		print >> sys.stderr, "Retained introns in phase {}\t{}".format(k,v)
	print >> sys.stderr, "{} genes with non-canonical or erroneous splicing".format(noncanonicals)
	print >> sys.stderr, "{} genes with non-overlapping transcripts".format(nonoverlaps)
	print >> sys.stderr, "{} genes are single-exon without splicing for a total length of {} and average length of {:.2f}".format(countsdict["no-variant-single-exon"], nslengthcounter, nslengthcounter*1.0/countsdict["no-variant-single-exon"])
	return exonclassbygene

def get_exon_set(allexonlist):
	'''return Counter of all exons per gene to determine canonical exons'''
	# c.keys() has a unique list of exons, and also the counts
	c = Counter()
	for e in allexonlist:
		c[e]+=1
	return c

def get_canonical_sites(exoncounts, minfraction):
	'''return a dict subset of only exons whose frequency is above some defined fraction'''
	canonicalexons = {}
	for k,v in exoncounts.iteritems():
		if v >= minfraction:
			canonicalexons[k] = True
	return canonicalexons

def parse_transdecoder_gff(transdecodergff, exclusiondict):
	'''read in TransDecoder genome GFF and return a dictionary where keys are transcript IDs and values are lists of CDS intervals'''
	commentlines = 0
	featurecounts = defaultdict(int)
	CDSbyTranscript = defaultdict(list)
	print >> sys.stderr, "# Parsing proteins from {}".format(transdecodergff), time.asctime()
	for line in open(transdecodergff, 'r').readlines():
		line = line.rstrip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				lsplits = line.split("\t")
				scaffold = lsplits[0]
				if exclusiondict and exclusiondict.get(scaffold, False):
					continue # skip anything that hits to excludable scaffolds
				feature = lsplits[2]
				featurecounts[feature] += 1
				attributes = lsplits[8]
				if feature=="CDS":
					if attributes.find("ID")==0: # indicates gtf format
						transid = re.search('ID=([\w.-]+)|', attributes).group(1)
					if lsplits[6]=="+":
						exonbounds = (int(lsplits[3]), int(lsplits[4]))
					else:
						exonbounds = (int(lsplits[4]), int(lsplits[3]))
					CDSbyTranscript[transid].append(exonbounds)
	print >> sys.stderr, "# Counted {} comment lines".format(commentlines), time.asctime()
	for k in sorted(featurecounts.keys()):
		print >> sys.stderr, "{}\t{}".format(k, featurecounts[k])
	return CDSbyTranscript

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="input gtf/gff3 file", required=True)
	parser.add_argument('-E','--exclude', help="file of list of bad contigs")
	parser.add_argument('-c','--cassette-exon', type=float, default=0.49, help="maximum fraction for cassette exons [0.49]")
	parser.add_argument('-d','--delimiter', help="gene delimiter from transcripts, if genes are not defined [_]", default='_')
	parser.add_argument('-g','--no-genes', action="store_true", help="genes are not defined, get gene ID for each exon")
	parser.add_argument('-m','--min-fraction', type=int, default=2, help="minimum count for canonical exon [2]")
	parser.add_argument('-o','--output', help="write splice information about each transcript to file")
	parser.add_argument('--exon-classes', help="write tabular exon information to file")
	parser.add_argument('-R','--rsem-counts', help="file for expression counts of Trinity sequences")
	parser.add_argument('-T','--transdecoder', help="transdecoder genome gff file")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	parser.add_argument('-x','--debug', action="store_true", help="inspect special output")
	args = parser.parse_args(argv)

	featurecounts = defaultdict(int)
	commentlines = 0
	TrboundsbyGene = defaultdict(list) # lists contain tuples of intervals
	ExonbyTrbyGene = defaultdict(lambda: defaultdict(list))
	highFPKMbyGene = {}

	if args.exclude:
		print >> sys.stderr, "# Reading exclusion list {}".format(args.exclude), time.asctime()
		exclusion_type_d = {}
		for term in open(args.exclude,'r').readlines():
			term = term.rstrip()
			if term[0] == ">":
				term = term[1:]
			exclusion_type_d[term] = True
		print >> sys.stderr, "# Found {} contigs to exclude".format(len(exclusion_type_d) ), time.asctime()
	else:
		exclusion_type_d = None

	transcount = 0
	genetoscaffold = {} # key is gene and value returns scaffold of that gene
	print >> sys.stderr, "# Parsing transcripts from {}".format(args.input), time.asctime()
	for line in open(args.input, 'r').readlines():
		line = line.strip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				lsplits = line.split("\t")
				scaffold = lsplits[0]
				if args.exclude and exclusion_type_d.get(scaffold, False):
					continue # skip anything that hits to excludable scaffolds
				feature = lsplits[2]
				featurecounts[feature] += 1
				attributes = lsplits[8]
				if feature=="transcript" or feature=="mRNA":
					transcount += 1
					if attributes.find("gene_id")==0: # indicates gtf format
						geneid = re.search('gene_id "([\w.-]+)";', attributes).group(1)
						transid = re.search('transcript_id "([\w.-]+)";', attributes).group(1)
						trbounds = (int(lsplits[3]), int(lsplits[4]))
						TrboundsbyGene[geneid].append(trbounds)
						try:
							fpkm = float(re.search('FPKM "([\d.]+)";', attributes).group(1) )
						except AttributeError:
							fpkm = 0.0
						if fpkm > highFPKMbyGene.get(geneid, ("",0.0) )[1]:
							highFPKMbyGene[geneid] = (transid,fpkm)
						genetoscaffold[geneid] = scaffold
					else:
						print >> sys.stderr, "# ERROR cannot parse gene_id for {}".format(attributes)
					if args.verbose:
						print >> sys.stderr, "found gene {}".format(geneid)
				elif feature=="exon":
					transid = re.search('transcript_id "([\w.-]+)";', attributes).group(1)
					if args.no_genes:
						geneid = re.search('gene_id "([\w.-]+)";', attributes).group(1)
						geneid = geneid.rsplit(args.delimiter,1)[0]
					if lsplits[6]=="+":
						exonbounds = (int(lsplits[3]), int(lsplits[4]))
					else:
						exonbounds = (int(lsplits[4]), int(lsplits[3]))
					ExonbyTrbyGene[geneid][transid].append(exonbounds)
				elif feature=="cDNA_match": # mostly for Trinity GMAP
					if re.search("path(\d+);", attributes).group(1)=="1": # only keep path1
						transid = re.search('Name=([\w.-]+);', attributes).group(1)
						geneid = transid.rsplit("_",1)[0] # for Trinity only
						if lsplits[6]=="+":
							exonbounds = (int(lsplits[3]), int(lsplits[4]))
						else:
							exonbounds = (int(lsplits[4]), int(lsplits[3]))
						ExonbyTrbyGene[geneid][transid].append(exonbounds)
	print >> sys.stderr, "# Counted {} comment lines".format(commentlines), time.asctime()
	for k in sorted(featurecounts.keys()):
		print >> sys.stderr, "{}\t{}".format(k, featurecounts[k])
	print >> sys.stderr, "# Counted {} genes".format(len(ExonbyTrbyGene)), time.asctime()

	if args.rsem_counts: # if given, this would add or overwrite existing dictionary entries
		print >> sys.stderr, "# Parsing expression counts from {}".format(args.rsem_counts), time.asctime()
		#       0         1       2            3                  4          5    6       7
		# transcript_id	gene_id	length	effective_length	expected_count	TPM	FPKM	IsoPct
		for line in open(args.rsem_counts, 'r').readlines():
			line = line.rstrip()
			lsplits = line.split("\t")
			if not lsplits[0]=="transcript_id":
				geneid = lsplits[1]
				transid = lsplits[0]
				fpkm = float(lsplits[6])
				if fpkm >= highFPKMbyGene.get(geneid, ("",0.0) )[1]:
					highFPKMbyGene[geneid] = (transid,fpkm)

	if args.transdecoder:
		CDSbyTranscript = parse_transdecoder_gff(transdecodergff, exclusion_type_d)
	else:
		CDSbyTranscript = None

	transtypesd = {"canonical":0, "single-exon":0, "retained-intron-transcripts":0, "cassette-transcripts":0, "cassette-exons":0, "intron-retention":0, "alt-N-terminal":0, "alt-C-terminal":0, "altacceptor":0, "altdonor":0, "skipped-transcripts":0, "skipped-exons":0, "intron-end":0, "intron-start":0, "no-splice-variants":0, "non-canonical":0, "no-variant-single-exon":0}
	print >> sys.stderr, "# Sorting variants", time.asctime()
	exontypedict = determine_splicing(ExonbyTrbyGene, TrboundsbyGene, transtypesd, highFPKMbyGene, args.min_fraction, args.cassette_exon, args.verbose, args.output, CDSbyTranscript, args.debug)

	# print final output counts
	print >> sys.stderr, "# Counted {} transcripts".format(transcount), time.asctime()
	for k in sorted(transtypesd.keys()):
		print >> sys.stderr, k, transtypesd[k]

	if args.exon_classes:
		exonclasscount = 0
		print >> sys.stderr, "# Writing exon information to {}".format(args.exon_classes), time.asctime()
		with open(args.exon_classes,'w') as el:
			for gene, exondict in exontypedict.iteritems():
				for exon, extype in sorted(exondict.items(), key=lambda x: x[0][0]):
					exonclasscount += 1
					print >> el, "{}\t{}\t{}\t{}\t{}".format(genetoscaffold[gene], gene, exon[0], exon[1], extype)
		print >> sys.stderr, "# Wrote {} exons to {}".format(exonclasscount, args.exon_classes), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
