#!/usr/bin/env python
#
# tabularpfam.py v1.0 created 2016-04-06

'''
tabularpfam.py  last modified 2022-03-15

    EXAMPLE USAGE:
    to convert fixed-width space delimited PFAM output to tab-delimited
tabularpfam.py proteins.pfam.txt > proteins.pfam.tsv

    GENERATE PFAM TABULAR BY:
hmmscan --cpu 4 --domtblout proteins.pfam.txt ~/PfamScan/data/Pfam-A.hmm transcripts.transdecoder.pep > proteins.pfam.log
    or discard STDOUT with:
hmmscan --cpu 4 --domtblout proteins.pfam.txt ~/PfamScan/data/Pfam-A.hmm transcripts.transdecoder.pep > /dev/null

    BULK RENAMING:
    in case many files need to be renamed, or reformatted, 
    such as if many were called .tab, when in fact it is not tab-delimited
for FILE in *.tab ; do BASE="${FILE%.tab}" ; mv $FILE $BASE.txt ; done

    to then format all of those to tab-delimited
for FILE in *.txt ; do BASE="${FILE%.txt}" ; tabularpfam.py $FILE > $BASE.tab ; done
'''

#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
#      0                 1         2     3                    4         5        6       7     8     9  10   11       12        13     14    15     16   17     18   19     20  21  22
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------

import sys
import time
import argparse

def parse_pfam_domains(pfamtabular, is_verbose, wayout):
	'''parse domains from hmm domtblout and write to stdout as tab separated fields'''
	if is_verbose:
		print( "# Parsing hmmscan PFAM tabular {}  {}".format(pfamtabular, time.asctime() ), file=sys.stderr )
	domaincounter = 0
	for line in open(pfamtabular, 'r'):
		line = line.strip()
		if not line or line[0]=="#": # skip comment lines
			continue # also catch for empty line, which would cause IndexError
		domaincounter += 1
		lsplits = line.split(None,22) # should split on whitespace
		print( "\t".join(lsplits), file=wayout )
	if domaincounter:
		print( "# Counted {} lines in {}  {}".format(domaincounter, pfamtabular, time.asctime() ), file=sys.stderr )
	else:
		print( "# No domains found in {}  {}".format(pfamtabular, time.asctime() ), file=sys.stderr )

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input', help="PFAM domain information as hmmscan tabular")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	parse_pfam_domains(args.input, args.verbose, wayout)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
