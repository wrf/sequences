#! /usr/bin/env python
#
# v1 created 2016-02-08
# filetype changed to rt instead of rU 2024-12-09

"""repeathistogram.py  last modified 2024-12-09
    Requires Bio library (biopython)

repeathistogram.py scaffolds.fasta > scaffolds_repeat_histo.txt

    Generates output as table of:
Count   Repeat length
"""

import sys
import argparse
import time
import re
from numpy import histogram
from numpy import arange
from Bio import SeqIO
from Bio.Seq import Seq

def make_poly_regex(repeat):
	# generates re polya as "(A)+"
	regex = re.compile("({0})+".format(repeat) )
	return regex

def get_repeats(sequence, regexp):
	'''return list of lengths of repeats in the sequence'''
	polyresults = regexp.finditer(str(sequence))
	try: # for multicharacter repeats, length with be number of repeats * length, so 'ACACAC' would return 6 rather than 3
		return [l.end()-l.start() for l in polyresults]
	except ValueError: # for cases where repeat does not occur once, and max of list fails
		return []

def make_histogram(wayout, minval, maxval, bin_size, seqsizes, ignore_max):
	bins = arange(minval, maxval+bin_size, bin_size)
	# unless using -I, values above the max will be changed to the max and put into the last bin
	if not ignore_max:
		for i,x in enumerate(seqsizes):
			if x >= maxval:
				seqsizes[i] = maxval
	# histogram function generates a tuple of arrays, which must be zipped to iterate
	sizehist = histogram(seqsizes,bins,(minval, maxval+bin_size))
	for x,y in zip(sizehist[0],sizehist[1]):
		print >> wayout, "%d\t%d" % (x,y)

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('r'), default = '-', help="fasta format file")
	parser.add_argument('-a', '--above', type=int, metavar='N', default=0, help="only count sequences longer than N")
	parser.add_argument('-b', '--below', type=int, metavar='N', default=1000000000, help="only count sequences shorter than N")
	parser.add_argument('-f', '--format', nargs='?', const='fastq', default='fasta', help="import fastq format sequences")
	# this may need to be "fastq-sanger" or "fastq-illumina"
	parser.add_argument('-r', '--repeats', metavar='A', default='N', help="measure length of the longest polyN, NX repeat, etc.")
	parser.add_argument('-z', '--bin-size', type=float, metavar='N', default=10.0, help="bin size for histogram output [10.0]")
	parser.add_argument('-m', '--minimum', type=float, metavar='N', default=1.0, help="minimum cutoff for histogram [1.0]")
	parser.add_argument('-M', '--maximum', type=float, metavar='N', help="maximum cutoff for histogram")
	parser.add_argument('-I', '--ignore_max', action='store_true', help="ignore values in histogram above maximum")
	args = parser.parse_args(argv)

	# all integers initialized
	seqcount = 0

	# form regular expression
	repoly = make_poly_regex(args.repeats)
	print >> sys.stderr, "# Finding repeats of %s:" % (args.repeats), time.asctime()

	# list for tracking total number of written bases
	repsizes = []

	# start main loop to iterate through sequence file
	print >> sys.stderr, "# Parsing sequences on %s:" % (args.input_file.name), time.asctime()
	for seq_record in SeqIO.parse(args.input_file, args.format):
		seqcount += 1
		seqlen = len(seq_record.seq)
		if (args.below > seqlen >= args.above):
			repsizes.extend( get_repeats(seq_record.seq, repoly) )

	seqsum = sum(repsizes)
	print >> sys.stderr, "# Counted {} sequences".format(seqcount), time.asctime()
	print >> sys.stderr,"# Total repeat letters is {}".format(seqsum)
	print >> sys.stderr,"# for {} repeats".format(len(repsizes))

	maxval = args.maximum if args.maximum else max(repsizes)
	make_histogram(wayout, args.minimum, maxval, args.bin_size, repsizes, args.ignore_max)

	print >> sys.stderr, "# Done: ", time.asctime()


if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
