#! /usr/bin/env python
#
# contigcomposition.py 2015-10-19
# v1.1 fix for gzip 2023-07-21

'''contigcomposition.py  last modified 2023-07-21

    output bulk composition of bases in fasta sequences, or other characters

contigcomposition.py seqs.fasta

    can be .gz

    FOR EXAMPLE, in Trichoplax:
contigcomposition.py Triad1_genomic_scaffolds.fasta
    reveals presence of null characters x00 in scaffold_8:
scaffold_8 Counter({'T': 933223, 'A': 929518, 'N': 596026, 'G': 454558, 'C': 453786, '\\x00': 8192})

    remove these characters using sed:
cat Triad1_genomic_scaffolds.fasta | sed s/"\\x00"//g > Triad1_genomic_scaffolds.corrected.fa
'''

import sys
import gzip
from collections import Counter

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	fastafile = sys.argv[1]
	c = Counter()
	if fastafile.rsplit('.',1)[1]=="gz": # autodetect gzip format
		_opentype = gzip.open
		sys.stderr.write("# Reading {} as gzipped\n".format(fastafile) )
	else: # otherwise assume normal open for fasta format
		_opentype = open
		sys.stderr.write("# Reading {}\n".format(fastafile) )

	for line in _opentype(fastafile,'rt'):
		line = line.rstrip()
		if line and line[0]==">":
			if c:
				sys.stdout.write( "{} {}\n".format(seqid, c) )
				c = Counter()
			seqid = line[1:]
		else:
			c.update(line)
	else: # for last line where there is no fasta
		sys.stdout.write( "{} {}\n".format(seqid, c) )
	sys.stderr.write("# Done\n")
