#!/usr/bin/env python
# blast_evalue_filter.py filter output format 6 blast hits by evalue
# v1.0 2015-05-20
# v1.1 2022-12-12 python3 update and other things
# v1.2 2023-01-31 report number of queries and subjects found, add quiet mode

'''filter blast hits by evalue  last modified 2023-01-31

blast_evalue_filter.py -b tblastn_output.tab -e 1e-3 > filtered_tblastn_output.tab

    tabular blast output should be made from blast programs with -outfmt 6

tblastn -query refprots.fa -db target_genome.fa -outfmt 6 > tblastn_output.tab
'''
#
import sys
import argparse
import time

#
### BLAST OUTPUTS
# default blastn or tblastn output for -outfmt 6 is printed tabular of:
#
# qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore
#
# referring to: query ID, subject ID, percent identity, length of alignment,
#   mismatch count, gap openings, query start and end, subject start and end,
#   evalue, and the bitscore
#
# for example tblastn output may look:
# prot12345	Contig248	86.89	122	0	1	213	318	16668	17033	1e-63	  214
#
# where protein 12345 hit Contig248 with evalue of 1e-63

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--blast', help="tabular blast results file (use -outfmt 6 )")
	parser.add_argument('-e','--evalue-cutoff', type=float, help="evalue cutoff [1e-3]", default=1e-3)
	parser.add_argument('-p','--percent-identity-cutoff', type=float, help="percent identity cutoff [1.0]", default=1.0)
	parser.add_argument('-r','--omit-reverse-match', action="store_true", help="omit hits from any reverse frame")
	parser.add_argument('-q','--quiet', action="store_true", help="no output, just count how many would be filtered")
	parser.add_argument('-v','--verbose', action="store_true", help="extra output")
	args = parser.parse_args(argv)

	# counter for number of lines
	linecounter = 0
	writecounter = 0
	blast_queries = {} # key is query id, value is count
	blast_subjects = {} # key is subject id, value is count

	evalue_count = 0 # default is 0.001
	low_ident_count = 0 # default is 1 percent
	reverse_count = 0 # and strand flips
	sys.stderr.write( "# Starting BLAST parsing on {}  {}\n".format( args.blast, time.asctime() ) )
	sys.stderr.write( "# Using: e-value threshold of {:.1e}\n# percent identity threshold of {:.1f}\n".format( args.evalue_cutoff, args.percent_identity_cutoff ) )
	for line in open(args.blast, 'r'):
		linecounter += 1
		qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = line.strip().split("\t")

		# track queries and subjects, for final counts
		blast_queries[qseqid] = blast_queries.get(qseqid,0) + 1
		blast_subjects[sseqid] = blast_subjects.get(sseqid,0) + 1

		# do both tests
		is_evalue_cut = (float(evalue) >= args.evalue_cutoff)
		is_pident_cut = (float(pident) < args.percent_identity_cutoff)

		# add to counts
		low_ident_count += int(is_pident_cut) # either 0 or 1
		evalue_count += int(is_evalue_cut)

		if is_evalue_cut or is_pident_cut:
			continue

		if qstart > qend:
			reverse_count += 1
			if args.omit_reverse_match:
				continue

		if not args.quiet:
			writecounter += 1
			wayout.write(line)

	sys.stderr.write( "# Parsed {} lines  {}\n".format(linecounter, time.asctime() ) )
	sys.stderr.write( "# Counted {} unique queries and {} unique subjects\n".format( len(blast_queries), len(blast_subjects) ) )
	if evalue_count or low_ident_count:
		sys.stderr.write( "# {} hits missing evalue cutoff ; {} hits below identity cutoff\n".format(evalue_count, low_ident_count) )
	if reverse_count:
		sys.stderr.write( "# Counted {} reverse hits\n".format(reverse_count) )
	sys.stderr.write( "# Wrote {} lines\n".format(writecounter) )

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
