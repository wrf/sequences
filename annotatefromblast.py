#! /usr/bin/env python
# v1.1 2019-02-04
# v1.2 2024-03-08 python3 update

'''
annotatefromblast.py v1.2 2024-03-08
    read in fasta and append data from best hit to fasta names

    USAGE:
annotatefromblast.py -i proteins_vs_hsap_blastp.tab -q proteins.fasta -d human_uniprot.fasta > annotation_vector.tab

    change the names of fasta proteins using rename_select_fasta.py:
rename_select_fasta.py annotation_vector.tab proteins.fasta > proteins_w_annot.fasta

    DATA PREPARATION:
    run blast against human proteins (taken from SwissProt)
blastp -query proteins.fasta -db human_uniprot.fasta -evalue 1e-5 -outfmt 6 -max_target_seqs 1 > proteins_vs_hsap_blastp.tab

    DEPENDENCIES:
    requires Python Bio (Biopython) library for rapid sequence indexing
'''

import sys
import argparse
import re
import time
from Bio import SeqIO

def parse_swissprot_header(hitstring):
	# hitstring can conveniently be taken from seq_record.description
	# for example
	# swissprotdict.next().description
	# 'sp|Q6GZX4|001R_FRG3G Putative transcription factor 001R OS=Frog virus 3 (isolate Goorha) GN=FV3-001R PE=4 SV=1'
	#
	# from the swissprot website http://www.uniprot.org/help/fasta-headers
	# fasta headers appear as:
	# >db|UniqueIdentifier|EntryName ProteinName OS=OrganismName[ GN=GeneName]PE=ProteinExistence SV=SequenceVersion
	#
	# Where:
	#
    # db is 'sp' for UniProtKB/Swiss-Prot and 'tr' for UniProtKB/TrEMBL.
    # UniqueIdentifier is the primary accession number of the UniProtKB entry.
    # EntryName is the entry name of the UniProtKB entry.
    # ProteinName is the recommended name of the UniProtKB entry as annotated in the RecName field. For UniProtKB/TrEMBL entries without a RecName field, the SubName field is used. In case of multiple SubNames, the first one is used. The 'precursor' attribute is excluded, 'Fragment' is included with the name if applicable.
    # OrganismName is the scientific name of the organism of the UniProtKB entry.
    # GeneName is the first gene name of the UniProtKB entry. If there is no gene name, OrderedLocusName or ORFname, the GN field is not listed.
    # ProteinExistence is the numerical value describing the evidence for the existence of the protein.
    # SequenceVersion is the version number of the sequence.
	#
	# Examples:
	#
	# >sp|Q8I6R7|ACN2_ACAGO Acanthoscurrin-2 (Fragment) OS=Acanthoscurria gomesiana GN=acantho2 PE=1 SV=1
	# or
	# sp|Q9GZU7|CTDS1_HUMAN Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1 OS=Homo sapiens GN=CT DSP1 PE=1 SV=1

	# assuming the format of "OS=Homo sapiens"
	organismre = "OS=(\w+) (\w+)"
	# extract genus and species as groups()
	try:
		osgroups = re.search(organismre,hitstring).groups()
	# in case of Nonetype for some reason, possibly weird names of bacteria or viruses
	# such as:
	# sp|P07572|POL_MPMV Pol polyprotein OS=Mason-Pfizer monkey virus GN=pol PE=3 SV=1
	except AttributeError:
		osgroups = re.search("OS=([\w-]+) (\w+)",hitstring).groups()
	# print in format of H.sapiens
	osname = "{}.{}".format(osgroups[0][0], osgroups[1])

	genedescRE = "(.+) OS="
	# split will take everything after the first space, so:
	# "Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1 OS=Homo sapiens GN=CT DSP1 PE=1 SV=1"
	nospnumber = hitstring.split(' ',1)[1]
	# this should pull everything before the species excluding " OS=", so:
	# "Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1"
	genedesc = re.search(genedescRE, nospnumber).groups()[0]
	# this should specifically remove the annotation "(Fragment)" for some proteins
	genedesc = genedesc.replace("(Fragment)","")
	# convert all remaining spaces to underscores, so:
	# Carboxy-terminal_domain_RNA_polymerase_II_polypeptide_A_small_phosphatase_1
	#gdwithdashes = genedesc.replace(" ","_")
	# join finally with the species, so:
	# H.sapiens_Carboxy-terminal_domain_RNA_polymerase_II_polypeptide_A_small_phosphatase_1
	#swissstring = "{}_{}".format(osname, gdwithdashes)
	genedesc = clean_header_line(genedesc)
	return genedesc
	
def clean_header_line(genedesc):
	genedesc = genedesc.replace("3'","3-prime")
	genedesc = genedesc.replace("5'","5-prime")
	genedesc = genedesc.replace("G(s)", "G_s")
	genedesc = genedesc.replace("G(q)", "G_q")
	genedesc = genedesc.replace("G(k)", "G_k")
	genedesc = genedesc.replace(" [GTP]","")
	genedesc = genedesc.replace(" [ubiquinone]","")
	genedesc = genedesc.replace(" [glutamine-hydrolyzing]","")

	genedesc = genedesc.replace("(","_")
	genedesc = genedesc.replace(")","_")
	genedesc = genedesc.replace("'","")
	genedesc = genedesc.replace("[","")
	genedesc = genedesc.replace("]","")
	genedesc = genedesc.replace(";","_")
	genedesc = genedesc.replace(":","_")
	genedesc = genedesc.replace(",","_")
	return genedesc

def make_seq_length_dict(sequencefile):
	'''from fasta file name, return dict where key is query ID and value is int of seq length'''
	sys.stderr.write( "# Parsing target sequences from {}  {}\n".format(sequencefile, time.asctime() ) )
	lengthdict = {}
	for seqrec in SeqIO.parse(sequencefile,'fasta'):
		lengthdict[seqrec.id] = len(seqrec.seq)
	if len(lengthdict):
		sys.stderr.write( "# Found {} sequences  {}\n".format(len(lengthdict), time.asctime() ) )
	else:
		sys.stderr.write( "WARNING: NO SEQUENCES IN FILE {}\n".format( sequencefile ) )
	return lengthdict

def query_to_sub_dict(blastfile, querylendict, dbseqdict, length_cutoff, bitscore_cutoff, bigbitscore, is_verbose):
	'''read tabular blast and return a dict where key is query ID and value is best hit'''
	hitcounter = 0
	all_blast_hits = {} # key is query ID, value is subject ID
	best_hit_dict = {} # key is query ID, value is subject ID
	lastquery = None
	sys.stderr.write( "# Reading blast hits from {}  {}\n".format( blastfile , time.asctime() ) )
	for line in open( blastfile ,'r'):
		hitcounter += 1
		lsplits = line.strip().split("\t")
		# qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore
		queryid = lsplits[0]
		all_blast_hits[queryid] = True
		if queryid == lastquery:
			continue # skip second hit to the same subject
		subid = lsplits[1]
		bitscore = float(lsplits[11])
		lengthfraction = querylendict[queryid]*1.0/len(dbseqdict[subid].seq)
		if bitscore >= bigbitscore or ( bitscore >= bitscore_cutoff and lengthfraction >= length_cutoff ) :
			lastquery = queryid
			best_hit_dict[queryid] = subid
		else:
			if is_verbose:
				sys.stderr.write( "Removed {} hit against {} with len:{} bits:{}\n".format(queryid, subid, lengthfraction, bitscore) )
	sys.stderr.write( "# Counted {} blast hits for {} unique queries\n".format(hitcounter, len(all_blast_hits) ) )
	sys.stderr.write( "# Kept matches for {} good blast hits\n".format( len(best_hit_dict) , blastfile) )
	return best_hit_dict

def main(argv, wayout):
	if not argv:
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="tabular BLAST results from -outfmt 6, or diamond", required=True)
	parser.add_argument('-q','--query', help="query proteins in fasta format")
	parser.add_argument('-d','--db', help="uniprot database from blasting in fasta format")
	parser.add_argument('-l','--length', type=float, help="minimum length for hit [0.75]", default=0.75)
	parser.add_argument('-b','--bitscore', type=float, help="minimum bitscore with length [300]", default=300.0)
	parser.add_argument('-B','--big-bitscore', type=float, help="minimum bitscore without length [1000]", default=1000.0)
	parser.add_argument('-p','--prepend', help="add tag to the front of each annotation [BLAST hit of]", default="BLAST hit of")
	parser.add_argument('-s','--split', help="split query name by this character, taking the 1st part")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output of filtering")
	parser.add_argument('--mmetsp', action="store_true", help="use output format for MMETSP")
	parser.add_argument('--tabular', action="store_true", help="expect input format of two column tab-delimited descriptions")
	args = parser.parse_args(argv)

	querylendict = make_seq_length_dict(args.query) # only length is needed
	dbseqdict = SeqIO.to_dict(SeqIO.parse(args.db,"fasta")) # sequence descriptions are also needed
	best_hit_dict = query_to_sub_dict(args.input, querylendict, dbseqdict, args.length, args.bitscore, args.big_bitscore, args.verbose)

	query_num = 0
	sys.stderr.write( "# Writing conversion vector  {}\n".format( time.asctime() ) )
	for queryid in querylendict.keys():
		query_num += 1
		best_hit = best_hit_dict.get(queryid, None)
		if best_hit is not None:
			hit_description = parse_swissprot_header(dbseqdict[best_hit].description).replace("/","_")
		else:
			hit_description = "NO HIT"
		if args.split:
			queryid = queryid.rsplit(args.split,1)[0]
		if args.mmetsp: # special output format
			mmetsp_re = "([\w\d_-]+)_CAMPEP_(\d+)"
			species, pepnum = re.search(mmetsp_re,queryid).groups()
			newname = "{} {} [{}]".format( pepnum, hit_description, species )
		else: # regular output
			newname = "{} {}".format( args.prepend, hit_description )#, lengthfraction, bitscore )
		outstring = "{}\t{}".format( queryid, newname )
		print( outstring, file = wayout)
	sys.stderr.write( "# Wrote conversions for {} queries\n".format(query_num) )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
