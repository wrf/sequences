#!/usr/bin/env python
#
'''checknogalignments.py v1.1 2017-01-09
tool to quickly check for abnormal sequences in fasta alignments

checknogalignments.py -l meNOG_raw_algs/ -s eggnog4.species_list.txt -r > meNOG_align_stats.tab

    to use with predicted ortholog families

    to use OrthoDB fasta files or alignments:
checknogalignments.py -l orthodb_ogs/ -s odb9v1_species.tab -r -d ':' -F orthodb > odb9v1_metazoan_align_stats.tab

    output consists of tab delimited fields:
name  taxa  alignlength  shortest  longest  meanlength  sd  medianlength  %X  %gaps  offmedian
'''

import sys
import os
import argparse
import time
import re
from glob import glob
from collections import defaultdict
import numpy
from Bio import SeqIO

def get_species_dict(speciesfile, dbformat):
	speciesdict = {} # keys are numbers, values are formatted species names
	print >> sys.stderr, "# Reading species list", time.asctime()
	for line in open(speciesfile,'r'):
		line = line.rstrip()
		if line and not line[0]=="#": # remove empty and comment lines
			lsplits = line.split("\t")
			if dbformat=="nog" or dbformat=="NOG":
				taxid = lsplits[1]
				rawspecies = lsplits[0]
			elif dbformat=="oma" or dbformat=="OMA":
				taxid = lsplits[0]
				rawspecies = lsplits[2]
			else:
				taxid = lsplits[0]
				rawspecies = lsplits[1]
			resp = re.search("(\w+) (\w+)", rawspecies)
			try:
				formatspecies = "{}_{}".format(resp.group(1), resp.group(2))
			except AttributeError: # for weird ones where it does not match
				formatspecies = rawspecies.replace(" ","_").replace("(","").replace(")","")
			speciesdict[taxid] = formatspecies
	return speciesdict

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-l','--alignments', nargs="*", help="alignment files or directory")
	parser.add_argument('-d','--delimiter', default='.', help="delimiter for species [.]")
	parser.add_argument('-m','--median', type=float, default=0.15, help="allowable deviation from median [0.15]")
	parser.add_argument('-M','--mrange', action="store_true", help="run through range of median lengths")
	parser.add_argument('-b','--bad-count', type=int, default=10, help="minimum number of bad proteins before reporting [10]")
	parser.add_argument('-g','--gaps', help="skip NOGs whose alignments have this fraction of gaps [0.8]", default=0.8)
	parser.add_argument('-E','--ensembl', action="store_true", help="split fasta IDs for EnsEMBL formats")
	parser.add_argument('-F','--db-format', default="nog", help="database format, as nog, oma or orthodb [nog]")
	parser.add_argument('-H','--header', action="store_true", help="include header line")
	parser.add_argument('-O','--OMA', action="store_true", help="split fasta IDs for OMA formats")
	parser.add_argument('-c','--column', type=int, default=1, help="column for sorting [1]")
	parser.add_argument('-q','--quiet', action="store_true", help="suppress warnings")
	parser.add_argument('-r','--reverse', action="store_true", help="reverse sorting")
	parser.add_argument('-s','--species-list', help="NOG species list, for replacing nubmers with species")
	args = parser.parse_args(argv)

	if os.path.isdir(args.alignments[0]):
		print >> sys.stderr, "# Reading alignments from directory {}".format(args.alignments[0]), time.asctime()
		globstring = "{}*".format(args.alignments[0])
		alignfiles = glob(globstring)
	elif os.path.isfile(args.alignments[0]):
		print >> sys.stderr, "# Reading alignments", time.asctime()
		alignfiles = args.alignments
	else:
		print >> sys.stderr, "WARNING: Unknown alignment files, exiting", time.asctime()
		sys.exit()

	# if no species list, use an empty dict, so that calls of get() will return the default
	speciesdict = get_species_dict(args.species_list, args.db_format) if args.species_list else {}

	# to analyze a range of median values for sensitivity tests
	mvalslist = [x/100.0 for x in range(5,101,5)] if args.mrange else [args.median]
	
	aligncount = 0
	xcounts = defaultdict(int)
	tooshortcounts = defaultdict( lambda: [0]*len(mvalslist) )
	toolongcounts = defaultdict( lambda: [0]*len(mvalslist) )
	genecounts = defaultdict(int)
	writeouts = []
	print >> sys.stderr, "# Parsing alignments", time.asctime()
	for af in alignfiles:
		aligncount += 1
		if not aligncount % 1000:
			sys.stderr.write(".")
		if not aligncount % 10000:
			print >> sys.stderr, aligncount, time.asctime()
		filename = os.path.basename(af)
		gapcount = 0
		poscount = 0
		xcount = 0
		lenlist = [] # list of tuples of ( speciesID , gapless length )
		alilen = [] # list of integers of alignment length
		taxacount = 0
		for sr in SeqIO.parse(af,'fasta'):
			taxacount += 1
			seqlen = len(str(sr.seq))
			alilen.append(seqlen)
			# count X and gaps
			gapcount += str(sr.seq).count("-")
			xinseq = str(sr.seq).count("X")
			xcount += xinseq
			if xinseq:
				xcounts[speciesid] += 1

			nogaplen = len(str(sr.seq).replace("-",""))
			poscount += nogaplen
			if args.ensembl: # for EnsEMBL format, where IDs have no .
				idsplits = sr.id.split("|",3)
				speciesid = idsplits[2] # should be species name
				protid = idsplits[2].replace("|","_").replace(".","_")
			elif args.OMA:
				speciesid = sr.id.strip()[0:5]
			else: # for NOG format
				try:
					speciesid, protid = sr.id.split(args.delimiter,1)
				except ValueError: # catch for anything else
					speciesid = sr.id
			sppostup = (speciesid, nogaplen)
			lenlist.append(sppostup)
		# calculate general stats for the alignment
		lengthsonly = [x[1] for x in lenlist]
		mean = numpy.mean(lengthsonly)
		std = numpy.std(lengthsonly)
		median = numpy.median(lengthsonly)
		shortest = min(lengthsonly)
		longest = max(lengthsonly)
		pctx = float(xcount)/poscount*100
		pctgap = float(gapcount)/sum(alilen)*100

		if len(set(alilen))>1 and not args.quiet:
			print >> sys.stderr, "WARNING Alignments of different lengths for {}".format(af)
		alignlength = alilen[0]

		badcount = 0
		for s,l in lenlist:
			genecounts[s] += 1
			for i, medianvalue in enumerate(mvalslist):
				if l > (median*(1+medianvalue)):
					badcount += 1
					toolongcounts[s][i] += 1
				elif l < (median*(1-medianvalue)):
					badcount += 1
					tooshortcounts[s][i] += 1

		writeout = [filename, taxacount, alignlength, shortest, longest, mean, std, median, pctx, pctgap, badcount]
		writeouts.append(writeout)

	if args.header:
		#                  0      1    2       3       4      5    6     7      8     9       10
		print >> wayout, "Name\ttaxa\tAlign\tShort\tLongest\tMean\tSD\tMedian\tPctX\tPctgap\tBad"

	for writeout in sorted(writeouts, reverse=args.reverse, key=lambda x: x[args.column] ):
		print >> wayout, "{}\t{}\t{}\t{}\t{}\t{:.1f}\t{:.2f}\t{:.1f}\t{:.1f}\t{:.1f}\t{}".format(*writeout)

	print >> sys.stderr, "Counted {} alignments".format(aligncount), time.asctime()
	print >> sys.stderr, "### FLAGGED SPECIES BY LENGTH ###", time.asctime()
	if len(mvalslist) > 1:
		longnames = ["LONG{}".format(int(x*100)) for x in mvalslist]
		shortnames = ["SHORT{}".format(int(x*100)) for x in mvalslist]
		print >> sys.stderr, "SPECIES\tTOTAL\t{}\t{}".format( "\t".join(longnames), "\t".join(shortnames) )
		for k,v in sorted(toolongcounts.items(), key=lambda x: x[1][0], reverse=True):
			print >> sys.stderr, "{}\t{}\t{}\t{}".format(speciesdict.get(k,k), genecounts[k], "\t".join(str(x) for x in v), "\t".join(str(y) for y in tooshortcounts[k]) )
	else:
		print >> sys.stderr, "# SPECIES   TOTAL   LONG{0}   PERCENT   SHORT{0}   PERCENT".format( int(args.median*100) )
		for k,v in sorted(toolongcounts.items(), key=lambda x: x[1][0], reverse=True):
			if v > args.bad_count:
				print >> sys.stderr, "{}\t{}\t{}\t{:.3f}\t{}\t{:.3f}".format(speciesdict.get(k,k), genecounts[k], v[0], 100.0*v[0]/genecounts[k], tooshortcounts.get(k,[0])[0], 100.0*tooshortcounts.get(k,[0])[0]/genecounts[k])
	print >> sys.stderr, "### FLAGGED SPECIES BY X COUNTS ###", time.asctime()
	print >> sys.stderr, "# SPECIES   FLAGS   TOTAL   PERCENT #", time.asctime()
	for k,v in sorted(xcounts.items(), key=lambda x: x[1], reverse=True):
		print >> sys.stderr, "{}\t{}\t{}\t{:.3f}".format(speciesdict.get(k,k), v, genecounts[k], 100.0*v/genecounts[k])

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
