#!/usr/bin/env python
#
# collectannotationinfo.py v1.0 created 2015-06-05

'''
collectannotationinfo.py v1.1 last modified 2016-08-05

  # EXAMPLE USAGE
collectannotationinfo.py -g contigs.fasta -E low_cov_contigs -S rnaseq_ss_stringtie.gtf --stringtie-td transdecoder/ss_stringtie.fasta.transdecoder.bed --stringtie-bx6 rnaseq_ss_stringtie.blastx.ref.tab rnaseq_ss_stringtie.blastx.swiss.tab -b ref_proteins.fasta ~/db/uniprot_sprot.fasta > annotation_w_uniprot.tab
'''

# BED file format:
# taken from https://genome.ucsc.edu/FAQ/FAQformat
#
# chrom, chromStart, chromEnd, name, score, strand, thickStart, thickEnd, itemRgb, blockCount, blockSizes, blockStarts
# chrom - The name of the chromosome (e.g. chr3, chrY, chr2_random) or scaffold (e.g. scaffold10671).
# chromStart - The starting position of the feature in the chromosome or scaffold. The first base in a chromosome is numbered 0.
# chromEnd - The ending position of the feature in the chromosome or scaffold. The chromEnd base is not included in the display of the feature.  For example, the first 100 bases of a chromosome are defined as chromStart=0, chromEnd=100, and span the bases numbered 0-99. 
#
# The 9 additional optional BED fields are:
# name - Defines the name of the BED line. This label is displayed to the left of the BED line in the Genome Browser window when the track is open to full display mode or directly to the left of the item in pack mode.
# score - A score between 0 and 1000. If the track line useScore attribute is set to 1 for this annotation data set, the score value will determine the level of gray in which this feature is displayed (higher numbers = darker gray).
# strand - Defines the strand - either '+' or '-'.
# thickStart - The starting position at which the feature is drawn thickly (for example, the start codon in gene displays). When there is no thick part, thickStart and thickEnd are usually set to the chromStart position.
# thickEnd - The ending position at which the feature is drawn thickly (for example, the stop codon in gene displays).
# itemRgb - An RGB value of the form R,G,B (e.g. 255,0,0). If the track line itemRgb attribute is set to "On", this RBG value will determine the display color of the data contained in this BED line. NOTE: It is recommended that a simple color scheme (eight colors or less) be used with this attribute to avoid overwhelming the color resources of the Genome Browser and your Internet browser.
# blockCount - The number of blocks (exons) in the BED line.
# blockSizes - A comma-separated list of the block sizes. The number of items in this list should correspond to blockCount.
# blockStarts - A comma-separated list of block starts. All of the blockStart positions should be calculated relative to chromStart. The number of items in this list should correspond to blockCount.

# from ftp://selab.janelia.org/pub/software/hmmer3/3.1b2/Userguide.pdf
#(12) c-Evalue: The conditional E-value, a permissive measure of how reliable this particular domain may be.  The conditional E-value is calculated on a smaller search space than the independent E-value.  The conditional E-value uses the number of targets that pass the reporting thresholds.  The null hypothesis test posed by the conditional E-value is as follows.   Suppose that we believe that there is already sufficient evidence (from other domains) to identify the set of reported sequences as homologs of our query; now, how many additional domains would we expect to find with at least this particular domain's bit score, if the rest of those reported sequences were random nonhomologous sequence (i.e. outside the other domain(s) that were sufficient to identified them as homologs in the first place)?
#(13) i-Evalue: The independent E-value, the E-value that the sequence/profile comparison would have received if this were the only domain envelope found in it, excluding any others.  This is a stringent measure  of  how  reliable  this  particular  domain  may  be.   The  independent  E-value  uses  the  total number of targets in the target database.

import sys
import os
import time
import re
import argparse
from itertools import chain
from collections import defaultdict,namedtuple
from Bio import SeqIO

SWISSPROT = "uniprot_sprot.fasta"

def get_exclusion_bool(contig, exclusion_dict):
	return exclusion_dict.get(contig, False)

def parse_swissprot_header(hitstring):
	# hitstring can conveniently be taken from seq_record.description
	# for example
	# swissprotdict.next().description
	# 'sp|Q6GZX4|001R_FRG3G Putative transcription factor 001R OS=Frog virus 3 (isolate Goorha) GN=FV3-001R PE=4 SV=1'
	#
	# from the swissprot website http://www.uniprot.org/help/fasta-headers
	# fasta headers appear as:
	# >db|UniqueIdentifier|EntryName ProteinName OS=OrganismName[ GN=GeneName]PE=ProteinExistence SV=SequenceVersion
	#
	# Where:
	#
    # db is 'sp' for UniProtKB/Swiss-Prot and 'tr' for UniProtKB/TrEMBL.
    # UniqueIdentifier is the primary accession number of the UniProtKB entry.
    # EntryName is the entry name of the UniProtKB entry.
    # ProteinName is the recommended name of the UniProtKB entry as annotated in the RecName field. For UniProtKB/TrEMBL entries without a RecName field, the SubName field is used. In case of multiple SubNames, the first one is used. The 'precursor' attribute is excluded, 'Fragment' is included with the name if applicable.
    # OrganismName is the scientific name of the organism of the UniProtKB entry.
    # GeneName is the first gene name of the UniProtKB entry. If there is no gene name, OrderedLocusName or ORFname, the GN field is not listed.
    # ProteinExistence is the numerical value describing the evidence for the existence of the protein.
    # SequenceVersion is the version number of the sequence.
	#
	# Examples:
	#
	# >sp|Q8I6R7|ACN2_ACAGO Acanthoscurrin-2 (Fragment) OS=Acanthoscurria gomesiana GN=acantho2 PE=1 SV=1
	# or
	# sp|Q9GZU7|CTDS1_HUMAN Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1 OS=Homo sapiens GN=CT DSP1 PE=1 SV=1

	# assuming the format of "OS=Homo sapiens"
	organismre = "OS=(\w+) (\w+)"
	# extract genus and species as groups()
	try:
		osgroups = re.search(organismre,hitstring).groups()
	# in case of Nonetype for some reason, possibly weird names of bacteria or viruses
	# such as:
	# sp|P07572|POL_MPMV Pol polyprotein OS=Mason-Pfizer monkey virus GN=pol PE=3 SV=1
	except AttributeError:
		osgroups = re.search("OS=([\w-]+) (\w+)",hitstring).groups()
	# print in format of H.sapiens
	osname = "{}.{}".format(osgroups[0][0], osgroups[1])

	genedescRE = "(.+) OS="
	# split will take everything after the first space, so:
	# "Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1 OS=Homo sapiens GN=CT DSP1 PE=1 SV=1"
	nospnumber = hitstring.split(' ',1)[1]
	# this should pull everything before the species excluding " OS=", so:
	# "Carboxy-terminal domain RNA polymerase II polypeptide A small phosphatase 1"
	genedesc = re.search(genedescRE, nospnumber).groups()[0]
	# this should specifically remove the annotation "(Fragment)" for some proteins
	genedesc = genedesc.replace("(Fragment)","")
	# convert all remaining spaces to underscores, so:
	# Carboxy-terminal_domain_RNA_polymerase_II_polypeptide_A_small_phosphatase_1
	gdwithdashes = genedesc.replace(" ","_")
	gdwithdashes = gdwithdashes.replace("'","") # remove ' which causes problems for import
	# join finally with the species, so:
	# H.sapiens_Carboxy-terminal_domain_RNA_polymerase_II_polypeptide_A_small_phosphatase_1
	swissstring = "{}_{}".format(osname, gdwithdashes)
	return swissstring

def parse_stringtie_gtf(stringtiegtf, contig_transcripts, excludedict=None):
	print >> sys.stderr, "# Parsing StringTie output {}".format(stringtiegtf), time.asctime()
	# parse attributes as:
	# gene_id "stringtie_ss.1"; transcript_id "stringtie_ss.1.1"; cov "3.099174";FPKM "0.203585";
	attributere = 'transcript_id "([\w.-|]+)"; cov "([\d.]+)"; FPKM "([\d.]+)";'
	transcript_exons = defaultdict(int)
	transcript_length = defaultdict(int)
	stringtie_output = {}
	for line in open(stringtiegtf,'r').readlines():
		line = line.strip()
		if line and not line[0]=="#": # ignore comment lines
			lsplits = line.split("\t")
			genomiccontig = lsplits[0]
			if excludedict and excludedict.get(genomiccontig, False):
				continue
			if lsplits[2] == "transcript":
				attributes = lsplits[8]
				transcript_id, stcov, stfpkm = re.search(attributere, attributes).groups()
				stringtie_output[transcript_id] = "{}\t{}\t{}\t{}\t{}\t{}".format(transcript_id, stcov, stfpkm, lsplits[3], lsplits[4], lsplits[6] )
				contig_transcripts[genomiccontig].append(transcript_id)
			elif lsplits[2] == "exon":
				transcript_exons[transcript_id] += 1
				exonlength = int(lsplits[4])-int(lsplits[3])+1
				transcript_length[transcript_id] += exonlength
	for tid in stringtie_output: # after all counting, add exon count and total length
		stringtie_output[tid] = "{}\t{}\t{}".format(stringtie_output.get(tid), transcript_exons.get(tid), transcript_length.get(tid) )
	print >> sys.stderr, "# Found {} StringTie transcripts".format(len(stringtie_output) ), time.asctime()
	return stringtie_output, contig_transcripts

def parse_transdecoder(tdfile, keepstrand=False):
	print >> sys.stderr, "# Parsing TransDecoder bed output {}".format(tdfile), time.asctime()
	category_transdec_d = {}
	for line in open(tdfile):
		line = line.strip()
		if line and not line[0]=="#": # ignore comment lines
			lsplits = line.split("\t")
			if len(lsplits) > 1: # header line would not be tab delimited, and can be removed
				td_name = lsplits[3].rsplit(":",2)[1:]
				td_type, td_len = [x.split("_")[0] for x in td_name]
				if keepstrand:
					category_transdec_list[lsplits[0]] = "\t".join([td_len, td_type, lsplits[5] ] )
				else:
					category_transdec_d[lsplits[0]] = "\t".join([td_len, td_type] )
	# for each transcript, dictionary contains list of two items, length and completion
	# blanks are only established later, as they are not stored here
	print >> sys.stderr, "# Found {} TransDecoder proteins".format(len(category_transdec_d) ), time.asctime()
	return category_transdec_d

def parse_nog_annotations(annotationsfile, trimlength=70):
	nogdescdict = {}
	removelist = ['(', ')', ',', '.', ':', "Pfam", "Inherit from", "'"] # ' causes problems in R
	print >> sys.stderr, "# Reading NOG annotations from {}".format(annotationsfile), time.asctime()
	for line in open(annotationsfile, 'r').readlines():
		# meNOG	ENOG410VUKR	2	2	S	Sushi domain (SCR repeat)
		line = line.strip()
		lsplits = line.split("\t")
		nogid = lsplits[1]
		description = lsplits[5]
		for ri in removelist:
			description = description.replace(ri,'')
		description = description[:trimlength].replace(' ','_')
		if description=="NA":
			description="None"
		nogdescdict[nogid] = description
	print >> sys.stderr, "# Found {} annotations".format(len(nogdescdict) ), time.asctime()
	return nogdescdict

def parse_ortholog_hits(noghitsfile, nogdescdict=None):
	noghitsdict = {}
	print >> sys.stderr, "# Reading NOG hits from {}".format(noghitsfile), time.asctime()
	for line in open(noghitsfile, 'r').readlines():
		# meNOG	ENOG410VUKR	2	2	S	Sushi domain (SCR repeat)
		line = line.strip()
		transid, nogmatches = line.split("\t")
		nogline = nogmatches.split("|")[0]
		if nogdescdict:
			nogid = nogline.split("_")[0]
			nogline += "_{}".format( nogdescdict.get(nogid,"None") )
		noghitsdict[transid] = nogline # only take first NOG match
	print >> sys.stderr, "# Found {} annotations".format(len(noghitsdict) ), time.asctime()
	return noghitsdict

def parse_transcript_dnds(dndsstats):
	dndsbytrans = {}
	print >> sys.stderr, "# Reading dN/dS stats from {}".format(dndsstats), time.asctime()
	for line in open(dndsstats, 'r').readlines():
		line = line.strip()
		lsplits = line.split("\t")
		transid = lsplits[0]
		mutationcount = sum(int(x) for x in lsplits[1:5])
		mutratio = lsplits[6]
		loflist = []
		dnlist = lsplits[7].split(",")
		for dn in dnlist:
			if dn[0]=="*" or dn[-1]=="*":
				loflist.append(dn)
		lofstring = "|".join(loflist) if loflist else "No LoF"
		dndsbytrans[transid] = "\t".join([str(mutationcount), mutratio, lofstring])
	print >> sys.stderr, "# Found dN/dS stats for {} transcripts".format(len(dndsbytrans) ), time.asctime()
	return dndsbytrans

def parse_tabular_blast(blasttabfile, evaluecutoff, blast_seq_dict=None):
	print >> sys.stderr, "# Parsing tabular blastx output {}".format(blasttabfile), time.asctime()
	BlastbyTransdict = {}
	evalueRemovals = 0
	for line in open(blasttabfile, 'r').readlines():
		line = line.strip()
		lsplits = line.split("\t")
		# qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore
		queryseq = lsplits[0]
		if not queryseq in BlastbyTransdict:
			if float(lsplits[10]) > evaluecutoff: # remove matches by evalue
				evalueRemovals += 1
				continue
			if blast_seq_dict:
				try:
					sbjlen = len(blast_seq_dict[lsplits[1]].seq)
					sbjcov = "{:.2f}".format(float(lsplits[3]) / sbjlen )
				except KeyError:
					print >> sys.stderr, "# Key {} not found, check database".format(lsplits[1]), time.asctime()
					sjbcov = lsplits[3]
			else:
				sjbcov = lsplits[3]
			# if using swissprot, reassign the sequence name
			subjectid = lsplits[1]
			if subjectid.split("|")[0]=="sp": # detect SwissProt header
				try:
					subjectid = "{}_{}".format(lsplits[1].split("|")[2] , parse_swissprot_header(blast_seq_dict[lsplits[1]].description) )
				except KeyError:
					pass
			# make string for output
			BlastbyTransdict[queryseq] = "{}\t{}\t{}\t{}\t{}".format(subjectid, lsplits[10].strip(), lsplits[11], sbjcov, lsplits[2] )
	print >> sys.stderr, "# Found blast hits for {} sequences".format(len(BlastbyTransdict)), time.asctime()
	print >> sys.stderr, "# Removed {} hits by evalue".format(evalueRemovals), time.asctime()
	return BlastbyTransdict

def parse_pfam_domains(pfamtabular, evaluecutoff=1e-1):
	print >> sys.stderr, "# Parsing hmmscan PFAM tabular {}".format(pfamtabular), time.asctime()
	PFAMbyTransdict = defaultdict(list)
	bestdomainbytrans = defaultdict(dict)
	evalueRemovals = 0
	for line in open(pfamtabular, 'r').readlines():
		if line[0]=="#": # skip comment lines
			continue
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
		line = line.strip()
		lsplits = line.split()
		targetname = lsplits[0]
		pfamacc = lsplits[1].rsplit('.',1)[0] # accs as PF00530.13, so chop off .13
		queryid = lsplits[3].rsplit('|',1)[0] # chop transcript from transdecoder peptide
		evalue = float(lsplits[11]) # or [6] for full seq or [12]
		if evalue >= evaluecutoff: # then skip sequence
			evalueRemovals += 1
			continue
		if evalue < bestdomainbytrans[queryid].get(pfamacc, [1.0,None] )[0]:
			bestdomainbytrans[queryid][pfamacc] = (evalue, targetname)
	for trans, domdict in sorted(bestdomainbytrans.items(), key=lambda x: x[0]):
		domlist = []
		for domid, evnametup in domdict.iteritems():
			domlist.append("{}_{}_{}".format(domid, *reversed(evnametup) ) )
		PFAMbyTransdict[trans] = "|".join(domlist)
	print >> sys.stderr, "# Found domains for {} transcripts".format(len(PFAMbyTransdict)), time.asctime()
	print >> sys.stderr, "# Removed {} hits by evalue".format(evalueRemovals), time.asctime()
	return PFAMbyTransdict

def extend_table_items(item_dict, extension_dict, nonelength):
	'''directly extend tabular string by adding dictionary string'''
	for seqid in item_dict.iterkeys():
		item_dict[seqid] += "\t{}".format(extension_dict.get(seqid, "\t".join(["None"]*nonelength ) ) )
	# probably do not need to return

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-g','--genome', help="fasta format genomic contigs")
	parser.add_argument('-c','--coverage', help="coverage of genomic contigs")
	parser.add_argument('-E','--exclude', help="file of list of bad contigs")
	parser.add_argument('-b','--blast', nargs='*', help="protein reference files for blastx")
	parser.add_argument('-t','--new-trinity', help="use new trinity annotation style")
	parser.add_argument('-D','--domains', help="PFAM domain information as hmmscan tabular")
	parser.add_argument('--ortho-anno', help="ortholog annotations file, as .tsv")
	parser.add_argument('-P','--pasa', help="PASA/Trinity gtf output")
	parser.add_argument('--pasa-td', help="PASA transdecoder bed output file (should be stranded)")
	parser.add_argument('--pasa-rsem', help="PASA RSEM isoform results file")
	parser.add_argument('--pasa-st', help="PASA vs StringTie blastn output 6 results")
	parser.add_argument('-S','--stringtie', help="StringTie gtf output")
	parser.add_argument('--stringtie-td', help="StringTie transdecoder bed output file (should be stranded)")
	parser.add_argument('--stringtie-dnds', help="StringTie dN/dS from classifymutationstats")
	parser.add_argument('--stringtie-ortho', help="StringTie parsed HMM ortholog info from NOGs")
	parser.add_argument('--stringtie-bx6', nargs='*', help="StringTie blastx output 6 results, compiled into one file")
	parser.add_argument('--stringtie-bn6', help="StringTie blastn rRNA/mito output 6 results")
	parser.add_argument('--td-unstranded', action="store_false", help="TransDecoder was not stranded")
	parser.add_argument('-e','--evalue', type=float, default=1e-4, help="evalue cutoff for post blast filtering [1e-4]")
	args = parser.parse_args(argv)

	startclock=time.asctime()
	starttime= time.time()

	# this is the dynamic list of column headers, items appended as needed
	headers = []

	if args.exclude:
		print >> sys.stderr, "# Reading exclusion list {}".format(args.exclude), time.asctime()
		exclusion_dict = {}
		for term in open(args.exclude,'r').readlines():
			exclusion_dict[term.rstrip()] = True
		print >> sys.stderr, "# Found {} contigs to exclude".format(len(exclusion_dict) ), time.asctime()
	else:
		exclusion_dict = None

	print >> sys.stderr, "# Parsing genomic contigs {}".format(args.genome), time.asctime()
	genomic_contigs_seq_d = SeqIO.to_dict(SeqIO.parse(args.genome,'fasta') )
	headers.extend(["Contig", "Contig length"])
	genomic_contig_annotations = {}
	for seqid, seqrec in genomic_contigs_seq_d.iteritems():
		if args.exclude and exclusion_dict.get(seqid, False):
			continue
		genomic_contig_annotations[seqid] = "{}\t{}".format(seqid, len(str(seqrec.seq)) )
	print >> sys.stderr, "# Found {} contigs".format(len(genomic_contig_annotations) ), time.asctime()

	if args.blast: # all blast databases, reference species, swissprot, etc.
		blastx_seq_d = {}
		for blastxfile in args.blast:
			print >> sys.stderr, "# Reading reference blast db {}".format(blastxfile), time.asctime()
			blastx_seqs = SeqIO.to_dict(SeqIO.parse(blastxfile,'fasta') )
			print >> sys.stderr, "# Found {} db entries".format(len(blastx_seqs) ), time.asctime()
			blastx_seq_d.update(blastx_seqs)
	else:
		blastx_seq_d = None

	if args.ortho_anno:
		nogannotationdict = parse_nog_annotations(args.ortho_anno)
	else:
		nogannotationdict = None

	transbycontig = defaultdict(list)
	transcripts_output = defaultdict(str)
	if args.pasa:
		print >> sys.stderr, "# Parsing PASA output {}".format(args.pasa), time.asctime()
		pasa_headers = ["Transcript", "StartPos", "Strand", "Exons", "Length"]
		pasa_params = {}
		for line in open(args.pasa,'r').readlines():
			line = line.strip()
			lsplits = line.split("\t")
			if lsplits[2] == "transcript":
				attrsplits = lsplits[8].split(";")
				transcript_id = attrsplits[1].strip().split(" ")[1].replace('"','')
				pasa_params[transcript_id] = ss_transcript(cov="None", fpkm="None", start=lsplits[3], end=lsplits[4], strand=lsplits[6] )
				genomic_contig_transcripts[lsplits[0]].append(transcript_id)
			elif lsplits[2] == "exon":
				transcript_exons[transcript_id] += 1
				exonlength = int(lsplits[4])-int(lsplits[3])+1
				transcript_length[transcript_id] += exonlength
		if args.pasa_td:
			transdecoder_headers = ["TransDecoder length", "Completeness"] #, "Strand"]
			pasa_headers.extend(transdecoder_headers)
			pasa_transdec_list = parse_transdecoder(args.pasa_td)
			for pok in pasa_params:
				transcripts_output[pok].extend(pasa_transdec_list.get(pok,["None"]*len(transdecoder_headers) ) )

	if args.stringtie:
		stringtie_headers = ["Transcript", "Coverage", "FPKM", "StartPos", "EndPos", "Strand", "Exons", "Length"]
		transcripts_output, transbycontig = parse_stringtie_gtf(args.stringtie, transbycontig, exclusion_dict)

		if args.stringtie_td:
			if args.td_unstranded:
				transdecoder_headers = ["TransDecoder length", "Completeness"] #, "Strand"]
			else:
				transdecoder_headers = ["TransDecoder length", "Completeness", "Strand"]
			stringtie_headers.extend(transdecoder_headers)
			stringtie_transdec_d = parse_transdecoder(args.stringtie_td)
			extend_table_items(transcripts_output, stringtie_transdec_d, len(transdecoder_headers) )

		if args.stringtie_dnds:
			dnds_headers = ["Mutations","Ratio","LoFs"]
			stringtie_headers.extend(dnds_headers)
			dndsdict = parse_transcript_dnds(args.stringtie_dnds)
			extend_table_items(transcripts_output, dndsdict, len(dnds_headers) )

		if args.stringtie_bx6:
			for blasttabfile in args.stringtie_bx6:
				blastx_headers = ["Blastx subject", "Evalue", "Bitscore", "Subj cov", "Pid"]
				stringtie_headers.extend(blastx_headers)
				stringtie_blast_dict = parse_tabular_blast(blasttabfile, args.evalue, blastx_seq_d)
				extend_table_items(transcripts_output, stringtie_blast_dict, len(blastx_headers) )

		if args.stringtie_ortho:
			nog_headers = ["NOG Matches"]
			stringtie_headers.extend(nog_headers)
			nogsbytrans = parse_ortholog_hits(args.stringtie_ortho, nogannotationdict)
			extend_table_items(transcripts_output, nogsbytrans, len(nog_headers) )

		if args.domains:
			pfam_headers = ["PFAM Domains"]
			stringtie_headers.extend(pfam_headers)
			domainsbytrans = parse_pfam_domains(args.domains)
			extend_table_items(transcripts_output, domainsbytrans, len(pfam_headers) )

		# after all options are checked, append the final headers to the main list
		headers.extend(stringtie_headers)

	### WRITE FINAL OUTPUT ###
	print >> wayout, "\t".join(headers)
	for k in sorted(genomic_contig_annotations.keys(), key=lambda x: int(x.split("_")[1]) ):
		# if there are transcripts, append those to each contig
		if len(transbycontig[k]) > 0:
			for trs in sorted(transbycontig[k], key=lambda x: int(x.split(".")[1]) ):
				print >> wayout, "{}\t{}".format(genomic_contig_annotations[k], transcripts_output[trs] )
		else:
			print >> wayout, "{}\t{}".format(genomic_contig_annotations[k], "\t".join(["None"]*len(stringtie_headers) ) )

	print >> sys.stderr, "# Processed completed in %.1f minutes" % ((time.time()-starttime)/60)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
