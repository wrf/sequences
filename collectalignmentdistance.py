#!/usr/bin/env python
#
# collectalignmentdistance.py

'''collectalignmentdistance.py  last modified 2016-04-27

    collect values from alignment distance matrices into a single table

collectalignmentdistance.py -i gene*.dist -g genes.gtf > distance_table.tsv

    TO GENERATE DISTANCE MATRICES FROM ALIGNMENTS WITH CLUSTAL OMEGA
clustalo -i gene_001_prot.fasta -o gene_001_prot.aln --percent-id --full --distmat-out=gene_001_prot.dist
'''

import sys
import os
import argparse
import time
from collections import defaultdict

# matrices look like:
# 4
# H13_ref 100.000000 99.691834 99.383667 78.120185
# H13_B   99.691834 100.000000 99.075501 77.812018
# H13_A   99.383667 99.075501 100.000000 78.120185
# Triad1  78.120185 77.812018 78.120185 100.000000

def parse_matrix(distancematrix, chopname):
	distancedict = defaultdict(dict)
	# assumes symmetrical distance matrix
	seqorder = []
	numentries = 0
	for line in open(distancematrix, 'r').readlines():
		line = line.strip()
		if not numentries:
			numentries = int(line)
		else:
			lsplits = line.split()
			seqid = lsplits[0]
			if chopname:
				seqid = seqid[0:3]
			for i,seq in enumerate(seqorder):
				reorder = sorted([seqid, seq]) # sort items, so geneA and geneB combinations are always as A-B
				distancedict[reorder[0]][reorder[1]] = lsplits[i+1]
			seqorder.append(seqid)
	return distancedict

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', nargs="*", help="distance matrices from clustal omega")
	#parser.add_argument('-g','--gtf', help="gene order as GTF/GFF file")
	parser.add_argument('-c','--cut-name', action="store_true", help="sequence naming is irregular, so use the first 3 letters")
	parser.add_argument('-d','--delimiter', default='_', help="file naming should split at [_]")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	dmatbygene = {}
	print >> sys.stderr, "# Parsing distance matrices", time.asctime()
	for dismat in args.input:
		distdict = parse_matrix(dismat, args.cut_name)
		# dismat is the name of the file, so string splittable
		try: # assumes naming format as gene_g1234_protein.dist, to take g1234
			gene = os.path.basename(dismat).split(args.delimiter,1)[1]
		except IndexError:
			gene = dismat
		dmatbygene[gene] = distdict
	print >> sys.stderr, "# Done parsing {} files".format(len(dmatbygene)), time.asctime()
	if len(dmatbygene) != len(args.input):
		print >> sys.stderr, "WARNING: FOUND {} GENES FOR {} INPUT FILES, CHECK FILENAMES".format(len(dmatbygene), len(args.input) )
	header = []
	for gene,dismat in dmatbygene.iteritems():
		outlist = [gene]
		if not header:
			header = ["Gene"] # add items to this list
			for k,vd in dismat.iteritems():
				for vdk in vd.iterkeys():
					header.append("{}--{}".format(k,vdk))
			print >> wayout, "\t".join(header)
		for entry in header[1:]:
			splitentry = entry.split("--")
			outlist.append(dismat[splitentry[0]][splitentry[1]])
		print >> wayout, "\t".join(outlist)
	print >> sys.stderr, "# Finished writing distance matrix", time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
