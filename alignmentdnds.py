#!/usr/bin/env python
#
# alignmentdnds.py v1 created 2017-01-31
# v1.2 2019-03-23
# v1.3 2023-01-31 python3 update

'''alignmentdnds.py v1.3  last modified 2023-01-31
    count dN/dS for nucleotide alignments
    alignments must be back-gapped CDS from a protein alignment
    such as using regapper.py

alignmentdnds.py -n human_mouse_gene.aln -s Hsap Mmus > hsap_to_mmus_dnds.tab

    more generally, for many alignments, to compare species A and B

alignmentdnds.py -n *.nucalign -s A B > a_to_b_dnds.tab

alignmentdnds.py -i regapped_cds/*nucalign -s Hoilungia Trichoplax -d '_' -w > Hh_Ta_ortholog_mod_dnds_w_double_hits.tab

   gaps are not counted, 4-fold degenerate Ns can be counted with -n
   full output (-w) is tab-delimited format of:

    1             2               3       4         5         6        7
filename  species1,species2  samecodons  gaps  ambig-bases  N-diffs  S-diffs
       8        9         10        11   12   13   14   15   16
     N-sites  S-sites  dN/dS-ratio  dN1  dN2  dN3  dS1  dS2  dS3

   basic output omits columns 6-9

   where dN1, dN2, etc. refer to dN with single base change, double, or triple
   numbers do NOT refer to position in the codon
   dS2 can only be S R L or *, dS3 can only be S
   N-sites + S-sites should be length of shortest sequence in nucleotides
   samecodons + gaps + ambig-bases + dN1,dN2... is number of codons
'''


import sys
import argparse
import time
import os
from fractions import Fraction
from itertools import combinations
from Bio import SeqIO

def get_differences(codon1, codon2):
	'''for each codon pair, return an integer of the number of bases that are different'''
	count = 0
	for b1, b2 in zip(codon1, codon2):
		if b1 != b2:
			count += 1
	return count

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', nargs="*", help="back-gapped nucleotide alignments")
	parser.add_argument('-d','--delimiter', default="__", help="delimiter to split ID [__]")
	parser.add_argument('-n','--degenerate', action="store_true", help="allow 4-fold degenerate Ns")
	parser.add_argument('-s','--species', nargs="*", help="list of species names or IDs to compare")
	parser.add_argument('--triples', action="store_true", help="print positions and codons of dS triple mutants")
	parser.add_argument('-w','--w', action="store_true", help="give alternate 16-column output")
	args = parser.parse_args(argv)

	transtable = {
	"AAA":"K", "AAG":"K", "AAC":"N", "AAT":"N", "ACA":"T", "ACG":"T", "ACC":"T", "ACT":"T",
	"AGA":"R", "AGG":"R", "AGC":"S", "AGT":"S", "ATA":"I", "ATG":"M", "ATC":"I", "ATT":"I",
	"CAA":"Q", "CAG":"Q", "CAC":"H", "CAT":"H", "CCA":"P", "CCG":"P", "CCC":"P", "CCT":"P",
	"CGA":"R", "CGG":"R", "CGC":"R", "CGT":"R", "CTA":"L", "CTG":"L", "CTC":"L", "CTT":"L",
	"GAA":"E", "GAG":"E", "GAC":"D", "GAT":"D", "GCA":"A", "GCG":"A", "GCC":"A", "GCT":"A",
	"GGA":"G", "GGG":"G", "GGC":"G", "GGT":"G", "GTA":"V", "GTG":"V", "GTC":"V", "GTT":"V",
	"TAA":"*", "TAG":"*", "TAC":"Y", "TAT":"Y", "TCA":"S", "TCG":"S", "TCC":"S", "TCT":"S",
	"TGA":"*", "TGG":"W", "TGC":"C", "TGT":"C", "TTA":"L", "TTG":"L", "TTC":"F", "TTT":"F" }

	# for each codon, number of possible synonymous sites, relative to 3 * S/9
	# for any codon, these values are fixed, and can be stored
	# used in calculation of pS = Sd / S
	# where pS is proportion synonmyous, Sd is synonymous differences (sdiffs), S is synonymous sites (ssites, calculated from syntable here)
	# dS is derived from pS with a correction for saturation, possibly unnecessarily
	# e.g. for AAA, 1/9 of changes keep the same amino acid (K, only from AAG), so S is 1/3
	# all stop codons are 1/3
	# note that 2-fold degenerate serine-AGY is 1/3, not 1 for serine-TCN
	# values in table below are only relevant for codon table 1, see:
	# https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi
	syntable_one = {
	"AAA": Fraction(1/3), "AAC": Fraction(1/3), "AAG": Fraction(1/3), "AAT": Fraction(1/3),
	"ACA": Fraction(1), "ACC": Fraction(1), "ACG": Fraction(1), "ACT": Fraction(1),
	"AGA": Fraction(2/3), "AGC": Fraction(1/3), "AGG": Fraction(2/3), "AGT": Fraction(1/3),
	"ATA": Fraction(2/3), "ATC": Fraction(2/3), "ATG": Fraction(0), "ATT": Fraction(2/3),
	"CAA": Fraction(1/3), "CAC": Fraction(1/3), "CAG": Fraction(1/3), "CAT": Fraction(1/3),
	"CCA": Fraction(1), "CCC": Fraction(1), "CCG": Fraction(1), "CCT": Fraction(1),
	"CGA": Fraction(4/3), "CGC": Fraction(1), "CGG": Fraction(4/3), "CGT": Fraction(1),
	"CTA": Fraction(4/3), "CTC": Fraction(1), "CTG": Fraction(4/3), "CTT": Fraction(1),
	"GAA": Fraction(1/3), "GAC": Fraction(1/3), "GAG": Fraction(1/3), "GAT": Fraction(1/3),
	"GCA": Fraction(1), "GCC": Fraction(1), "GCG": Fraction(1), "GCT": Fraction(1),
	"GGA": Fraction(1), "GGC": Fraction(1), "GGG": Fraction(1), "GGT": Fraction(1),
	"GTA": Fraction(1), "GTC": Fraction(1), "GTG": Fraction(1), "GTT": Fraction(1),
	"TAA": Fraction(1/3), "TAC": Fraction(1/3), "TAG": Fraction(1/3), "TAT": Fraction(1/3),
	"TCA": Fraction(1), "TCC": Fraction(1), "TCG": Fraction(1), "TCT": Fraction(1),
	"TGA": Fraction(1/3), "TGC": Fraction(1/3), "TGG": Fraction(0), "TGT": Fraction(1/3),
	"TTA": Fraction(2/3), "TTC": Fraction(1/3), "TTG": Fraction(2/3), "TTT": Fraction(1/3) }

	if args.degenerate: # add 4-fold degenerate codons as allowed
		transtable.update({"ACN":"T", "CCN":"P", "CGN":"R", "CTN":"L", "GCN":"A", "GGN":"G", "GTN":"V", "TCN":"S" })

	specieslist = args.species
	if len(specieslist) < 2:
		sys.exit("MUST PROVIDE AT LEAST 2 SPECIES FOR PAIRWISE COMPARISON")

	for alignment in args.input:
		sys.stderr.write( "# Reading alignment from {}  {}\n".format( alignment, time.asctime() ) )
		seqdict = {} # key is species, value is string of sequence
		for seqrec in SeqIO.parse(alignment, "fasta"):
			speciesid = seqrec.id.split(args.delimiter)[1]
			if speciesid in specieslist:
				seqdict[speciesid] = str(seqrec.seq)
		if len(seqdict) < 2:
			sys.stderr.write( "ERROR: ONLY FOUND SEQUENCE FOR {} IN {}\n".format( seqdict.keys(), alignment) )
			continue
		# for all pairwise comparisons of keys, meaning species
		# (i) Count synonymous and nonsynonymous sites.
		# (ii) Count synonymous and nonsynonymous differences.
		# (iii) Calculate the proportions of differences and correct for multiple hits.
		for k1,k2 in combinations(seqdict.keys(),2):
			# initialize counters
			nsites = 0
			ssites = 0
			ndiffs = 0
			sdiffs = 0
			same = 0
			gaps = 0
			ambig = 0
			#totalcodons = 0 # should be same as dn + ds + same + gaps
			dNmuts = {1:0, 2:0, 3:0} # counter of nucleotide changes per codon for non synonymous
			dSmuts = {1:0, 2:0, 3:0} # same for synonymous
			for i in xrange(0, max([len(seqdict[k1]),len(seqdict[k2]) ] ), 3):
				codon1, codon2 = seqdict[k1][i:i+3], seqdict[k2][i:i+3]
			#	totalcodons += 1 
				gaps1, gaps2 = codon1.count("-"), codon2.count("-")
				if gaps1 or gaps2:
					gaps += 1 # one amino acid is gap
					if (gaps1 and gaps1 < 3) or (gaps2 and gaps2 < 3):
						sys.stderr.write( "WARNING: CODON {},{} IN {} NOT GAPPED CORRECTLY\n".format(codon1, codon2, alignment) )
				else: # meaning no gaps
					# step (i), get average of syn sites of the two codons,
					# by definition, nonsyn sites is (3 - syn sites)
					try:
						synsites = float(syntable_one[codon1] + syntable_one[codon2]) / 2
					except KeyError: # in case of bizarre codon with Ns
						continue
					ssites += synsites
					nsites += (3-synsites)

					# (ii) calculate differences
					if codon1 == codon2: # codons are the same, no dN or dS
						same += 1
					else: # codons not the same
						aa1 = transtable.get(codon1,None)
						aa2 = transtable.get(codon2,None)
						changes = get_differences(codon1, codon2)
						if aa1 is None or aa2 is None: # meaning contains Ns, or non-4-fold-degenerate
							ambig += 1
						else: # both are real codons
							if aa1 == aa2: # meaning dS
								# exception for double/triple hit serine
								if aa1=="S" and changes >= 2:
									ndiffs += changes
									dNmuts[changes] += 1
								else:
								# changes should be 1, except for 6-fold L and R, which can be 2
								# almost everything else with changes > 1 is non-synonymous
									sdiffs += changes
									dSmuts[changes] += 1
								# optionally track triple hits
								if args.triples and changes==3:
									sys.stderr.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format( alignment, k1,k2, i, aa1, aa2, codon1, codon2 ) )
							else: # meaning dN
								# using ndiffs += changes, assumes most costly path
								### TODO calculate Sd and Nd for all codon changes
								ndiffs += changes
								dNmuts[changes] += 1
			# pS = Sd / S, Sd is sdiffs, S is ssites
			# pN = Nd / N
			# (iii) ds is approximated from pS without correction
			# correction is flawed, better to have more intermediate taxa
			dn = 1.0 * ndiffs / nsites
			ds = 1.0 * sdiffs / ssites
			if args.w: # trim file names, include diffs and sites
				sys.stdout.write( "{}\t{},{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{:.3f}\t{}\t{}\n".format( os.path.basename(alignment).split("_")[1], k1,k2, same, gaps, ambig, ndiffs, sdiffs, nsites, ssites, 1.0*dn/ds, "\t".join(str(n) for n in dNmuts.values()), "\t".join(str(n) for n in dSmuts.values()) ) )
			else: # basic output
				sys.stdout.write( "{}\t{},{}\t{}\t{}\t{}\t{}\t{}\t{:.3f}\t{}\t{}\n".format( alignment, k1,k2, same, gaps, ambig, dn, ds, 1.0*dn/ds, "\t".join(str(n) for n in dNmuts.values()), "\t".join(str(n) for n in dSmuts.values()) ) )
			#	print >> sys.stdout, totalcodons, same+dn+ds+gaps

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
