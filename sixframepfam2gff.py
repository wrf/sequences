#!/usr/bin/env python
#
# sixframepfam2gff.py v1.0 created 2016-03-07
#
# Sequence Ontology terms from:
# https://github.com/The-Sequence-Ontology/SO-Ontologies/blob/master/subsets/SOFA.obo

'''
sixframepfam2gff.py  last modified 2016-03-16

    convert protein coordinates to genome nucleotide coordinates
sixframepfam2gff.py -i 6frame.pfam.tab -g scaffolds.fasta > 6frame.pfam.gtf

    GENERATE PFAM TABULAR BY:
hmmscan --cpu 4 --domtblout 6frame.pfam.tab ~/PfamScan/data/Pfam-A.hmm scaffolds.6frame.faa > 6frame.pfam.log

    GENERATE 6-FRAME TRANSLATION BY:
prottrans.py -t 6 scaffolds.fasta > scaffolds.6frame.faa
'''

import sys
import time
import argparse
import re
from collections import defaultdict
from Bio import SeqIO

def make_seq_length_dict(contigsfile):
	print >> sys.stderr, "# Parsing genomic contigs {}".format(contigsfile), time.asctime()
	lengthdict = {}
	for seqrec in SeqIO.parse(contigsfile,'fasta'):
		lengthdict[seqrec.id] = len(seqrec.seq)
	print >> sys.stderr, "# Found {} contigs".format(len(lengthdict)), time.asctime()
	return lengthdict

def parse_pfam_domains(pfamtabular, evaluecutoff, programname, outputtype, donamechop, lengthdict):
	'''parse domains from hmm domtblout and write to stdout as protein gff or genome gff'''
	print >> sys.stderr, "# Parsing hmmscan PFAM tabular {}".format(pfamtabular), time.asctime()
	domaincounter = 0
	protnamedict = {}
	evalueRemovals = 0
	for line in open(pfamtabular, 'r').readlines():
		line = line.strip()
		if not line or line[0]=="#": # skip comment lines
			continue # also catch for empty line, which would cause IndexError
		domaincounter += 1
		lsplits = line.split()
#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
#      0                 1         2     3                    4         5        6       7     8     9  10   11       12        13     14    15     16   17     18   19     20  21  22
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
		targetname = lsplits[0]
		pfamacc = lsplits[1].rsplit('.',1)[0] # accs as PF00530.13, so chop off .13
		scaffold, frame = lsplits[3].rsplit('_',1) # split scaffold contig01_1 to contig01 and 1
		frame = int(frame)
		offset = frame%3
		strand = '+' if frame < 3 else '-'
		if donamechop:
			scaffold = scaffold.rsplit(donamechop,1)[0]
		protnamedict[scaffold] = True
		evalue = float(lsplits[11]) # or [6] for full seq or [12]
		domscore = lsplits[13]
		# convert domain protein positions to transcript nucleotide
		# protein position 1 becomes nucleotide position 1, position 2 becomes nucleotide 4, 3 to 7
		if strand=='+':
			domstart = int(lsplits[17]) * 3 - 2 + offset
			domend = int(lsplits[18]) * 3 + offset # end is necessarily the end of a codon
		else: # so strand=='-'
			querylen = lengthdict[scaffold]
		#	querylen = int(lsplits[5]) * 3 # multiplied by 3 for nucleotides
			# this will not work since the length can be offset by up to 3
			domend = querylen - (int(lsplits[17]) - 1) * 3 - offset # start and end must be switched for gff
			domstart = querylen - (int(lsplits[18]) * 3 - 1 ) - offset
		domnumber = lsplits[9]
		if evalue >= evaluecutoff: # skip domains with bad evalue
			evalueRemovals += 1
			continue
		print >> sys.stdout, "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{9}\t.\tID={0}.{7}.{8};Name={6}.{7}.{8}".format(scaffold, programname, outputtype, domstart, domend, domscore, pfamacc, targetname, domnumber, strand)
	print >> sys.stderr, "# Found {} domains for {} scaffolds".format(domaincounter, len(protnamedict) ), time.asctime()
	print >> sys.stderr, "# Removed {} domain hits by evalue".format(evalueRemovals), time.asctime()
	# NO RETURN

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="PFAM domain information as hmmscan tabular", required=True)
	parser.add_argument('-d','--delimiter', help="optional delimiter for protein names, cuts off end split")
	parser.add_argument('-e','--evalue', type=float, default=1e-1, help="evalue cutoff for domain filtering [1e-1]")
	parser.add_argument('-g','--genome', help="genomic contigs in fasta format", required=True)
	parser.add_argument('-p','--program', help="program for 2nd column in output [hmmscan]", default="hmmscan")
	parser.add_argument('-t','--type', help="gff type [PFAM]", default="PFAM")
	# this could more properly be SO:0000349 protein_match
	args = parser.parse_args(argv)

	seqlens = make_seq_length_dict(args.genome)
	parse_pfam_domains(args.input, args.evalue, args.program, args.type, args.delimiter, seqlens)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
