#!/usr/bin/env python
#
# taxa_from_tree.py  v1.0 created 2017-08-21

'''taxa_from_tree.py  v1.1   last modified 2018-11-04
  print taxa in order from a nexus-format tree:

taxa_from_tree.py partial_tree.nex

  or add a fasta file and print sequences directly

taxa_from_tree.py partial_tree.nex all_seqs.fasta > tree_only_seqs.fasta

  reverse order by adding -r as last option

taxa_from_tree.py partial_tree.nex all_seqs.fasta -r
'''

import sys
from Bio import Phylo
from Bio import SeqIO

# trees in nexus format
# #NEXUS
#begin trees;
#	tree tree_1 = [&R] ((AGAP008544-PA:0.02348,AGAP028140-PA:0.0908)[&bs=1.0]:0.34487,(AGAP028059-#PA:0.07144,AGAP008545-PA:0.08111)[&bs=1.0]:0.34047)[&bs=1.0];
#end;

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	tree = Phylo.read(sys.argv[1],"nexus")
	if len(sys.argv) > 2:
		seqdict = SeqIO.to_dict(SeqIO.parse(sys.argv[2],"fasta"))
		terminallist = tree.get_terminals()
		if len(sys.argv) > 3 and sys.argv[3]=="-r":
			terminallist = reversed(tree.get_terminals())
		for clade in terminallist:
			cleanname = str(clade.name).replace("'","")
			try:
				sys.stdout.write( seqdict[cleanname].format("fasta") )
			except KeyError: # if does not exist, then skip
				print >> sys.stderr, "WARNING: CANNOT FIND TAXON {}, SKIPPING".format(cleanname)
	else:
		for clade in tree.get_terminals():
			print >> sys.stdout, str(clade.name).replace("'","")
