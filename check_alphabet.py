#!/usr/bin/env python
#
# created v1 2014-03-10

'''check_alphabet.py v1.1 2022-01-02

tool to quickly check for non alphabet characters in fasta sequences

  replaces X and . with N in nucleotides, with X in proteins
  removes sequences with no sequence
'''

import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq

def checknucs(line):
	xcount = line.count("X")
	if xcount:
		print( "nucleotide contains {} Xs".format(xcount), file=sys.stderr )
	dotcount = line.count(".")
	if dotcount:
		print( "nucleotide contains {} .s".format(dotcount), file=sys.stderr )
	for c in ".X-?":
		line = line.replace(c,"N")
	for c in "0123456789":
		line = line.replace(c,"")
	return line

def checkprots(line):
	nonmcount = 0
	dotcount = line.count(".")
	if dotcount:
		print( "protein contains {} .s".format(dotcount), file=sys.stderr )
	for c in ".":
		line = line.replace(c,"X")
	for c in "0123456789":
		line = line.replace(c,"")
	line = line.upper()
	if line[0]!="M":
		nonmcount += 1
		print( "protein does not start with M", file=sys.stderr )
	return line, nonmcount

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('fasta', type=argparse.FileType('r'), help="fasta sequence file", default="-")
	parser.add_argument('-t','--type', help="sequence type: p (proteins), n (nucleotide)", default='p')
	args = parser.parse_args(argv)

	non_m_total = 0
	for seq_record in SeqIO.parse(args.fasta, 'fasta'):
		# requires a non-zero length
		if len(seq_record.seq):
			if args.type=='p':
				outseq, nonmcount = checkprots( str(seq_record.seq) )
				non_m_total += nonmcount
			else:
				outseq = checknucs(str(seq_record.seq))
			seq_record.seq = Seq(outseq)
			wayout.write(seq_record.format('fasta'))
	if non_m_total:
		print( "# counted {} proteins without M".format( non_m_total ), file=sys.stderr )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
