#! /usr/bin/env python
#
# v4.2 - filetype changed to r instead of rU 2024-12-09
# v4.1 - add gzip 2023-07-18
# v4.0 - python3 update, added rotation feature 2019-11-01
# v3.6 - minor cleanups 2016-02-08
# v3.5 - n90 should have been n10 2015-07-22
# v3.4 - added break for -d option for long sequence files
#               added option for output format and changed -f to -i 2014-06-09
# v3.3 - added cut option to cut into equal size pieces 2014-04-24
# v3.2 - functionalized n50 and histogram 2014-04-17
# v3.1 - corrected usage info and histogram bug 2014-03-13
# v3.0 - created more general histogram using numpy 2014-02-27
# v2.4 - added option to print first N sequences 2013-11-14
# v2.3 - changed output of N50 to float 2013-04-04
# v2.2 - changed repeat to allow for multi-character repeats 2013-04-03
# v2.1 - corrected error from GC content of unequal length sequences 19/oct/2012
#               changed GC counter to use set(), to avoid repeated letters
# v2.0 - added feature to generate histogram from 0 to 100 17/jul/2012
# v1.9 - changed return options 26/jun/2012
# v1.8 - added trimming function, to only output first N letters 26/may/2012
# v1.7 - moved all parameters to 'outvalue' to enable cutting of all outputs 11/may/2012
# v1.6 - added option to count longest polyN 9/may/2012
# v1.5 - minor cleanups of format 23/apr/2012
# v1.4 - added gc counter 11/jan/2012
# v1.3 - added n50 calculator with -n, and only-count option 9/jan/2012
# v1.2 - converted inputs as argparse, rather than sys.argv 31/dec/2011
# v1.1 - added options of size cutoff and print sequence output
#               also uses Bio fasta parser, rather than previous
#               dictionary-based approach 13/dec/2011
# v1.0 - first version
#
# email wrf at biology dot sdu dot dk with questions or comments

"""SIZECUTTER.PY v4.2 2024-12-09
Requires Bio library (biopython) and numpy

A simple program to output the length of fasta sequences to stdout
or can be used to filter by size, repeat content, base content, and print
those sequences to stdout.

For a quick summary, use -Q
sizecutter.py -Q some_seqs.fasta

It can be integrated into pipelines for both input and output
cat file1.fasta file2.fasta | sizecutter.py - > combined_sizes.txt

For example commands:
sizecutter.py -X -
"""

import sys
import os
import argparse
import time
import gzip
from numpy import histogram
from numpy import arange
from Bio import SeqIO
from Bio.Seq import Seq

def make_poly_regex(repeat):
	import re
	# generates repolya as "(A)+"
	regex = re.compile("({0})+".format(repeat) )
	return regex

def get_longest_repeat(sequence, regexp):
	# this will allow for multi-character repeats, but takes 2x as long (v2.2)
	polyresults = regexp.finditer(str(sequence))
	# for multicharacter repeats, length with be number of repeats * length, so 'ACACAC' would return 6 rather than 3
	try:
		longestrepeat = max([l.end()-l.start() for l in polyresults])
		return longestrepeat
	except ValueError: # for cases where repeat does not occur once, and max of list fails
		return 0

def get_gc_content(sequence, letters):
	'''takes a sequence object and a set of letters, by default GC, count each letter in the set of letters, and return the sum'''
	gcsum = sum([str(sequence).count(i) for i in set(letters)])
	return gcsum

def get_by_length(value, seqlength):
	'''from an arbitrary value and the length of the seq, return the percent'''
	by_length = 100.0 * value / seqlength
	return by_length

def trim_seq_ends(seq_record,trimstring):
	'''trim some sequence to arbitrary positions as specified in argv'''
	# "715500,716500," should return [715500, 716500]
	# needs if x in case of extra comma
	trimindex = [int(x) for x in trimstring.split(",") if x]
	if len(trimindex) < 2:
		print( "ERROR: option -t must give 2 integers in form of A,B  found only {}".format( " ".join(trimindex) ) , file=sys.stderr)
	elif len(trimindex) > 2:
		print( "ERROR: option -t expects 2 integers, found {}, taking first and last numbers".format( len(trimindex) ) , file=sys.stderr)
	trimstring = seq_record.seq[trimindex[0]:trimindex[-1]] # use -1, will always return a string
	return trimstring

def chop_in_blocks(seq_record,blocksize,wayout,formattype):
	'''break up a fasta sequence into fragments the size of blocksize'''
	seqlength = len(seq_record.seq)
	seqseq = str(seq_record.seq)
	seqname = str(seq_record.id)
	seq_record.description = ""
	for block in range(0,seqlength,blocksize):
		outseq = seqseq[block:block+blocksize]
		seq_record.seq = Seq(outseq)
		# rename the sequence by the blocksize, or the seqlength for the last block
		if block+blocksize > seqlength:
			seq_record.id = seqname+"_{}:{}".format(block,seqlength)
		else:
			seq_record.id = seqname+"_{}:{}".format(block,block+blocksize)
		wayout.write(seq_record.format(formattype))
	#THIS IS A FUNCTION OF NO RETURN

def rotate_sequence(seq_record, bases_to_rotate):
	'''take bases from the beginning of a sequence, and return a string with them added to the end'''
	rotatedstring = seq_record.seq[bases_to_rotate:] + seq_record.seq[:bases_to_rotate]
	return rotatedstring

def calc_n50(seqsizes, seqsum, writtenseqs):
	if seqsum:
		n50, n10, ncountsum = 0,0,0
		sys.stderr.write("# Sorting sequences:  {}\n".format(time.asctime()) )
		seqsizes.sort()
		halfsum = seqsum//2 # round down to the base
		ninesum = int(seqsum*.9)
		seqmean = float(seqsum)/writtenseqs
		seqmedian = seqsizes[(writtenseqs//2)]
		seqmin, seqmax = min(seqsizes), max(seqsizes)
		sys.stderr.write("# Average length is: {:.2f}\n".format(seqmean) )
		sys.stderr.write("# Median length is: {}\n".format(seqmedian) )
		sys.stderr.write("# Shortest sequence is: {}\n".format(seqmin) )
		sys.stderr.write("# Longest sequence is: {}\n".format(seqmax) )
		for i in seqsizes:
			# changed to output float (%.2f, not %d) for v2.3
			ncountsum += i
			if ncountsum >= halfsum and not n50:
				n50 = i
				sys.stderr.write("# n50 length is: {} and n50 is: {:.2f}\n".format(ncountsum, n50) )
			if ncountsum >= ninesum and not n10:
				n10 = i
				sys.stderr.write("# n10 length is: {} and n10 is: {:.2f}\n".format(ncountsum, n10) )
		#return seqmean, seqmedian, n50, n10  # n10 stopped being useful
		return seqmean, seqmedian, n50, seqmax
	else:
		sys.stderr.write("# All lengths and parameters are: 0\n")
		return 0,0,0,0

def make_histogram(wayout, minval, maxval, bin_size, seqsizes, ignore_max):
	bins = arange(minval, maxval+bin_size, bin_size)
	# unless using -I, values above the max will be changed to the max and put into the last bin
	if not ignore_max:
		for i,x in enumerate(seqsizes):
			if x >= maxval:
				seqsizes[i] = maxval
	# histogram function generates a tuple of arrays, which must be zipped to iterate
	sizehist = histogram(seqsizes,bins,(minval, maxval+bin_size))
	for x,y in zip(sizehist[0],sizehist[1]):
		wayout.write("{}\t{}\n".format(x,y) )

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type=argparse.FileType('r'), default = '-', help="fasta format file, can be .gz")
	parser.add_argument('-X', '--examples', action='store_true', help="display examples for usage and then exit")
	parser.add_argument('-a', '--above', type=float, metavar='MIN', default=0.0, help="only count sequences longer than N")
	parser.add_argument('-b', '--below', type=float, metavar='MAX', default=100000000000.0, help="only count sequences shorter than N")
	parser.add_argument('-B', '--BED', action='store_true', help="print BED format contig information")
	parser.add_argument('-c', '--cut', type=int, metavar='N', help="cut sequences into chunks of N bases")
	parser.add_argument('-d', '--head', type=int, metavar='N', help="only count the first N sequences")
	parser.add_argument('-e', '--exclude-gaps', action='store_true', help="exclude alignment gaps '-' from length count")
	parser.add_argument('-f', '--print-fasta', action='store_true', help="print fasta header with length")
	parser.add_argument('-g', '--gc-content', metavar='GC', help="instead of size, print the count of N or N...")
	# this may need to be "fastq-sanger" or "fastq-illumina"
	parser.add_argument('-i', '--input-format', nargs='?', const='fastq', default='fasta', help="import fastq format sequences")
	parser.add_argument('-l', '--by-length', action='store_true', help="for -g or -r, output percent of length, rather than count; this option will affect most calculations")
	parser.add_argument('-n', '--n50', action='store_true', help="calculate the mean, median, n50 and n10 for the set")
	parser.add_argument('-o', '--output-format', nargs='?', const='fastq', default='fasta', help="print fastq format sequences")
	parser.add_argument('-p', '--print-seq', action='store_true', help="print sequences instead of sizes, such as when using cutoffs")
	parser.add_argument('-q', '--quiet', action='store_true', help="only count, do not print output (except -n or -H)")
	parser.add_argument('-Q', '--quiet-summary', action='store_true', help="same as -q -n")
	parser.add_argument('-r', '--repeats', metavar='A', help="instead of size, print the length of the longest polyN, NX repeat, etc.")
	parser.add_argument('-R', '--rotate-sequence', type=int, metavar='N', help="rotate circular sequences by N bases, counter-clockwise (+N) or clockwise (-N), use with -p")
	parser.add_argument('-t', '--trim', metavar='N,N', help="trim sequences from N to N, as 10,20, starting from 0 (not 1)")
	parser.add_argument('-H', '--histogram', action='store_true', help="generate histogram of output, must use -q")
	parser.add_argument('-z', '--bin-size', type=float, metavar='N', default=1.0, help="bin size for histogram output [1.0]")
	parser.add_argument('-m', '--minimum', type=float, metavar='N', default=0.0, help="minimum cutoff for histogram [0.0]")
	parser.add_argument('-M', '--maximum', type=float, metavar='N', default=100.0, help="maximum cutoff for histogram [100.0]")
	parser.add_argument('-I', '--ignore_max', action='store_true', help="ignore values in histogram above maximum")
	args = parser.parse_args(argv)

	if args.examples:
		sys.exit(examples)

	if args.quiet_summary:
		args.quiet = True
		args.n50 = True

	# all integers initialized
	seqcount = 0
	rawsum = 0

	# alert user of non-unique letters in gc-content string
	getgc = args.gc_content
	if getgc:
		if len(getgc) != len(set(getgc)):
			sys.stderr.write("# WARNING: Redundant letters in list {}  {}\n".format( getgc, time.asctime() ) )
			getgc = "".join(set(getgc))
			sys.stderr.write("# Making non-redundant list for: {}\n".format(getgc) )
	# form regular expression
	getpolya = args.repeats
	if getpolya:
		repoly = make_poly_regex(getpolya)
		sys.stderr.write("# Finding repeats of {}:  {}\n".format( getpolya, time.asctime() ) )
	# list for tracking total number of written bases
	seqsizes = []

	# start main loop to iterate through sequence file
	if args.input_file.name.rsplit('.',1)[-1]=="gz":
		input_handler = gzip.open(args.input_file.name,'rt')
	else:
		input_handler = args.input_file
	sys.stderr.write("# Parsing sequences from {}:  {}\n".format(args.input_file.name, time.asctime() ) )
	for seq_record in SeqIO.parse(input_handler, args.input_format):
		# count of current number of sequences, and if head count is specified then only process N sequences
		if not (args.head) or (args.head and seqcount < args.head):
			seqcount += 1
		# for '-e' option
			if args.exclude_gaps:
				seqlength = len(str(seq_record.seq).replace('-',''))
			else:
				seqlength = len(seq_record.seq)
			# track total number of read bases
			rawsum += seqlength
		# for '-r' mode
			# uses regular expression for finding longest repeat out of list of all repeats
			if getpolya:
				outvalue = get_longest_repeat(seq_record.seq, repoly)
		# for '-g' mode
			# for each unique letter in string of args.gc_content, count it, and sum all of the results
			elif getgc:
				outvalue = get_gc_content(seq_record.seq, args.gc_content)
			else:
				outvalue = seqlength
		# for '-l' option
			# if using by-length parameter, get percentage by length
			# this is meant for viewing individual transcripts
			# summary will not be accurate unless all sequences are the same length
			if args.by_length:
				outvalue = get_by_length(outvalue, seqlength)
		# for '-a' and '-b'
			# if sizes are within high and low cutoffs
			if (args.below > outvalue >= args.above):
		# for '-t' option
				if args.trim:
					seq_record.seq = trim_seq_ends(seq_record, args.trim)
					# update written value with trimmed sequence length
					outvalue = len(seq_record.seq)
				# make list of all sequences that match above criteria
				seqsizes.append(outvalue)
			# for '-q' option
				# if this is changed, then do all calculations up to this point but print nothing
				if not args.quiet:
					if args.print_seq or args.trim or args.cut:
				# for '-c' option
						if args.cut:
							chop_in_blocks(seq_record,args.cut,wayout,args.output_format)
				# for '-p' option or '-t'
						else:
				# for '-R' with '-p'
							if args.rotate_sequence:
								seq_record.seq = rotate_sequence(seq_record, args.rotate_sequence)
							wayout.write(seq_record.format(args.output_format))
					else:
				# for '-f' option
						if args.print_fasta:
							wayout.write("{}\t{}\n".format( seq_record.id, outvalue ) )
				# for '-B' option
						elif args.BED: # print BED format with contig names, like contig123   0   25000
							wayout.write("{}\t0\t{}\n".format(seq_record.id, seqlength) )
				# normal output
						else:
							wayout.write("{}\n".format(outvalue))
		# must have break, otherwise it will still try to parse through very large files when using -d option
		else:
			break
	# determine number of sequences within cutoff parameters, and output
	# this is the only part that is printed with option '-q'
	writecount = len(seqsizes)
	seqsum = sum(seqsizes)
	try:
		sumratio = float(seqsum)/rawsum
		writeout_percent = 100.0 * writecount / seqcount
	# for cases where the file is empty
	except ZeroDivisionError:
		sumratio = 0
		writeout_percent = 0.0
	sys.stderr.write("# Counted {} sequences:  {}\n".format(seqcount, time.asctime() ) )
	sys.stderr.write("# Wrote {} sequences: {:.1f}% of total\n".format(writecount, writeout_percent) )
	sys.stderr.write("# Total input letters is: {}\n".format(rawsum) )
	if not args.by_length:
		sys.stderr.write("# Total output count is: {}\n".format(seqsum) )
		sys.stderr.write("# Ratio of out/in is: {}\n".format(sumratio) )

	# for '-n' option
		# if finding the n50 and other stats, and if seqsizes list is not empty
	if args.n50 and writecount:
		seqmean, seqmedian, n50, seqmax = calc_n50(seqsizes, seqsum, writecount)

	# for '-H' option
		# if generating frequency histogram, and if seqsizes list is not empty
		# histogram is printed to stdout, so it can be written directly to file
		# -q option should be used
	if args.histogram and writecount:
		make_histogram(wayout, args.minimum, args.maximum, args.bin_size, seqsizes, args.ignore_max)

	sys.stderr.write("# Done:  {}\n".format( time.asctime() ) )

	# for integration with other programs, return these stats
	if args.n50 and writecount:
		return seqcount, seqsum, seqmean, seqmedian, n50, seqmax
	else:
		return seqcount

examples="""  For simple length output for each sequence:
sizecutter.py sequences.fasta

  To only display summary statistics, and not write out lengths:
sizecutter.py -q sequences.fasta

  For sequences longer or shorter, use -a or -b, respectively:
sizecutter.py -a 1000 sequences.fasta > longsizes.txt
sizecutter.py -b 1000 sequences.fasta > shortsizes.txt

  To print out the fasta format sequences in use with the size filter:
sizecutter.py -a 1000 -p sequences.fasta > longsequences.fasta

  To cut out a specific piece of a sequence, like from a genomic scaffold:
sizecutter.py -p -t 1500,3000 scaffold_01.fasta > scaffold_01_1500_3000.fasta

  To cut a large sequence to many sequences of 1000bp:
sizecutter.py -p -c 1000 long_sequence.fasta > split_sequences.fasta

  To rotate circular chromosomes clockwise by 1000bp (origin at 12 o'clock):
sizecutter.py -p -R -1000 chr.fasta > rotated_chr.fasta

  To rotate circular chromosomes counter-clockwise by 2000bp:
sizecutter.py -p -R 2000 chr.fasta > rotated_chr.fasta

  To additionally calculate the n50 and other values:
sizecutter.py -n sequences.fasta

  Use fastq sequences with:
sizecutter.py -i short_reads.fastq

  Convert from fastq to fasta:
sizecutter.py -i -o -p reads.fastq > reads.fasta

  To print GC content for each sequence:
sizecutter.py -g GC -l sequences.fasta

  To only print sequences with greater than 50% GC:
sizecutter.py -g GC -l -a 50 -p sequences.fasta

  To print the length of the longest AT repeat in each sequence:
sizecutter.py -r AT sequences.fasta

  To make a histogram of sizes, ranging from 0 to 10000:
sizecutter.py -q -H -m 0 -M 10000 -z 500 sequences.fasta

  To output the GC content in histogram form:
sizecutter.py -q -l -g GC -H -m 0 -M 100 sequences.fasta
"""

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
