## README ##

Repository of all Python scripts for all projects

Many make use of the Bio python library for handling parsing of fasta files, 
though occasionally other non-standard libraries (numpy, networkx).

**Many scripts become integrated into dedicated projects. If it is no longer maintained here, check my [GitHub repo](https://github.com/wrf/).**

* [add_taxa_to_align.py](https://bitbucket.org/wrf/sequences/src/master/add_taxa_to_align.py) - add proteins from new taxa to an existing alignment, [moved to GitHub](https://github.com/wrf/supermatrix)
* [alignmentdnds.py](https://bitbucket.org/wrf/sequences/src/master/alignmentdnds.py) - calculate dN/dS based on back-aligned nucleotides
* [align_pos_to_fasta.py](https://bitbucket.org/wrf/sequences/src/master/align_pos_to_fasta.py) - print select alignment positions in a FASTA header, for tree building
* [blast2genomegff.py](https://bitbucket.org/wrf/sequences/src/master/blast2genomegff.py) - [moved to GitHub](https://github.com/wrf/genomeGTFtools)
* [blast2gff.py](https://bitbucket.org/wrf/sequences/src/master/blast2gff.py) - make GFF of BLAST hits, [moved to GitHub](https://github.com/wrf/genomeGTFtools)
* [bowtiepipeline.py](https://bitbucket.org/wrf/sequences/src/master/bowtiepipeline.py) - compile counts of mapped RNAseq reads to a reference
* [check_supermatrix_alignments.py](https://bitbucket.org/wrf/sequences/src/master/check_supermatrix_alignments.py) - diagnostic for supermatrices, [moved to GitHub](https://github.com/wrf/supermatrix)
* [classifymutationstats.py](https://bitbucket.org/wrf/sequences/src/master/classifymutationstats.py) - run after vcfstats.py
* [cleanfastqcomposition.py](https://bitbucket.org/wrf/sequences/src/master/cleanfastqcomposition.py) - remove disallowed characters from FASTQ scores
* [commonseq.py](https://bitbucket.org/wrf/sequences/src/master/commonseq.py) - **DEPRICATED** use [remove_identical_seqs.py](https://bitbucket.org/wrf/sequences/src/master/remove_identical_seqs.py)
* [contigcomposition.py](https://bitbucket.org/wrf/sequences/src/master/contigcomposition.py) - check for unusual base composition, such as NULL characters
* [degapper.py](https://bitbucket.org/wrf/sequences/src/master/degapper.py) - remove gaps from alignments
* [extract_features.py](https://bitbucket.org/wrf/sequences/src/master/extract_features.py) - make FASTA files of CDS and proteins from AUGUSTUS GFF
* [fasta2twoline.py](https://bitbucket.org/wrf/sequences/src/master/fasta2twoline.py) - convert multi-line FASTA to two-line
* [fastaqual2fastq.py](https://bitbucket.org/wrf/sequences/src/master/fastaqual2fastq.py) - combine FASTA and QUAL scores into FASTQ
* [fastarenamer.py](https://bitbucket.org/wrf/sequences/src/master/fastarenamer.py) - simple string modification for changing FASTA headings
* [fastqdumps2histo.py](https://bitbucket.org/wrf/sequences/src/master/fastqdumps2histo.py) - make matrix of GC and coverage, see [same script on GitHub](https://github.com/wrf/lavaLampPlot)
* [findmotif.py](https://bitbucket.org/wrf/sequences/src/master/findmotif.py) - regular expression search for motifs in nucleotide or proteins
* [flagchimeras.py](https://bitbucket.org/wrf/sequences/src/master/flagchimeras.py) - flag potential chimeras in a transcriptome based on long, non-overlapping ORFs
* [getAinB.py](https://bitbucket.org/wrf/sequences/src/master/getAinB.py) - highly flexible search for sequences by name
* [gtfstats.py](https://bitbucket.org/wrf/sequences/src/master/gtfstats.py) - summary info exon and gene length from GTF, used in [genome-reannotations](https://bitbucket.org/wrf/genome-reannotations)
* [kmersorter.py](https://bitbucket.org/wrf/sequences/src/master/kmersorter.py) - for read extraction based on kmers, see [same script on GitHub](https://github.com/wrf/lavaLampPlot)
* [microsynteny.py](https://bitbucket.org/wrf/sequences/src/master/microsynteny.py) - predict blocks of microsynteny from BLAST hits and GTF files, [now on GitHub](https://github.com/wrf/genomeGTFtools)
* [pfam2gff.py](https://bitbucket.org/wrf/sequences/src/master/pfam2gff.py) - PFAM domains as GFF, [this is now maintained on GitHub](https://github.com/wrf/genomeGTFtools)
* [pfampipeline.py](https://bitbucket.org/wrf/sequences/src/master/pfampipeline.py) - controller for PFAM protein domain scripts
* [prealigner.py](https://bitbucket.org/wrf/sequences/src/master/prealigner.py) - extract nucleotides or proteins based on BLAST hits
* [protrepeater.py](https://bitbucket.org/wrf/sequences/src/master/protrepeater.py) - count simple repeats in proteins
* [prottrans.py](https://bitbucket.org/wrf/sequences/src/master/prottrans.py) - simple protein translator
* [randomizesites.py](https://bitbucket.org/wrf/sequences/src/master/randomizesites.py) - take random gene subsets from a supermatrix alignment
* [regapper.py](https://bitbucket.org/wrf/sequences/src/master/regapper.py) - re-align nucleotides based on a protein alignment
* [revcomp.py](https://bitbucket.org/wrf/sequences/src/master/revcomp.py) - simple reverse complement tool
* [revtrans.py](https://bitbucket.org/wrf/sequences/src/master/revtrans.py) - simple reverse translate tool returning degenerate base code
* [rsemgetbestseqs.py](https://bitbucket.org/wrf/sequences/src/master/rsemgetbestseqs.py) - extract highest abundance splice variant from Trinity/RSEM
* [sizecutter.py](https://bitbucket.org/wrf/sequences/src/master/sizecutter.py) - tool for measuring lengths of sequences, trimming, counting, or generating histograms
* [sortreadsfrombam.py](https://bitbucket.org/wrf/sequences/src/master/sortreadsfrombam.py) - take subsets of reads based on mapping
* [splicevariantstats.py](https://bitbucket.org/wrf/sequences/src/master/splicevariantstats.py) - get information about splice variation from StringTie GTF file
* [taxa_from_tree.py](https://bitbucket.org/wrf/sequences/src/master/taxa_from_tree.py) - print taxa in order from a nexus format tree
* [vcfstats.py](https://bitbucket.org/wrf/sequences/src/master/vcfstats.py) - assorted summary information of SNPs from a VCF file
