#!/usr/bin/env python
#
# v1.9 removed some completely unused options 2016-04-21
# v1.8 added automatic generation of stderr log 2015-07-24
# v1.7 added option to import sam files directly 2015-07-23
# v1.6 changed to count only pairs 2015-03-31
# v1.5 fixed bug with reduced library output 2015-02-13
# v1.4 added flag for new trinity version 2015-01-19
# v1.3 added feature to generate binary matrix 2014-12-04
# v1.2 changed to operate with bowtie for many files 2014-10-21
# v1.1 added naming for output files 9/may/2012
# v1.0 initial script for bowtie to take a master contig list and make maps of a series of read-pairs 27/apr/2012
#
# refer to bowtie instructions at:
# http://bowtie-bio.sourceforge.net/manual.shtml#command-line
# or to the bowtie2 instructions at:
# http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#command-line

"""bowtiepipeline.py 2016-12-17
    to map many sets to the same database:

bowtiepipeline.py -i *.fastq -b allseqs_build_v1 -f allseqs.fasta -A -C -E 2 -F
    or for most cases:
bowtiepipeline.py -i data/*/*.txt -b allseqs_build_v1 -f allseqs.fasta -H -K -E -p 4 -s stringtie

    where:
-i *.fastq is all fastq files, assumed to be paired end
    thus one is expected to be _1.fastq, other is _2.fastq
-b is the prefix of the build file (from bowtie-build)
-f is the fasta file of all sequences used to make the build
-A means do alignment
-C means count to a general file at the end of all mappings
-E means exclude sequences with fewer than N hits
-F means generate a TPM counts file
-K means combine trinity components into a single value for tpm

    other options:
-B make bowtie build from file in -f
-p change number of processors, default is 1

    to pick up previous counts files and not remap or count SAM files, omit -C
bowtiepipeline.py -i *.fastq -b allseqs_build_v1 -f allseqs.fasta -E 2 -F

    to pick up previous SAMs, for recounting, omit -A
bowtiepipeline.py -m *.sam -f allseqs.fasta -C -E 2 -F

    to use previous counts files if SAM files were removed, use -m
bowtiepipeline.py -m *.counts -f allseqs.fasta -K
"""

# -2 means use bowtie2 instead of bowtie

import sys
import os
import argparse
import subprocess
import time
import multiprocessing
import re
from collections import defaultdict

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

def run_bowtie(bowtie_args, bowtiestderr):
	print >> sys.stderr, "# Starting bowtie: ", time.asctime()
	print >> sys.stderr, "Making system call:\n", " ".join(bowtie_args)
	# bowtie output for mapping efficiency is written to stderr
	with open(bowtiestderr, 'a') as be:
		print >> be, "#", " ".join(bowtie_args)
		subprocess.call(bowtie_args, stderr=be)
	print >> sys.stderr, "# Mapping finished at:", time.asctime()

def bowtie_build(fastafile, buildname, progstart, bowtieprog):
	# if user provides a name, use that
	if buildname:
		buildout = os.path.join(os.path.dirname(fastafile),buildname)
	# otherwise chop of the extension and use that
	else:
		buildfile = os.path.abspath(fastafile)
		buildout = os.path.splitext(buildfile)[0]
	print >> sys.stderr, "# Starting build of %s at: " % (buildout), time.asctime()
	bowtiebuild_args = [bowtieprog, fastafile, buildout]
	print >> sys.stderr, "Making system call:\n{}".format( " ".join(bowtiebuild_args) )
	subprocess.call(bowtiebuild_args)
	print >> sys.stderr, "# Finished build of %s at: " % (build_name), time.asctime()
	print >> sys.stderr, "# Build completed in %.1f minutes" % ((time.time()-progstart)/60)

### summary of bowtie2 flags
#1 The read is one of a pair
#2 The alignment is one end of a proper paired-end alignment
#4 The read has no reported alignments
#8 The read is one of a pair and has no reported alignments
#16 The alignment is to the reverse reference strand
#32 The other mate in the paired-end alignment is aligned to the reverse reference strand
#64 The read is mate 1 in a pair
#128 The read is mate 2 in a pair

def bowtie_map_to_counts(map_name, countappend=".counts"):
	### TODO this should be split into two functions, one to count, one to write to file
	print >> sys.stderr, "# Counting read mappings of %s: " % (map_name), time.asctime()
	count_d = defaultdict(int)
	countsfile_name = map_name + countappend
	if os.path.isfile(countsfile_name):
		print >> sys.stderr, "# Counts file {} already detected, skipping".format(countsfile_name), time.asctime()
		return False
	insertsizelist = [] # measure insert size
	readCount = 0
	mappedCount = 0
	mismatches = 0
	unpaireds = 0
	for line in open(map_name, 'r'):
		if not line[0]=="@": # ignore dummy header lines
			readCount += 1
			samsplits = line.split('\t')
			contig_name = samsplits[2]
			pair_contig_name = samsplits[6]
			if contig_name=="*": # read did not map
				continue
			if pair_contig_name=="=":
				mappedCount += 1
			elif pair_contig_name=="*":
				unpaireds += 1
			elif contig_name!=pair_contig_name:
				mismatches += 1
			count_d[contig_name] += 1
			insertsize = int(samsplits[8])
			if insertsize and insertsize > 0:
				insertsizelist.append(insertsize)
			### TODO also check mapping quality, or give a threshold, samsplits[4]
			# mapping quality looks like this 83M1I8M4I4M
	print >> sys.stderr, "Counted %d reads from %s finished at: " % (readCount, countsfile_name), time.asctime()
	print >> sys.stderr, "Counted {} aligned paired-reads, {} unpaired, {} mismatched".format(mappedCount, unpaireds, mismatches)
	if insertsizelist:
		averageinsert = 1.0*sum(insertsizelist)/len(insertsizelist)
		print >> sys.stderr, "For {} pairs, average insert size was {}bp".format(len(insertsizelist), averageinsert)
	# then write all counts to file
	print >> sys.stderr, "# Making counts file %s: " % (countsfile_name), time.asctime()
	with open(countsfile_name, 'w') as count_file:
		for contig_name in count_d:
			print >> count_file, str(count_d[contig_name]) + "\t" + contig_name
	print >> sys.stderr, "# Finished counts file %s: " % (countsfile_name), time.asctime()
	return True

def populate_empty_dict(fastafile, countsdict):
	print >> sys.stderr, "Generating master list of %s: " % (fastafile), time.asctime()
	# generate empty lists in the dictionary for each sequence
	# as only names are required, no need to parse with biopython library
	with open(fastafile, 'r') as obf:
		for line in obf:
			if line[0]=='>':
				transcriptname = line[1:].rstrip().split(" ")[0]
				countsdict[transcriptname]=[]
	# add entry for * for unmapped reads
	countsdict["*"] = []
	# function is void

def count_for_all(fastaname, samfiles, exclude):
	# dictionary of lists
	# not using defaultdict as this might cause problems if some samples do not map to all transcripts
	rawcntdict = {}
	populate_empty_dict(fastaname, rawcntdict)

	# make a list of totals for each sample
	totalcountlist = []
	samcount = 0
	# iterate through each .sam.counts file, append them to the dictionary of lists
	for x, samfile in enumerate(samfiles):
		samcount += 1
		totalreads = 0
		# create totally empty dict to ensure that all values are considered when exporting
		for item in rawcntdict.keys():
			rawcntdict[item].append(0)
		if samfile.rsplit('.',1)[1]=="counts":
			countsfile_name = samfile
		else:
			countsfile_name = "{}.counts".format(samfile)
		print >> sys.stderr, "Reading read counts from %s : " % (countsfile_name), time.asctime()
		with open(countsfile_name, 'r') as mapcounts:
			for line in mapcounts:
				count, contig_name = line.rstrip().split('\t')
				try:
					rawcntdict[contig_name][x] = int(count)
				except KeyError:
					# this happens from a sequence in SAM or counts file that is not in the fasta reference
					print >> sys.stderr, "Cannot find sequence {} in fasta file, check sequences".format(contig_name)
					print >> sys.stderr, "exiting", time.asctime()
					sys.exit()
				totalreads += int(count)
		totalcountlist.append(totalreads)
	print >> sys.stderr, "Counted {} files of {} : ".format(samcount, len(samfiles) ), time.asctime()
	print >> sys.stderr, "With {} reads: ".format(sum(totalcountlist) ), time.asctime()
	# pop items where sum is not greater than zero
	if exclude:
		exclude_non_hits(rawcntdict, exclude)
	# finally return the raw counts as dict and totals as list for each counts file
	return rawcntdict, totalcountlist

def make_reduced_db(rawcntdict, samcnt, restring, mergevector, filtersplit=False):
	'''from dictionary of raw counts per seq ID, return a dict where Trinity IDs are summed per component and other genes are left the same'''
	print >> sys.stderr, "Combining counts for trinity components", time.asctime()
	# with args.components combine trinity components into a single count
	reducedict = {}
	for k in rawcntdict.keys():
		# if restring ("comp123...") is not found in x, that is None,
		# then treat x as its own component
		# this is needed for spike-in RNA, mitochondria, etc.
		rekey = re.search(restring, k)
		if rekey or filtersplit or mergevector:
			if filtersplit:
				newkey = k.rsplit('.',1)[0]
			elif mergevector:
				try:
					newkey = mergevector[k]
				except KeyError:
					newkey = k
					if rekey:
						newkey = rekey.group(1)
			else:
			# use regular expression to chop out something_comp1234_c0_seq1
			# new trinity format is c1234_g1_i1
			# the previous partition strategy could produce an error if the name
			# were in new trinity format and the 'something' contained another 'c'
				newkey = rekey.group(1)
			# check if new key is not in the dictionary and make a list of length samcnt
			if newkey not in reducedict:
				reducedict[newkey] = reducedict.get(newkey, [0.0]*samcnt)
			# iterate through each key and add the items to the new dict at that key
			for n,i in enumerate(rawcntdict[k]):
				reducedict[newkey][n] += i
		else: # should be cases that are not Trinity sequences, such as mitochondria or spike-ins
			reducedict[k] = rawcntdict.get(k)
	print >> sys.stderr, "Reduced counts has %d entries" % (len(reducedict)), time.asctime()
	return reducedict

def make_merge_vector(merge_file, restring):
	'''from file of tab separated sequence IDs, return a dict where key is the ID and value is the first ID from that line'''
	vectordict = {}
	linecount = 0
	print >> sys.stderr, "Reading merge components from {}".format(merge_file), time.asctime()
	for line in open(merge_file,'r'):
		line = line.strip()
		if line: # skip blank lines
			linecount += 1
			splitids = line.split("\t")
			newkey = re.search(restring, splitids[0]).group(1) # assume Trinity format
			for seqid in splitids:
				vectordict[seqid] = newkey
	print >> sys.stderr, "Merged {} components into {} genes".format( len(vectordict), linecount ), time.asctime()
	return vectordict

def exclude_non_hits(rawcntdict, excluden):
	print >> sys.stderr, "Library has %d entries: " % len(rawcntdict), time.asctime()
	print >> sys.stderr, "Excluding seqs with fewer than {} hits: ".format(excluden), time.asctime()
	for transcript in rawcntdict.keys():
		# if sum is zero, pop the entry
		#if not sum(rawcntdict[transcript]):
		#	rawcntdict.pop(transcript,None)
		# if number of items in list is fewer than excluden, pop the entry
		# due to a yes/no requirement, count is turned to bool, then recounted
		if sum(bool(x) for x in rawcntdict[transcript]) <= excluden:
			rawcntdict.pop(transcript,None)
	print >> sys.stderr, "Filtered library has %d entries: " % len(rawcntdict), time.asctime()

def make_tpm_dict(rawcntdict, totalcountlist, fasta, readlen):
	lendict = {}
	for seq in SeqIO.parse(fasta,'fasta'):
		lendict[seq.id] = len(seq.seq)
	# length of each sequence is needed to calculate tpm
	tpmdict = defaultdict(list)
	#rpkm = rcount * 1000000 * 1000 / genelength / totalreads
	#tpm = rcount * readlen * 1000000 / genelength / totalhits
	# thus according to Wagner et al (2012), the proportionality is:
	#rpkm = tpm * totalhits * 1000 / totalreads / readlen
	for k,v in rawcntdict.iteritems():
		for x,c in enumerate(v):
			try:
				tpm = c * readlen * 1000000.0 / lendict[k] / totalcountlist[x]
				tpmdict[k].append(tpm)
			# ignore cases for unknown keys, such as '*'
			except KeyError:
				pass
	return tpmdict

def make_counts_file(fasta, countdict, outname, style, header):
	countsfile_name = "%s.%s" % (fasta, outname)
	print >> sys.stderr, "Making %s file %s : " % (outname, countsfile_name), time.asctime()
	with open(countsfile_name, 'w') as cf:
		# if header line is requested, add it here
		if header:
			print >> cf, "Transcript\t%s" % ("\t".join(header) )
		# this will sort the transcripts by most hits
		for transcript, val in sorted(countdict.items(), key=lambda x: sum(x[1]), reverse=True):
			# style is a short string such as '%d' or '%.3f' for output scheme
			print >> cf, "%s\t%s" % (transcript, "\t".join(style % v for v in val) )
	print >> sys.stderr, "Finished %s file %s : " % (outname, countsfile_name), time.asctime()

def make_binary_matrix(reducecntdict, samcnt):
	print >> sys.stderr, "Generating gene matrix", time.asctime()
	matrixlist = [""]*samcnt
	# this should take the nth item from each list for each transcript and
	# append it to the nth string in matrixlist as a 0 or 1
	for transcript, val in sorted(reducecntdict.items(), key=lambda x: sum(x[1]), reverse=True):
		for i, n in enumerate(val):
			matrixlist[i] += str(int(bool(n)))
	return matrixlist

def make_matrix_file(binarymatrix, fasta, samfiles):
	matrixfile_name = "%s.genematrix.fasta" % fasta
	print >> sys.stderr, "Writing gene matrix to %s" % matrixfile_name, time.asctime()
	with open(matrixfile_name,'w') as nf:
		for x in zip(samfiles, binarymatrix):
			# generate a SeqRecord object for each file and binary sequence
			outseqrec = SeqRecord(Seq(x[1]), id=os.path.splitext(x[0])[0], description="")
			nf.write(outseqrec.format("fasta"))
	print >> sys.stderr, "Finished writing gene matrix %s" % matrixfile_name, time.asctime()

def get_trinity_re(v2trinity, v2014trinity):
	# for making the reduced dictionary, the components are needed
	# these are pulled out using a regular expression
	# the syntax varies based on which version of trinity
	# where the old version is: comp2134_c0_seq1
	# and the new version is: c1234_g1_i1
	if v2trinity:
		return "(TR\d+\|c\d+_g\d+)_i\d+"
	elif v2014trinity:
		return "(c\d+_g\d+)_i\d+"
	else:
		return "(comp\d+_c\d+)_seq\d+"

def make_header(samfiles, verbose, forcesn=True):
	# example headers are:
	# lane7C8x850_1_sequence.sam _C10x2401_1_sequence.sam lane7C8x05d_1_sequence.sam
	# regex should return just C00x000
	headSS = "([CSN]L*\d*[x][\d\w]+)_1"
	# for debugging this does not use list comprehension
	hds = []
	for sn in samfiles:
		try:
			hd = re.search(headSS, sn).group(1)
		except AttributeError:
			# or lane115s010319TWxCC4_1_sequence.sam
			v2re = re.search("_(lane[\d\w]+TWxCC[\d\w]+)_1", sn)
			# or lane2Tw-SS2-3_1_sequence.sam
			v3re = re.search("_(lane[\d\w]+Tw-SS[-\d\w]+)_1", sn)
			if v2re:
				hd = v2re.group(1)
			elif v3re:
				hd = v3re.group(1)
			else:
				if forcesn:
					hd = sn.split('.',1)[0]
				print >> sys.stderr, "CANNOT PROCESS {}".format(sn)
		if verbose:
			print >> sys.stderr, hd
		if hd not in hds: # avoid redundant header information
			hds.append(hd)
		else: # try adding just the filename instead
			print >> sys.stderr, "HEADER NAME {} FROM {} ALREADY USED".format(hd, sn)
			hds.append("{}{}".format(hd,os.path.dirname(sn).split("/")[-1]))
		#	hds.append(os.path.basename(sn).split("_1_sequence.sam.counts")[0])
	return hds

def get_threads(threads):
	if threads:
		threadcount = threads
	else:
		print >> sys.stderr, "# Detecting processors..."
		threadcount = str(min(multiprocessing.cpu_count(),1) )
	print >> sys.stderr, "# Using %s threads" % (threadcount)
	return threadcount

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--inputs', nargs='*', help="reads from each sample", default=[])
	parser.add_argument('-b','--buildfile', help="build then map onto this file")
	parser.add_argument('-f','--fasta', help="fasta reference file")
	parser.add_argument('-m','--maps', nargs='*', help="list of sam files from previous mapping")
	parser.add_argument('-k','--merge-components', help="text file of gene groups, used with option -K")
	parser.add_argument('-l','--length', type=int, help="read length for TPM calculations [100]", default=100)
	parser.add_argument('-p','--processors', help="number of processors [1]")
	parser.add_argument('-s','--stringtie', help="header from stringtie annotation (option -l for stringtie)")
	parser.add_argument('-S','--sam-tag', default="", help="output tag to append to SAM file, no default")
	parser.add_argument('--trinity-v2', action="store_true", help="use Trinity v2.0 annotation style (TR1|c0_g0_i1)")
	parser.add_argument('--trinity-2014', action="store_true", help="use 2014 Trinity annotation style (c1_g1_i1)")
	parser.add_argument('--filtered', action="store_true", help="for filtered genes, get gene name from split")
	parser.add_argument('-A','--align', action="store_true", help="run bowtie-align")
	parser.add_argument('-B','--build', action="store_true", help="generate bowtie-build")
	parser.add_argument('-C','--count', action="store_true", help="make combined counts file")
	parser.add_argument('-D','--despliced', default="despliced", help="tag to append to despliced file [despliced]")
	parser.add_argument('-E','--exclude', type=int, metavar="N", default=0, help="exclude seqs with N or fewer hits [0]")
	parser.add_argument('-F','--fpkm', action="store_true", help="generate file for fpkm or tpm")
	parser.add_argument('-G','--genes', action="store_true", help="count genes for each treatment")
	parser.add_argument('-H','--header', action="store_true", help="add header line for counts files")
	parser.add_argument('-K','--components', action="store_true", help="combine trinity/stringtie components")
	parser.add_argument('-M','--matrix', action="store_true", help="generate fasta format binary matrix")
	parser.add_argument('-R','--raw', default="rawcounts", help="tag to append to raw counts file [rawcounts]")
	parser.add_argument('-u','--unpaired', action="store_true", help="use unpaired inputs, otherwise default is paired")
	#parser.add_argument('-2','--bowtie2', action="store_true", help="use bowtie2 instead of bowtie")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	parser.add_argument('--no-unal', action="store_true", help="ignore unaligned reads in the SAM file")
	args = parser.parse_args(argv)

	startclock=time.asctime()
	starttime= time.time()

	# get threads
	threadcount = get_threads(args.processors)

	# bowtie-build section
	if args.build:
		#if args.bowtie2:
		build_name = bowtie_build(args.buildfile, args.fasta, starttime, "bowtie2-build")
		#else:
		#	build_name = bowtie_build(args.buildfile, args.fasta, starttime, "bowtie-build")
	elif args.align:
		build_name = os.path.abspath(args.buildfile)
	else:
		build_name = None
	fasta_db = os.path.abspath(args.fasta)

	if args.align:
		bowtiestderr = "bowtie_{}.log".format(time.strftime("%Y%m%d-%H%M%S") )

	# collect list of sam files
	sam_list = []
	samcount = 0
	if args.inputs and len(args.inputs)%2:
		print >> sys.stderr, "WARNING: Odd number of inputs, expecting pairs", time.asctime()
	if len(args.inputs):
		print >> sys.stderr, "# Counted {} input files".format(len(args.inputs)), time.asctime()
	else:
		print >> sys.stderr, "# No reads as inputs, using {} SAM files".format(len(args.maps)), time.asctime()
	inputs = zip(*[iter(args.inputs)]*2)
	for inputgroup in inputs:
		basefilename,extension = os.path.splitext(inputgroup[0])
		if extension==".gz":
			basefilename = os.path.splitext(basefilename)[0]
		mapname = basefilename + args.sam_tag + ".sam"
		# bowtie-align section
		if args.align:
			# -q means fastq, no-sq means no header info
			# --no-sq suppresses output of database sequence names at the start
			# of the same file, such as
			# @SQ	SN:tethya_000001_comp0_c0_seq1	LN:337
			bowtie_args = ["bowtie2","-p",threadcount,"-x", build_name, "-1", inputgroup[0], "-2", inputgroup[1], "-q", "-S", mapname, "--no-sq", "--no-discordant"] # "--fr", "--phred33",
			if args.no_unal:
				bowtie_args.append("--no-unal")
			run_bowtie(bowtie_args, bowtiestderr)
			# this is strictly to make an individual counts file
			# since the master count is remade from the original sam files
		if args.count or args.align:
			countscode = bowtie_map_to_counts(mapname)
		# even if no alignment is made, the sam file can be called again from a prior run
		sam_list.append(mapname)

	# take sam files directly
	if args.maps:
		for samfile in args.maps:
			if args.count:
				countscode = bowtie_map_to_counts(samfile)
			sam_list.append(samfile)
	print >> sys.stderr, "Counted {} SAM files".format(len(sam_list) ), time.asctime()

	# make header line for Kaia
	if args.header:
		headerLine = make_header(sam_list, args.verbose)
	else:
		headerLine = None

	# count occurrence across all files either from .sam or from .counts
	# as fpkm requires the counts, do this for fpkm as well
	if args.count or args.fpkm or args.components:
		rawcntdict, totalcounts = count_for_all(fasta_db, sam_list, args.exclude)
		# print the overall counts file
		make_counts_file(fasta_db, rawcntdict, args.raw, "%d", headerLine)
		if args.components:
			if args.stringtie:
				transcriptregex = "({}[.]\d+)[.]\d+".format(args.stringtie)
			else:
				transcriptregex = get_trinity_re(args.trinity_v2, args.trinity_2014)
			if args.merge_components:
				mergevector = make_merge_vector(args.merge_components, transcriptregex)
			else:
				mergevector = None
			reducecntdict = make_reduced_db(rawcntdict, len(sam_list), transcriptregex, mergevector, args.filtered)
			# print the tpm counts file
			make_counts_file(fasta_db, reducecntdict, args.despliced, "%d", headerLine)

	# for fpkm also make the tpm
	if args.fpkm:
		tpmdict = make_tpm_dict(rawcntdict, totalcounts, fasta_db, args.length)
		# when -K is used to combine results by trinity component
		if args.components:
			transcriptregex = get_trinity_re(args.trinity_v2, args.trinity_2014)
			reducecntdict = make_reduced_db(tpmdict, len(sam_list), transcriptregex)
			# print the tpm counts file
			make_counts_file(fasta_db, reducecntdict, "tpmcounts", "%.3f", headerLine)
		# otherwise make the tpm file as usual
		else:
			make_counts_file(fasta_db, tpmdict, "tpmcounts", "%.3f", headerLine)

	if args.matrix and args.components:
		binarymatrix = make_binary_matrix(reducecntdict, len(sam_list))
		make_matrix_file(binarymatrix, fasta_db, sam_list)

	# exclude transcripts with no hits
	#if args.exclude:
	#	rl = inf
	#	rt = [x.rstrip().split("\t") for x in rl]
	#	rf = [x for x in rt if len(set(x[1:]))>1]
	#	nd = {}
	#	for l in rf:
	#		nd[l[0]] = sum([int(x) for x in l[1:]])

	# TODO for counting genes per cell
	#m = range(1:len(rt[0]))
	#cells = defaultdict(list)
	#for r in rt:
	#	for n in m:
	#		cells[n].append(int(bool(float(r[n]))))
	#cgc = []
	#for x in cells.itervalues():
	#	cgc.append(sum(x))


	print >> sys.stderr, "# Processed completed in %.1f minutes" % ((time.time()-starttime)/60)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)

