#!/usr/bin/env python
#
# removeplatanussingles.py created v1.0 2016-01-08

'''removeplatanussingles.py   v1.0 2016-02-04
    remove single contigs from planatus below length -l

removeplatanussingles.py -s avic_scaffold.fa -g avic_gapClosed.fa > avic_no_single_gapClosed.fa
'''

import sys
import argparse
import time
from Bio import SeqIO

# format of gapClosed.fa
#>scaffold1_cov223
#>scaffold2_cov52

# format of scaffold.fa
#>scaffold1_len110_cov223_single
#>scaffold2_len307_cov52_single

def parse_scaffolds(scaffoldfile):
	'''return a dictionary of single scaffold IDs'''
	scaffoldcount = 0
	singlescaffolds = {}
	print >> sys.stderr, "# Parsing scaffold IDs from {}".format(scaffoldfile), time.asctime()
	for line in open(scaffoldfile,'r').readlines():
		if line[0]==">":
			scaffoldcount += 1
			if line.find("single") != -1:
				scaffoldnumber = line[1:].split("_")[0]
				singlescaffolds[scaffoldnumber] = True
	print >> sys.stderr, "# From {} total scaffolds".format( scaffoldcount ), time.asctime()
	print >> sys.stderr, "# Found {} single scaffolds".format( len(singlescaffolds) ), time.asctime()
	return singlescaffolds

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-g','--gapclose', help="fasta file from gap_close step")
	parser.add_argument('-s','--scaffold', help="fasta file from scaffold step")
	parser.add_argument('-l','--length', type=int, help="minimum contig length [500]", default=500)
	args = parser.parse_args(argv)

	shortsingles = 0
	singlescaffolds = parse_scaffolds(args.scaffold)
	print >> sys.stderr, "# Parsing sequences from {}".format(args.gapclose), time.asctime()
	for seqrec in SeqIO.parse(args.gapclose,"fasta"):
		scaffoldnumber = seqrec.id.split("_")[0]
		if singlescaffolds.get(scaffoldnumber,False):
			if len(seqrec.seq) >= args.length:
				sys.stdout.write(seqrec.format("fasta") )
			else:
				shortsingles += 1
		else:
			sys.stdout.write(seqrec.format("fasta") )
	print >> sys.stderr, "# Omitted {} short single scaffolds".format( shortsingles ), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
