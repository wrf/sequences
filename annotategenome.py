#!/usr/bin/env python
#
# annotategenome.py v1.0 created 2015-04-20 modified 2015-05-05

'''
annotategenome.py v1.0 last modified 2015-05-05

  # EXAMPLE USAGE
annotategenome.py -g
'''

import sys
import os
import time
import argparse
import subprocess
import multiprocessing

#####
# EDIT THE PATHS OF THESE PROGRAMS BEFORE BEGINNING
#####
# websites are given in case new versions are needed
# https://genome.ucsc.edu/FAQ/FAQblat.html
# http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/
BLAT = os.path.expanduser("~/blat_v35/blatSrc/bin/x86_64/blat")
BLAT2GFF = os.path.expanduser("~/blat_v35/blatSrc/bin/x86_64/blat2gff.pl")
# http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml
BOWTIE2 = os.path.expanduser("~/bowtie2-2.2.4/bin/bowtie2")
BOWTIE2BUILD = os.path.expanduser("~/bowtie2-2.2.4/bin/bowtie2-build")
# http://ccb.jhu.edu/software/tophat/manual.shtml
TOPHAT2 = os.path.expanduser("~/tophat-2.0.13.Linux_x86_64/tophat2")
# http://cole-trapnell-lab.github.io/cufflinks/
CUFFLINKS = os.path.expanduser("~/cufflinks-2.2.1.Linux_x86_64/bin/cufflinks")
GFFREAD = os.path.expanduser("~/cufflinks-2.2.1.Linux_x86_64/bin/gffread")
# http://ccb.jhu.edu/software/stringtie/
STRINGTIE = os.path.expanduser("~/stringtie-1.0.2.Linux_x86_64/stringtie")
# http://dendrome.ucdavis.edu/resources/tooldocs/wise2/doc_wise2.html
# genewise might require a precompiled binary, as compilation may fail
GENEWISE = "/usr/bin/genewise"
# ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/
BLAST_PATH = os.path.expanduser("~/ncbi-blast-2.2.30+/bin/")
BLASTX = os.path.join(BLAST_PATH, "blastx")
TBLASTN = os.path.join(BLAST_PATH, "tblastn")
MAKEBLASTDB = os.path.join(BLAST_PATH, "makeblastdb")
# http://research-pub.gene.com/gmap/
GMAP = os.path.expanduser("~/gmap-2014-12-28/bin/gmap")
GMAPBUILD = os.path.expanduser("~/gmap-2014-12-28/util/gmap_build")
# http://pasapipeline.github.io/
# https://github.com/PASApipeline/PASA_Lite
PASAVALIDATE = os.path.expanduser("~/PASA_Lite-master/PASA.alignmentValidator")
PASAASSEMBLER = os.path.expanduser("~/PASA_Lite-master/PASA.alignmentAssembler")
# http://evidencemodeler.sourceforge.net/
#EVM = os.path.expanduser("~/EVM_r2012-06-25/evidence_modeler.pl")
EVMPARTITION = os.path.expanduser("~/EVM_r2012-06-25/EvmUtils/partition_EVM_inputs.pl")
EVMCOMMANDS = os.path.expanduser("~/EVM_r2012-06-25/EvmUtils/write_EVM_commands.pl")
PARALLEL = "/usr/bin/parallel"
EVMRECOMBINE = os.path.expanduser("~/EVM_r2012-06-25/EvmUtils/recombine_EVM_partial_outputs.pl")
EVMCONVERT = os.path.expanduser("~/EVM_r2012-06-25/EvmUtils/convert_EVM_outputs_to_GFF3.pl")
#####

def run_gffread(gtfFile):
	gff_output = "{}.gff3".format(os.path.splitext(gtfFile)[0] )
	gffread_args = [GFFREAD, "-E", "-o", gff_output, gtfFile]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(gffread_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(gffread_args) )
	subprocess.call(gffread_args)
	print >> sys.stderr, "# gffread completed", time.asctime()
	if os.path.isfile(gff_output):
		return gff_output
	else:
		raise OSError("Cannot find expected output file {}".format(gff_output) )

def run_blat(genomeFile, genBuildName, transFile, transcriptomeName, logfile):
	blat_output = "{}_vs_{}_blat_{}.psl".format(transcriptomeName, genBuildName, time.strftime("%Y%m%d") )
	blat_args = [BLAT, "-t=dna", "-q=rna", genomeFile, transFile, blat_output]
	print >> logfile, "{}".format(find_version_info(BLAT) )
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(blat_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(blat_args) )
	subprocess.call(blat_args)
	print >> sys.stderr, "# blat completed", time.asctime()
	# default output of blat is .psl format, which must be converted to gff3 for EVM
	blatgff_output = "{}.gff3".format(os.path.splitext(blat_output)[0] )
	subprocess.call([BLAT2GFF], stdin=blat_output, stdout=blatgff_output )
	if os.path.isfile(blatgff_output):
		return blatgff_output
	else:
		raise OSError("Cannot find expected output file {}".format(blatgff_output) )

def run_tophat(genBuildName, groupName, reads, threadcount, logfile, stranded=True):
	tophat_output = "{}_tophat_out_{}".format(groupName, time.strftime("%Y%m%d") )
	left_reads = reads[0]
	right_reads = reads[1]
	tophat2_args = [TOPHAT2, "-p", threadcount, "-o", tophat_output, genBuildName, left_reads, right_reads]
	if stranded:
		tophat2_args = [TOPHAT2, "-p", threadcount, "-o", tophat_output, "--library-type", "fr-firststrand", genBuildName, left_reads, right_reads]
	print >> logfile, "{}".format(find_version_info(TOPHAT2) )
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(tophat2_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(tophat2_args) )
	subprocess.call(tophat2_args)
	print >> sys.stderr, "# tophat2 completed", time.asctime()
	if os.path.isfile(tophat_output):
		return tophat_output
	else:
		raise OSError("Cannot find expected output file {}".format(tophat_output) )

def run_cufflinks(groupName, tophat_output, threadcount, logfile):
	tophat_hits = os.path.join(tophat_output, "accepted_hits.bam")
	cufflinks_output = "{}_cufflinks_out_{}".format(groupName, time.strftime("%Y%m%d") )
	cufflinks_args = [CUFFLINKS, "-p", threadcount, "-o", cufflinks_output, tophat_hits]
	print >> logfile, "{}".format(find_version_info(CUFFLINKS) )
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(cufflinks_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(cufflinks_args) )
	subprocess.call(cufflinks_args)
	print >> sys.stderr, "# cufflinks completed", time.asctime()
	cufflinks_gff = run_gffread(cufflinks_output)
	if os.path.isfile(cufflinks_gff):
		return cufflinks_gff
	else:
		raise OSError("Cannot find expected output file {}".format(cufflinks_gff) )

def run_stringtie(genBuildName, groupName, tophat_output, threadcount, logfile):
	tophat_hits = os.path.join(tophat_output, "accepted_hits.bam")
	stringtie_output = "{}_stringtie_{}.gft".format(groupName, time.strftime("%Y%m%d") )
	# -l is for the gtf format gene_id, nothing to do with file names
	stringtie_args = [STRINGTIE, tophat_hits, "-o", stringtie_output, "-p", threadcount, "-l", genBuildName]
	print >> logfile, "{}".format(find_version_info(STRINGTIE) )
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(stringtie_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(stringtie_args) )
	subprocess.call(stringtie_args)
	print >> sys.stderr, "# stringtie completed", time.asctime()
	stringtie_gff = run_gffread(stringtie_output)
	if os.path.isfile(stringtie_gff):
		return stringtie_gff
	else:
		raise OSError("Cannot find expected output file {}".format(stringtie_gff) )

def run_tblastn(genomeFile, genBuildName, protName, evalue, threadcount, logfile):
	if check_blastdb(genomeFile):
		print >> sys.stderr, "# blast database already exists for {}".format(genomeFile), time.asctime()
	else:
		run_makeblastdb(genomeFile)
	tblastn_output = "{}_vs_{}_tblastn_{}.tab".format(protName, genBuildName, time.strftime("%Y%m%d") )
	tblastn_args = [TBLASTN, "-query", protName, "-db", genomeFile, "-num_threads", threadcount, "-evalue", evalue, "-outfmt", "6" "-max_target_seqs", "100", "-out", tblastn_output]
	print >> logfile, "{}".format(find_version_info(TBLASTN) )
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(tblastn_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(tblastn_args) )
	subprocess.call(tblastn_args)
	print >> sys.stderr, "# tblastn completed", time.asctime()
	return tblastn_output

def run_makeblastdb(genomeFile):
	makeblastdb_args = [MAKEBLASTDB, "-in", genomeFile, "-dbtype", "nucl"]
	print >> logfile, "{}".format(find_version_info(MAKEBLASTDB) )
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(makeblastdb_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(makeblastdb_args) )
	subprocess.call(makeblastdb_args)
	if not check_blastdb(genomeFile)
		raise OSError("Cannot find expected blast database for {}".format(genomeFile) )
	print >> sys.stderr, "# makeblastdb completed", time.asctime()
	# nothing to return

def check_blastdb(genomeFile):
	f1, f2, f3 = "{}.nhr".format(genomeFile), "{}.nin".format(genomeFile), "{}.nsq".format(genomeFile)
	if os.path.isfile(f1) and os.path.isfile(f2) and os.path.isfile(f3):
		return 1
	else:
		return 0

def blast_to_gff(genomeFile, protName, evalue, logfile):
	# blast2gff.py -b tblastn_output.tab -q refprots.fa -d target_genome.fa
	return 1



def run_gmap(genomeFile, genBuildName, transFile, transcriptomeName, threadcount, logfile):
	gmap_output = "{}_vs_{}_gmap_{}.gff".format(transcriptomeName, genBuildName, time.strftime("%Y%m%d") )
	# for PASA to work, this must be format 3 which is gff3_match_cdna format
	gmap_args = [GMAP, "-g", genomeFile, "-t", threadcount, transFile, "-f", "3"]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(gmap_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(gmap_args) )
	subprocess.call(gmap_args)
	print >> sys.stderr, "# gmap completed", time.asctime()
	if os.path.isfile(gmap_output):
		return gmap_output
	else:
		raise OSError("Cannot find expected output file {}".format(gmap_output) )

def run_pasa(genomeFile, gmap_output, genBuildName, transcriptomeName, threadcount, logfile):
	pasa_outtag = "{}_vs_{}_{}".format(transcriptomeName, genBuildName, time.strftime("%Y%m%d") )
	#pasavalidate_output = "pasa_lite.valid_alignments.gtf" # default output name
	pasavalidate_output = "{}.valid_alignments.gtf".format(pasa_outtag)
	pasavalidate_args = [PASAVALIDATE, "--genome", genomeFile, "--CPU", threadcount, "--out_prefix", pasa_outtag, gmap_output]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(pasavalidate_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(pasavalidate_args) )
	subprocess.call(pasavalidate_args)
	print >> sys.stderr, "# pasa validate completed", time.asctime()

	pasaassembler_output = "{}.pasa_assembled_alignments.gtf".format(pasa_outtag)
	#pasaassemble_output = "pasa_lite.pasa_assembled_alignments.gtf"
	pasaassembler_args = [PASAASSEMBLER, "--CPU", threadcount, "--out_prefix", pasa_outtag, pasavalidate_output]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(pasaassembler_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(pasaassembler_args) )
	subprocess.call(pasaassembler_args)
	print >> sys.stderr, "# pasa assembler completed", time.asctime()
	pasa_gff = run_gffread(pasaassembler_output)
	if os.path.isfile(pasa_gff):
		return pasa_gff
	else:
		raise OSError("Cannot find expected output file {}".format(pasa_gff) )
	### TODO PASA output contains a lot of comment lines as "#" which must be removed for downstream
	return pasaassembler_output

def run_evm(genomeFile, gff_outputs, segmentsize, overlap, threadcount, logfile):
	# evm weights files is tab delimited
	transcript_outputs = ""
	protein_outputs = ""
	partitions_list = "partitions_{}.txt".format(time.strftime("%Y%m%d") )
	evm_args = [EVMPARTITION, "--genome", genomeFile, "--protein_alignments", protein_outputs, "--transcript_alignments", transcript_outputs, "--segmentSize", segmentsize, "--overlapSize", overlap, "--partition_listing",  partitions_list]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(evm_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(evm_args) )
	subprocess.call(evm_args)
	#
	commands_file = "commands_{}.txt".format(time.strftime("%Y%m%d") )
	output_name = "evm.out"
	evm_args = [EVMCOMMANDS, "--genome", genomeFile, "--weights", weights_file, "--protein_alignments", protein_outputs, "--transcript_alignments", transcript_outputs, "--output_file_name", output_name, "--partition_listing",  partitions_list]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(evm_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(evm_args) )
	with open(commands_file,'w') as cmf:
		subprocess.call(evm_args, stdout=cmf)
	#
	parallel_args = ["parallel", "--gnu", "-a", commands, "-j", threads, "--joblog", "evm_{}.log".format(time.strftime("%Y%m%d")), "--halt 1"]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(parallel_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(parallel_args) )
	subprocess.call(parallel_args)
	#
	evm_args = [EVMRECOMBINE, "--partition_listing",  partitions_list, "--output_file_name", output_name]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(evm_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(evm_args) )
	subprocess.call(evm_args)
	#
	evm_args = [EVMCONVERT, "--partition_listing",  partitions_list, "--output_file_name", output_name, "--genome", genomeFile]
	print >> logfile, "{}\n{}".format(time.asctime(), " ".join(evm_args) )
	print >> sys.stderr, "{}\n{}".format("Making system call", " ".join(evm_args) )
	subprocess.call(evm_args)
	#

def subprogram_checks(args):
	programList = []
	readyToGo = True
	if args.blat:
		programList.append(BLAT)
	if args.cufflinks:
		programList.extend([CUFFLINKS, TOPHAT2, GFFREAD])
	if args.evm:
		programList.append(EVM)
	if args.gmap:
		programList.extend([GMAP, GMAPBUILD])
	if args.ncbi_blast:
		programList.extend([BLASTX, TBLASTN, MAKEBLASTDB])
		if not args.blast:
			print >> sys.stderr, "# No protein files specified with -b", time.asctime()
			readyToGo = False
	if args.pasa:
		programList.extend([PASAVALIDATE, PASAASSEMBLER, GFFREAD])
		if not args.transcriptome:
			print >> sys.stderr, "# No transcriptome files specified with -t", time.asctime()
			readyToGo = False
	if args.stringtie:
		programList.extend([STRINGTIE, TOPHAT2, GFFREAD])
	if args.tophat:
		programList.append(TOPHAT2)
		if not args.transcriptome:
			print >> sys.stderr, "# No rnaseq files specified with -r", time.asctime()
			readyToGo = False
	if args.genewise:
		programList.append(GENEWISE)
	for prog in set(programList):
		if os.path.isfile(prog):
			print >> sys.stderr, "# %s found... OK" % (prog)
		else:
			print >> sys.stderr, "# Cannot find program %s" % (prog), time.asctime()
			readyToGo = False
	return readyToGo

def find_version_info(PROGRAM):
	if PROGRAM == BLAT:
		fullstdout = subprocess.Popen(PROGRAM, stdout=subprocess.PIPE).communicate()[0]
		# 'blat - Standalone BLAT v. 35 fast sequence search command line tool'
		return fullstdout.split("\n")[0]
	elif PROGRAM == BLASTX:
		fullstdout = subprocess.Popen([PROGRAM, "-h"], stdout=subprocess.PIPE).communicate()[0]
		# '   Translated Query-Protein Subject BLAST 2.2.30+'
		return fullstdout.split("\n")[24][3:]
	elif PROGRAM == TBLASTN:
		fullstdout = subprocess.Popen([PROGRAM, "-h"], stdout=subprocess.PIPE).communicate()[0]
		# '   Protein Query-Translated Subject BLAST 2.2.30+'
		return fullstdout.split("\n")[24][3:]
	elif PROGRAM == MAKEBLASTDB:
		fullstdout = subprocess.Popen([PROGRAM, "-h"], stdout=subprocess.PIPE).communicate()[0]
		# '   Application to create BLAST databases, version 2.2.30+'
		return fullstdout.split("\n")[10][3:]
	elif PROGRAM == GENEWISE:
		fullstdout = subprocess.Popen([PROGRAM, "-version"], stdout=subprocess.PIPE).communicate()[0]
		# 'Version: $Name: wise2-4-1 $'
		return fullstdout.split("\n")[1].split(' ')[2]
	elif PROGRAM == CUFFLINKS:
		fullstdout = subprocess.Popen(PROGRAM, stderr=subprocess.PIPE).communicate()[1]
		# 'cufflinks v2.2.1'
		return fullstdout.split("\n")[0]
	elif PROGRAM == STRINGTIE:
		fullstdout = subprocess.Popen([PROGRAM, "-h"], stderr=subprocess.PIPE).communicate()[1]
		# 'StringTie v1.0.2 usage:'
		return fullstdout.split("\n")[0].rsplit(' ',1)[0]
	elif PROGRAM == TOPHAT2:
		fullstdout = subprocess.Popen([PROGRAM, "-v"], stdout=subprocess.PIPE).communicate()[0]
		# 'TopHat v2.0.13\n'
		return fullstdout.split("\n")[0]
	elif PROGRAM == GMAP:
		fullstdout = subprocess.Popen([PROGRAM, "--version"], stderr=subprocess.PIPE).communicate()[1]
		# 'GMAP version 2014-12-28 called with args: /home/wrf/gmap-2014-12-28/bin/gmap --version'
		return fullstdout.split("\n")[0]
	else:
		print >> sys.stderr, "# program {} not recognized".format(PROGRAM), time.asctime()
		return "NO VERSION FOR {}".format(PROGRAM)

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--inputs', nargs='*', help="reads from each sample")
	parser.add_argument('-b','--blast', nargs='*', help="protein reference files for tblastn")
	parser.add_argument('-c','--curated', nargs='*', help="manually curated genes as gff3")
	parser.add_argument('-g','--genome', help="genome to annotate, build and map to this file")
	parser.add_argument('-r','--rnaseq', nargs='*', help="RNAseq libraries to map to the genome")
	parser.add_argument('-t','--transcriptome', nargs='*', help="assembled transcriptomes as fasta")
	parser.add_argument('-e','--evalue', type=float, default=1e-4, help="evalue cutoff for post blast filtering [1e-4]")
	parser.add_argument('-n','--name', help="name of the genome build, such as Avic-v1.0")

	parser.add_argument('-l','--length', type=int, help="read length, default: 100", default=100)

	parser.add_argument('-p','--processors', type=int, help="number of processors [16]", default=16)
	#parser.add_argument('-t','--new-trinity', const='-', help="use new trinity annotation style")
	parser.add_argument('-B','--blat', const='-', help="run blat (requires transcriptome -t)")
	parser.add_argument('-C','--cufflinks', const='-', help="run cufflinks (requires tophat -T)")
	parser.add_argument('-E','--evm', const='-', help="run evidence modeler as final step")
	parser.add_argument('-G','--gmap', const='-', help="run gmap (requires transcriptome -t")
	parser.add_argument('-N','--ncbi-blast', const='-', help="run tblastn (requires blast inputs -b)")
	parser.add_argument('-P','--pasa', const='-', help="run PASA (requires gmap -G)")
	parser.add_argument('-S','--stringtie', const='-', help="run stringtie (requires tophat -T)")
	parser.add_argument('-T','--tophat', const='-', help="run tophat (requires RNAseq data -r)")
	parser.add_argument('-W','--genewise', const='-', help="run genewise (requires blast -N)")
	args = parser.parse_args(argv)

	startclock=time.asctime()
	starttime= time.time()

	# check if all programs are there
	print >> sys.stderr, "### Checking for sub programs", time.asctime()
	if not subprogram_checks(args):
		print >> sys.stderr, "# Exiting", time.asctime()
		sys.exit()

	if args.name:
		genBuildName = args.name
	elif args.genome:
		genBuildName = os.path.splitext(os.path.basename(args.genome))[0]
	else:
		print >> sys.stderr, "# Must specify genome with -g"
		print >> sys.stderr, "# Exiting", time.asctime()
		sys.exit()

	logName = "{0}.annotategenome.{1}.log".format(genBuildName, time.strftime("%Y-%m-%d-%H%M%S"))
	logfile = open(logName, 'w')

	print >> logfile, "{}".format(" ".join(sys.argv) )
	print >> logfile, "Last modified: {}".format(time.ctime(os.path.getmtime(__file__)))

	print >> sys.stderr, "### Checking processors", time.asctime()
	threadcount = min(multiprocessing.cpu_count(), args.processors)
	print >> sys.stderr, "# Using %d threads" % (threadcount), time.asctime()

	



	print >> sys.stderr, "# Processed completed in %.1f minutes" % ((time.time()-starttime)/60)
	logfile.close()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
