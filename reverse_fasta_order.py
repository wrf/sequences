#!/usr/bin/env python
# reverse_fasta_order.py v1 created 2022-10-19

"""
reverse_fasta_order.py  last modified 2022-10-19

reverse_fasta_order.py sequences.fasta > sequences.rev.fasta

    duplicate IDs will have _# appended to the fasta header

"""

import sys
from Bio import SeqIO

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	seq_order = []
	seq_dict = {}
	for seqrec in SeqIO.parse(sys.argv[1],"fasta"):
		if seqrec.id not in seq_dict:
			seq_order.append(seqrec.id)
			seq_dict[seqrec.id] = seqrec
		else: # catch for duplicate entries
			seqrec.id = "{}_#".format(seqrec.id)
			seq_order.append(seqrec.id)
			seq_dict[seqrec.id] = seqrec
	for seqid in reversed(seq_order):
		sys.stdout.write(seq_dict.get(seqid).format("fasta"))

