#!/usr/bin/env python
#
# removesvgbootstraps.py v1.0 2016-10-05

'''removesvgbootstraps.py  last modified 2016-10-05

removesvgbootstraps.py figtree.svg 100 > figtree_no100.svg

    CHANGE MAX BOOTSTRAP IN SECOND ARGUMENT

removesvgbootstraps.py figtree.svg 97 > figtree_no97.svg
'''

#  <text
#     x="497.56769"
#     xml:space="preserve"
#     y="416.23209"
#     style="font-size:8.13226318px;stroke:none;font-family:Bitstream Charter"
#     id="text971"
#     transform="matrix(0.98726342,-0.1590941,0.1590941,0.98726342,0,0)">100</text>

import sys
import os
import xml.etree.ElementTree

def main(argv, wayout):
	svginput = argv[0]
	if not os.path.isfile(svginput):
		sys.exit("ERROR: CANNOT FIND FILE {}".format(svginput))
	if len(argv) > 1:
		try:
			maxbsvalue = int(argv[1])
		except ValueError: # cannot convert non-integer
			print >> sys.stderr, "ERROR: {} IS NOT AN INTEGER, USING 100".format(argv[1])
			maxbsvalue = 100
	else:
		maxbsvalue = 100

	# parse SVG as XML format
	svgtree = xml.etree.ElementTree.parse(svginput)
	svgroot = svgtree.getroot()

	#for item in svgroot.iter():
	svgiter = svgroot.findall(".//text")
	if svgiter is None: # use format from Inkscape SVG
		svgiter = svgroot.findall(".//{http://www.w3.org/2000/svg}text")
	for item in svgiter:
		if item.text:
			#print >> sys.stderr, item.text
			try:
				bsval = int(item.text)
				if bsval >= maxbsvalue:
					svgroot.remove(item)
			except ValueError: # item is probably a label
				continue
	svgtree.write(wayout)

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
