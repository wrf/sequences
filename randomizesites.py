#! /usr/bin/env python
#
usage='''randomizesites.py
Randomize sites in large alignments without replacement

Requires Biopython library

Example:
randomizesites.py -i bigalign.fa -s 1000 -n 100

To subset blocks at a time, such as entire genes up to 500 positions:
randomizesites.py -i bigalign.fa -s 500 -n 100 -l "0:320,321:650,651:990"
randomizesites.py -i bigalign.fa -s 500 -n 100 -l partition_file.txt

partition file must be a single line in format of:
1:136,137:301,302:519,520:1015,1016:1519,1520:2018,2019:2497

This will automatically generate separate files for each iteration
Files are named by iteration, such as bigalign.1000s_i99.fa

Formats include:
clustal, fasta, nexus, phylip, phylip-relaxed, stockholm
'''

import sys
import random
import argparse
import os
from Bio import AlignIO

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-i','--input_file', help="alignment file")
	parser.add_argument('-f','--format', help="alignment format (fasta,clustal,phylip), default: fasta", default="fasta")
	parser.add_argument('-l','--loci', help='subsample loci/genes instead of sites, in quotes as "0:80,150:250"')
	parser.add_argument('-n','--iterations', type=int, help="number of randomize iterations: 1", default=1)
	parser.add_argument('-s','--sites', type=int, help="number of sites to be selected default: 100", default=100)
	args = parser.parse_args(argv)

	itercount = 0
	sitenum = args.sites
	alignedseqs = AlignIO.read(open(args.input_file,'r'),args.format)
	alength = alignedseqs.get_alignment_length()
	print >> sys.stderr, "Read alignment of %d sequences with %d positions" % (alignedseqs.__len__(), alength)

	# if args.loci exist, generate the loci from the partitions
	if args.loci:
		if os.path.exists(args.loci):
			print >> sys.stderr, "Using %s as partitions file" % (args.loci)
			# if loci file is real, then read the first line and use it
			with open(args.loci,'r') as lf:
				locistring = lf.readlines()[0].rstrip()
		else:
			# otherwise assume it is from the args
			locistring = args.loci
		# need to split multiple times, first time to separate number pairs
		# then to generate the tuple of ints of the two numbers
		locitotuples = [(int(n.split(":")[0]),int(n.split(":")[1])) for n in locistring.split(",")]
		# using those indices for slicing, make the list of alignments
		alignloci = [alignedseqs[:,x[0]:x[1]] for x in locitotuples]

	# if number of sites is greater than the alignment length, reassign that value to the length
	if alength < sitenum:
		print >> sys.stderr, "-s of %d is too high, forcing to %d" % (sitenum,alength)
		sitenum = alength

	print >> sys.stderr, "Generating %d iterations of %d random positions" % (args.iterations, sitenum)
	for i in xrange(args.iterations):
		itercount += 1
		newalign = alignedseqs[:,0:0]
		# behavior is fundamentally different by selecting entire blocks at once
		if args.loci:
			lociorder = random.sample(xrange(len(alignloci)),len(alignloci))
			for s in lociorder:
				newalign += alignloci[s]
				# if the new alignment is longer than require number of sites, break
				if newalign.get_alignment_length() >= sitenum:
					print >> sys.stderr, "Alignment contains %d positions" % newalign.get_alignment_length()
					break
		# otherwise continue as normal with site sampling
		else:
			# take slices of size 1 from random sites without replacement
			slices = [alignedseqs[:,x:x+1] for x in random.sample(xrange(alength),sitenum)]
			for s in slices:
				newalign += s
		outputname = "%s.%ds_i%d%s" % (os.path.splitext(args.input_file)[0],sitenum,i,os.path.splitext(args.input_file)[1])
		AlignIO.write(newalign, outputname, args.format)
	print >> sys.stderr, "Wrote %d alignments" % itercount

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
