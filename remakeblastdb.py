#!/usr/bin/env python
#
# remake master blast server for databases 2013-10-17
# updated usage, reads db from ~/.ncbirc 2015-01-28

import sys
import os
import subprocess
import glob

usage='''
remakeblastdb.py your_sample_name
for example:
remakeblastdb.py aequorea_filtered


this functions as the nr database generator in assemblerblaster
submit the directory or organism name with no '/', 
  the program generates a list of all available dbs with .fa
then calls the function to create the non redundant db in the ncbi/db folder

db folder is read from ~/.ncbirc
'''

def makenrdb(db_list,output_dir,ncbi_path):
	vbdb_name = os.path.join(ncbi_path, output_dir + "_all.nr.fasta")
	redundant_path = output_dir + "_all.fasta"
	with open(redundant_path,'w') as redundant_file:
	#redundant_file = open(redundant_path,'w')
		print >> sys.stderr, "Generating non-redundant dataset for %s" % (output_dir)
		for db in db_list:
			for line in open(db,'r'):
				redundant_file.write(line)
	#redundant_file.close()
	sequniq_path = os.path.expanduser("~/gt-1.4.1-Linux_x86_64-64bit/bin/gt")
	sequniq_args = [sequniq_path,"sequniq","-force","-o",vbdb_name,redundant_path]
	subprocess.call(sequniq_args)
	blastdb_args = ["makeblastdb","-in", vbdb_name,"-dbtype","nucl"]
	subprocess.call(blastdb_args)
	return vbdb_name

def main(argv):
	if len(argv) < 2:
		print usage
	else:
		outdir = argv[1]
		ncbirc_info = open(os.path.expanduser("~/.ncbirc"), 'r').readlines()
		ncbi_path = ncbirc_info[1].split("=")[1].rstrip()
		targetstring = os.path.join(ncbi_path, outdir+"*.fa")
		print >> sys.stderr, "Recreating nr db for %s" % (outdir)
		dblist = glob.glob(targetstring)
		for db in dblist:
			print >> sys.stderr, db
		finaldb = makenrdb(dblist, outdir, ncbi_path)
		print >> sys.stderr, "Done"

if __name__ == "__main__":
	main(sys.argv)
