#!/usr/bin/env python
#
# splitjob.py v1 2015-03-18

import sys
import os
import argparse
import time
import multiprocessing
from Bio import SeqIO

usage='''
splitjob.py -f too_much_seqs.fasta -c "program -i $ -x -y -z" -d splits
'''

def split_fasta(fasta, command, blocksize, outputdir, commandfile):
	filenum = 0
	basecount = 0
	print >> sys.stderr, "Starting fasta split", time.asctime()
	# create the file to list all the commands
	with open(commandfile, 'w') as cf:
		# then start reading in the fasta file which will be split
		for record in SeqIO.parse(fasta, 'fasta'):
			# start counting total bases
			basecount += len(record.seq)
			# if basecount is greater than one blocks worth, iterate to the next file
			# because the default filenum is 0, the first records should write immediately
			if basecount > (filenum * blocksize):
				filenum = basecount / blocksize + 1
				fastablock = os.path.join(outputdir, '%d.fa' % filenum)
				block_file = open(fastablock,'w')
				print >> cf, command.replace("$",os.path.abspath(fastablock))
			# otherwise just print the sequence to the first block
			SeqIO.write(record, block_file, 'fasta')
	print >> sys.stderr, "Split fasta into %d blocks" % (filenum), time.asctime()
	print >> sys.stderr, "Commands look like this:\n%s" % (command.replace("$",os.path.abspath(fastablock)))

def safe_mkdir(path):
	if os.path.isfile(path):
		raise ValueError("{0} is a regular file, a directory is required".format(path))
	elif os.path.isdir(path):
		print("The directory {0} already exists, and will be used".format(path))
	else:
		print("Making the directory {0}".format(path))
		os.mkdir(path)

def get_progname(command):
	# assume first term is a program name,
	# and could be absolute path, so change to basename
	return os.path.basename(command.split(" ")[0])

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-c','--command', help="command to be repeated, must be in quotes")
	parser.add_argument('-d','--dir', help="directory for split files")
	parser.add_argument('-f','--fasta', help="fasta reference file")
	parser.add_argument('-s','--size', type=int, default=50000, help="size of split fasta files in bases (or AAs), default 50k")
	args = parser.parse_args(argv)

	progname = get_progname(args.command)
	base_out = "{0}.{1}".format(progname, time.strftime("%H%M%S"))
	commands_file = "{0}.commands.sh".format(base_out)
	log_file = "{0}.log".format(base_out)

	safe_mkdir(args.dir)
	split_fasta(args.fasta, args.command, args.size, args.dir, commands_file)

	print >> sys.stderr, "Detecting processors..."
	threadcount = str(multiprocessing.cpu_count())

	print >> sys.stderr, "RUN THIS COMMAND:"
	print >> sys.stderr, "parallel --gnu -a {0} -j {1} --joblog {2} --halt 1".format(commands_file, threadcount, log_file)

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
