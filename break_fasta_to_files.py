#! /usr/bin/env python
#
# created 2015-10-23
# python3 update 2022-04-28
# filetype changed to rt instead of rU 2024-12-09

"""break_fasta_to_files.py  last modified 2024-12-09
  Requires Bio library (biopython)
  split fasta file into multiple files where fasta header is the filename

break_fasta_to_files.py sequences.fasta
"""

import sys
import os
from Bio import SeqIO
import argparse
import time

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('rt'), default = '-', help="fasta format file")
	parser.add_argument('-f', '--format', default='fasta', help="sequence format [fasta]")
	args = parser.parse_args(argv)

	seqcount = 0
	sys.stderr.write( "# Parsing sequences on {}  {}\n".format(args.input_file.name, time.asctime() ) )
	for seq_record in SeqIO.parse(args.input_file, args.format):
		seqcount += 1
		outfilename = "{}.{}".format(seq_record.id.replace("/","_").split()[0], args.format)
		with open(outfilename, 'w') as sf:
			sf.write(seq_record.format(args.format))
	sys.stderr.write( "# Generated {} files for sequences  {}\n".format(seqcount, time.asctime() ) )

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
