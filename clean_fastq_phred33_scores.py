#! /usr/bin/env python
#
# clean_fastq_phred33_scores.py 2015-11-24
'''clean_fastq_phred33_scores.py  last modified 2022-01-02

convert erroneous FASTQ PHRED33 quality scores to letter J

clean_fastq_phred33_scores.py seqs.fastq > seqs_cleanquals.fastq

    FOR EXAMPLE, in this actual read:
@2426:0 1:N:0:
CTTAGCCAAATTTCTGAAAGAAACCCTGATGGATATTCATGATCATAGCTTATGACATACTTTTTCTTCTGTATCCTATT
+
GBBBBGGGGGG;:??BG@BGBBG?=?B@BBGBGGGGGGGGBBGGBBBBGBBGEGGGGG@BGG?>NGGGGGBBCG??GGGG
                              note the presence of the letter N ^

    The maximum score in PHRED33 is J, so N should only occur in PHRED64
    Thus, all K,L,M, and N's are replaced by J
'''

import sys
from collections import Counter

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	fastqfile = sys.argv[1]
	qual_score_counter = Counter()
	readCount = 0
	lnum = 1
	print( "# Reading {}".format(fastqfile) , file=sys.stderr )
	for line in open(fastqfile,'r'):
		line = line.strip()
		if lnum==1:
			lnum += 1
			readCount += 1
			if line[0]=="@":
				print( line , file=sys.stdout )
			else:
				print( "# ERROR: expecting @ as first character for line 1 for read {}, found {}".format(readCount, line[0]) , file=sys.stderr )
				line = "@" + line[1:]
				print( line , file=sys.stdout )
		elif lnum==2:
			print( line , file=sys.stdout )
			lnum += 1
		elif lnum==3 and line[0]=="+":
			print( line , file=sys.stdout )
			lnum += 1
		elif lnum==4:
			qual_score_counter.update(line)
			line = line.replace("K","J")
			line = line.replace("L","J")
			line = line.replace("M","J")
			line = line.replace("N","J")
			print( line , file=sys.stdout )
			lnum = 1
	print( "{}".format( qual_score_counter ) , file=sys.stderr )
	print( "# Done cleaning quals for {} reads".format(readCount) , file=sys.stderr )

