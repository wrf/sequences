#!/usr/bin/env python
#
# reformatgff.py
# v1.0 2015-05-22

# EVM outputs appear as follows, with gene, mRNA, then alternating exons and CDS
# Avic_s1	EVM	gene	3819	6818	.	+	.	ID=evm.TU.Avic_s1.1;Name=EVM%20prediction%20Avic_s1.1
# Avic_s1	EVM	mRNA	3819	6818	.	+	.	ID=evm.model.Avic_s1.1;Parent=evm.TU.Avic_s1.1
# Avic_s1	EVM	exon	3819	3965	.	+	.	ID=evm.model.Avic_s1.1.exon1;Parent=evm.model.Avic_s1.1
# Avic_s1	EVM	CDS	3819	3965	.	+	1	ID=cds.evm.model.Avic_s1.1;Parent=evm.model.Avic_s1.1
# Avic_s1	EVM	exon	4065	4587	.	+	.	ID=evm.model.Avic_s1.1.exon2;Parent=evm.model.Avic_s1.1
# Avic_s1	EVM	CDS	4065	4587	.	+	1	ID=cds.evm.model.Avic_s1.1;Parent=evm.model.Avic_s1.1
#
# each exon is then displayed as a single feature, rather than a connected chain
# this format is different from the sequence ontology format, where exons are in blocks
# http://www.sequenceontology.org/gff3.shtml

# some JGI formats appear as:
# scaffold_2	JGI	exon	214046	214100	.	+	.	name "gw1.2.819.1"; transcriptId 3192
# scaffold_2	JGI	CDS	214046	214100	.	+	0	name "gw1.2.819.1"; proteinId 3192; exonNumber 1
# scaffold_2	JGI	start_codon	214046	214048	.	+	0	name "gw1.2.819.1"
# scaffold_2	JGI	stop_codon	214098	214100	.	+	0	name "gw1.2.819.1"
# scaffold_2	JGI	exon	214221	214429	.	+	.	name "gw1.2.819.1"; transcriptId 3192
# scaffold_2	JGI	CDS	214221	214429	.	+	1	name "gw1.2.819.1"; proteinId 3192; exonNumber 2
# scaffold_2	JGI	exon	214516	214587	.	+	.	name "gw1.2.819.1"; transcriptId 3192
# scaffold_2	JGI	CDS	214516	214587	.	+	0	name "gw1.2.819.1"; proteinId 3192; exonNumber 3
# scaffold_2	JGI	exon	214715	214819	.	+	.	name "gw1.2.819.1"; transcriptId 3192
# scaffold_2	JGI	CDS	214715	214819	.	+	0	name "gw1.2.819.1"; proteinId 3192; exonNumber 4
#
# script should reorder blocks of exons and CDS


'''reformatgff.py  last modified 2016-03-24
    reorders exons and CDS in some interleaved gff3 formats
    where format is:
    gene  mRNA  exon  CDS  exon  CDS
    and should be:
    gene  mRNA  exon  exon  CDS  CDS

reformatgff.py evm.out.combined.gff3 > evm.out.reformat.gff3

    for AUGUSTUS GFF3, use -a
'''

import sys
import time
import argparse
import re
from collections import defaultdict

def writeCDS(CDScollector, wayout):
	for cds in CDScollector:
		print >> wayout, cds

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('gff3', help="gene model gff3 file")
	parser.add_argument('-a','--augustus', action="store_true", help="use presets for AUGUSTUS GFF3")
	parser.add_argument('-c','--comments', action="store_true", help="keep comments, else removed")
	parser.add_argument('-C','--no-comments', action="store_true", help="only remove comments, do nothing else")
	parser.add_argument('-e','--exons', action="store_true", help="duplicate CDS only and add exons, e.g. for gene models")
	parser.add_argument('-i','--introns', action="store_true", help="keep introns from AUGUSTUS, else removed")
	parser.add_argument('-n','--number', action="store_true", help="number exons and CDS")
	parser.add_argument('-t','--transcript', action="store_true", help="make transcript GTF format")
	parser.add_argument('-v','--verbose', action="store_true", help="extra output")
	args = parser.parse_args(argv)

	print >> sys.stderr, "# Starting parsing on %s" % (args.gff3), time.asctime()

	CDScollector = []
	unknowncounter = defaultdict(int)
	exoncounter,mrnacounter = 0,0
	for line in open(args.gff3):
		line = line.strip()
		if line: # skip empty lines
			if line[0]=="#": # check comment lines
				if args.comments: # keep them if requested
					print >> wayout, line
			else:
				if args.no_comments:
					print >> wayout, line
					continue
				lsplits = line.split("\t")
				if lsplits[2]=="gene":
					if CDScollector:
						# write CDS from last gene, and reset collector
						writeCDS(CDScollector, wayout)
						CDScollector = []
						exoncounter,mrnacounter = 0,0
					if args.number:
						parentid = re.search("ID=(\w+)", lsplits[8]).group(1)
					if not args.transcript: # ignore gene types
						print >> wayout, line
				elif lsplits[2]=="mRNA" or lsplits[2]=="transcript":
					if args.augustus and CDScollector:
						# write CDS from last mRNA for multi RNA genes, and reset collector
						if not args.transcript: # unless displaying in transcript GTF format
							writeCDS(CDScollector, wayout)
						CDScollector = []
						exoncounter = 0
					if args.number:
						mrnacounter += 1
						lsplits[8] = "ID={0}.mrna{1};Parent={0}".format(parentid,mrnacounter)
						parentid = "{0}.mrna{1}".format(parentid,mrnacounter)
					if args.augustus and not args.transcript: # augustus uses transcript by default
						lsplits[2] = "mRNA"
					line = "\t".join(lsplits)
					print >> wayout, line
				elif lsplits[2]=="exon":
					if args.augustus: # augustus should not have exons by default
						continue
					if args.number:
						exoncounter += 1
						#parentid = re.search("Parent=(\w+)", lsplits[8]).group(1)
						lsplits[8] = "ID={0}.exon{1};Parent={0}".format(parentid,exoncounter)
						line = "\t".join(lsplits)
					print >> wayout, line
				elif lsplits[2]=="intron":
					if args.introns and not args.augustus:
						line = "\t".join(lsplits)
						print >> wayout, line
				elif lsplits[2]=="CDS" or lsplits[2]=="cds":
					if args.number:
						#parentid = re.search("Parent=(\w+)", lsplits[8]).group(1)
						lsplits[8] = "ID={0}.cds{1};Parent={0}".format(parentid,exoncounter)
						line = "\t".join(lsplits)
					CDScollector.append(line)
					if args.exons or args.augustus:
						lsplits[2] = "exon" # feature is exon
						lsplits[7] = "." # phase is null
						if args.augustus:
							lsplits[8] = lsplits[8].replace("cds","exon")
						if args.number:
							exoncounter += 1
							lsplits[8] = "ID={0}.exon{1};Parent={0}".format(parentid,exoncounter)
						line = "\t".join(lsplits)
						print >> wayout, line
				elif lsplits[2]=="start_codon" or lsplits[2]=="stop_codon":
					if not args.augustus:
						print >> wayout, line
				elif lsplits[2]=="five_prime_UTR" or lsplits[2]=="three_prime_UTR":
					print >> wayout, line
				else:
					# this should not be anything else
					unknowncounter[lsplits[2]] += 1
					if unknowncounter.get(lsplits[2]) < 10:
						print >> sys.stderr, "Unknown annotation: {}".format(lsplits[2])
	else:
		writeCDS(CDScollector, wayout)
		# TODO add features from reformatjgigff
			# should extract gw1.2.819.1 from:
			# name "gw1.2.819.1"; transcriptId 3192
		#	attrs = lsplits[8]
		#	genename = attrs.split('"')[1]
		#	newattrs = 'gene_id "{0}"; transcript_id "{0}";'.format(genename)
		#	lsplits[8] = newattrs
		#	if lastgene and not lastgene==genename:
		#		if CDScollector:
		#			# write CDS from last gene, and reset collector
		#			writeCDS(CDScollector, wayout)
		#			CDScollector = []
		#	if lsplits[2]=="gene": # should not occur, but just in case
		#		print >> wayout, "\t".join(lsplits)
		#	if lsplits[2]=="mRNA": # should not occur, but just in case
		#		print >> wayout, "\t".join(lsplits)
		#	elif lsplits[2]=="start_codon" or lsplits[2]=="stop_codon":
		#		print >> wayout, "\t".join(lsplits)
		#	elif lsplits[2]=="exon":
		#		print >> wayout, "\t".join(lsplits)
		#	elif lsplits[2]=="CDS":
		#		CDScollector.append("\t".join(lsplits))
		#		lastgene = genename
		#	else:
		#		# this should not be anything else
		#		print >> sys.stderr, "Unknown annotation {}".format(lsplits[2])
	print >> sys.stderr, "# Finished parsing on %s" % (args.gff3), time.asctime()
	if unknowncounter:
		for k,v in unknowncounter.iteritems():
			print >> sys.stderr, "Found {} {} times".format(k,v)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
