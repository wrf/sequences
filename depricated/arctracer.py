#! /usr/bin/env python
#
# arctracer.py v1.0

# count arc, nodes, and connected components for a Velvet graph2 file

import sys
import networkx
from collections import defaultdict

### VELVET OUTPUT ###
# One header line for the graph:
# $NUMBER_OF_NODES	$NUMBER_OF_SEQUENCES	$HASH_LENGTH

# One block for each node:
# NODE	$NODE_ID	$NODE_LENGTH	$COV_SHORT1	$O_COV_SHORT1
# $ENDS_OF_KMERS_OF_NODE
# $ENDS_OF_KMERS_OF_TWIN_NODE

# One line for each arc:
# ARC $START_NODE	$END_NODE	$MULTIPLICITY
# Note: this one line implicitly represents an arc from node A to B and
# another, with same multiplicity, from -B to -A.

# For each long sequence, a block containing its path:
# SEQ $SEQ_ID
# $NODE_ID $OFFSET_FROM_START $START_COORD $END_COORD $OFFSET_FROM_END
# $NODE_ID2 etc.

# If short reads are tracked, for every node a block of read identifiers:
# NR $NODE_ID $NUMBER_OF_SHORT_READS
# $READ_ID $OFFSET_FROM_START_OF_NODE $START_COORD
# $READ_ID2 etc.

if len(sys.argv) < 2:
	print >> sys.stderr, "Usage:\narctracer.py some_assembly/Graph2 [N.N]"
else:
	# velvet Graph2 filename
	graph2file = sys.argv[1]

	# if target genome size is supplied, set it, otherwise set to 0 and ignore later
	if len(sys.argv) > 2:
		targetsize = sys.argv[2]
	else:
		targetsize = 0

	ng = networkx.Graph()

	nlens = {}
	nlencounts = defaultdict(int)
	nodeseqs = defaultdict(str)
	nodereads = defaultdict(list)

	print >> sys.stderr, "Reading %s" % graph2file
	linecount = 0
	nodecount = 0
	# flag to parse nodes and retrieve sequences
	getnodes = False
	getreads = False
	for line in open(graph2file,'r'):
		if linecount == 0:
			nodepred, readpred, kmer, dummy = line.rstrip().split("\t")
			getnodes = True
		else:
			if getnodes:
				if line.find("NODE") == 0:
					parsenode = line.rstrip().split("\t")
					workingnode = parsenode[1]
					nodelength = int(parsenode[2])
					nlens[parsenode[1]] = nodelength
					nlencounts[nodelength] += 1
				else:
					nodeseqs[workingnode] += line.rstrip()
			if line.find("ARC") == 0:
				# upon finding the first ARC, turn of getnodes
				getnodes = False
				# rstrip removes terminal /n
				# replace is needed to distinguish between arc directions between nodes
				# since dna graph is not actually directional, 
				# negative arcs must be converted to positive nodes
				arcsplit = line.rstrip().replace("-","").split("\t")
				ng.add_edge(arcsplit[1], arcsplit[2], weight=arcsplit[3])
			if line.find("NR") == 0:
				getreads = True
				parsenr = line.rstrip().split("\t")
				workingnode = parsenr[0]
			elif getreads:
				parseread = line.rstrip().split("\t")
				readid = parseread[0]
				nodereads[workingnode].append(readid)
		linecount += 1

	print >> sys.stderr, "Counted %d edges" % ng.number_of_edges()
	print >> sys.stderr, "Counted %d nodes" % ng.number_of_nodes()

	#nodes_to_prune = []

	#nc = defaultdict(int)
	al = []
	degreedict = defaultdict(int)
	concompcount = 0

	for x in networkx.connected_components(ng):
		# whether to write nodes to file, because they compose a component of target size
		writenode = False

		# count of total connected components, just for reference
		concompcount += 1
		# count of nodes in this component
		nodespercomp = len(x)
		#nc[nodecount] += 1

		# total sequence length in this connected component
		arclength = sum([nlens[y] for y in x])
		al.append(arclength)

		# check if node lengths are correct
		altlength = sum([len(nodeseqs[y]) for y in x])
		if arclength != altlength:
			print >> sys.stderr, "%d is not %d" % (arclength, altlength)

		# check if this component is the target
		if targetsize:
			# assume target is in format of 4.5, referring to megabases
			targetinbases = float(targetsize)*1000000
			# allows estimate of +/- 10 percent from target size
			if targetinbases*1.1 > arclength > targetinbases*0.9:
				writenode = True

		# if writenode, generate a fasta format file of the nodes in that component
		if writenode:
			# filename should be unique, based on arclength
			# but still allow multiple components per assembly if sizes are close
			outfile = "%s_%s.fasta" % (graph2file, str(arclength))
			print >> sys.stderr, "Writing node of %d to %s" % (arclength, outfile)
			with open(outfile, 'w') as ccf:
				for y in x:
					node_forward = nodeseqs[y][:nlens[y]]
					node_reverse = nodeseqs[y][nlens[y]:]
					if len(node_forward) and len(node_reverse):
						print >> ccf, ">{}a".format(y)
						print >> ccf, "{}".format(node_forward)
						print >> ccf, ">{}b".format(y)
						print >> ccf, "{}".format(node_reverse)
			# extract reads when parsed from Graph2

		# count node degree information for each connected component
		degcount = defaultdict(int)
		for node in x:
			ndegree = ng.degree(node)
			degcount[ndegree] += 1
			if ndegree > 8:
				print ng.neighbors(node)

		# print stats for each component
		print >> sys.stdout, nodespercomp, arclength, arclength/nodespercomp, degcount
	# print overall component count
	print >> sys.stderr, "Counted %d subgraphs" % concompcount

	#	if nodecount < 100 or arclength < 10000:
	#		nodes_to_prune.extend(x)
	#print >> sys.stderr, "Pruning small subgraphs (%d nodes)" % len(nodes_to_prune)
	#ng.remove_nodes_from(nodes_to_prune)
	#print >> sys.stderr, "Counted %d edges" % ng.number_of_edges()
	#print >> sys.stderr, "Counted %d nodes" % ng.number_of_nodes()

	#	print arclength, nodespercomp, float(arclength)/nodespercomp


#print sorted(al)
