#!/usr/bin/env python

# v4 - removed velvet, cleaned up some blocks
# v3 - modified to speed up searching and compliation of kogs
# v2 - streamlined for standard assembly
#
__version__ = "4"
__version_info__ = ('2015', '02', '19')
# pipeline controller for transcriptome assembly and analysis

import sys
import os
import traceback
import argparse
import subprocess
import time
import multiprocessing

#sub-programs
import rsemgetbestseqs
import blastplustable
import fpaligner
import kogcollector
import kogtranscollect
import copyblastserver
import commonseq
import generateviroblastini

from Bio import SeqIO

##################################
# DEFAULT DIRECTORIES FOR PROGRAMS

biolitedir = os.path.expanduser("~/repo/biolite-tools-0.4.0/src/")
trinityhomedir = os.path.expanduser("~/trinityrnaseq/trinityrnaseq_r20140717/")

#####################

def safe_mkdir(path):
	if os.path.isfile(path):
		raise ValueError("{0} is a regular file, a directory is required".format(path))
	elif os.path.isdir(path):
		print("The directory {0} already exists, and will be used".format(path))
	else:
		print("Making the directory {0}".format(path))
		os.mkdir(path)

def initlog(output_dir,log_dir):
	# filename processing for output log
	log_file = "{0}.{1}.ab.log".format(output_dir, time.strftime("%Y-%m-%d-%H%M%S"))
	log_path = os.path.join(log_dir, log_file)
	print >> sys.stderr, "Log files being written to %s: " % (log_path), time.asctime()
	global writetolog
	writetolog = open(log_path, 'w')
	return log_path

def logreport(loginput):
	# if input is string, then output like this:
	# This is a sample string for stderr Wed Oct 23 2013 16:31
	# or:
	# Wed Oct 23 2013 16:31
	# This is the sample string written to log
	if type(loginput) is str:
		print >> sys.stderr, loginput, time.asctime()
		print >> writetolog, time.asctime(), "\n", loginput
	# otherwise if input is a list, then assume it is a list of arguments and print as such
	elif type(loginput) is list:
		print >> sys.stderr, "Making system call:\n", " ".join(loginput)
		print >> writetolog, time.asctime(), "\n", " ".join(loginput)

def unzipper(input_file1, input_file2):
	print >> sys.stderr, "Starting unzipper: ", time.asctime()
	gunzip_args = ["gunzip", "-d", input_file1]
	logreport(gunzip_args)
	subprocess.call(gunzip_args)
	unzip_file1 = os.path.splitext(os.path.abspath(input_file1))[0]
	gunzip_args = ["gunzip", "-d", input_file2]
	logreport(gunzip_args)
	subprocess.call(gunzip_args)
	unzip_file2 = os.path.splitext(os.path.abspath(input_file2))[0]
	return unzip_file1, unzip_file2

def randomizer(fastq_file1, fastq_file2):
	print >> sys.stderr, "Starting randomizer: ", time.asctime()
	randomize_file1 = fastq_file1+".r"
	randomize_file2 = fastq_file2+".r"
	order_file = fastq_file1+"_randomize.order"
	randomize_path = os.path.join(biolitedir, "bl-randomize")
	randomize_args = [randomize_path, "-i", fastq_file1, "-o", randomize_file1, "-w", order_file]
	logreport(randomize_args)
	subprocess.call(randomize_args)
	randomize_args = [randomize_path, "-i", fastq_file2, "-o", randomize_file2, "-r", order_file]
	logreport(randomize_args)
	subprocess.call(randomize_args)
	return randomize_file1, randomize_file2

def filterillumina(fastq_file1, fastq_file2):
	print >> sys.stderr, "Starting filter-illumina: ", time.asctime()
	filtered_file1 = fastq_file1+".f"
	filtered_file2 = fastq_file2+".f"
	unpaired_file = fastq_file1+".u"
	filter_path = os.path.join(biolitedir, "bl-filter_illumina")
	print >> sys.stderr, "Filtering reads of %s, %s: " % (fastq_file1,fastq_file2), time.asctime()
	filter_args = [filter_path,"-i",fastq_file1,"-i",fastq_file2,"-o",filtered_file1,"-o",filtered_file2,"-u",unpaired_file]
	logreport(filter_args)
	# stderr could otherwise be a file object, but needs to be piped with Popen to capture for the log
	filt_out = subprocess.Popen(filter_args, stderr=subprocess.PIPE)
	fstdout,fstderr = filt_out.communicate()
	# get numerical extracts from the stderr to report to log
	fstringextract = [l.split(" ")[1] for l in fstderr.split("\n") if l.find("filter_illumina") > -1]
	print >> writetolog, fstderr
	print >> writetolog, "FIS:\t%s\t%s" % (fastq_file1, "\t".join(fstringextract))
	return filtered_file1, filtered_file2, unpaired_file

def subsetter(filtered_file1, filtered_file2, subset_size, use_fasta):
	print >> sys.stderr, str(subset_size)
	print >> sys.stderr, filtered_file1
	if use_fasta:
		subcount = int(float(subset_size)*2000000)
	else:
		subcount = int(float(subset_size)*4000000)
	sub_file1 = filtered_file1+".h"+str(subset_size)
	sub_file2 = filtered_file2+".h"+str(subset_size)
	hargs = ["head", "-n", str(subcount),filtered_file1]
	logreport(hargs)
	head_command = subprocess.Popen(hargs, stdout=subprocess.PIPE)
	print >> sys.stderr, "Generating subset as %s" % (sub_file1)
	with open(sub_file1, 'w') as head_file:
		head_file.write(head_command.communicate()[0])
	hargs = ["head", "-n", str(subcount),filtered_file2]
	logreport(hargs)
	head_command = subprocess.Popen(hargs, stdout=subprocess.PIPE)
	print >> sys.stderr, "Generating subset as %s" % (sub_file2)
	with open(sub_file2, 'w') as head_file:
		head_file.write(head_command.communicate()[0])
	return sub_file1, sub_file2

def trinity(sub_file1, sub_file2, assembly_outdir, strand_specific, use_fasta, threadcount, normalizing):
	trinity_output_dir = os.path.abspath(assembly_outdir)
	trinity_path = os.path.join(trinityhomedir, "Trinity")
	trinity_log = "%s.%s.trinity.log" % (assembly_outdir, time.strftime("%H%M%S") )
	print >> sys.stderr, "Starting Trinity assembly to %s: " % (trinity_output_dir), time.asctime()
	if use_fasta:
		readformat = "fa"
	else:
		readformat = "fq"
	trinity_args = [trinity_path, "--seqType", readformat, "--left",sub_file1,"--right",sub_file2,"--JM", "48G","--CPU", threadcount, "--min_glue", "3", "--output", trinity_output_dir]
	if strand_specific:
		trinity_args.extend(["--SS_lib_type","RF"])
	if normalizing:
		trinity_args.extend(["--normalize_reads", "--normalize_max_read_cov", "30"])
	logreport(trinity_args)
	with open(trinity_log, 'w') as tlog:
		# as all trinity messages are written to stdout, stderr can be ignored
		subprocess.call(trinity_args, stdout=tlog)
	contigs_file = os.path.join(os.path.abspath(trinity_output_dir),"Trinity.fasta")
	return contigs_file

def rsem(contigs_file, sub_file1, sub_file2, assembly_outdir, strand_specific, use_fasta, threadcount):
	print >> sys.stderr, "Running RSEM to get expression levels for %s: " % (contigs_file), time.asctime()
	trinity_rsem_path = os.path.join(trinityhomedir, "util/align_and_estimate_abundance.pl")
	#bamfile = os.path.join(os.path.dirname(contigs_file),"bowtie.nameSorted.sam")
	if use_fasta:
		readformat = "fa"
	else:
		readformat = "fq"
	rsem_args = [trinity_rsem_path, "--transcripts", contigs_file, "--seqType", readformat, "--left",sub_file1,"--right",sub_file2, "--output_dir", assembly_outdir, "--est_method", "RSEM", "--aln_method", "bowtie2", "--thread_count", threadcount, "--trinity_mode", "--prep_reference"]
	if strand_specific:
		rsem_args.extend(["--SS_lib_type","FR"])
	logreport(rsem_args)
	subprocess.call(rsem_args)
	isoforms_file = os.path.join(os.path.dirname(contigs_file),"RSEM.isoforms.results")
	print >> sys.stderr, "Extracting best sequences from %s: " % (isoforms_file), time.asctime()
	sortrsem_args = [isoforms_file, contigs_file]
	print >> sys.stderr, time.asctime(), "\n", "rsemgetbestseqs.py", " ".join(sortrsem_args)
	print >> writetolog, time.asctime(), "\n", "rsemgetbestseqs.py", " ".join(sortrsem_args)
	bestvariants = rsemgetbestseqs.main(sortrsem_args)
	return bestvariants

def makedb(contigs_file,assembly_outdir,assembler):
	print >> sys.stderr, "Generating blast database of %s" % (contigs_file), time.asctime()
	copy_trans_args=["copyblastserver.py",contigs_file,assembly_outdir,assembler]
	logreport(copy_trans_args)
	transcripts_db = copyblastserver.main(copy_trans_args)
	print >> sys.stderr, "Database completed as %s" % (transcripts_db), time.asctime()
	return transcripts_db

def collector(results_list,db_list,output_dir,fullseqs_list):
	allnucs_filename = "%s_all_kog_nucleotides.fasta" % output_dir
	print >> sys.stderr, "Collecting kogs to %s" % (allnucs_filename), time.asctime()
	kogcollector_args = ["-r"]+results_list+["-f"]+db_list+["-o",allnucs_filename]
	print >> sys.stderr, time.asctime(), "\n", "kogcollector.py", " ".join(kogcollector_args)
	print >> writetolog, time.asctime(), "\n", "kogcollector.py", " ".join(kogcollector_args)
	kogcollector.main(kogcollector_args, sys.stdout)
	allprots_filename = "%s_all_kog_proteins.fasta" % output_dir
	print >> sys.stderr, "Collecting kogs to %s" % (allprots_filename), time.asctime()
	kogtranscollect_args = ["-n",allnucs_filename,"-f"]+fullseqs_list+["-o",allprots_filename]
	print >> sys.stderr, time.asctime(), "\n", "kogtranscollect.py", " ".join(kogtranscollect_args)
	print >> writetolog, time.asctime(), "\n", "kogtranscollect.py", " ".join(kogtranscollect_args)
	kogtranscollect.main(kogtranscollect_args, sys.stdout)

def makenrdb(db_list,output_dir):
	ncbi_path = os.path.expanduser("~/ncbi-blast-2.2.29+/db/")
	vbdb_name = os.path.join(ncbi_path,"%s_all.nr.fasta" % output_dir)
	redundant_path = "%s_all.fasta" % output_dir
	with open(redundant_path,'w') as redundant_file:
		print >> sys.stderr, "Generating non-redundant dataset for %s" % (output_dir), time.asctime()
		for db in db_list:
			for line in open(db,'r'):
				redundant_file.write(line)
	sequniq_path = os.path.expanduser("~/gt-1.4.1-Linux_x86_64-64bit/bin/gt")
	sequniq_args = [sequniq_path,"sequniq","-force","-o",vbdb_name,redundant_path]
	logreport(sequniq_args)
	subprocess.call(sequniq_args)
	blastdb_args = ["makeblastdb","-in", vbdb_name,"-dbtype","nucl"]
	logreport(blastdb_args)
	subprocess.call(blastdb_args)
	return vbdb_name

def viroblastupdater(output_dir):
	# takes the database list and adds the new string to the end
	ncbi_path = os.path.expanduser("~/ncbi-blast-2.2.29+/db/")
	vbdb_name = os.path.join(ncbi_path,output_dir + "_all.nr.fasta")
	viroblast = "/var/www/viroblast/viroblast_list_n"
	newdbstring = "{0} => All {1} Transcripts\n".format(os.path.basename(vbdb_name) ,output_dir)
	logreport("Modifying viroblast database list")
	# writes out to viroblast.ini
	with open(viroblast, 'a') as writeviroblast:
		writeviroblast.write(newdbstring)
	generateviroblastini.main()
	print >> sys.stderr, "New databases added:", time.asctime()

def main(argv):
	usage = '''
All files are written into either pwd or -o output_dir

Example usage starting with .gz files:
-i jelly_sample_1.*.gz -o jelly_out

Starting with fastq files:
-i jelly_sample_1.*.fastq -o jelly_out -p stmkcpd

To assemble all reads only:
-i jelly_sample_1.*.fastq -o jelly_out -p stmk -s 0

Programs are:
u = unzip (gunzip) - probably not required
r = randomize - allows gzipped inputs
f = filter-illumina
s = subset
t = Trinity
e = RSEM
m = makeblastdb 
k = KogBlaster
c = KogCollector
d = update ViroblastDB
'''
	if not len(argv):
		argv.append("-h")

	program_list = "urfstemkcd"

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-i','--inputs', nargs="*", help="left, then right, reads", required=True)
	parser.add_argument('-o','--output-dir', help="directory of output files", default='', required=True)
	parser.add_argument('-p','--program', help="string for programs used: '{}'".format(program_list), default="rfstemkcd")
	parser.add_argument('-u','--unpaired', action="store_true", help="assume use of unpaired files")
	parser.add_argument('-a','--use-fasta', action="store_true", help="input files are fasta, not fastq")
	parser.add_argument('-n','--normalize', action="store_true", help="run digital normalization for assembly")
	parser.add_argument('-r','--stranded', action="store_true", help="input files are strand-specific protocol")
	parser.add_argument('-t','--threads', help="number of processors, default:all")
	parser.add_argument('-z','--insert-size', help="insert size for assemblers", default="0")
	parser.add_argument('-s','--subset-size', help="millions of reads to subset, 0 for all reads, default: '0.5,10'", default="0.5,10")
	parser.add_argument('-b','--blast-params', help="options specific to blast, default: 'sn'", default="sn")
	parser.add_argument('-c','--code', help="genetic code for blasting, default: 1", default=1)
	parser.add_argument('-l','--log-dir', help="optional directory of log files", default='./')
	args = parser.parse_args(argv)

	print >> sys.stderr, "Starting process: ", time.asctime()
	startclock=time.asctime()
	starttime= time.time()
	
	# parse program string to get operations
	program_string = args.program
	get_programs = [bool(args.program.count(x)) for x in program_list]
	do_unzip, do_randomize, do_filter, do_subset, do_trinity, do_rsem, do_makedb, do_kogb, do_kogc, do_viroblastdb = get_programs

	# catch for if -p h but without -u
	if do_subset and not args.subset_size:
		print >> sys.stderr, "Must specify subset size with -s"
		print >> sys.stderr, "Exiting"
		sys.exit()

	print >> sys.stderr, "Do          Unzip:", do_unzip
	print >> sys.stderr, "Do      Randomize:", do_randomize
	print >> sys.stderr, "Do         Filter:", do_filter
	print >> sys.stderr, "Do         Subset:", do_subset
	print >> sys.stderr, "Do        Trinity:", do_trinity
	print >> sys.stderr, "Do           RSEM:", do_rsem
	print >> sys.stderr, "Do         MakeDB:", do_makedb
	print >> sys.stderr, "Do     KogBlaster:", do_kogb
	print >> sys.stderr, "Do   KogCollector:", do_kogc
	print >> sys.stderr, "Do    ViroblastDB:", do_viroblastdb

	initlog(args.output_dir, args.log_dir)
	# write all the arguments from the command line to file
	logreport(" ".join(sys.argv))
	logreport("Using assemblerblaster.py v%s %s" % (__version__, '-'.join(__version_info__)))

	# get version info and write to the log
	minorversion = sys.version_info[1]
	logreport("Using Python version: %s" % (".".join([str(i) for i in sys.version_info[0:3]])))

	# get number of processors
	if args.threads:
		threadcount = args.threads
	else:
		threadcount = str(multiprocessing.cpu_count())
		logreport("Autodetecting cpu count")
	logreport("Using %s processors" % threadcount)

	# standard output of file counts
	logreport("%d input files detected" % (len(args.inputs)))
	logreport("\n".join(args.inputs))

	# beginning of main processes
	try:
		# for program 'u'
		if do_unzip:
			unzip_file1, unzip_file2 = unzipper(os.path.abspath(args.inputs[0]),os.path.abspath(args.inputs[1]))
		else:
			unzip_file1 = os.path.abspath(args.inputs[0])
			unzip_file2 = os.path.abspath(args.inputs[1])
		# for program 'r'
		if do_randomize:
			# appends ".r" to files
			randomize_file1, randomize_file2 = randomizer(unzip_file1, unzip_file2)
		else:
			randomize_file1 = unzip_file1
			randomize_file2 = unzip_file2
		# for program 'f'
		if do_filter:
			# appends ".f" to files, thus randomized and filtered would be "sample_1.fasta.r.f"
			filtered_file1, filtered_file2, unpaired_file = filterillumina(randomize_file1, randomize_file2)
		else:
			filtered_file1 = randomize_file1
			filtered_file2 = randomize_file2
			# if using unpaired file, then assume third input is unpaired
			if args.unpaired:
				unpaired_file = args.inputs[2]
		# some general variables used in assembly and searching
		insert_size = int(args.insert_size)
		results_list = []
		fullseqs_list = []
		db_list = []
		# executing main body for programs 'h' 't' 'm'
		# assemble and search for each subset size as specified
		subset_targets = args.subset_size.split(",")
		for subset_size in subset_targets:
			# for program 'h'
			if do_subset and subset_size != '0':
				sub_file1, sub_file2 = subsetter(filtered_file1, filtered_file2, subset_size, args.use_fasta)
			else:
				sub_file1, sub_file2 = filtered_file1, filtered_file2
			if subset_size == '0':
				outdir_label = "_x0"
				subset_string = outdir_label
			else:
				# when assembling for only one subset size, use the static output name
				# otherwise generate a directory name based on the subset size
				outdir_label = int(float(subset_size)*2)
				if minorversion > 6:
					subset_string = "_h{:02}".format(outdir_label)
				else:
					subset_string = "_h{0:02}".format(outdir_label)
			# executing assembly programs for 't'
			# for program 't'
			if do_trinity:
				assembly_outdir = "%s_t%s" % (args.output_dir, subset_string.format(outdir_label))
				contigs_file = trinity(sub_file1, sub_file2, assembly_outdir, args.stranded, args.use_fasta, threadcount, args.normalize)
				if do_rsem:
					if os.path.exists(contigs_file):
						best_contigs = rsem(contigs_file, sub_file1, sub_file2, assembly_outdir, args.stranded, args.use_fasta, threadcount)
						contigs_file = best_contigs
					else:
						logreport("Error: Transcripts file not found!")
				if do_makedb:
					if os.path.exists(contigs_file):
						transcripts_db = makedb(contigs_file,assembly_outdir,"Trinity")
						db_list.append(os.path.abspath(transcripts_db))
					else:
						logreport("Error: Transcripts file not found!")
			# in both assemblers, if program 'm', add the database name to the list
		for db in db_list:
			# for program 'k'
			if do_kogb:
				fpaligner_args = ["-x","f","-f","-d",db]
				print >> sys.stderr, "Blasting for proteins in %s" % (db)
				print >> sys.stderr, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				print >> writetolog, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				fpresults_file,fpseqs_file = fpaligner.main(fpaligner_args, sys.stdout)
				fpaligner_args = ["-x","m","-f","-d",db,"-c",str(args.code)]
				print >> sys.stderr, "Blasting for proteins in %s" % (db)
				print >> sys.stderr, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				print >> writetolog, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				mitoresults_file,mitoseqs_file = fpaligner.main(fpaligner_args, sys.stdout)
				fpaligner_args = ["-x","r","-f","-d",db]
				print >> sys.stderr, "Blasting for proteins in %s" % (db)
				print >> sys.stderr, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				print >> writetolog, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				r18sresults_file,r18s_seqs_file = fpaligner.main(fpaligner_args, sys.stdout)
				fpaligner_args = ["-x","k","-f","-d",db]
				print >> sys.stderr, "Blasting for kogs in %s" % (db)
				print >> sys.stderr, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				print >> writetolog, time.asctime(), "\n", "fpaligner.py", " ".join(fpaligner_args)
				kogresults_file,kogseqs_file = fpaligner.main(fpaligner_args, sys.stdout)
				results_list.append(kogresults_file)
				fullseqs_list.append(kogseqs_file)
		# for program 'c'
		if do_kogc:
			collector(results_list,db_list,args.output_dir,fullseqs_list)
		# for program 'd'
		if do_viroblastdb:
			nrdb = makenrdb(db_list,args.output_dir)
			viroblastupdater(args.output_dir)
	except:
		#prints the error to the log file, as of 18-mar-2013
		traceback.print_exc(file=sys.stderr)
		traceback.print_exc(file=writetolog)

	# final output
	logreport("Process completed in %.1f minutes" % ((time.time()-starttime)/60))
	print >> sys.stderr, "Done: ", time.asctime()
	print >> writetolog, "Great success!!: ", time.asctime()
	writetolog.close()
	return 1

if __name__ == "__main__":
	main(sys.argv[1:])
