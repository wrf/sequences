#!/usr/bin/env python
#
# v1 2013-04-08

import sys
import os
import time

# generates viroblast.ini from viroblast_list_n and viroblast_list_p
# the two lists were created to be readable with a large number of databases

#

def main():
	# important lines for the .ini file
	leadstring = "# This file generated automatically by generateviroblastini.py"
	blaststring = "blast+: /home/biolum/ncbi-blast-2.2.29+/bin"

	header1 = """
##############################################################################
# 
#	This is dynamic configuration file for ViroBLAST service
#              All lines started with '#' are ignored.
#
##############################################################################
#
# The path to NCBI blast+ program downloaded in your computer.
# Note: the package contains NCBI blast+ for linux/IA32. Leave the path
# below unchanged if your computer runs on linux/IA32.
# Format: blast+: <path to blast+ program in your computer>
#"""

	header2 = """#
# Below are list of combinations program/database, 
# that allowed by ViroBLAST service.
# Format: <program>: <database_file_name1 => database_name1_for_display>, <database_file_name2 => database_name2_for_display>, ...
#
# test_na_db => Nucleotide test database
# test_aa_db => Protein test database"""

	dbcount = 0
	nucldblist = []
	protdblist = []

	print >> sys.stderr, "Reading viroblast database list"

	with open("/var/www/viroblast/viroblast_list_n",'r') as dblist:
		for line in dblist:
			if line[0]=="#":
				dbcount+=1
			else:
				nucldblist.append(line.rstrip())

	with open("/var/www/viroblast/viroblast_list_p",'r') as dblist:
		for line in dblist:
			if line[0]=="#":
				dbcount+=1
			else:
				protdblist.append(line.rstrip())

	with open("/var/www/viroblast/viroblast.ini", 'w') as vbini:
		print >> vbini, leadstring, time.asctime()
		print >> vbini, header1
		print >> vbini, blaststring
		print >> vbini, header2
		print >> vbini, "blastn:", ", ".join(nucldblist)
		print >> vbini, "blastp:", ", ".join(protdblist)
		print >> vbini, "blastx:", ", ".join(protdblist)
		print >> vbini, "tblastn:", ", ".join(nucldblist)
		print >> vbini, "tblastx:", ", ".join(nucldblist)
			
	print >> sys.stderr, "Done generating .ini file"

if __name__ == "__main__":
    main()
