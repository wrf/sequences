#!/usr/bin/env python
#
# v7.0 changed automatic renaming of trinity sequences 2015-01-20
# v6.0 moved auto-detect ncbi path to blastplustable 2014-11-26
# v5.0 changed to softcoded ncbi blast path 2013-07-10
# v4.0 most functions moved to fastarenamer 2013-04-01
# v3.1 if using trinity, then rename the file 06/mar/2013
# v3.0 removed functionality of updating blast server list 01/feb/2013
# v2.2 checks version of python and generates string accordingly 12/12/12
# v2.1 changes number of zeroes for identifier by number of total seqs 5/oct/2012
# v2.0 renames each sequence in database with unique identifier
#      reconfigured for blast 2.2.26+  1/aug/2012
# v1.1 added return for name of database file in ncbi db folder 26/may/2012

import sys
import os
import time
import subprocess
from Bio import SeqIO
import fastarenamer
from blastplustable import get_blast_path

usage='''
  copyblastserver.py newtranscriptsfile.fa outdir
'''

def main(argv):
	if len(argv)<2:
		print usage
	else:
		newtrans_file = argv[1]
		outdir_name = argv[2]
		#assembler removed for version 3, to integrate with viroblastupdater v3.0
		#reverted later, to allow assembler specific filename changes v3.1

		#blastdb_path = os.path.expanduser("~/ncbi-blast-2.2.26+/db/")
		# changed in v5.0 to allow softcoded version number to be acquired from .ncbirc
		blastdb_path = get_blast_path(None)

		trans_path = os.path.abspath(argv[1])
		if len(argv)>3 and argv[3] == "Trinity":
			# converts "Trinity.fasta" to "trinity.fa"
			if trans_path.rsplit(".",1)[1] == "fasta":
				newdb_path = os.path.join(blastdb_path,"%s_%s" % (os.path.basename(outdir_name), os.path.basename(newtrans_file.lower()[:-3])))
			# when filename for Trinity.fasta is already modified, or using a different fasta file such as RSEM results
			else:
				newdb_path = os.path.join(blastdb_path,"%s_%s" % (os.path.basename(outdir_name), os.path.basename(newtrans_file.lower())))
			# use the following settings for fastarenamer
			# prepend outdir name, add numbers, split at first space (removes len=234 and path=[123:0-233] from Trinity output)
			renamer_args = ['-p',outdir_name,'-n','-s','0',trans_path]
		else:
			# otherwise assume some other assembler
			newdb_path = os.path.join(blastdb_path,"%s_%s" % (os.path.basename(outdir_name), os.path.basename(newtrans_file)))
			# only prepend outdir name and add numbers
			renamer_args = ['-p',outdir_name,'-n',trans_path]

		print >> sys.stderr, "Copying blast database, renaming sequences"
		# generate new database file with renamed sequences
		with open(newdb_path,'w') as new_db_file:
			fastarenamer.main(renamer_args, new_db_file)

		blastdb_args = ["makeblastdb","-in", newdb_path,"-dbtype","nucl"]
		print >> sys.stderr, "Making system call:\n", " ".join(blastdb_args)
		subprocess.call(blastdb_args)

		return newdb_path

if __name__ == "__main__":
	main(sys.argv)
