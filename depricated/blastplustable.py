#! /usr/bin/env python
# 
# This could easily be modified to run with other blast programs, or to use
# a different program depending on a run-time parameter.
#
# ver. 7.9 - configured detection of output tag from database 2015-02-27
# ver. 7.8 - allows printing of blastp hits 2015-02-06
# ver. 7.7 - added methionine check for translated proteins 2015-01-30
# ver. 7.6 - added check to auto db path detection 2015-01-28
# ver. 7.5 - changed some presets for trembl, fixed bug 2015-01-16
# ver. 7.4 - moved some things to functions 2014-11-26
# ver. 7.3 - fixed bug with translation for blastx 2014-11-18
# ver. 7.2 - corrected db import issue for nr and nt 2014-04-11
# ver. 7.1 - fixed bug with reporting of subject length and identity percent 2014-03-18
# ver. 7.0 - added bitscore to output 2014-02-18
# ver. 6.4 - corrected usage to update output change, use identity % 2014-02-13
# ver. 6.3 - changed block counting option, processor auto count 2013-11-14
# ver. 6.2 - fixed error for translate with tblastn 2013-10-23
# ver. 6.1 - changed number of displayed alignments to x rather than 1 2013-10-22
# ver. 6.0 - added option for commandline input of sequence 2013-09-27
# ver. 5.5 - fixed error with blastn for genetic code 2013-06-11
# ver. 5.4 - added a few comments/clarifications, genetic code option 27/feb/2013
# ver. 5.3 - fixed bug for blastn, and 'ac' regexp in blastp 25/oct/2012
# ver. 5.2 - updated for blast 2.2.26+ 07/aug/2012
# ver. 5.1 - added blast-translate mode 20/jun/2012
# ver. 5.0 - changed to argparse input, rather than sys 11/jun/2012
# ver. 4.7 - added mouse database with -u, columns for query length and identity 01/jun/2012
# ver. 4.6 - added column for frame 28/mar/2012
# ver. 4.5 - added column for number of alignments 2/mar/2012
# ver. 4.4 - corrected bug with absolute file paths 23/jan/2012
# ver. 4.3 - returns hitsfile as well, added option for no-header line 17/jan/2012
# ver. 4.2 - added blastp and blastn 10/jan/2012
# ver. 4.1 - fixed output error with dictionary, reorganized 4/jan/2012
# ver. 4   - reworked to blast in blocks of 1000 and also parse output
#            files as findAinB after blasting, for speed reasons 3/jan/2012
# ver. 3.5 - configured output for nr and trembl, added return of nomatches 
#            to integrate with pipelines 31/dec/2011
# ver. 3.4 - reconfigured for trembl on server
# ver. 3.3 - fixed file output error, formats 21/dec/2011
# ver. 3.2 - utilizes cStringIO to run stdout as input for ncbixml.read()
#            also outputs fasta file of matches 
# ver. 3.1 - cleaned up output error, added nr option
# ver. 3.01- integrates fasta parser to both allow progress report and
#            output of non-matches to fasta format 14/dec/2011
# ver. 2.4 - adds os to write non-matches to another file 12/dec/2011
# ver. 2.3 - corrected bug with accession number for swissprot 1/dec/2011
# ver. 2.2 - Accounted for non-matching sequences
# ver. 2.1 - Fixed ?error about /tmp folder. Added flag to search trembl database
# ver. 2   - rewritten to work with blastplus (blast+)
# ver. 1   - 12 Jun 2009

usage="""
blastplustable.py v7.9

By default:
Run blastx against database (swissprot) and print out a table of results
table fields separated by tabs then writes out sequences with no match.

Output consists of tab separated fields:
"Resulttitle", "Evalue", "Bits", "Num_Align", "Query", "Accession", "Matched"
"Frame", "Subject Length", "Identity %", "Align_length", "Query Length"

Requires biopython (1.55+) and blast+ package installed on your system

A normal transcriptome using blastx against swissprot
takes around 7 minutes per 1000 when using 16 CPUs

Usage:

  for nearly all cases:
blastplustable.py -q query_sequences.fasta -x s > results.tab

  for other databases, use -x t, r, n
blastplustable.py -q sequences.fasta -x t > trembl_results.tab

    -x t trembl database
    -x r nr (NCBI non-redundant) protein database
    -x n nt (forces blastn)

  for other programs, use -b PROGRAM
blastplustable.py -q proteins.faa -x s -b blastp > results.tab

  for cases using a specialized db, specify with -d and -D:
blastplustable.py -q query -d jellyfish_genes.fa -D database_dir > results.tab

  -D tries by default to read BLASTDB path from ~/.ncbirc

  use -o to change the tag for automatic renaming, or -M to disable it
  where default would appear as:
    query_jellyfish_hits.fa

  if using blastx or tblastn, use -t to keep translated proteins
"""

import sys
import argparse
import time
import re
import os
import multiprocessing
import cStringIO
from Bio import SeqIO
from Bio.Blast.Applications import NcbitblastxCommandline
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord

def get_basicseq(seqdict, sn):
	# takes a dictionary of Seq objects and returns seq 'sn'
	try:
		basicseq = seqdict[sn]
	# the most common error is the presence of information after a space
	except KeyError:
		basicseq = seqdict[sn.split(" ")[0]]
	return basicseq

def get_blast_path(directory):
	if directory:
		dbpath = os.path.abspath(os.path.expanduser(directory))
	# read the BLAST db directory from .ncbirc
	else:
		ncbirc_path = os.path.expanduser("~/.ncbirc")
		print >> sys.stderr, "Detecting database path from %s..." % ncbirc_path
		if os.path.exists(ncbirc_path):
			ncbirc_info = open(ncbirc_path, 'r').readlines()
			dbpath = ncbirc_info[1].split("=")[1].rstrip()
		else:
			print >> sys.stderr, "Cannot find file %s, BLAST may fail" % (ncbirc_path)
			dbpath = os.path.expanduser("~/ncbi-blast-2.2.29+/db/")
	print >> sys.stderr, "Using DB path %s" % (dbpath)
	return dbpath

def frame_to_orfs(protframe):
	# splits a translated protein frame into ORFs
	# note that the '*'s are excluded, thus ORFs will not have stop codons if back translated
	orfs = protframe.split('*')
	return orfs

def get_threads(threads):
	if threads:
		threadcount = threads
	else:
		print >> sys.stderr, "Detecting processors..."
		threadcount = str(multiprocessing.cpu_count())
	print >> sys.stderr, "Using %s threads" % (threadcount)
	return threadcount

def get_blast_command(blastprogram, maxtargets, blast_db, evalue, threadcount, gencode):
	if blastprogram == 'blastp' or blastprogram=='blastn' or blastprogram=='tblastn':
		return NcbitblastxCommandline(cmd=blastprogram, query="-", db=blast_db, evalue=evalue, outfmt=5, num_threads=threadcount, max_target_seqs = maxtargets)
	# blastn and blastp are incompatible with query_gencode
	else:
		return NcbitblastxCommandline(cmd=blastprogram, query="-", db=blast_db, evalue=evalue, outfmt=5, num_threads=threadcount, max_target_seqs = maxtargets, query_gencode = gencode)
	# num_descriptions and num_alignments removed for blast-2.2.26+

def get_matchseq(match, withblastn, query):
	# match is a string, and query is the string of the sequence
	if withblastn:
		matchseq = ''.join(map(lambda keep,val: val if keep else '-', [z=='|' for z in match], query))
	else:
		matchseq = match.replace(' ','-')
	# returns a string
	return matchseq

def get_subjctlength(subend, substart, blastprogram):
	# this might have to be absolute value, and 1 must be added as the first base is position 1
	sublen = abs(subend-substart)+1
	if blastprogram == "tblastn" or blastprogram == "tblastx":
		sublen /= 3
	return sublen

def get_trans_inframe(sequence, frame, match, gencode):
	# function takes Seq object, integer, string, and string of a number
	# gets longest piece of the alignment
	alignPiece = get_align_piece(match)
	# translates seq or reverse complement as indicated by the frame
	transseq = get_transseq(sequence, frame, gencode)
	preorfs = frame_to_orfs(transseq)
	# generates orfs list if preorf contains the alignment, to restrict frame
	orfs = [orf for orf in preorfs if alignPiece in orf]
	# take longest orf which contains the alignment, last term in length-sorted list
	if orfs:
		longestfragment = sorted(orfs, key=len)[-1]
		# returns Bio.Seq.Seq class
		return longestfragment
	else:
		return 0

def get_met_orf(transprot):
	# takes Bio.Seq.Seq class and chops sequence at first Methionine
	metprot = transprot[str(transprot).find("M"):]
	# this might overestimate if there is a long stretch between two M at the beginning of the protein
	# return Bio.Seq.Seq class
	return metprot

def check_met_prot(mplen, alignnogaps, targetlength):
	# mplen is int, al and ag are ints, tl is int
	# check if:
	# the protein from methionine to stop is longer than the alignment minus gaps
	# and the protein is within 10 percent boundaries of the length of the hit
	if mplen > alignnogaps and (targetlength*0.9 <= mplen < targetlength*1.2):
		return True
	else:
		return False

def get_transseq(nucleotideseq,frame,gencode):
	# takes nucleotide Seq object, and returns translated Seq object
	# for negative frames, reverse complement first
	if frame < 0:
		# frame is converted to index by subtracting 1, or absolute value then subtracting
		transseq = nucleotideseq.reverse_complement()[(abs(frame)-1):].translate(table=gencode)
	else:
		transseq = nucleotideseq[((frame)-1):].translate(table=gencode)
	return transseq

def get_align_piece(matchseq):
	# finds longest continuous piece of the alignment
	afrags = sorted(matchseq.replace('+','-').split('-'), key=lambda l: len(l))
	apiece = afrags[-1]
	# returns string of that piece for string.find()
	return apiece

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-q','--query', help="query file")
	parser.add_argument('-d','--db', help="database file - default: swissprot", default="uniprot_sprot.fasta")
	parser.add_argument('-D','--directory', help="folder containing all blast DBs, default read from ~/.ncbirc")
	parser.add_argument('-b','--blast-program', help="blast program - default: blastx", default="blastx")
	parser.add_argument('-o','--output-tag', help="label to append to files - defaults to database")
	parser.add_argument('-s','--search-tag', help="label to index blast hits - default: sp", default="sp")
	parser.add_argument('-S','--use-search', action="store_true", help="use search string, instead of accession number")
	parser.add_argument('-H','--no-header', action="store_false", help="omit header line in output table")
	parser.add_argument('-M','--no-match-file', action="store_false", help="do not write hits and nomatch output files")
	parser.add_argument('-r','--trim', action="store_true", help="trim hit sequences to coding sequence")
	parser.add_argument('-t','--translate', action="store_true", help="based on the alignment, print a translation (use with blastx)")
	parser.add_argument('-p','--processors', metavar='N', help="number of processors, default: all")
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff, as either 0.01 or 1e-2, default: 1e-5", default=1e-5)
	parser.add_argument('-m','--max-seqs', type=int, metavar='N', help="maximum number of records per blast search - default: 2", default=2)
	parser.add_argument('-a','--alignment-cutoff', metavar='N', type=int, help="minimum number of alignments - default: 1", default=1)
	parser.add_argument('-c','--code', type=int, help="genetic code number - default: 1", default='1')
	parser.add_argument('-i','--identities', action="store_true", help="report identites as number rather than percentage")
	parser.add_argument('-z','--step-size', type=int, metavar='N', help="frequency of progress updates - default: 1000", default=1000)
	parser.add_argument('-x','--defaults', help="for specific database defaults - this will override other parameters; use -x t, etc")
	args = parser.parse_args(argv)

	# checks to see if query is file type, otherwise assumes it is the search string
	if os.path.isfile(args.query):
		# get full path of file
		blast_file = os.path.abspath(args.query)
		use_blast_file=True
	else:
		# for consistency, use upper case
		inputsequence = args.query.upper()
		blast_file = time.strftime("query%H%M%S")
		use_blast_file=False
		# as no file is used, disable automatic file outputs
		args.no_match_file = False

	# establish default blasting parameters for single commandline run
	blastprogram = args.blast_program
	blast_db = args.db

	# get db directory
	db_folder = get_blast_path(args.directory)

	# searchheader is used to pull accession numbers from the description
	searchheader = args.search_tag
	readDBdict = True

	# set alternate parameters for commandline arguments
	if (args.defaults=='s'):
		blast_db   = os.path.join(db_folder,"uniprot_sprot.fasta")
		outputtag = "sp"
		searchheader = outputtag
		args.use_search = True
	elif (args.defaults=='t'):
		blast_db   = os.path.join(db_folder,"uniprot_trembl.fasta")
		outputtag = "tr"
		searchheader = outputtag
		args.use_search = True
		# do not load in trembl, as it is ~90M sequences of 37G
		readDBdict = False
	elif (args.defaults=='r'):
		blast_db   = os.path.join(db_folder,"nr")
		outputtag = "nr"
		searchheader = outputtag
		# do not load in nr or nt, as they are not stored as fasta
		readDBdict = False
	elif (args.defaults=='n'):
		blast_db   = os.path.join(db_folder,"nt")
		blastprogram = "blastn"
		outputtag = "nt"
		searchheader = outputtag
		readDBdict = False

	# outputtag is used for automated naming of hits/no-hits
	if args.output_tag:
		outputtag = args.output_tag
	else:
		print >> sys.stderr, '# Autodetecting database name...', time.asctime()
		otre = re.search("^[a-zA-Z0-9]+", os.path.basename(args.db))
		if otre:
			outputtag = otre.group()
		else:
			print >> sys.stderr, '# Cannot detect database name: ', time.asctime()
			outputtag = blastprogram
	print >> sys.stderr, '# Using %s as output tag: ', time.asctime()

	# auto detect thread count
	threadcount = get_threads(args.processors)

	# search string is regular expression for locating accession in the db names
	searchstring = "{}\|([\w\.]+)\|".format(searchheader)

	# if input file is "jelly.fa", autogenerated files are as "jelly_sp_hits.fasta" or "jelly_sp_nom.fasta"
	fastabasename = os.path.splitext(os.path.abspath(blast_file))[0]
	if args.no_match_file:
		hitsoutputfilename = "%s_%s_hits.fasta" % (fastabasename, outputtag)
		hitsoutfile = open(hitsoutputfilename, 'w')
		nomatchoutputfilename = "%s_%s_nom.fasta" % (fastabasename, outputtag)
		nomatchoutfile = open(nomatchoutputfilename, 'w')
	# file for translated proteins if using blastx
	if args.translate:
		protcount = 0
		nonsensecount = 0
		transoutputfilename = "%s_%s_prots.fasta" % (fastabasename, outputtag)
		transoutfile = open(transoutputfilename, 'w')

	## start main
	recnum = 0
	nomatch = 0
	stepsize = args.step_size

	if use_blast_file:
		# imports the sequences, first as list
		# the list gets chopped while the dictionary is used to find the hit/no-hit sequences
		print >> sys.stderr, '# Reading sequences: ', time.asctime()
		seq_records = list(SeqIO.parse(blast_file, "fasta"))
		print >> sys.stderr, '# Finished reading %d sequences: ' % (len(seq_records)), time.asctime()
	else:
		# creates a seq record object out of the query seq
		print >> sys.stderr, '# Converting command line input: ', time.asctime()
		inputseq = Seq(inputsequence)
		seq_record = SeqRecord(inputseq)
		seq_record.id = blast_file
		seq_records = [seq_record]

	# parameters needed for stepsize and the dictionary for translated blast hits
	seqreclength = len(seq_records)
	blasthitdict = {}

	# ignore reading in database for large databases like nr and nt
	if readDBdict:
		# creates a dictionary from the seqs, first of the database, then from the seq_record list
		print >> sys.stderr, '# Loading database from %s: ' % (blast_db), time.asctime()
		target_db_dict = SeqIO.to_dict(SeqIO.parse(os.path.expanduser(os.path.join(db_folder,blast_db)), "fasta"))
		print >> sys.stderr, '# Database contains %d sequences: ' % (len(target_db_dict)), time.asctime()
	# even for nr, nt and trembl, convert the query to a dictionary for later steps
	query_seq_dict = SeqIO.to_dict(seq_records)

	# determine if translation steps are usable
	do_translation = args.translate
	if blastprogram == "blastn" or blastprogram == "blastp":
		# if blastn, do not translate at all
		# if blastp, do not translate but can later take sequences that match
		do_translation = False
	else:
		if not readDBdict and blastprogram != "blastx":
			do_translation = False

	# the blast command
	blastCommand = get_blast_command(blastprogram, args.max_seqs, blast_db, args.evalue, threadcount, args.code)

	startclock=time.asctime()
	starttime= time.time()
	print >> sys.stderr, '# Started BLAST on file {} at: '.format(blast_file), startclock
	print >> sys.stderr, 'COMMAND:', blastCommand

	# print the header line for the table
	if args.no_header:
		print >> wayout, "\t".join(["Resulttitle", "Evalue", "Bit_Score", "Num_Align", "Query", "Accession", "Matched", "Frame", "Subject Length", "Identity %", "Align_length", "Query Length"])
	# generates input as a list of n sequences, where n = stepsize
	for setnum in xrange(0,seqreclength,stepsize):
		# first a sub list is made of the stepsize or whatever is left, for counting purposes v6.2
		subseqset = [seqrec.format("fasta") for seqrec in seq_records[setnum:setnum+stepsize]]
		recnum += len(subseqset)
		# the seqs are joined into one string
		subsequences = "".join(subseqset)

		# this is the actual blast command and the results
		result_handle = cStringIO.StringIO(blastCommand(stdin=subsequences)[0])

		# progress counter, as percentage ofrecnum (cumulative sum) / seqreclength (total sequences)
		print >> sys.stderr, "{} records: {:.2f}%:".format(recnum, (float(recnum)/seqreclength*100)), time.asctime()

		if result_handle:
			for record in NCBIXML.parse(result_handle):
				# originally used try because if there is no match, just skip the record
				# there was minimal change in time for using len, but was better for debugging
				if len(record.alignments) >= args.alignment_cutoff:
					# print hits here, so each hit is printed only once 2013-10-25
					if args.no_match_file:
						### TODO ###
						#	if args.trim:
						#		query_seq_dict[record.query].seq = 
						hitsoutfile.write("%s" % get_basicseq(query_seq_dict,record.query).format("fasta"))
					# print each alignment, for all hits with more than args.alignment_cutoff number of alignments v6.1
					for x,alignment in enumerate(record.alignments):
						rt = alignment.hit_def
						ev = record.descriptions[x].e
						bt = record.descriptions[x].bits #this is the normalized score, bit-score
						#sc = record.descriptions[x].score #this is the raw score
						al = alignment.hsps[0].align_length
						# this is not the same as alignment.length
						rq = record.query
						if args.use_search:
							try:
								ac = re.search(searchstring,record.descriptions[x].title).group(1)
							# for cases where NoneType is flagged, possibly due to database errors
							except AttributeError:
								ac = alignment.accession
						else:
							ac = alignment.accession
						#if blast_db == 'nr' or blast_db == 'nt':
						#	ac = record.alignments[0].accession

						# get matching sequence for table
						if blastprogram == "blastn":
							ms = get_matchseq(alignment.hsps[0].match, True, record.alignments[x].hsps[0].query)
						else:
							ms = get_matchseq(alignment.hsps[0].match, False, None)

						# number of alignments
						na = len(record.alignments)

						# to calculate query coverage as on ncbi
						# appears to be query length / subject length
						ql = record.query_length

						# get subject length, which is not the same as length of the full subject hit
						sl = get_subjctlength(alignment.hsps[0].sbjct_end, alignment.hsps[0].sbjct_start, blastprogram)
						# subject gaps is a count of - in the subject, which is equal to hsps.align_length - sl
						sg = al - sl
						# also note that al = sl + subject side gaps
						# hsps[0].gaps includes both sides
						ag = alignment.hsps[0].gaps

						# to calculate identity % as on ncbi
						qi = alignment.hsps[0].identities
						# intuitively the identity percentage is calculated as identities/query length
						if args.identities:
							ip = qi
						else:
							#ip = qi/float(ql)*100
							# however on ncbi it is calculated as identities/alignment length, which is the same as subject end - start + 1 + subject gaps
							ip = qi/float(al)*100

						# to correct for cases where db is not a text file
						if readDBdict:
							tl = len(get_basicseq(target_db_dict,rt).seq)
						else:
							tl = sl

						# choose query frame based on program used
						qf = alignment.hsps[0].frame[0]
						sf = alignment.hsps[0].frame[1]
						if blastprogram == "blastx" or blastprogram == "tblastx":
							fr = qf
						elif blastprogram == "tblastn":
							fr = sf
						else:
							fr = 0

						# this does not use join() because of the ints and floats
						print >> wayout, "%s\t%s\t%s\t%d\t%s\t%s\t%s\t%s\t%d\t%.2f\t%d\t%d" % (rt, ev, bt, na, rq, ac, ms, fr, tl, ip, al, ql)

						# if '-t' is enabled, gets the alignment and frame information
						# translates that frame, then gets the piece with the alignment, and writes to file
						# also check for readDBdict,
						# large databases are not read, therefore skip this step unless it is blastx
						if args.translate and do_translation:
							# TODO add check for tblastx

							# only translate each seq once, so disregard if either returns 1
							if not blasthitdict.get(rt,0) and not blasthitdict.get(rq,0):
								if blastprogram == "blastx":
									blasthitdict[rq]=1
									transframe = qf
									untransseq = get_basicseq(query_seq_dict,rq)
								elif blastprogram == "tblastn":
									blasthitdict[rt]=1
									transframe = sf
									untransseq = get_basicseq(target_db_dict,rt)

								# get translated sequence by frame hit, and break into orfs by stop codons
								transprot = get_trans_inframe(untransseq.seq, transframe, ms, args.code)

								# tries to force M-containing ORF, otherwise use full translated piece
								metprot = get_met_orf(transprot)
								mplen = len(metprot)
								# check is done by comparing lengths
								if check_met_prot(mplen, al-ag, tl):
									untransseq.seq = metprot
								# if check fails, then print the two lengths for debugging
								else:
									untransseq.seq = transprot
									print >> sys.stderr, "%s: PREDICTED PROTEIN INCORRECT LENGTH" % untransseq.id
									print >> sys.stderr, "%s: %d vs %d" % (untransseq.id, mplen, tl)
									# also check if alignment extends beyond protein frame
									if len(transprot) < (sl-ag):
										nonsensecount += 1
										print >> sys.stderr, "CHECK FOR NONSENSE: %s" % untransseq.id
								# in all cases, write whatever was returned to file
								protcount += 1
								transoutfile.write("%s" % untransseq.format("fasta"))
						# this is to collect proteins that were hit by the db
						# no translation is necessary, though this number should not equal hits or nomatch
						elif args.translate and blastprogram == "blastp":
							protcount += 1
							transoutfile.write("%s" % get_basicseq(target_db_dict,rt).format("fasta"))
								
				else:
					# no match output
					nomatch+=1
					if args.no_match_file:
						nomatchoutfile.write("%s" % get_basicseq(query_seq_dict,record.query).format("fasta"))
			result_handle.close()
		else:
			raise IOError("ERROR - no BLAST XML handle")
	print >> sys.stderr, '# Started BLAST: ', startclock
	print >> sys.stderr, '# Finished BLAST: ', time.asctime()
	print >> sys.stderr, "# Processed %d records in %.1f minutes" % (recnum, (time.time()-starttime)/60)
	if args.no_match_file:
		print >> sys.stderr, "# %d records had hits, written to %s" % ((recnum-nomatch),hitsoutputfilename)
		print >> sys.stderr, "# %d records had no match, written to %s" % (nomatch, nomatchoutputfilename)
		hitsoutfile.close()
		nomatchoutfile.close()
	else:
		print >> sys.stderr, "# %d records had hits" % ((recnum-nomatch))
		print >> sys.stderr, "# %d records had no match" % (nomatch)
	if args.translate:
		print >> sys.stderr, "# %d proteins translated, written to %s" % (protcount,transoutputfilename)
		print >> sys.stderr, "# %d proteins predicted with nonsense mutations" % (nonsensecount)
		transoutfile.close()
	return recnum
"""
	###########################################################
	# contents of a record in records:

['__doc__', '__init__', '__module__', 'alignments', 'application', 'blast_cutoff', 
	'database', 'database_length', 'database_letters', 'database_name', 
	'database_sequences', 'date', 'descriptions', 'dropoff_1st_pass', 
	'effective_database_length', 'effective_hsp_length', 'effective_query_length', 
	'effective_search_space', 'effective_search_space_used', 'expect', 'filter', 
	'frameshift', 'gap_penalties', 'gap_trigger', 'gap_x_dropoff', 'gap_x_dropoff_final', 
	'gapped', 'hsps_gapped', 'hsps_no_gap', 'hsps_prelim_gapped', 'hsps_prelim_gapped_attemped', 
	'ka_params', 'ka_params_gap', 'matrix', 'multiple_alignment', 'num_good_extends', 'num_hits', 
	'num_letters_in_database', 'num_seqs_better_e', 'num_sequences', 'num_sequences_in_database', 
	'posted_date', 'query', 'query_id', 'query_length', 'query_letters', 'reference', 'sc_match', 
	'sc_mismatch', 'threshold', 'version', 'window_size']

	# contents of a description in a record
	dir(r.descriptions[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'bits', 'e', 
	'num_alignments', 'score', 'title']

	# contents of an alignment in a record
	dir(r.alignments[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'hit_def', 
	'hit_id', 'hsps', 'length', 'title']

	dir(r.alignments[0].hsps[0])
	['__doc__', '__init__', '__module__', '__str__', 'align_length', 'bits', 'expect', 'frame', 
	'gaps', 'identities', 'match', 'num_alignments', 'positives', 'query', 'query_end', 
	'query_start', 'sbjct', 'sbjct_end', 'sbjct_start', 'score', 'strand']
"""

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

