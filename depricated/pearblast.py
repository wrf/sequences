#! /usr/bin/env python
#
# PEARBLAST - a reciprocal blast script for comparative genomics
#
# v3.2 changed output names 2015-02-06
# v3.1 added comments and fixed blastx seq count bug 2015-01-08
# v3.0 2014-12-09
# v2.0 2013-07-11
# version 1.0 01/feb/2012
#
# TODO
# output list of no-hit sequences from database

# general operation:
# takes a file of sequences as fasta
# aligns with tblastn against target database of nucleotides, genomic or transcriptomic
# collects all hits into a dictionary such that any sequence hit more than once will be counted as such
# the subset of seqs in the dictionary are aligned back with blastx, the results are added to the same dictionary
# processes each seq in the dictionary with the knowledge of all sequences that it hits and that hit it
# outputs the long results file, nucleotides for correct kogs, and predicted full-length proteins

'''
Results output:
Hit classifications:
$1 easy case of mutual best hit
$2 slightly more complicated case of mutual best hit where second place has the same evalue
$4 ambiguous case, probably examine other hits instead
$8 clearly wrong, could be few or single tblastn hit with many blastx hits at much higher evalue, indicates wrong kog, but possibly a case of a conserved domain
$16 no blastx hits

Protein type classifications:
$$1 within length of the target protein
$$2 too short, possible exon or domain missing
$$4 too long, possible exon or domain added
$$16 nonsense protein, the alignment extends beyond the predicted protein

Results file is shown as a table of:
n, Name, n, f.0, n, Name, n, f.0, n, Name, f.0

corresponding to:
hit class (above), query, query length, bitscore/length ratio, number of hits
...hit name (from target genome) , protein length, bits/len of reverse blast
...number of reverse hits, name of best hit back, bits/len of best reverse hit

table is by default sorted by first column, though is often useful to view
when sorted by first then fifth (number of hits)

Usage:
  pearblast.py -q query_seqs.fa -d target_gene_db.fa -r query_refs.fa
'''
# for example:
# pearblast.py -q HsapCandidates.fa -d Ml_ALPACA_PLUS_WALLABY -r HumRef_w_names
# results files are automatically generated based on the name from the database (-d)

import sys
import subprocess
import argparse
import os.path
import time
import re
import multiprocessing
import cStringIO
from collections import defaultdict 
from Bio import SeqIO
from Bio.Blast.Applications import NcbitblastnCommandline
from Bio.Blast.Applications import NcbiblastxCommandline
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord

# use functions of blastplustable
from blastplustable import get_threads, get_blast_path, get_basicseq, get_trans_inframe, get_align_piece, get_transseq, frame_to_orfs

def run_blast(blastcommand, blast_query, blasthits, order, targetseqs, proteindict):
	runtime=time.time()
	blastprogram = blastcommand.program_name
	print >> sys.stderr, 'COMMAND:', blastcommand
	print >> sys.stderr, '# Started %s:' % blastprogram, time.asctime()
	# creates handle from standard output of blast command
	if not order:
		result_handle = cStringIO.StringIO(blastcommand()[0])
	else:
		result_handle = cStringIO.StringIO(blastcommand(stdin=blast_query)[0])
	print >> sys.stderr, '# Finished %s:' % blastprogram, time.asctime()
	print >> sys.stderr, "# BLASTed against %d seqs in %.1f minutes" % (len(targetseqs), (time.time()-runtime)/60)

	if result_handle:
		Recnum, NoMatch, HitCount = parse_xml(result_handle, blasthits, order, targetseqs, proteindict)
		result_handle.close()
	else:
		print >> sys.stderr, "ERROR - no handle"

	# entertain user part 1
	twotime=time.time()
	print >> sys.stderr, '# Finished parsing records: ', time.asctime()
	print >> sys.stderr, "# Processed %d records in %.1f minutes" % (Recnum, (time.time()-twotime)/60)
	print >> sys.stderr, "# {0} {2} records had {1} hits".format((Recnum-NoMatch), HitCount, blastprogram)
	print >> sys.stderr, "# {0} {1} records had no match".format(NoMatch, blastprogram)
	# this function is void

def parse_xml(result_handle, blasthits, order, targetdict, proteindict):
	reccount, hitcount, nomatches = 0,0,0
	for record in NCBIXML.parse(result_handle):
		reccount+=1
		if len(record.alignments):
			# all of the results are returned
			for x,alignment in enumerate(record.alignments):
				hitcount += 1
				rt = record.alignments[x].hit_def # record transcript/ subject is a unicode string
				bt = record.descriptions[x].bits # bitscore is a float
			#	ev = record.descriptions[x].e # evalue is a float
				ag = record.alignments[x].hsps[0].gaps # alignment gaps is an integer
				sg = record.alignments[x].hsps[0].sbjct.count("-")
				ql = record.query_length # query length is an integer
				qu = record.alignments[x].hsps[0].query # is a unicode string of the sequence - u'MSTLKX...'
				rq = record.query # record query is a unicode string, e.g. name of candidate protein
			#	ac = record.alignments[x].accession
				sb = record.alignments[x].hsps[0].sbjct # like for qu
				sl = len(sb)
				ms = record.alignments[x].hsps[0].match.replace(' ','-')
				qf = record.alignments[x].hsps[0].frame # query frame is a tuple of two integers

				# for blastx  fl  int st  int st  int  int
				if order:
					output = (bt, sg, sb, sl, ms, qf[0], x)
					blasthits[rq][rt] = output

				# for tblastn fl  int st  int st  int  int
				else:
					output = (bt, ag, qu, ql, ms, qf[1], x)
					blasthits[rt][rq] = output

					# translate for tblastn step
					if not proteindict.get(rt,0):
						targetseq = get_basicseq(targetdict, rt).seq
						proteindict[rt] = get_trans_inframe(targetseq, qf[1], ms, 1)
		else:
			nomatches+=1
	return reccount, nomatches, hitcount

def score_hit(querylen, querybits, querynum, protlen, backbits, backnum, bestbits):
	if bestbits:
		if backnum > -1:
		# examining hit classifications
			# mutual first hits, as first item in list
			if querynum == 0 and backnum == 0:
				hitclass = 1
			# the best and query have very different bit ratios
			elif bestbits > 2*backbits or bestbits > 2*querybits:
				hitclass = 8
			# bitscore ratios to the best are similar to the query
			elif bestbits < 1.3*backbits and bestbits < 1.3*querybits:
				hitclass = 2
			# unsure, catch the rest
			else:
				hitclass = 4
		# examining protein length classifications
			if querylen > 1.4*protlen:
				protclass = 2
			elif querylen*1.4 < protlen:
				protclass = 4
			else:
				protclass = 1
		else:
			hitclass, protclass = 8,16
	else:
		hitclass, protclass = 16,16
	return hitclass, protclass

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-q','--query', help="query file of candidate sequences", required=True)
	parser.add_argument('-d','--db', help="database file, should be nucleotides of novel organism", required=True)
	parser.add_argument('-D','--directory', help="database director, default read from ~/.ncbirc")
	parser.add_argument('-r','--reference', help="reference sequences of query for reciprocal blast", required=True)
	parser.add_argument('-k','--key', help="letter key for output names, default: test", default="test")
	parser.add_argument('-p','--processors', help="number of processors, default: all")
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff blast search - default: 1e-6", default=1e-6)
	parser.add_argument('-H','--header', action="store_true", help="include header line in output table")
	parser.add_argument('-m','--max-tblastn', type=int, help="maximum number of records per tblastn search - default: 100", default=100)
	parser.add_argument('-M','--max-blastx', type=int, help="maximum number of records per blastx search - default: 10", default=10)
	parser.add_argument('-v','--verbose', action='store_true', help="verbose output")
	parser.add_argument('-s','--strict', action='store_true', help="strict size control for proteins")
	parser.add_argument('-c','--code', help="genetic code", default=1)
	args = parser.parse_args(argv)

	# establish default blasting parameters for single commandline run
	blast_query = os.path.abspath(args.query.strip())
	target_db = os.path.abspath(args.db)
	query_ref_seqs = args.reference
	db_path = get_blast_path(args.directory)

	evaluethres = args.evalue
	mainquery = args.query
	outkey = args.key
	transcode = args.code

	# autodetect thread count from blastplustable
	threadcount = get_threads(args.processors)

	## start main

	## gather the sequences
	print >> sys.stderr, '# Reading sequences: ', time.asctime()
	cand_seq_list = list(SeqIO.parse(blast_query, "fasta"))
	target_db_dict = SeqIO.to_dict(SeqIO.parse(target_db, "fasta"))
	ref_db_dict = SeqIO.to_dict(SeqIO.parse(os.path.join(db_path,query_ref_seqs), "fasta"))
	print >> sys.stderr, '# Query has %d seqs: ' % (len(cand_seq_list)), time.asctime()
	print >> sys.stderr, '# Target has %d seqs: ' % (len(target_db_dict)), time.asctime()
	# generates dictionary entries for each kog

	## start blast loop
	starttime= time.time()
	# establish tblastn parameters
	tblastnhits = defaultdict(dict)
	protdict = {}
	tblastn_command = NcbitblastnCommandline(cmd="tblastn", query=blast_query, db=target_db, evalue=evaluethres, outfmt=5, num_threads=threadcount, max_target_seqs = args.max_tblastn, db_gencode =transcode)
	run_blast(tblastn_command, blast_query, tblastnhits, 0, target_db_dict, protdict)

	# get sequences for the allblasthits and compile into single fasta string
	tblastnseqs = ''
	for hk in tblastnhits.iterkeys():
		tblastnseqs += get_basicseq(target_db_dict, hk).format('fasta')

	## begin reciprocal blastx

	# establish blastx parameters
	blastxhits = defaultdict(dict)
	blastx_command = NcbiblastxCommandline(cmd="blastx", query="-", db=query_ref_seqs, evalue=evaluethres, outfmt=5, num_threads=threadcount, max_target_seqs = args.max_blastx, query_gencode=transcode)
	run_blast(blastx_command, tblastnseqs, blastxhits, 1, ref_db_dict, None)

	## classification of hits, as $1 $2 $4 $8 $16, respectively

	classcounter = {1:0, 2:0, 4:0, 8:0, 16:0}
	hitclassdict = {1:"Positive-Match", 2:"Tie-Match", 4:"Possible-correct-Match", 8:"Wrong-Match", 16:"No-Hits"}
	hitclass = 0

	bitsmultiplier = 0.8       # close is defined as:

	# protein length classifications
	prottypecounter = {1:0, 2:0, 4:0, 8:0, 16:0}
	prottypedict = {1:"Expected-length", 2:"Short-ORF", 4:"Long-ORF", 8:"Nonsense-ORF", 16:"No-Hits"}
	protclass = 0

	# counters for output files
	printnucl = 0
	printprot = 0

	# parameters for three default output files
	nuclsout = {}
	protsout = {}

	## sort output sequences by bitscore ratios
	print >> sys.stderr, '# Starting best-hit comparisons: ', time.asctime()
	resultsoutlines = []
 	for tbnhitname, queryvals in tblastnhits.iteritems():
		# tbk is for key in the dictionary of tblasthits
		# where keys correspond to the tblastn queries (the orthologs of the desired genes)
		# and tbhitname is the hit by the tblastn queries, and also the blastx query
		for tbk in queryvals.iterkeys():
			querylen, querybits, queryi = queryvals[tbk][3], queryvals[tbk][0]/queryvals[tbk][3], queryvals[tbk][6]
			protlen = len(protdict.get(tbnhitname,""))
			try:
				backnum, backbits = blastxhits.get(tbnhitname)[tbk][6], blastxhits.get(tbnhitname)[tbk][0]/protlen
			except KeyError: # this could be keyerror if missing the first value or
				backnum, backbits = -1, 0
			except TypeError: # for where there are no hits maybe? fails attribute call for NoneType
				backnum, backbits = -1, 0
			try:
				tophit = [j for j in blastxhits.get(tbnhitname).iterkeys() if blastxhits[tbnhitname][j][6] == 0][0]
				besthit, bestbits = tophit, blastxhits[tbnhitname][tophit][0]/protlen
			except TypeError:
				besthit, bestbits = "None", 0
			except AttributeError: # tries to call iterkeys on None
				besthit, bestbits = "None", 0

			# classify hits based on various numbers
			hitclass, protclass = score_hit(querylen, querybits, queryi, protlen, backbits, backnum, bestbits )
			classcounter[hitclass] += 1
			prottypecounter[protclass] += 1

			# generate dictionaries of good matches, since dictionaries will
			# only contain hitnames if the match was good
			# then later iterate to print the good hits to file
			if hitclass == 1 or hitclass == 2:
				nuclsout[tbnhitname] = 1
			if protclass == 1:
				protsout[tbnhitname] = 1

			outline = "%d\t%s\t%d\t%.3f\t%d\t%s\t%d\t%.3f\t%d\t%s\t%.3f" % (hitclass, tbk, querylen, querybits, queryi, tbnhitname, protlen, backbits, backnum, besthit, bestbits )
			resultsoutlines.append(outline)

	# finally write results files
	targetbasename = os.path.splitext(os.path.abspath(target_db))[0]
	nuclfilename = "%s_%s_nucl.fasta" % (targetbasename, outkey)
	with open(nuclfilename, 'w') as nof: 
		for key in nuclsout:
			nof.write(target_db_dict[key].format('fasta'))
			printnucl+=1

	protfilename = "%s_%s_prot.fasta" % (targetbasename, outkey)
	with open(protfilename, 'w') as pof:
		for key in protsout:
			target_db_dict[key].seq = protdict[key]
			pof.write(target_db_dict[key].format('fasta'))
			printprot+=1

	resultsfilename = "%s_%s_results.tab" % (targetbasename, outkey)
	with open(resultsfilename, 'w') as rof:
		if args.header:
			print >> rof, "\t".join(["Hitclass", "Query", "QLen", "QBits/Len", "HitNum", "Subject", "SLen", "ReciprocalBits/Len", "ReciprocalNum", "BestReciprocalHit", "BestReciprocalBits/Len"])
		# this should sort to put 1s and 2s at the top of the results table
		# must be converted to int otherwise '16' would come before '2'
		for l in sorted(resultsoutlines, key=lambda x: int(x[0])):
			print >> rof, l

	print >> sys.stderr, '# Counted %d exact matches' % (classcounter[1])
	print >> sys.stderr, '# Counted %d good-matches' % (classcounter[2])
	print >> sys.stderr, '# Counted %d close-bitscore' % (classcounter[4])
	print >> sys.stderr, '# Counted %d wrong pairs' % (classcounter[8])
	print >> sys.stderr, '# Counted %d no-blastx-hits' % (classcounter[16])
	print >> sys.stderr, '# wrote %d genes to file' % (printnucl)

	print >> sys.stderr, '# %d proteins were expected length' % (prottypecounter[1])
	print >> sys.stderr, '# %d were too short' % (prottypecounter[2])
	print >> sys.stderr, '# %d were too long' % (prottypecounter[4])
	print >> sys.stderr, '# %d had nonsense mutations' % (prottypecounter[8])
	print >> sys.stderr, '# %d were wrong or not found' % (prottypecounter[16])
	print >> sys.stderr, '# wrote %d proteins to file' % (printprot)

	print >> sys.stderr, "# Process completed in %.1f minutes" % ((time.time()-starttime)/60)

"""
###########################################################
# contents of a record in records:

# use
from Bio.Blast import NCBIXML
recs = NCBIXML.parse(open("/home/wrf/est/chaetopterus_short.nr.xml",'r'))
rec1 = recs.next()
dir(rec1)

['__class__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'alignments', 'application', 'blast_cutoff', 'database', 'database_length', 'database_letters', 'database_name', 'database_sequences', 'date', 'descriptions', 'dropoff_1st_pass', 'effective_database_length', 'effective_hsp_length', 'effective_query_length', 'effective_search_space', 'effective_search_space_used', 'expect', 'filter', 'frameshift', 'gap_penalties', 'gap_trigger', 'gap_x_dropoff', 'gap_x_dropoff_final', 'gapped', 'hsps_gapped', 'hsps_no_gap', 'hsps_prelim_gapped', 'hsps_prelim_gapped_attemped', 'ka_params', 'ka_params_gap', 'matrix', 'multiple_alignment', 'num_good_extends', 'num_hits', 'num_letters_in_database', 'num_seqs_better_e', 'num_sequences', 'num_sequences_in_database', 'posted_date', 'query', 'query_id', 'query_length', 'query_letters', 'reference', 'sc_match', 'sc_mismatch', 'threshold', 'version', 'window_size']

	# contents of a description in a record
	dir(r.descriptions[0])
['__doc__', '__init__', '__module__', '__str__', 'accession', 'bits', 'e', 'num_alignments', 'score', 'title']

	# contents of an alignment in a record
	dir(r.alignments[0])
['__class__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'accession', 'hit_def', 'hit_id', 'hsps', 'length', 'title']

	dir(r.alignments[0].hsps[0])
	['__class__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'align_length', 'bits', 'expect', 'frame', 'gaps', 'identities', 'match', 'num_alignments', 'positives', 'query', 'query_end', 'query_start', 'sbjct', 'sbjct_end', 'sbjct_start', 'score', 'strand']
"""

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

