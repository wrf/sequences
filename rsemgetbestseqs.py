#! /usr/bin/env python
# v1.1
# filter RSEM results by highest IsoPct for each component
# remove components with IsoPct of 0.00
# v1.2 2015-01-07
# rename fasta sequences to include IsoPct and numbers of seqs per component
# v1.3 2015-01-27
# added option to take prefixes from renamed fasta

'''rsemgetbestseqs.py  last modified 2016-05-02
    filter Trinity RSEM results by highest IsoPct for each component
    and remove components with IsoPct of 0.00

rsemgetbestseqs.py RSEM.isoforms.results Trinity.fasta

    output files are automatically generated and renamed as:

  RSEM.isoforms.results.highest_isopct.txt
  RSEM.isoforms.results.best.fa

    to prepend prefixes from previous fastarenamer.py output
    add option: -r Trinity.renamed.fasta
    note that the output file is renamed as ...best.renamed.fa
'''

import sys
import argparse
from collections import defaultdict
from Bio import SeqIO

def make_renamed_fasta_dict(renamed_fasta):
	rfd = {}
	with open(renamed_fasta,'r') as rf:
		for line in rf:
			if line[0]==">":
				ls = line[1:].rstrip().split("_")
				# assuming old trinity format, for example
				# aequorea_00123_comp2345_c0_seq1
				# dictionary key is comp2345_c0_seq1 and value is aequorea_00123
				rfd["_".join(ls[2:])] = "_".join(ls[0:2])
	return rfd

def main(argv):
	if not argv:
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('rsemfile', help="RSEM.isoforms.results")
	parser.add_argument('fastafile', help="Trinity.fasta")
	parser.add_argument('-r',"--renamed-fasta", help="Renamed Trinity.fasta with naming prefixes")
	args = parser.parse_args(argv)

	print >> sys.stderr, "# Reading input from %s" % args.rsemfile
	with open(args.rsemfile,'r') as rsf:
		rsemlines = rsf.readlines()
	# remove first header line, readlines is used only to allow pop instead of 'if not line==0'
	rsemlines.pop(0)

	# stores the best sequence for that component
	od = {}
	# tracks the maximum score for that component
	cd = defaultdict(float)
	# counts number of sequences per component
	sd = defaultdict(int)

	print >> sys.stderr, "# Finding highest isopct and removing isopct of 0.00"
	for rl in rsemlines:
		# remove \n and split tabs
		rlsplit = rl.rstrip().split()
		isopct = float(rlsplit[7])

		# add to count for that component
		sd[rlsplit[1]] += 1

		# discard cases where the sole transcript in a component has isopct of 0.00
		# also implies that if isopcts are equal, the first will arbitrarily be taken
		if isopct > cd[rlsplit[1]] and isopct > 0.00:
			cd[rlsplit[1]]=isopct
			od[rlsplit[1]]=rlsplit[0]

	outfilename = "{}.highest_isopct.txt".format(args.rsemfile)
	print >> sys.stderr, "# Writing highest isopct SeqIDs to {}".format(outfilename)
	writecount = 0
	with open(outfilename,'w') as outf:
		for k in od:
			writecount += 1
			print >> outf, od[k]
	print >> sys.stderr, "# Wrote {} SeqIDs to {}".format(writecount, outfilename)

	# make seq dictionary from fasta file
	print >> sys.stderr, "# Reading sequences from %s" % args.fastafile
	seqdict = SeqIO.to_dict(SeqIO.parse(args.fastafile,'fasta'))
	bestsplicefile = args.rsemfile+".best.fa"

	# if a renamed fasta is used, take the name prefixes from that file
	if args.renamed_fasta:
		print >> sys.stderr, "# Reading name prefixes from %s" % args.renamed_fasta
		# also renamed output
		bestsplicefile = args.rsemfile+".best.renamed.fa"
		# renamed fasta dictionary for holding the prefixes
		rfd = make_renamed_fasta_dict(args.renamed_fasta)

	print >> sys.stderr, "# Writing sequences to %s" % bestsplicefile
	hc = 0
	with open(bestsplicefile,'w') as ff:
		# keys in od are in form of 'comp1234_c0' while values are the 'comp1234_c0_seq1'
		for k in od:
			# remove len and path from fasta sequence name for trinity
			# and then append the isopct and number of sequences in that component
			# so final should appear like:
			# comp1234_c0_seq1_76.54_3
			newid = "%s_%.2f_%d" % (seqdict[od[k]].id.split(' ')[0], cd[k], sd[k])
			# if using renamed fasta, add back those conserved prefixes
			if args.renamed_fasta:
				newid = "%s_%s" % (rfd[od[k]], newid)
			# again, have to set description to empty string
			seqdict[od[k]].id, seqdict[od[k]].description = newid, ""
			try:
				ff.write("%s" % seqdict[od[k]].format("fasta"))
			except KeyError:
				ff.write("%s" % seqdict[od[k].split(" ")[0]].format("fasta"))
	return bestsplicefile

if __name__ == "__main__":
	main(sys.argv[1:])
