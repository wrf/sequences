#!/usr/bin/env python
#
# transdecodersplitgenes.py

'''transdecodersplitgenes.py  last modified 2016-05-17

transdecodersplitgenes.py -i trinity_gmap.f2.gff -T stringtie_transdecoder.genome.gff

    requires:
  TransDecoder genome gff of Stringtie transcripts (by gtf)
  Trinity GMAP gff
    and to resolve cases where transcripts should be merged (likely few):
  Trinity TransDecoder CDS GMAP gff (the CDS output mapped to the genome)

    create GMAP gff by mapping against build genome01 using format 2:
gmap-2014-12-28/bin/gmap -d genome01 -f 2 -B 5 -t 4 trinity.fasta > trinity_gmap.f2.gff
    also create the Trinity Transdecoder CDS GMAP by:
gmap-2014-12-28/bin/gmap -d genome01 -f 2 -B 5 -t 4 trinity.fasta.transdecoder.cds > trinity_cds_gmap.f2.gff

    generate the non-overlapping split gene names for stringtie by:
stringtiesplitgenes.py -i stringtie.gtf > stringtie_splitgenes.gtf
    and then run TransDecoder as normal on stringtie_splitgenes.gtf
'''

import sys
import argparse
import time
import re
from itertools import chain
from collections import defaultdict

def combine_intervals(rangelist):
	'''convert list of tuples to non redundant intervals'''
	# sl = [(1,10), (1,6), (15,20), (1,10), (19,29), (30,35), (6,13), (40,48), (15,21), (42,50)]
	nrintervallist = []
	srtrangelist = sorted(rangelist) # sort list now, to only do this once
	interval = srtrangelist[0] # need to start with first time, which will be the same for the first bounds
	for bounds in srtrangelist:
		# since it is sorted bounds[0] should always be >= interval[0]
		if bounds[0] > interval[1]+1: # if the next interval starts past the end of the first + 1
			nrintervallist.append(interval) # add to the nr list, and continue with the next
			interval = bounds
		else:
			if bounds[1] > interval[1]: # bounds[1] <= interval[1] means do not extend
				interval = (interval[0], bounds[1]) # otherwise extend the interval
	else: # append last interval
		nrintervallist.append(interval)
	# should return [(3, 13), (15, 35), (40, 50)]
	return nrintervallist

def parse_transdecoder_gff(transdecodergff, exclusiondict, verbose=False):
	'''read in TransDecoder genome GFF and return dictionaries where keys are transcript IDs and values are lists of CDS intervals for multiprotein genes'''
	commentlines = 0
	featurecounts = defaultdict(int)
	protcountbygene = defaultdict(int) # count where key is number of prots

	CDSbyTranscript = defaultdict(list) # key is prot ID, value is list of exon bounds
	protnamesbygene = defaultdict(set) # key is gene, value is set of prot IDs
	genetoscaf = {}
	#genebyscaf = defaultdict(list)
	print >> sys.stderr, "# Parsing proteins from {}".format(transdecodergff), time.asctime()
	for line in open(transdecodergff, 'r').readlines():
		line = line.rstrip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				lsplits = line.split("\t")
				scaffold = lsplits[0]
				if exclusiondict and exclusiondict.get(scaffold, False):
					continue # skip anything that hits to excludable scaffolds
				feature = lsplits[2]
				featurecounts[feature] += 1
				attributes = lsplits[8]
				attrd = dict([(field.strip().split("=")) for field in attributes.split(";") if field])
				if feature=="CDS":
					# ID=gene.236.1|m.123.exon1;Parent=gene.236.1|m.123
					proteinid = attrd["Parent"] # should be gene.236.1|m.123
					geneid = proteinid.rsplit("|",1)[0].rsplit('.',1)[0] # should be gene.236
					protnamesbygene[geneid].add(proteinid)
					genetoscaf[geneid] = scaffold
					exonbounds = (int(lsplits[3]), int(lsplits[4]))
					CDSbyTranscript[proteinid].append(exonbounds)
	print >> sys.stderr, "# Counted {} comment lines".format(commentlines), time.asctime()
	for k in sorted(featurecounts.keys()):
		print >> sys.stderr, "{}\t{}".format(k, featurecounts[k])

	print >> sys.stderr, "# Sorting proteins from each gene", time.asctime()
	protboundsbygene = defaultdict(list) # key is gene ID, value is list of prot bounds
	for prot,exonlist in CDSbyTranscript.iteritems():
		transname = prot.rsplit("|",1)[0]
		genename = transname.rsplit(".",1)[0]
		cdsbounds = ( min(e[0] for e in exonlist), max(e[1] for e in exonlist) )
		protboundsbygene[genename].append(cdsbounds)

	multiprotsbygene = defaultdict(list) # key is gene ID, value is list of prot bounds, only for multiprot genes
	for gene,cdslist in protboundsbygene.iteritems():
		nrprotlist = combine_intervals(cdslist)
		numprots = len(nrprotlist)
		protcountbygene[numprots] += 1
		if numprots > 1:
			multiprotsbygene[gene] = nrprotlist
			if verbose: # output looks like
			# 2 prots for gene stringtie.2499 on contig_010: [(320114, 322831), (323159, 329400)]
			# 4 prots for gene stringtie.2493 on contig_010: [(266369, 294099), (306604, 306987), (307249, 308412), (308772, 309566)]
				print >> sys.stderr, "{} prots for gene {} on {}: {}".format(numprots, gene, genetoscaf[gene], nrprotlist)
	for k in sorted(protcountbygene.keys()):
		print >> sys.stderr, "# Counted {} genes with {} proteins".format(protcountbygene[k], k), time.asctime()
	return CDSbyTranscript, multiprotsbygene, genetoscaf, protnamesbygene

def parse_trinity_gmap(trinitygmap, exclusiondict, verbose=False):
	'''from the Trinity GMAP gff, return two dicts, one with keys of scaffolds, then boundaries, to transcripts, and the other with keys as transcript IDs and values as lists of exon boundaries'''
	commentlines = 0
	featurecounts = defaultdict(int)
	boundsbyscaffold = defaultdict(dict) # keys are scaffolds, then boundaries, values are transcript IDs
	exonsbymrna = defaultdict(list) # keys are transcript IDs, values are lists of exon intervals
	print >> sys.stderr, "# Parsing Trinity transcripts from {}".format(trinitygmap), time.asctime()
	for line in open(trinitygmap, 'r').readlines():
		line = line.rstrip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				lsplits = line.split("\t")
				scaffold = lsplits[0]
				if exclusiondict and exclusiondict.get(scaffold, False):
					continue # skip anything that hits to excludable scaffolds
				feature = lsplits[2]
				featurecounts[feature] += 1
				attributes = lsplits[8]
				attrd = dict([(field.strip().split("=")) for field in attributes.split(";") if field])
				if feature=="mRNA": # attributes should be ID=c15_g1_i1.path1;Name=c15_g1_i1
					mrnaname = attrd["Name"]
					boundaries = (int(lsplits[3]), int(lsplits[4]))
					boundsbyscaffold[scaffold][boundaries] = mrnaname
				if feature=="exon": 
					# ID=c15_g1_i1.mrna1.exon1;Name=c15_g1_i1;Parent=c15_g1_i1.mrna1;Target=c15_g1_i1 1 355 +
					mrnaname = attrd["Name"]
					boundaries = (int(lsplits[3]), int(lsplits[4]))
					exonsbymrna[mrnaname].append(boundaries)
	print >> sys.stderr, "# Counted {} comment lines".format(commentlines), time.asctime()
	for k in sorted(featurecounts.keys()):
		print >> sys.stderr, "{}\t{}".format(k, featurecounts[k])
	return boundsbyscaffold, exonsbymrna

def parse_trinity_cds(trinitycdsgmap, exclusiondict, exonsbytrinity, verbose=False):
	'''parse Trinity cds gmap gff and return a dict where keys are scaffolds, then Trinity components, and values are lists of intervals (should be only one for normal genes) '''
	commentlines = 0
	boundsbymrna = defaultdict(list) # keys are transcript IDs, values are boundaries
	cdscounter = 0
	componentcounter = {}
	print >> sys.stderr, "# Parsing Trinity CDS from {}".format(trinitycdsgmap), time.asctime()
	for line in open(trinitycdsgmap, 'r').readlines():
		line = line.rstrip()
		if line: # ignore empty lines
			if line[0]=="#": # count comment lines, just in case
				commentlines += 1
			else:
				lsplits = line.split("\t")
				scaffold = lsplits[0]
				if exclusiondict and exclusiondict.get(scaffold, False):
					continue # skip anything that hits to excludable scaffolds
				feature = lsplits[2]
				attributes = lsplits[8]
				attrd = dict([(field.strip().split("=")) for field in attributes.split(";") if field])
				if feature=="gene": # attributes should be c10028_g1_i1|m.1577.path2;Name=c10028_g1_i1|m.1577
					cdscounter += 1
					mrnaname = attrd["ID"].rsplit("|",1)[0] # should be c10028_g1_i1
					componentname = mrnaname.rsplit("_",1)[0] # should be c10028_g1
					componentcounter[componentname] = True
				#	pathnumber = attrd["ID"].rsplit("_",1)[1] # should be path2
				# chimeric paths should not be on the same scaffold
					boundaries = (int(lsplits[3]), int(lsplits[4]))
				#	mrnabounds = ( min(e[0] for e in exonsbytrinity[mrnaname]), max(e[1] for e in exonsbytrinity[mrnaname]) )
					boundsbymrna[mrnaname].append(boundaries)
	print >> sys.stderr, "# Counted {} comment lines".format(commentlines), time.asctime()
	print >> sys.stderr, "# Counted {} CDS paths for {} components".format(cdscounter, len(componentcounter) ), time.asctime()
	return boundsbymrna

def make_exclude_dict(excludefile):
	print >> sys.stderr, "# Reading exclusion list {}".format(excludefile), time.asctime()
	exclusion_dict = {}
	for term in open(excludefile,'r').readlines():
		term = term.rstrip()
		if term[0] == ">":
			term = term[1:]
		exclusion_dict[term] = True
	print >> sys.stderr, "# Found {} contigs to exclude".format(len(exclusion_dict) ), time.asctime()
	return exclusion_dict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="Trinity GMAP gff file")
	parser.add_argument('-c','--cds', help="Trinity Transdecoder cds GMAP gff file")
	parser.add_argument('-T','--transdecoder', help="Stringtie Transdecoder genome gff file")
	parser.add_argument('-e','--min-exon', type=float, default=0.5, help="minimum fraction of covered exons for Trinity [0.5]")
	parser.add_argument('-E','--exclude', help="file of list of bad contigs")
	parser.add_argument('-m','--max-prots', type=int, default=2, help="maximum number of proteins per transcript to try and correct [2]")
	parser.add_argument('-r','--remove-name', help="filename for list of Stringtie IDs to remove, otherwise assigned based on Trinity GMAP name")
	parser.add_argument('-t','--trinity-name', help="filename for list of Trinity IDs to add, otherwise assigned based on Trinity GMAP name")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	exclusiondict = make_exclude_dict(args.exclude) if args.exclude else None

	cdsbyprot, tdcdsbygene, tdgenetoscaf, protsbygene = parse_transdecoder_gff(args.transdecoder, exclusiondict)
	tringenebyscaf, exonsbytrinity = parse_trinity_gmap(args.input, exclusiondict)
	trincdsbymrna = parse_trinity_cds(args.cds, exclusiondict, exonsbytrinity) if args.cds else None

	# MAIN SORTING
	stringtieSpopset = set()
	trinitySaddset = set()
	stringtieMpopset = set()
	trinityMaddset = set()
	mergecount = 0
	falsemerge = 0
	splitcount = 0
	print >> sys.stderr, "# Sorting genes with {} or fewer proteins".format(args.max_prots), time.asctime()
	for genename, cdsbounds in tdcdsbygene.iteritems():
		scaffold = tdgenetoscaf[genename]
		protsbytrinity = defaultdict(set)
		trincompbyprots = defaultdict(set)
		trincdsbyprots = defaultdict(set)
		numnrcds = len(cdsbounds)
		if numnrcds>args.max_prots:
			if args.verbose:
				print >> sys.stderr, "{} proteins for gene {}, skipping...".format( numnrcds, genename )
			continue
		for protname in protsbygene[genename]: # iterate over list of protein names
			protcds = cdsbyprot[protname]
			numcds = len(protcds)
			protbounds = ( min(e[0] for e in protcds), max(e[1] for e in protcds) )
			for mrnabounds, mrnaname in tringenebyscaf[scaffold].iteritems():
				if mrnabounds[0] <= protbounds[0] and mrnabounds[1] >= protbounds[1]: # trinity must totally span the gene
					cdstotal = 0
					matchingcds = 0
					trinityexons = exonsbytrinity[mrnaname]
					for i,cds in enumerate(sorted(protcds)):
						cdstotal += 1
						if cds in trinityexons:
							matchingcds += 1
						else: # if no direct match, check for first and last exons
							if numcds==1: # allow cds to be shorter than exon if only one exon
								for te in trinityexons:
									if cds[0] >= te[0] and cds[1] <= te[1]:
										matchingcds += 1
										break
							else:
								if i==0: # for first exon
									for te in trinityexons:
										if cds[0] > te[0] and cds[1]==te[1]:
											matchingcds += 1
											break
								elif i==numcds-1: # for last exon
									for te in trinityexons:
										if cds[0]==te[0] and cds[1] < te[1]:
											matchingcds += 1
											break
					if args.verbose:
						print >> sys.stderr, mrnaname, mrnabounds, "{}/{}".format(matchingcds,cdstotal), protname, protbounds, cdsbounds
					exoncoverage = 1.0*matchingcds/cdstotal
					if exoncoverage >= args.min_exon: # declare protein covered by Trinity
						trinitycomponent = mrnaname.rsplit("_",1)[0]
						protpart = None
						for cb in cdsbounds:
							if cb[0] <= protbounds[0] and cb[1] >= protbounds[1]:
								if protpart: # should never already have a value
									print >> sys.stderr, "WARNING: {} matches {} and {}".format(protbounds, protpart, cb)
								protpart = cb
						if protpart is None: # should always have a value at this point
							print >> sys.stderr, "WARNING: no match for {} in {}".format(protbounds, cdsbounds)
						protsbytrinity[trinitycomponent].add(protpart)
						trincompbyprots[protpart].add(trinitycomponent)
						trincdsbyprots[protpart].add(mrnaname)
		# decide merging or splitting based on whether Trinity to Stringtie matches are 1 to 1 or all to 1
		onebyones = set() # set of Trinity loci that have one to one match to proteins
		yestomerge = False
		for protbound, mrnacompset in trincompbyprots.iteritems():
			nummrna = len(mrnacompset)
		#	print >> sys.stderr, protbound, nummrna, mrnacompset
			if nummrna > 1:
				print >> sys.stderr, "CHECK: protein {} covered by {} Trinity mRNAs".format(genename, nummrna)
			for locusname in mrnacompset:
			#	print >> sys.stderr, locusname, len(protsbytrinity[locusname]), protsbytrinity[locusname]
				if len(protsbytrinity[locusname])==1 and protbound in protsbytrinity[locusname]:
					onebyones.add(locusname)
					if args.verbose: # output looks like
					# 1 to 1 c27338_g1 to stringtie.2358 at (99838, 101625)
					# 1 to 1 c11756_g1 to stringtie.2403 at (152321, 153896)
					# 1 to 1 c26341_g1 to stringtie.2403 at (157491, 158788)
						print >> sys.stderr, "1 to 1 {} to {} at {}".format(locusname, genename, protbound)
				elif len(protsbytrinity[locusname])>1: # for possible merge cases
					if trincdsbymrna: # if no CDS information for trinity is there, skip
						if all(len(trincompbyprots[pb])==1 for pb in protsbytrinity[locusname]): # all to 1
							trincdslist = [trincdsbymrna[mrna] for mrna in trincdsbyprots[protbound]]
							trincdsbounds = [bound for bbymrna in trincdslist for bound in bbymrna]
							finalmergebounds = combine_intervals(chain(trincdsbounds, cdsbounds) )
							if args.verbose:
								print >> sys.stderr, "MERGE TEST:", genename, cdsbounds, locusname, trincdsbounds, scaffold, finalmergebounds
							if len(finalmergebounds)==1: # trinity needs to combine all proteins
								yestomerge = True
								stringtieMpopset.add(genename)
								trinityMaddset.add(locusname)
							else: # otherwise Trinity is falsely merging two adjacent genes
								falsemerge += 1
		if len(onebyones)==numnrcds:
			splitcount += 1
			stringtieSpopset.add(genename)
			trinitySaddset.update(onebyones)
			if args.verbose:
				print >> sys.stderr, "### All proteins covered by Trinity for {}".format(genename)
		if yestomerge:
			mergecount += 1
			if args.verbose:
				print >> sys.stderr, "### Merge candidate of {} for {}".format(locusname, protsbytrinity[locusname])
	print >> sys.stderr, "# Found {} Stringtie transcripts to split".format(splitcount)
	if mergecount:
		print >> sys.stderr, "# Found {} Stringtie transcripts to merge".format(mergecount)
	if falsemerge:
		print >> sys.stderr, "# Skipped {} Stringtie merges where Trinity is also split".format(falsemerge)
	print >> sys.stderr, "# Excluding {} Stringtie transcripts".format(len(stringtieSpopset)+len(stringtieMpopset)), time.asctime()

	srlistoutname = "{}_st_sp_remove.txt".format(args.remove_name) if args.remove_name else "{}_st_sp_remove.txt".format(args.input.rsplit('.',1)[0])
	with open(srlistoutname,'w') as srout:
		for sr in stringtieSpopset:
			print >> srout, sr
	if stringtieMpopset:
		srlistoutname = "{}_st_me_remove.txt".format(args.remove_name) if args.remove_name else "{}_st_me_remove.txt".format(args.input.rsplit('.',1)[0])
		with open(srlistoutname,'w') as srout:
			for sr in stringtieMpopset:
				print >> srout, sr
	print >> sys.stderr, "# Adding {} Trinity transcripts".format(len(trinitySaddset)+len(trinityMaddset)), time.asctime()
	talistoutname = "{}_tr_sp_add.txt".format(args.trinity_name) if args.trinity_name else "{}_tr_sp_add.txt".format(args.input.rsplit('.',1)[0])
	with open(talistoutname,'w') as taout:
		for ta in trinitySaddset:
			print >> taout, ta
	if trinityMaddset:
		talistoutname = "{}_tr_me_add.txt".format(args.trinity_name) if args.trinity_name else "{}_tr_me_add.txt".format(args.input.rsplit('.',1)[0])
		with open(talistoutname,'w') as taout:
			for ta in trinityMaddset:
				print >> taout, ta

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
