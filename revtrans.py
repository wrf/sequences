#! /usr/bin/env python
#
# v1 2013/08/30
#
# reverse translation to generate dna code

import sys

usage='''
reverse translation tool to generate dna code
disregards 5th and 6th codons for L, R and S:  TTR-L AGR-R AGY-S

revtrans.py KIRGXRIEL
'''

if len(sys.argv)<2:
	print >> sys.stderr, usage
else:
	# dictionary of degenerate DNA code
	gencode = { "A":"GCN","C":"TGY","D":"GAY","E":"GAR","F":"TTY", "G":"GGN","H":"CAY","I":"ATH","K":"AAR","L":"CTN", "M":"ATG","N":"AAY","P":"CCN","Q":"CAR","R":"CGN", "S":"TCN","T":"ACN","V":"GTN","W":"TGG","Y":"TAY", "X":"NNN"}

	instring = sys.argv[1]
	outstring = ''

	# simple list loop is used, as replace has potential to replace twice
	for i in instring:
		outstring += gencode[i]

	print >> sys.stdout, outstring

