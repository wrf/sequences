#!/usr/bin/env python
#
# uniprotphylumtable.py v1.0 created 2016-02-15
#
# download all swissprot data using:
# wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/taxonomic_divisions/uniprot_sprot_*.dat.gz

'''
uniprotphylumtable.py  last modified 2019-10-27

uniprotphylumtable.py -d *.dat > phylum_table.tab

   to make a table for sorting species IDs by higher clades
uniprotphylumtable.py -d uniprot_sprot_invertebrates.dat.gz -c -f
'''

import sys
import os
import time
import argparse
import gzip
from collections import Counter

sample_entry='''
ID   12AH_CLOS4              Reviewed;          29 AA.
AC   P21215;
DT   01-AUG-1991, integrated into UniProtKB/Swiss-Prot.
DT   01-AUG-1991, sequence version 1.
DT   09-DEC-2015, entry version 45.
DE   RecName: Full=12-alpha-hydroxysteroid dehydrogenase;
DE            EC=1.1.1.176;
DE   Flags: Fragment;
OS   Clostridium sp. (strain ATCC 29733 / VPI C48-50).
OC   Bacteria; Firmicutes; Clostridia; Clostridiales; Clostridiaceae;
OC   Clostridium.
OX   NCBI_TaxID=1507;
RN   [1]
RP   PROTEIN SEQUENCE.
RX   PubMed=2007406; DOI=10.1111/j.1432-1033.1991.tb15835.x;
RA   Braun M., Luensdorf H., Bueckmann A.F.;
RT   "12 alpha-hydroxysteroid dehydrogenase from Clostridium group P,
RT   strain C 48-50. Production, purification and characterization.";
RL   Eur. J. Biochem. 196:439-450(1991).
CC   -!- FUNCTION: Catalyzes the oxidation of the 12-alpha-hydroxyl group
CC       of bile acids, both in their free and conjugated form. Also acts
CC       on bile alcohols.
CC   -!- CATALYTIC ACTIVITY: 3-alpha,7-alpha,12-alpha-trihydroxy-5-beta-
CC       cholanate + NADP(+) = 3-alpha,7-alpha-dihydroxy-12-oxo-5-beta-
CC       cholanate + NADPH.
CC   -!- SUBUNIT: Homotetramer.
CC   -!- MISCELLANEOUS: The thermostability of the enzyme is greatly
CC       increased due to NADP binding.
CC   -----------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see http://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution-NoDerivs License
CC   -----------------------------------------------------------------------
DR   PIR; S14099; S14099.
DR   BioCyc; MetaCyc:MONOMER-15705; -.
DR   GO; GO:0047013; F:cholate 12-alpha dehydrogenase activity; IEA:UniProtKB-EC.
DR   GO; GO:0030573; P:bile acid catabolic process; IEA:UniProtKB-KW.
DR   Gene3D; 3.40.50.720; -; 1.
DR   InterPro; IPR016040; NAD(P)-bd_dom.
DR   SUPFAM; SSF51735; SSF51735; 1.
PE   1: Evidence at protein level;
KW   Bile acid catabolism; Direct protein sequencing; Lipid degradation;
KW   Lipid metabolism; NADP; Oxidoreductase; Steroid metabolism.
FT   CHAIN         1    >29       12-alpha-hydroxysteroid dehydrogenase.
FT                                /FTId=PRO_0000064347.
FT   NON_TER      29     29
SQ   SEQUENCE   29 AA;  2900 MW;  A827DB34DB6C8812 CRC64;
     MIFDGKVAII TGGGKAKSIG YGIAVAYAK
//'''

def parse_uniprot_data(datfile, taxaposition, keepgenus, keepfull):
	'''parse uniprot taxonomy data file and return a dictionary where keys are species codes and values are phyla'''
	phylumdict = {} # key is species code, value is phyla
	speciescounts = Counter() # key is species code, value is count of sequences

	if os.path.splitext(datfile)[1]==".gz":
		sys.stderr.write("# Parsing data from {} as gz  ".format(datfile) + time.asctime() + os.linesep)
		opentype = gzip.open
	else:
		sys.stderr.write("# Parsing data from {}  ".format(datfile) + time.asctime() + os.linesep)
		opentype = open

	speciesid = ""
	phylum = ""
	ocstring = ""
	genusspecies = ""

	idcounts = 0
	doubleups = 0
	phylumerrors = 0

	for line in opentype(datfile,'rt'):
		line = line.strip() # sequence info contains leading spaces
		if line:
			linetag = line[0:2]
			if linetag=="ID" and not speciesid and not phylum:
				idcounts += 1
				lsplits = line[5:].split()
				seqid = lsplits[0]
				speciesid = seqid.split("_")[1]
				speciescounts[speciesid] = speciescounts.get(speciesid,0) + 1
			elif linetag=="OS" and speciesid and not genusspecies:
				lsplits = line[5:].split()
				genusspecies = lsplits[0:2]
			elif linetag=="OC" and speciesid and not phylum:
				if len(ocstring): # if there is already taxon info, include an additional space
					ocstring += line[4:]
				else:
					ocstring += line[5:]
			elif linetag=="//": # end of sequence, get phylum, and force reset if not already
				lsplits = ocstring.split()
				try:
					if keepgenus:
						phylum = "{} {}".format(*genusspecies)
					elif keepfull:
						phylum = " ".join(lsplits[:-2] + genusspecies)
					else:
						phylum = lsplits[taxaposition]
					# clean out extra punctuation, remove all ; , and .
					phylum = phylum.replace(";","").replace(",","").replace(".","")
				except IndexError: # if some bacteria do not have a phylum, take last position available
					phylum = lsplits[-1].replace(";","").replace(",","").replace(".","")
					sys.stderr.write("WARNING: no phylum found for {}, using {}\n".format(seqid, phylum) )
				#if phylum=="Candidatus": ### TODO add correction for candidate phyla
				#	phylum = lsplits[taxaposition+1].replace(";","").replace(",","").replace(".","")
				lastphylum = phylumdict.get(speciesid, False)
				if lastphylum: # if species is there, check it is the same
					if phylum==lastphylum: # if already there and same, add that up
						doubleups += 1
					else: # otherwise flag as potential problem
						sys.stderr.write("WARNING: {} is not {} for {}\n".format(phylum, lastphylum, seqid) )
						phylumerrors += 1
				else: # otherwise add it to the dict
					phylumdict[speciesid] = phylum
				speciesid = ""
				phylum = ""
				ocstring = ""
				genusspecies = ""
	sys.stderr.write("# Found {} sequence IDs  ".format(idcounts) + time.asctime() + os.linesep)
	sys.stderr.write("# Counted {} sequences that already have a phylum\n".format(doubleups) )
	if phylumerrors: # only print if errors are found
		sys.stderr.write("# Found {} possible annotation errors\n".format(phylumerrors) )
	return speciescounts, phylumdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-d','--data', nargs='*', help="uniprot dat file, or files, can be .gz")
	parser.add_argument('-p','--phylum-rank', type=int, default=2, help="phylum ranking, ordinal number in a list [2]")
	parser.add_argument('-c','--counts', action="store_true", help="print counts of each species ID")
	parser.add_argument('-f','--full-taxonomy', action="store_true", help="print full taxonomy of each species ID")
	parser.add_argument('-g','--genus', action="store_true", help="report only genus and species")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	phylumdict = {}
	speciescounts = {}
	for datfile in args.data:
		ncounts, nphyla = parse_uniprot_data(datfile, args.phylum_rank, args.genus, args.full_taxonomy)
		phylumdict.update( nphyla )
		speciescounts.update( ncounts )

	### print final table to stdout
	phylacount = len(set(phylumdict.values() ) )
	sys.stderr.write("# Final table contains {} entries for {} phyla  ".format( len(phylumdict), phylacount ) + time.asctime() + os.linesep)
	if args.counts:
		for k,v in phylumdict.items():
			sys.stdout.write( "{}\t{}\t{}\n".format(k, speciescounts.get(k,0), v) )
	else:
		for k,v in phylumdict.items():
			sys.stdout.write( "{}\t{}\n".format(k, v) )
	if args.verbose:
		sys.stderr.write("{}\n".format(set(phylumdict.values())))

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
