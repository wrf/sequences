#! /usr/bin/env python
#
# v2.01 corrected error with accession numbers in blasttable 3/jan/2012
# v2.0 spawned from size_ec_capturer.py to use local database rather than curl 21/dec/2011
# v1.22 added timer 1/dec/11
# v1.21 cleaned up code
# v1.2 takes user input to specify file in command line
# v1.1 also appends EC number, as available
# v1.0 takes in blasttable from blasttable.py and appends protein size info from uniprot

# http://www.uniprot.org/uniprot/Q9PV90.txt
# SQ   SEQUENCE   319 AA;  34753 MW;  CE9F47819AD54ABF CRC64;
# 60S acidic ribosomal protein P0	9.51078e-14	47	isotig00480  gene=isogroup00440  length=211  numContigs=1	http://www.uniprot.org/uniprot/Q9PV90	S-Y+P-VLDIT+D-L-KRF+--V--+-++-L-IGYPT+AS+PH-++N

usage="""
Usage: 
	uniprot_ecmw.py filename.txt
	can output to text with > newfilename.txt
"""

import os
import sys
import time

def main(argv, wayout):

	if len(argv)<2:
		print usage	

	else:
		inputfilename = argv[1]
		uniprotdb = "/home/biolum/ncbi-blast-2.2.25+/db/uniprot_sprot.dat"
		#uniprotdb = "uniprot_sprot.short.dat"

		import re
		from Bio import SwissProt
		from collections import namedtuple

		if not(os.path.exists(inputfilename)):
			print >> sys.stderr, "# No file found"
		else:
			InFile = open(inputfilename,"rU")
			dbfile = open(uniprotdb,"rU")

			startclock=time.asctime()
			starttime= time.time()
			print >> sys.stderr,"Loading %s at: " % (uniprotdb), time.asctime()

			sprecord = namedtuple("sprecord", ["SQ", "DE", "DR"])
			spdb = {}
			for record in SwissProt.parse(dbfile):
	#			print record.accessions, record.seqinfo, record.description
				rec = sprecord(SQ = record.seqinfo, DE = record.description, DR = record.cross_references)
				for acc in record.accessions:
					spdb[acc] = rec
			dbfile.close()
			print >> sys.stderr,"Finished loading %d records at: " % (len(spdb)), time.asctime()
			Count = -1
			full_length = 0
	#		testac = "Q12702"
	#		if testac in spdb:
	#			hit = spdb[testac]
	#			print hit.SQ[1]
	#			print hit.DE
			print >> sys.stderr,"Extracting EC and MW from %s at: " % (inputfilename), time.asctime()
			for Line in InFile:
				Count += 1
				if Count > 0:
					Element = Line.split("\t")
					Name = Element[0]
					UniprotAc=str(Element[4])
					hit_length = int(Element[2])
	#				print UniprotAc
					if UniprotAc in spdb:
						hit = spdb[UniprotAc]
	#					print hit.SQ[1]
	#					print hit.DE
						try:
							molweight = hit.SQ[1]
						except:
							molweight = "NULL"
						try:
							prot_length = float(hit.SQ[0])
						except:
							prot_length = 1.0
						try:
							ECResult=re.search(".+EC=([0-9.]+);",hit.DE)
							EC=ECResult.group(1)
						except:
							EC="0"
						print >> wayout, Line.rstrip(), "\t%s\t%s\t%s\t%.2f" % (EC,molweight,prot_length,(hit_length/prot_length))
						if .99 <= (hit_length/prot_length) < 1.25:
							full_length += 1
					if not(Count % 1000):
						sys.stderr.write(".")
					if not(Count % 10000):
						sys.stderr.write(str(Count) + "\n")	
				else:
					print >> wayout, Line.rstrip(), "\tEC\tMW\tLength\tCoverage"

			print >> sys.stderr,"# Started capture: ", startclock
			print >> sys.stderr,"# Finished capture: ", time.asctime()
			print >> sys.stderr,"# Done loading %d sequences" % (Count)
			InFile.close()
		return full_length
'''
 dir(record)
 ['__doc__', '__init__', '__module__', 'accessions', 'annotation_update',
 'comments', 'created', 'cross_references', 'data_class', 'description',
 'entry_name', 'features', 'gene_name', 'host_organism', 'keywords',
 'molecule_type', 'organelle', 'organism', 'organism_classification',
 'references', 'seqinfo', 'sequence', 'sequence_length',
 'sequence_update', 'taxonomy_id']

entry_name        Name of this entry, e.g. RL1_ECOLI.
data_class        Either 'STANDARD' or 'PRELIMINARY'.
molecule_type     Type of molecule, 'PRT',
sequence_length   Number of residues.

accessions        List of the accession numbers, e.g. ['P00321']
created           A tuple of (date, release).
sequence_update   A tuple of (date, release).
annotation_update A tuple of (date, release).

description       Free-format description.
gene_name         Gene name.  See userman.txt for description.
organism          The source of the sequence.
organelle         The origin of the sequence.
organism_classification  The taxonomy classification.  List of strings.
                         (http://www.ncbi.nlm.nih.gov/Taxonomy/)
taxonomy_id       A list of NCBI taxonomy id's.
host_organism     A list of names of the hosts of a virus, if any.
host_taxonomy_id  A list of NCBI taxonomy id's of the hosts, if any.
references        List of Reference objects.
comments          List of strings.
cross_references  List of tuples (db, id1[, id2][, id3]).  See the docs.
keywords          List of the keywords.
features          List of tuples (key name, from, to, description).
                  from and to can be either integers for the residue
                  numbers, '<', '>', or '?'

seqinfo           tuple of (length, molecular weight, CRC32 value)
sequence          The sequence.
'''

if __name__ == "__main__":
	main(sys.argv, sys.stdout)

