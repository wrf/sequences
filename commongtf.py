#!/usr/bin/env python
# commongtf.py
# v1.0 2015-08-13

'''
commongtf.py last modified 2015-08-14
    find and keep transcripts with identical exon structure between two gtfs

    EXAMPLE USAGE:

commongtf.py -1 stringtie.gtf -2 pasa.gtf > common_stringtie_pasa.gtf

    output order is based on the first file, so -1 stringtie.gtf
    gtf files must be in transcript-exon format
'''

import sys
import argparse
import time
import re
from collections import defaultdict

def gtf_to_exonstrdict(gtffile):
	genecount, exoncount = 0,0
	exonScaffold = defaultdict(lambda: defaultdict(list)) # dict of dicts of lists, as scaffold[gene][exons]
	for line in open(gtffile,'r').readlines():
		line = line.rstrip()
		if not line or line[0]=="#":
			continue
		lsplits = line.split("\t")
		if lsplits[2]=="transcript":
			genecount += 1
			transid = re.search('transcript_id "([\w.|-]+)";', lsplits[8]).group(1)
		elif lsplits[2]=="exon":
			exoncount += 1
			contig, startpos, endpos = lsplits[0], int(lsplits[3]), int(lsplits[4])
			boundaries = (startpos,endpos)
			exonScaffold[contig][transid].append(boundaries)
	print >> sys.stderr, "Counted {} transcripts and {} exons".format(genecount, exoncount), time.asctime()
	exonStrDict = defaultdict(dict)
	for scaf,exbygene in exonScaffold.iteritems():
		for gene, exons in exbygene.iteritems():
			sortedexons = str(sorted(exons, key=lambda x: x[0]))
			exonStrDict[scaf][sortedexons] = True
	return exonStrDict

def main(argv):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-1','--gtf1', help="gtf format file")
	parser.add_argument('-2','--gtf2', help="gtf format file")
	parser.add_argument('-o','--output', help="file for common transcripts, default written to stdout", type=argparse.FileType("w"), default=sys.stdout)
	parser.add_argument('-v','--verbose', help="verbose output", action="store_true")
	args = parser.parse_args(argv)

	# read file 2 into dictionary, then sort for each transcript in file 1
	print >> sys.stderr, "Starting exon parsing on {}".format(args.gtf2), time.asctime()
	exonstringbyScaffold = gtf_to_exonstrdict(args.gtf2)

	print >> sys.stderr, "Starting exon parsing on {}".format(args.gtf1), time.asctime()
	genecount, exoncount, misccount = 0,0,0
	writecount = 0
	exonlist = []
	linestorage = []
	for line in open(args.gtf1,'r').readlines():
		line = line.rstrip()
		if not line or line[0]=="#":
			continue
		lsplits = line.split("\t")
		if lsplits[2]=="transcript":
			if exonlist: # check for previous transcript before continuing
				exonstring = str(sorted(exonlist, key=lambda x: x[0]))
				if exonstringbyScaffold[contig].get(exonstring,False):
					print >> args.output, "\n".join(linestorage)
					writecount += 1
				# in all cases, reset exonlist
				exonlist, linestorage = [],[]
			# moving on
			genecount += 1
			linestorage.append(line)
		elif lsplits[2]=="exon":
			exoncount += 1
			contig, startpos, endpos = lsplits[0], int(lsplits[3]), int(lsplits[4])
			boundaries = (startpos,endpos)
			exonlist.append(boundaries)
			linestorage.append(line)
		else:
			misccount += 1
			linestorage.append(line)

	print >> sys.stderr, "Counted {} transcripts and {} exons".format(genecount, exoncount), time.asctime()
	if misccount:
		print >> sys.stderr, "Counted {} other features".format(misccount), time.asctime()
	print >> sys.stderr, "Wrote {} common transcripts".format(writecount), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:])
