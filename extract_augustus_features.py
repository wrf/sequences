#!/usr/bin/env python

# ripped off from:
# https://github.com/peterjc/bgruening_galaxytools/blob/master/augustus/extract_features.py
# which is from:
# https://github.com/bgruening/galaxytools/blob/master/tools/augustus/extract_features.py
# updated 2016-04-20 by WRF
# filetype changed to rt instead of rU 2024-12-09

"""extract_augustus_features.py  last modified 2024-12-09

    extract CDS and protein features from the AUGUSTUS GFF format
    features are given as commented lines between GFF features

extract_augustus_features.py -p ml0180_piezo.prot.fa -c ml0180_piezo.cds.fa testdata/ml0180_piezo.gff

    for example, generated from Mlei with AUGUSTUS:
~/augustus-3.0.3/scripts/msa2prfl.pl all_piezo.no_partial.aln > all_piezo.no_partial.prfl

augustus --proteinprofile=/home/user/db/piezo/all_piezo.no_partial.prfl --predictionStart=250000 --predictionEnd=325000 --species=Mnemiopsis_leidyi --protein=on --cds=on --strand=both --gff3=on --genemodel=exactlyone ml0180.fa > ml0180.augustus.gff
"""

import os
import sys
import argparse
import textwrap
import time
import re

def main( argv ):
	"""
	Extract the protein and coding section from an augustus gff, gtf file
	Example file:
HS04636	AUGUSTUS	stop_codon	6901	6903	.	+	0	Parent=g1.t1
HS04636	AUGUSTUS	transcription_end_site	8857	8857	.	+	.	Parent=g1.t1
# protein sequence = [MLARALLLCAVLALSHTANPCCSHPCQNRGVCMSVGFDQYKCDCTRTGFYGENCSTPEFLTRIKLFLKPTPNTVHYIL
# THFKGFWNVVNNIPFLRNAIMSYVLTSRSHLIDSPPTYNADYGYKSWEAFSNLSYYTRALPPVPDDCPTPLGVKGKKQLPDSNEIVEKLLLRRKFIPD
# PQGSNMMFAFFAQHFTHQFFKTDHKRGPAFTNGLGHGVDLNHIYGETLARQRKLRLFKDGKMKYQIIDGEMYPPTVKDTQAEMIYPPQVPEHLRFAVG
# QEVFGLVPGLMMYATIWLREHNRVCDVLKQEHPEWGDEQLFQTSRLILIGETIKIVIEDYVQHLSGYHFKLKFDPELLFNKQFQYQNRIAAEFNTLYH
# WHPLLPDTFQIHDQKYNYQQFIYNNSILLEHGITQFVESFTRQIAGRVAGGRNVPPAVQKVSQASIDQSRQMKYQSFNEYRKRFMLKPYESFEELTGE
# KEMSAELEALYGDIDAVELYPALLVEKPRPDAIFGETMVEVGAPFSLKGLMGNVICSPAYWKPSTFGGEVGFQIINTASIQSLICNNVKGCPFTSFSV
# PDPELIKTVTINASSSRSGLDDINPTVLLKERSTEL]
# end gene g1
###
#
# ----- prediction on sequence number 2 (length = 2344, name = HS08198) -----
#
# Predicted genes for sequence number 2 on both strands
# start gene g2
HS08198	AUGUSTUS	gene	86	2344	1	+	.	ID=g2
HS08198	AUGUSTUS	transcript	86	2344	.	+	.	ID=g2.t1;Parent=g2
HS08198	AUGUSTUS	transcription_start_site	86	86	.	+	.	Parent=g2.t1
HS08198	AUGUSTUS	exon	86	582	.	+	.	Parent=g2.t1
HS08198	AUGUSTUS	start_codon	445	447	.	+	0	Parent=g2.t1
	"""

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('gff', help='AUGUSTUS GFF, or stdin', type=argparse.FileType('rt'), default='-')
	parser.add_argument('-c', '--codingseq', help='Path to the coding sequence output file.')
	parser.add_argument('-p', '--protein', help='Path to the protein output file.')
	parser.add_argument('-n', '--name', help='Optional name to prepend to the fasta ID')
	parser.add_argument('-s', '--scaffold', help='Include scaffold in the fasta ID')
	args = parser.parse_args(argv)

	wrappersize = 60
	transcriptcount = 0

	prot_count = 0
	coding_seq_count = 0

	protein_seq = ''
	coding_seq = ''
	if args.protein:
		po = open( args.protein, 'w+' )
	if args.codingseq:
		co = open( args.codingseq, 'w+' )
	print( "# Extracting features from {}  {}".format(args.gff.name, time.asctime() ), file=sys.stderr )
	for line in args.gff:
		line = line.strip()
		# protein- and coding-sequence are stored as comments
		if line.startswith('#'):
			line = line[2:]
			#if line.startswith('start gene'):
			#	gene_name = line[11:].strip()
			if protein_seq:
				if line.endswith(']'):
					protein_seq += line[:-1]
					po.write( '>%s\n%s\n' % (gene_name, '\n'.join( textwrap.wrap( protein_seq, wrappersize ) ) ) )
					protein_seq = ''
				else:
					protein_seq += line
			if args.protein and line.startswith('protein sequence = ['):
				prot_count += 1
				if line.endswith(']'):
					protein_seq = line[20:-1]
					po.write( '>%s\n%s\n' % (gene_name, '\n'.join( textwrap.wrap( protein_seq, wrappersize ) ) ) )
					protein_seq = ''
				else:
					line = line[20:]
					protein_seq = line
			if coding_seq:
				if line.endswith(']'):
					coding_seq += line[:-1]
					co.write( '>%s\n%s\n' % (gene_name, '\n'.join( textwrap.wrap( coding_seq, wrappersize ) ) ) )
					coding_seq = ''
				else:
					coding_seq += line
			if args.codingseq and line.startswith('coding sequence = ['):
				coding_seq_count += 1
				if line.endswith(']'):
					coding_seq = line[19:-1]
					co.write( '>%s\n%s\n' % (gene_name, '\n'.join( textwrap.wrap( coding_seq, wrappersize ) ) ) )
					coding_seq = ''
				else:
					line = line[19:]
					coding_seq = line
		elif line:
			lsplits = line.split('\t')
			if lsplits[2]=="transcript":
				# from:
				# HS08198	AUGUSTUS	transcript	86	2344	.	+	.	ID=g2.t1;Parent=g2
				# name would be:
				# >g2.t1__HS08198
				try:
					gene_name = "{}".format(re.search("ID=([\w.]+);",lsplits[8]).group(1))
				except AttributeError:
					gene_name = lsplits[8]
				if args.name:
					gene_name = "{}_{}".format(args.name, gene_name)
				if args.scaffold:
					gene_name += "__{}".format(lsplits[0])
				transcriptcount += 1
				if not transcriptcount % 100:
					sys.stderr.write(".")
				if not transcriptcount % 5000:
					sys.stderr.write( "{} {}\n".format( transcriptcount, time.asctime() ) )
	else:
		sys.stderr.write( "{} transcripts  {}\n".format( transcriptcount, time.asctime() ) )
	if args.codingseq:
		co.close()
	if args.protein:
		po.close()
	print( "# Finished parsing  {}".format( time.asctime() ), file= sys.stderr )
	if coding_seq_count:
		print( "# Counted {} CDS".format( coding_seq_count ), file= sys.stderr )
	else:
		print( "# No CDS found in {}".format( args.gff.name ), file= sys.stderr )
	if prot_count:
		print( "# Counted {} proteins".format( prot_count ), file= sys.stderr )
	else:
		print( "# No protein sequences found in {}".format( args.gff.name ), file= sys.stderr )


if __name__ == '__main__':
	main( sys.argv[1:] )

