#!/usr/bin/env python
#
# stringtiesplitgenes.py

'''stringtiesplitgenes.py  last modified 2016-05-17

    separates stringtie genes that contain non-overlapping transcripts

stringtiesplitgenes.py -i stringtie.gtf > stringtie_splitgenes.gtf

    given three non-overlapping stringtie transcripts, all from gene 2387:
contig012	StringTie	transcript	41195	46051	1	-	.	gene_id "stringtie.2387"; transcript_id "stringtie.2387.1"; cov "66.588913";FPKM "4.374235";
contig012	StringTie	transcript	48529	51197	1	-	.	gene_id "stringtie.2387"; transcript_id "stringtie.2387.2"; cov "267.580475";FPKM "17.577400";
contig012	StringTie	transcript	51251	57303	1	-	.	gene_id "stringtie.2387"; transcript_id "stringtie.2387.3"; cov "170.123047";FPKM "11.175407";

    should separate into three genes:
contig012	StringTie	transcript	41195	46051	1	-	.	gene_id "stringtie.2387a"; transcript_id "stringtie.2387a.1"; cov "66.588913";FPKM "4.374235";
contig012	StringTie	transcript	48529	51197	1	-	.	gene_id "stringtie.2387b"; transcript_id "stringtie.2387b.2"; cov "267.580475";FPKM "17.577400";
contig012	StringTie	transcript	51251	57303	1	-	.	gene_id "stringtie.2387c"; transcript_id "stringtie.2387c.3"; cov "170.123047";FPKM "11.175407";
'''

import sys
import argparse
import time
import re
import string
from collections import defaultdict

def combine_intervals(rangelist):
	'''convert list of tuples to non redundant intervals'''
	# sl = [(1,10), (1,6), (15,20), (1,10), (19,29), (30,35), (6,13), (40,48), (15,21), (42,50)]
	nrintervallist = []
	srtrangelist = sorted(rangelist) # sort list now, to only do this once
	interval = srtrangelist[0] # need to start with first time, which will be the same for the first bounds
	for bounds in srtrangelist:
		# since it is sorted bounds[0] should always be >= interval[0]
		if bounds[0] > interval[1]+1: # if the next interval starts past the end of the first + 1
			nrintervallist.append(interval) # add to the nr list, and continue with the next
			interval = bounds
		else:
			if bounds[1] > interval[1]: # bounds[1] <= interval[1] means do not extend
				interval = (interval[0], bounds[1]) # otherwise extend the interval
	else: # append last interval
		nrintervallist.append(interval)
	# should return [(3, 13), (15, 35), (40, 50)]
	return nrintervallist

def parse_stringtie_gtf(stringtiegtf, excludedict, wayout):
	'''read Stringtie GTF file, identify transcripts of the same gene which are non overlapping and rename them for the GTF output'''
	print >> sys.stderr, "# Parsing StringTie output {}".format(stringtiegtf), time.asctime()
	# parse attributes as:
	# gene_id "stringtie_ss.1"; transcript_id "stringtie_ss.1.1"; cov "3.099174";FPKM "0.203585";
	#attributere = 'transcript_id "([\w.-|]+)"; cov "([\d.]+)";FPKM "([\d.]+)";'
	featuresbytranscript = defaultdict(list) # list of feature lines for each transcript
	transbygene = defaultdict(list) # all transcripts per gene
	boundsbygene = defaultdict(list) # boundaries per gene, to combine
	boundsbytrans = {} # key is transcript id, value is bounds
	stringtie_output = {}
	for line in open(stringtiegtf,'r').readlines():
		line = line.strip()
		if not line or line[0]=="#": # skip empty lines and comments
			continue
		lsplits = line.split("\t")
		scaffold = lsplits[0]
		if excludedict and excludedict.get(scaffold, False):
			continue
		feature = lsplits[2]
		attributes = lsplits[8]
		attrd = dict([(field.strip().split(" ")) for field in attributes.split(";") if field])
		if feature == "transcript": # only two feature types in stringtie
			geneid = attrd["gene_id"].replace('"','')
			transid = attrd["transcript_id"].replace('"','')
			boundaries = (int(lsplits[3]), int(lsplits[4]))
			boundsbytrans[transid] = boundaries
			boundsbygene[geneid].append(boundaries)
			transbygene[geneid].append(transid)
			featuresbytranscript[transid].append(line)
		elif feature == "exon":
			transid = attrd["transcript_id"].replace('"','')
			featuresbytranscript[transid].append(line)
	print >> sys.stderr, "# Found {} StringTie transcripts for {} genes".format(len(featuresbytranscript), len(transbygene) ), time.asctime()
	### SPLIT GENES
	transpergene = defaultdict(int)
	totalgenecount = 0
	for gene,bounds in boundsbygene.iteritems():
		nrbounds = combine_intervals(bounds) # this should be sorted already
		transpergene[len(nrbounds)] += 1
		totalgenecount += len(nrbounds)
		if len(nrbounds) > 1: # meaning more than one non overlapping gene
		#	print >> sys.stderr, gene, nrbounds
			for trans in transbygene[gene]:
				transbounds = boundsbytrans[trans]
				for i,nrb in enumerate(nrbounds):
					if nrb[0] <= transbounds[0] and nrb[1] >= transbounds[1]:
						newgenename = "{}{}".format(gene,string.ascii_lowercase[i])
						print >> wayout, "\n".join(featuresbytranscript[trans]).replace(gene, newgenename)
						break # in practice no two nrb were ever found for any gene, so no real check needed
		else:
			for trans in transbygene[gene]:
				print >> wayout, "\n".join(featuresbytranscript[trans])
	print >> sys.stderr, "# Wrote {} genes and identified {} new non overlapping genes".format(totalgenecount, totalgenecount-len(transbygene))
	for k in sorted(transpergene.keys()):
		print >> sys.stderr, "# Counted {} genes with {} transcripts".format(transpergene[k], k), time.asctime()
	# NO RETURN

def make_exclude_dict(excludefile):
	print >> sys.stderr, "# Reading exclusion list {}".format(excludefile), time.asctime()
	exclusion_dict = {}
	for term in open(excludefile,'r').readlines():
		term = term.rstrip()
		if term[0] == ">":
			term = term[1:]
		exclusion_dict[term] = True
	print >> sys.stderr, "# Found {} contigs to exclude".format(len(exclusion_dict) ), time.asctime()
	return exclusion_dict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="Trinity GMAP gff file")
	parser.add_argument('-E','--exclude', help="file of list of bad contigs")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	exclusiondict = make_exclude_dict(args.exclude) if args.exclude else None

	parse_stringtie_gtf(args.input, exclusiondict, wayout)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
