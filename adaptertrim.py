#! /usr/bin/env python
#
# adaptertrim.py
# simple adapter trimming utility for otherwise junk sequences

import sys
import argparse
import time
from Bio import SeqIO

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', help="fastq format file")
	args = parser.parse_args(argv)

	# sequence shared by illumina adapters, this is followed by either:
	# GTCGTGTAGGGAAAGAGTGTA or ACACGTCTGAACTCCAGTCAC
	adapter = "AGATCGGAAGAGC"
	seqcount = 0
	adcount = 0

	badseqs = args.input_file + ".trimmed"
	print >> sys.stderr, "Trimming adapters from fastq sequences", time.asctime()
	with open(badseqs,'w') as bs:
		for sr in SeqIO.parse(args.input_file,'fastq'):
			seqcount += 1
			pos = sr.seq.find(adapter)		
			if not pos == -1:
				adcount += 1
				outstring = str(sr.seq)[0:pos]
				if outstring:
					print >> bs, ">%d" % seqcount
					print >> bs, outstring
	print >> sys.stderr, "Counted %d sequences and trimmed %d adapters" % (seqcount,adcount), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
