#!/usr/bin/env python
#
# bridgemoleculoreads.py v1.0 created 2015-12-10

'''bridgemoleculoreads.py  last modified 2015-12-11
bridgemoleculoreads.py -i all_moleculo_reads.fastq -s Scaffolds.txt
'''

import sys
import argparse
import time
import gzip
import os
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

def link_dict_names(seqdict, delimiter=":"):
	'''return dictionary of where keys are shortened seq IDs and values are full IDs'''
	seqidlinkdict = {}
	for seqid in seqdict.iterkeys():
		splitid = seqid.split(delimiter)[0]
		seqidlinkdict[splitid] = seqid
	return seqidlinkdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="single file of long reads")
	parser.add_argument('-g','--gzip', action="store_true", help="gzip output files")
	parser.add_argument('-o','--output', help="optional output name, otherwise uses input name as base")
	parser.add_argument('-s','--scaffolds', help="moleculo scaffolds txt file")
	parser.add_argument('-f','--format', default="fastq", help="read format, default: fastq")
	parser.add_argument('-l','--length', default=200, type=int, help="insert length of gaps [200]")
	args = parser.parse_args(argv)

	print >> sys.stderr, "# Parsing long reads from {}".format(args.input), time.asctime()
	seqdict = SeqIO.to_dict(SeqIO.parse(args.input,args.format))
	shortseqids = link_dict_names(seqdict)

	outputbase = args.output if args.output else os.path.splitext(args.input)[0]
	outputfile = "{}.bridged.{}".format(outputbase, args.format)
	if args.gzip:
		print >> sys.stderr, "# Generating gzipped outputs", time.asctime()
		outputfile += ".gz"
		opentype = gzip.open
	else:
		opentype = open
	print >> sys.stderr, "# Writing joined reads to {}".format(outputfile), time.asctime()

	gapstring = 'N'*args.length
	quallist = [1]*args.length # list of ints for quality scores

	linecount = 0
	joincount = 0
	readsizes = []
	joinedsizes = []
	with opentype(outputfile, 'w') as of:
		print >> sys.stderr, "# Parsing scaffolds file {}".format(args.scaffolds), time.asctime()
		for line in open(args.scaffolds,'r').readlines():
			line = line.strip()
			if line:
				linecount += 1
				scafreads = line.split('\t')
				readsizes.extend([len(seqdict[shortseqids[readID]].seq) for readID in scafreads])
				joincount += len(scafreads)
				joinedseq = Seq(gapstring.join([str(seqdict[shortseqids[readID]].seq) for readID in scafreads]))
				joinedlen = len(joinedseq)
				joinedsizes.append(joinedlen)
				joinedid = "_".join(scafreads) + ":length={}".format(joinedlen)
				joined_read = SeqRecord(joinedseq, id=joinedid, description="")
				if args.format=="fastq": # add quality scores for fastq format
					joinedqual = []
					for i,readID in enumerate(scafreads):
						if i: # for second or later
							joinedqual.extend(quallist)
						joinedqual.extend(seqdict[shortseqids[readID]].letter_annotations.get("phred_quality"))
					joined_read.letter_annotations["phred_quality"] = joinedqual
				of.write(joined_read.format(args.format))
	print >> sys.stderr, "# Counted {} scaffoldable groups".format(linecount), time.asctime()
	print >> sys.stderr, "# Average read length {} bp".format(sum(readsizes)/len(readsizes)), time.asctime()
	print >> sys.stderr, "# Joined {} reads".format(joincount), time.asctime()
	print >> sys.stderr, "# Average joined length {} bp".format(sum(joinedsizes)/len(joinedsizes)), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
