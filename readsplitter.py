#! /usr/bin/env python
#
# readsplitter.py v1 2015-02-03

import sys
import argparse
import time
from itertools import izip

usage='''
READ FILTERING STEPS:

1) kmers must be pre-processed with kmersorter.py, and grep -v ">"
pre-processing to change all kmers to lowercase
  ( to avoid grepping high quality scores like GGG...)
and grep to remove fasta headers
kmersorter.py -a 380 -b 820 -l 31 -m fastq.dumps > filtered_kmers.fasta
grep -v ">" filtered_kmers.fasta > kmerset.txt

2) merge the reads to search paired end reads as pairs
readsplitter.py -m -f reads1.fq reads2.fq > mergedreads.txt

default delimiter is 'ooo' as this is unlikely to occur in either the base
sequence or in the quality score (ASCII value is 111, way over normal quality)

3) search for kmers in the reads with:
grep -F -f kmerset.txt -B 1 --no-group-separator mergedreads.txt > merged_with_kmer.txt

or for fastq reads with:
grep -F -f kmerset.txt -A 2 -B 1 --no-group-separator mergedreads.txt > merged_with_kmer.txt

4) split into reads again with:
readsplitter.py -s merged_with_kmer.txt

mergedstrings.txt is raw reads without header, separated by some letter
'''

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-f', '--files', nargs="*", default = '-', help="fasta or fastq reads")
	parser.add_argument('-m', '--merge', action="store_true", help="combine reads end to end")
	parser.add_argument('-s', '--split', help="split merged reads with this file")
	parser.add_argument('-d', '--delimiter', help="split at this character [default: ooo]", default="ooo")
	parser.add_argument('-t', '--type', help="line numbering by fasta or fastq [default: fastq]", default='fastq')
	args = parser.parse_args(argv)

	linecount = 0

	# declare variables for filtering by read type
	# this is entirely to modify the line containing the DNA sequence
	# for fastq files, this is the 2nd line per 4, so mod4 is 2
	# for fasta files, this is the 2nd line per 2, so mod2 is 0
	if args.type == "fastq":
		modval = 4
		keepval = 2
	elif args.type == "fasta":
		modval = 2
		keepval = 0
	# check for invalid types
	else:
		print >> sys.stderr, "Invalid sequence type '%s'" % args.type, time.asctime()
		sys.exit()

	# for merging two files
	if args.merge:
		print >> sys.stderr, "Converting %s sequences to lowercase" % args.type, time.asctime()
		if len(args.files) > 1:
			print >> sys.stderr, "Merging sequences with '%s'" % args.delimiter, time.asctime()
			for line1, line2 in izip( open(args.files[0],'r'), open(args.files[1],'r') ):
				linecount += 1
				# this actually does the counting, if keepval, change to lowercase
				if linecount % modval == keepval:
					outstring = "%s%s%s" % (line1.rstrip().lower(), args.delimiter, line2.rstrip().lower() )
				# otherwise just print the line as normal
				else:
					outstring = "%s%s%s" % (line1.rstrip(), args.delimiter, line2.rstrip() )
				print >> wayout, outstring
		else:
			print >> sys.stderr, "Using single input file", time.asctime()
			for line1 in open(args.files[0], 'r'):
				linecount += 1
				if linecount % modval == keepval:
					outstring = "%s" % (line1.rstrip().lower() )
				else:
					outstring = "%s" % (line1.rstrip() )
				print >> wayout, outstring
		print >> sys.stderr, "Wrote %d lines for %d sequences" % (linecount, linecount / modval), time.asctime()

	# for splitting of merged files
	elif args.split:
		print >> sys.stderr, "Converting %s sequences to uppercase" % args.type, time.asctime()
		print >> sys.stderr, "Splitting sequences at '%s'" % args.delimiter, time.asctime()
		# automatic renaming of files
		outfile1, outfile2 = "%s_1.%s" % (args.split, args.type), "%s_2.%s" % (args.split, args.type)
		with open(args.split) as rrps, open(outfile1, 'w') as of1, open(outfile2, 'w') as of2:
			for line in rrps:
				linecount += 1
				readpairs = line.rstrip().split(args.delimiter)
				# as above with merge, if keepval, then change back to uppercase
				# and write the split lines to each file respectively
				if linecount % modval == keepval:
					print >> of1, "%s" % (readpairs[0].upper())
					print >> of2, "%s" % (readpairs[1].upper())
				else:
					print >> of1, "%s" % (readpairs[0])
					print >> of2, "%s" % (readpairs[1])
		print >> sys.stderr, "Split %d lines for %d pairs" % (linecount, linecount / modval), time.asctime()

	# for a single input file with neither split or merge
	else:
		print >> sys.stderr, "Converting %s sequences to uppercase" % args.type, time.asctime()
		with open(args.files[0]) as rrps:
			for line in rrps:
				linecount += 1
				if linecount % modval == keepval:
					outstring = "%s" % (line.rstrip().upper() )
				else:
					outstring = "%s" % (line.rstrip() )
				print >> wayout, outstring
		print >> sys.stderr, "Generated %s for %d sequences" % (args.type, linecount / modval), time.asctime()

	# in all cases, check the line count is correct
	if linecount % modval:
		print >> sys.stderr, "WARNING: Line count %d is not multiple of %d" % (linecount, modval), time.asctime()

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
