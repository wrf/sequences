#!/usr/bin/env python
#
# check_microarray_genotypes.py v1 2017-07-19

'''
check_microarray_genotypes.py  v1 2017-07-19

check_microarray_genotypes.py

  RefSeq SNP IDs should be in a tabular format like:
#SNP	Chr.	Position	Nearest gene	Effect allele	Alternate allele	Effect allele frequency	OR	P
rs35749011a	1	155135036	GBA-SYT11	A	G	0.017	1.762	6.09*10-23
rs823118	1	205723572	RAB7L1-NUCKS1	T	C	0.559	1.126	1.36*10-13

  Genomic data should be in tabular format like:
# rsid	chromosome	position	genotype
rs12564807	1	734462	AA
'''

import sys
import argparse

def get_rs_list(rslist, allelecolumn):
	''' '''
	rs_to_disease_allele = {} # key is RefSeq ID, value is disease allele
	rs_to_pvalue = {}
	print >> sys.stderr, "Parsing loci from {}".format( rslist )
	for line in open(rslist,'rU'):
		line = line.strip()
		if line and line[0]!="#": # ignore blank and comment lines
			lsplits = line.split("\t")
			rsid = lsplits[0]
			diseaseallele = lsplits[allelecolumn]
			rs_to_disease_allele[rsid] = diseaseallele
			rs_to_pvalue[rsid] = (lsplits[11],lsplits[12])
	print >> sys.stderr, "Parsed {} loci from {}".format( len(rs_to_disease_allele), rslist )
	return rs_to_disease_allele, rs_to_pvalue

def read_genome_data(datafile, locidict, oddsdict, wayout):
	''' '''
	print >> sys.stderr, "Parsing genotypes from {}".format( datafile )
	print >> wayout, "RSID\tEffect allele\tGenotype\tOR\tP-value"
	for line in open(datafile,'r'):
		line = line.strip()
		if line and line[0]!="#": # ignore blank and comment lines
			lsplits = line.split("\t")
			rsid = lsplits[0]
			genotype = lsplits[3]
			diseaseallele = locidict.get(rsid, None)
			if not diseaseallele is None:
				print >> wayout, "{}\t{}\t{}\t{}\t{}".format( rsid, diseaseallele, genotype, *oddsdict.get(rsid,("None","None")) )

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--effect-allele-index', type=int, help="column index of effect allele")
	parser.add_argument('-g','--genome-data', nargs="*", help="microarray data or SNP calls")
	parser.add_argument('-r','--rslist', nargs="*", help="file name of RefSeq SNP IDs, see example")
	args = parser.parse_args(argv)

	locidict = {} # start with blank dict
	oddsdict = {}
	for rslist in args.rslist:
		newloci, newodds = get_rs_list(rslist, args.effect_allele_index)
		locidict.update( newloci )
		oddsdict.update( newodds )

	for genome in args.genome_data:
		read_genome_data(genome, locidict, oddsdict, wayout)

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
