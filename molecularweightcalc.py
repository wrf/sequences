#!/usr/bin/env python
#
# molecularweightcalc.py v1 2017-01-04
# v1.2 2022-01-28
# v1.3 allow gzipped input 2023-10-07
# v1.31 filetype changed to r instead of rU 2024-12-09

'''
molecularweightcalc.py v1.31 2024-12-09

molecularweightcalc.py proteins.fasta

    output is tab separated as:

PROTEIN   MW   PI

    such as:

LUCI_LUCMI	60476.86	6.160
'''

import sys
import argparse
import gzip
from numpy import arange
from collections import Counter
from Bio import SeqIO

FreeAminoDict={'X':0.0, '-':0.0, '*':0.0,
'A':89.09, 'R':174.20, 'N':132.12, 'D':133.10,
'C':121.15, 'Q':146.15, 'E':147.13, 'G':75.07,
'H':155.16, 'I':131.17, 'L':131.17, 'K':146.19,
'M':149.21, 'F':165.19, 'P':115.13, 'S':105.09,
'T':119.12, 'W':204.23, 'Y':181.19, 'V':117.15 }

# values taken from https://web.expasy.org/findmod/findmod_masses.html#AA
# * includes the H of the N terminus and OH of C terminus
AminoDict = {'X':0.0, '-':0.0, '*':18.0,
'A':71.0788, 'R':156.1875, 'N':114.1038, 'D':115.0886,
'C':103.1388, 'Q':128.1307, 'E':129.1155, 'G':57.0519,
'H':137.1411, 'I':113.1594, 'L':113.1594, 'K':128.1741,
'M':131.1926, 'F':147.1766, 'P':97.1167, 'S':87.0782,
'T':101.1051, 'W':186.2132, 'Y':163.1760, 'V':99.1326, 'U':150.0388 , 'O':237.3018 }
# also includes Selenocysteine (U) and Pyrrolysine (O)

# values taken from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5075173
# IPC protein set
PKADict = {'D':3.872, 'E':4.412, 'C':7.555, 'H':5.637, 'K':9.052, 'R':11.84, 'Y':10.85, "Cterm":2.869, "Nterm":9.094}
# IPC peptide set
PKADictIPCpep = {'D':3.887, 'E':4.317, 'C':8.297, 'H':6.018, 'K':10.517, 'R':12.503, 'Y':10.071, "Cterm":2.383, "Nterm":9.564}
# EMBOSS values
PKADictEMBOSS = {'D':3.9, 'E':4.1, 'C':8.5, 'H':6.5, 'K':10.8, 'R':12.5, 'Y':10.1, "Cterm":3.6, "Nterm":8.6}
AminoCharge = {'D':-1, 'E':-1, 'C':-1, 'H':1, 'K':1, 'R':1, 'Y':-1, "Cterm":-1, "Nterm":1}

def charge_ratio(AA, pH):
	'''return ratio of deprotonated over protonated'''
	# when pH = pKa, ratio should be 1 / (1 + 10^0), 1 / (1+1), 1 / 2
	a_over_ha = 1 / (1 + 10**(pH - PKADict[AA]))
	if AminoCharge[AA]==1: # deprotonated for neutral charge at high pH
		return a_over_ha
	else: # deprotonated for negative charge at high pH
		return 1 - a_over_ha

def get_charge(aacounts, pH):
	'''calculate overall charge of a protein at a given pH'''
	chargesum = charge_ratio("Nterm", pH) + charge_ratio("Cterm", pH) # default include values for N and C term
	for AA in PKADict.keys():
		chargesum += aacounts.get(AA, 0.0) * charge_ratio(AA, pH) * AminoCharge[AA]
	return chargesum

def calculate_pi(aacounts, stepsize=0.01):
	'''iterate through pH values and test isoelectric point'''
	for pH in arange(2.0, 14.0, stepsize):
		protein_charge = get_charge(aacounts, pH)
		if protein_charge <= 0.0:
			return pH
	# if no pI can be calculated, return 14
	return 14.0

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type=argparse.FileType('r'), default = '-', help="fasta format file")
	parser.add_argument('-a', '--above', type=float, metavar='N', default=0.0, help="only print sequences lighter than N")
	parser.add_argument('-b', '--below', type=float, metavar='N', default=1000000000.0, help="only print sequences heavier than N")
	args = parser.parse_args(argv)

	if args.input_file.name.rsplit('.',1)[-1]=="gz":
		input_handler = gzip.open(args.input_file.name,'rt')
		sys.stderr.write( "# Calculating weight of proteins from {} as gzipped\n".format(args.input_file.name) )
	else:
		input_handler = args.input_file
		sys.stderr.write( "# Calculating weight of proteins from {}\n".format(args.input_file.name) )
	for seqrec in SeqIO.parse(input_handler,"fasta"):
		molweight = 0
		aacounts = Counter(seqrec.seq)
		for AA in aacounts.keys():
			molweight += aacounts[AA] * AminoDict.get(AA,0.0)
		proteinpi = calculate_pi(aacounts)
		if args.below >= molweight >= args.above:
			try:
				sys.stdout.write( "{}\t{:.2f}\t{:.3f}\n".format(seqrec.id, molweight, proteinpi) )
			except ValueError:
				sys.stderr.write( "WARNING: CANNOT CALCULATE PI FOR {}\n".format( seqrec.id ) )

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
