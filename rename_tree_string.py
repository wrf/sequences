#!/usr/bin/env python

'''
rename_tree_string.py renamed_vector.tab mytree.tree > mytree_renamed.tree

renamed_vector.tab is any text in 2 columns per line as:
old_name    New_name
'''

import sys

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	renamedict = {} # key is first column old ID, value is new ID
	print >> sys.stderr, "# Reading rename vector from {}".format( sys.argv[1] )
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		old_name, new_name = line.split("\t")
		renamedict[old_name] = new_name
	print >> sys.stderr, "# Found {} name changes".format( len(renamedict) )

	changecounter = 0
	print >> sys.stderr, "# Reading tree file {}".format( sys.argv[2] )

	treestring = open(sys.argv[2],'r').read()

	for k,v in renamedict.iteritems():
		changecounter += 1
		treestring = treestring.replace(k,v)

	sys.stdout.write(treestring)

	print >> sys.stderr, "# Changed {} IDs".format( changecounter )
