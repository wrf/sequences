#!/usr/bin/env python
#
# compareblastxhits.py v1.0 created 2015-06-09

'''compareblastxhits.py v1.0 last modified 2015-06-09
  script for comparison of blast results between two databases
  such as isoform-containing and gene-only

  # EXAMPLE USAGE
compareblastxhits.py -g tethya-0_1.fa -t stringtie.gtf -b stringtie.blastx.aqu2iso.tab stringtie.blastx.aqu2.tab
'''

import sys
import os
import time
import argparse
from itertools import chain
from collections import defaultdict,namedtuple
from Bio import SeqIO

def get_exclusion_bool(contig, exclusion_dict):
	return exclusion_dict.get(contig, False)

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-g','--genome', nargs='*', help="fasta format genomic contigs [avg coverage file]")
	parser.add_argument('-E','--exclude', help="file of list of bad contigs")
	parser.add_argument('-b','--blast', nargs='*', help="tabular blastx output files")
	parser.add_argument('-t','--transcriptome', help="assembled transcriptomes as gtf")
	parser.add_argument('-d','--delimiter', help="delimiter scheme, default: '._' ", default="._")
	args = parser.parse_args(argv)

	startclock=time.asctime()
	starttime= time.time()

	print >> sys.stderr, "# Parsing genomic contigs {}".format(args.genome[0]), time.asctime()
	genomic_contigs_seq_d = SeqIO.to_dict(SeqIO.parse(args.genome[0],'fasta') )
	genomic_contig_len_d = {}
	genomic_contig_annotations = defaultdict(list)
	for seqid, seqrec in genomic_contigs_seq_d.iteritems():
		genomic_contig_len_d[seqid] = len(str(seqrec.seq))
	print >> sys.stderr, "# Found {} contigs".format(len(genomic_contigs_seq_d) ), time.asctime()

	if args.exclude:
		print >> sys.stderr, "# Reading exclusion list {}".format(args.exclude), time.asctime()
		exclusion_type_d = {}
		for term in open(args.exclude,'r'):
			exclusion_type_d[term.rstrip()] = True
		print >> sys.stderr, "# Found {} contigs to exclude".format(len(exclusion_type_d) ), time.asctime()

	genomic_contig_transcripts = defaultdict(list)
	if args.transcriptome:
		print >> sys.stderr, "# Parsing transcriptome gtf {}".format(args.transcriptome), time.asctime()
		#stringtie_headers = ["StringTie Transcript", "Coverage", "FPKM", "StartPos", "Strand", "Exons", "Length"]
		contigs_by_transcript_d = {}
		trans_by_gene = defaultdict(list)
		for line in open(args.transcriptome,'r'):
			line = line.rstrip()
			lsplits = line.split("\t")
			if lsplits[2] == "transcript":
				attrsplits = lsplits[8].split(";")
				transcript_id = attrsplits[1].strip().split(" ")[1].replace('"','')
				contigs_by_transcript_d[transcript_id] = lsplits[0]
				genomic_contig_transcripts[lsplits[0]].append(transcript_id)
		if args.blast:
			trans_blastx_list = []
			db_prots_hit_by_d = defaultdict(list)
			for blasttabfile in args.blast:
				print >> sys.stderr, "# Parsing tabular blastx output {}".format(blasttabfile), time.asctime()
				blastx_headers = ["Blastx subject", "Evalue", "Bitscore", "Subj cov", "Pid"]
				trans_blast_dict = {}
				for line in open(blasttabfile, 'r'):
					line = line.rstrip()
					lsplits = line.split("\t")
		# qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore
					queryseq = lsplits[0]
					if not queryseq in trans_blast_dict:
						#blastx_list = [lsplits[1], lsplits[10], lsplits[11], sbjcov, lsplits[2] ]
						trans_blast_dict[queryseq] = lsplits[1]
						db_prots_hit_by_d[lsplits[1]].append(queryseq)
				print >> sys.stderr, "# Found blast hits for {} sequences".format(len(trans_blast_dict)), time.asctime()
				trans_blastx_list.append(trans_blast_dict)
		# after all options are checked, append the final headers to the main list
	else:
		print >> sys.stderr, "# No transcriptome provided with -t", time.asctime()

	genecountsd = {"samegenecount":0, "sameisocount":0, "differenthits":0, "onenullhit":0, "nohits":0}
	fscountsd = {"possiblefusion":0, "possiblesplit":0}

	for k in sorted(genomic_contigs_seq_d.keys(), key=lambda x: int(x.split("_")[1]) ):
		# skip all contigs that are True for exclude
		if args.exclude:
			if get_exclusion_bool(k, exclusion_type_d):
				continue
		if len(genomic_contig_transcripts[k]) > 0:
			for trs in sorted(genomic_contig_transcripts[k], key=lambda x: int(x.split(".")[1]) ):
				# check blast hit 1 vs blast hit 2
				bhits = [bd.get(trs,"None") for bd in trans_blastx_list]

				# check for any None hits, one or both
				if any(bh=="None" for bh in bhits):
					if all(bh=="None" for bh in bhits):
						genecountsd["nohits"] += 1
					else:
						genecountsd["onenullhit"] += 1
				else:
					if len(bhits)>1:
						if len(set(bhits))==1:
							genecountsd["sameisocount"] += 1
							# if sequences are in format of Aqu2.16905_001, for organism.gene_isoform
							# this should split at the _ to give the organism.gene
						elif len(set([n.split(args.delimiter[1])[0] for n in bhits]))==1:
							genecountsd["samegenecount"] += 1
						else:
							genecountsd["differenthits"] += 1

	print >> sys.stderr, "# {} transcripts in gtf".format(len(contigs_by_transcript_d))
	print >> sys.stderr, "# {} sequences with no hits".format(genecountsd["nohits"])
	print >> sys.stderr, "# {} sequences with at least one hit (but at least one None)".format(genecountsd["onenullhit"])
	print >> sys.stderr, "# {} sequences where all hit the same isoform".format(genecountsd["sameisocount"])
	print >> sys.stderr, "# {} sequences where all hit the same gene but different isoforms".format(genecountsd["samegenecount"])
	print >> sys.stderr, "# {} sequences which hit different genes".format(genecountsd["differenthits"])
	print >> sys.stderr, "# {} sequences are accounted".format(sum(v for v in genecountsd.values()))

	print >> sys.stderr, "# Processed completed in %.1f minutes" % ((time.time()-starttime)/60)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
