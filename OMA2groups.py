#!/usr/bin/env python
#
# rawOMA2groups.py v1 2017-01-09

'''
OMA2groups.py v1.0  last modified 2017-01-09

    usage:
OMA2groups.py -f oma-seqs.fa -g metazoa-hogs.tsv -s oma-species.txt

'''

import sys
import os
import argparse
import time
import re
from collections import defaultdict
from Bio import SeqIO

def parse_ogs_to_genes(ogsgenesfile, outdirectory, verbose=False):
	'''read in tabular information and return dict where key is gene ID and value is ortholog number'''
	# 000003.1	Metazoa	CAPTE31325
	ogtogenesdict = {} # keys are protein IDs, values are ortholog numbers
	ogdict = {}
	linecounter = 0
	doublecounts = 0
	print >> sys.stderr, "# Reading orthologs-genes list from {}".format(ogsgenesfile), time.asctime()
	for line in open(ogsgenesfile,'r'):
		line = line.rstrip()
		if line and not line[0]=="#": # remove empty and comment lines
			linecounter += 1
			lsplits = line.split("\t")
			ognumber = lsplits[0]
			proteinid = lsplits[2]
			if ogtogenesdict.get(proteinid, False) and verbose:
				doublecounts += 1
				print >> sys.stderr, "# WARNING: {} overwrites existing value {} for {}".format(ognumber, ogtogenesdict.get(proteinid), proteinid)
			ogtogenesdict[proteinid] = ognumber
			ogdict[ognumber] = "{}.fasta".format(os.path.join(outdirectory,ognumber.replace(".","_")))
	print >> sys.stderr, "# Counted {} lines and kept ortholog IDs for {} genes".format(linecounter, len(ogtogenesdict)), time.asctime()
	if doublecounts:
		print >> sys.stderr, "# {} proteins are in multiple OGs".format(doublecounts), time.asctime()
	return ogtogenesdict, ogdict

def parse_species(speciesfile):
	'''read in tabular information and return dict where key is ID number and value is species name'''
	# Note: OMA species codes are whenever possible identical to UniProt codes.
	# Format: OMA code<tab>Taxon ID<tab>Scientific name<tab>Genome source<tab>Version/Release
	speciesdict = {} # keys are numbers, values are formatted species names
	linecounter = 0
	print >> sys.stderr, "# Reading species list from {}".format(speciesfile), time.asctime()
	for line in open(speciesfile,'r'):
		line = line.rstrip()
		if line and not line[0]=="#": # remove empty and comment lines
			linecounter += 1
			lsplits = line.split("\t")
			taxacode = lsplits[0] # 5-letter Uniprot code
			taxid = lsplits[1] # NCBI numerical ID
			rawspecies = lsplits[2]
			resp = re.search("(\w+) (\w+)", rawspecies)
			try:
				formatspecies = "{}_{}".format(resp.group(1), resp.group(2))
			except AttributeError: # for weird ones where it does not match
				formatspecies = rawspecies.replace(" ","_").replace("(","").replace(")","")
			speciesdict[taxacode] = formatspecies
	print >> sys.stderr, "# Counted {} lines and kept {} species".format(linecounter, len(speciesdict)), time.asctime()
	return speciesdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-a','--annotations', help="ortholog annotations tsv file")
	parser.add_argument('-d','--directory', help="directory for ortholog FASTA files ['hogs/']", default="hogs/")
	parser.add_argument('-f','--fasta', nargs="*", help="fasta reference files")
	parser.add_argument('-g','--ortholog-genes', help="ortholog-to-genes tabular file")
	parser.add_argument('-G','--output-orthologs', action="store_true", help="output ortholog-to-genes information for kept species to stdout")
	parser.add_argument('-o','--orthologs', help="ortholog names tabular file")
	parser.add_argument('-s','--species-list', help="OG species list, for replacing numbers with species")
	parser.add_argument('-t','--taxa', default="33208", help="taxa levels [33208]")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	startclock=time.asctime()
	starttime= time.time()

	speciesnamedict = parse_species(args.species_list) if args.species_list else {}
	ogtogenedict, ogdict = parse_ogs_to_genes(args.ortholog_genes, args.directory)

	if os.path.isdir(args.directory):
		print >> sys.stderr, "# Using existing directory {}".format(args.directory), time.asctime()
	else:
		print >> sys.stderr, "# Making directory {}".format(args.directory), time.asctime()
		os.mkdir(args.directory)

	protcounts = defaultdict(int)
	goodcounts = defaultdict(int)
	badcounts = defaultdict(int)
	for speciesprots in args.fasta:
		print >> sys.stderr, "# Reading prots from {}".format(speciesprots), time.asctime()
		for seqrec in SeqIO.parse(speciesprots,"fasta"):
			speciesid = seqrec.id.strip()[0:5]
			protcounts[speciesid] += 1
			ogID = ogtogenedict.get(seqrec.id.strip(), None)
			if ogID:
				goodcounts[speciesid] += 1
				with open(ogdict[ogID],'a') as og:
					og.write(seqrec.format("fasta"))
			else:
				badcounts[speciesid] += 1
				if args.verbose:
					print >> sys.stderr, "# WARNING: cannot find OG for {}".format(seqrec.id)
	print >> sys.stderr, "# Process completed in {:.1f} minutes".format((time.time()-starttime)/60)
	print >> sys.stderr, "# Sorted {} total proteins for {} species".format(sum(protcounts.values()),len(args.fasta)), time.asctime()
	print >> sys.stderr, "# Found OGs for {} proteins".format(sum(goodcounts.values()), time.asctime()
	print >> sys.stderr, "# Count not find OGs for {} proteins".format(sum(badcounts.values())), time.asctime()
	print >> sys.stderr, "NCBI-ID\tSPECIES\tTOTAL\tWITH-OG\tNO-OG\tPERCENT"
	for k,v in sorted(badcounts.items(), key=lambda x: x[1], reverse=True):
		if goodcounts[k]:
			print >> sys.stderr, "{}\t{}\t{}\t{}\t{}\t{}".format(k, speciesnamedict.get(k,"None"), protcounts[k], goodcounts[k], v, v*100.0/protcounts[k])

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
