#!/usr/bin/env python
#
# blast2phylumpie.py v1.0 created 2016-02-12

'''
blast2phylumpie.py  last modified 2016-02-16

    EXAMPLE USAGE:
blast2phylumpie.py -p phylum_table.tab -b sample1_blastx.tab

    for simplicity, all metazoan phyla can be merged into Metazoa with -m

    generate phylum_table.tab using uniprotphylumtable.py
    phylum_table.tab expected in format:
SWISSPROTSPECIES    PHYLUM
    such as:
MUNMN	Chordata
'''

import sys
import time
import argparse
from collections import defaultdict

replacements = {
"AGRT5":"AGRFC", # Agrobacterium tumefaciens (C58) to Agrobacterium fabrum (C58 / ATCC 33970)
"ALBFT":"RHOFT", # Albidiferax ferrireducens to Rhodoferax ferrireducens
"BACA2":"BACMF", # Bacillus amyloliquefaciens to Bacillus methylotrophicus (DSM 23117 / BGSC 10A6 / FZB42)
"BACMS":"HALMS", # Bacteriovorax marinus to Halobacteriovorax marinus (ATCC BAA-682 / DSM 15412 / SJ)
"BORGA":"BORBP", #  to Borrelia bavariensis (strain ATCC BAA-2496 / DSM 23469 / PBi)
"BURS3":"BURL3", #  to Burkholderia lata (ATCC 17760 / LMG 22485 / NCIMB 9086 / R18194 / 383)
"CANFA":"CANLF", # Canis familiaris to Canis lupus familiaris (Dog)
"CROTZ":"SICTZ", # Cronobacter turicensis to Siccibacter turicensis (DSM 18703 / LMG 23827 / z3032)
"HISS1":"HAES1", # Histophilus somni to Haemophilus somnus (129Pt)
"MAGSM":"MAGMM", #  to Magnetococcus marinus (ATCC BAA-1437 / JCM 17883 / MC-1)
"MARAV":"MARHV", #  to Marinobacter hydrocarbonoclasticus (ATCC 700491 / DSM 11845 / VT8)
"PASHA":"MANHA", # Pasteurella haemolytica to Mannheimia haemolytica
"PELLD":"CHLL7", # Pelodictyon luteolum to Chlorobium luteolum (DSM 273 / 2530)
"PENMQ":"TALMQ", # Penicillium marneffei to Talaromyces marneffei (ATCC 18224 / CBS 334.59 / QM 7333)
"PROVI":"CHLPM", # Prosthecochloris vibrioformis (DSM 265) to Chlorobium phaeovibrioides (DSM 265 / 1930)
"PSESB":"PSEKB", #  to Pseudomonas knackmussii (DSM 6978 / LMG 23759 / B13)
"RALME":"CUPMC", # Ralstonia metallidurans to Cupriavidus metallidurans
"RHOSR":"RHOJR", #  to Rhodococcus jostii (RHA1)
"SPHAR":"NOVAR", # Sphingomonas aromaticivorans to Novosphingobium aromaticivorans
"THELT":"PSELT", # Thermotoga lettingae to Pseudothermotoga lettingae (ATCC BAA-301)
"THETN":"CALS4", # Thermoanaerobacter tengcongensis to Caldanaerobacter subterraneus subsp. tengcongensis
"UNCMA":"METAR", #  to Methanocella arvoryzae (DSM 22066 / NBRC 105507 / MRE50)
"VIBMA":"MORMI", # Vibrio marinus to Moritella marina
"VIBSL":"VIBTL", # Vibrio splendidus (Mel32) to Vibrio tasmaniensis (LGP32)
}

metazoanlist = ["Chordata", "Porifera", "Ctenophora", "Cnidaria", "Ecdysozoa", "Lophotrochozoa", "Hemichordata", "Echinodermata", "Chaetognatha", "Xenacoelomorpha", "Platyhelminthes", "Placozoa"]

def read_phylum_table(phylumfile, verbose):
	phylumdict = {}
	print >> sys.stderr, "# Parsing phyla from {}".format(phylumfile), time.asctime()
	for line in open(phylumfile,'r').readlines():
		lsplits = line.strip().split("\t")
		phylumdict[lsplits[0]] = lsplits[1]
	print >> sys.stderr, "# Final table contains {} entries for {} phyla".format( len(phylumdict), len(set(phylumdict.values() ) ) ), time.asctime()
	if verbose:
		print >> sys.stderr, set( phylumdict.values() )
	return phylumdict

def parse_blast_results(blastfile, phylumdict, evaluecutoff, mergemetazoa):
	phylumcounter = defaultdict(int)
	print >> sys.stderr, "# Parsing blast results from {}".format(blastfile), time.asctime()
	for line in open(blastfile, 'r').readlines():
		lsplits = line.strip().split("\t")
		evalue = float(lsplits[10])
		if evalue > evaluecutoff: # skip high evalue matches, and add to Filtered
			phylumcounter["Filtered"] += 1
			continue
		subjectid = lsplits[1]
		if subjectid[0:2]=="sp": # for swissprot IDs
			seqid = subjectid.split("|")[2]
			speciesid = seqid.split("_")[1]
			try:
				phylum = phylumdict[speciesid]
			except KeyError: # for when species is not in the phylum table
				if replacements.get(speciesid, False):
					phylum = phylumdict[replacements.get(speciesid)]
				else: # if no match can be found, use phylum None
					phylum = "None"
					print >> sys.stderr, "WARNING: phylum {} not found for {}, using {}".format(speciesid, seqid, phylum)
			if mergemetazoa and phylum in metazoanlist:
				phylum = "Metazoa"
			phylumcounter[phylum] += 1
		elif subjectid[0:2]=="gi": # for genbank IDs
			phylumcounter["GenBank"] += 1
		else: # for any other ID style
			phylumcounter["Other"] += 1
	print >> sys.stderr, "# Found {} blast results".format( sum(phylumcounter.values()) ), time.asctime()
	for k,v in sorted( phylumcounter.items(), key=lambda x: x[1], reverse=True):
		print >> sys.stdout, "{}\t{}".format(k, v)
	# NO RETURN

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--blast', help="blastx results as output format 6 or 7")
	parser.add_argument('-e','--evalue', type=float, default=1e-4, help="evalue cutoff for blast filtering [1e-4]")
	parser.add_argument('-m','--metazoan', action="store_true", help="combine counts for metazoan phyla")
	parser.add_argument('-p','--phylum', help="phylum table text file")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose output")
	args = parser.parse_args(argv)

	phylumdict = read_phylum_table(args.phylum, args.verbose)

	parse_blast_results(args.blast, phylumdict, args.evalue, args.metazoan)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
