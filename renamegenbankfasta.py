#!/usr/bin/env python
#
# renamegenbankfasta.py created 2015-09-25

'''renamegenbankfasta.py last updated 2021-12-22

USAGE:  input is a single fasta file, can be .gz

renamegenbankfasta.py bacterial_rRNA.fasta > bacterial_rRNA_renamed.fasta

    RENAMES FASTA HEADER:
>gi|873224441|emb|CEM26286.1| unnamed protein product [Vitrella brassicaformis CCMP3155]
    INTO:
>Vitrella_brassicaformis_CEM26286.1

    FOR ENTRIES FORMATTED AS:
>gi|723007519|gb|KM386371.1| Uncultured Clostridiales bacterium clone BNVV4 16S ribosomal RNA, partial sequence
    USE -g TO TAKE GENE NAME INSTEAD OF [SPECIES]
>Uncultured_Clostridiales_KM386371.1
'''

import sys
import re
import gzip

if len(sys.argv)<2:
	sys.exit(__doc__)
else:
	# check for flag to take gene names instead of [Genus species]
	if len(sys.argv)>2 and sys.argv[2]=="-g":
		genenames = True
	else:
		genenames = False

	# check for gzip
	fastafilename = sys.argv[1]
	if fastafilename.rsplit('.',1)[-1]=="gz": # autodetect gzip format
		opentype = gzip.open
		sys.stderr.write("# Opening {} as gzipped\n".format(fastafilename))
	else: # otherwise assume normal open for fasta format
		opentype = open
		sys.stderr.write("# Opening {}\n".format(fastafilename))

	# parse as text, only changes occur on header lines
	for line in opentype(fastafilename,'r'):
		if line[0]==">":
			line = line.split(">")[1] # should always be first item after >
			try:
				accession = line.split("|")[3]
			except IndexError: # probably not genbank sequence, so take all ID info
				accession = line.split(" ")[0]
			if genenames:
				genusspecies = line.split(' ')[1:3]
			else:
				try:
					genusspecies = re.search("\[(.+)\]", line).group(1).replace("Candidatus ","").split(" ")[0:2]
				except AttributeError: # when re search fails
					genusspecies = line.split(' ')[1:3]
			try:
				outline = ">{1}_{2}|{0}\n".format(accession, *genusspecies)
			except IndexError: # if mixed genbank and non genbank entries
				outline = ">{}\n".format(accession)
			outline = outline.replace("sp.","sp") # replace empty species names
			for repitem in ["(", ")", "[", "]", ":"]: # : from PREDICTED:_something
				outline = outline.replace(repitem,"") # these seem to cause trouble downstream
			sys.stdout.write( outline )
		else: # write all other lines as normal
			sys.stdout.write(line)

