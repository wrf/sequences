#!/usr/bin/env python
#
# comparevcfsnps.py v1.0 2016-01-12

'''comparevcfsnps.py v1.0 last modified 2016-01-12

comparevcfsnps.py -i reference_genome.vcf sample1.vcf sample2.vcf

    OR USE -r TO SPECIFY A REFERNECE
comparevcfsnps.py -r reference_genome.vcf -i samples/*.vcf
'''

import sys
import argparse
import time
from collections import defaultdict

def get_reference_snps(referencevcf, excludedict, qualcut, aocut, dpcut):
	'''returns dict where keys are contigs and values are dict where keys are positions and values are alternative bases'''
	vcfsnps = defaultdict(dict)
	featuretotal = 0
	snptotal = 0
	excludesnps = 0
	lowqualsnps = 0
	keptsnps = 0
	print >> sys.stderr, "# Parsing reference SNPs from {}".format(referencevcf), time.asctime()
	for line in open(referencevcf,'r').readlines():
		line = line.strip()
		if line and not line[0]=="#": # ignore empty and comment lines
			featuretotal += 1
			lsplits = line.split("\t")
			#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	unknown
			# contig1234	54321	.	A	G,T	150	.
			info = dict([(field.split("=")) for field in lsplits[7].split(";")]) # make fast dict of INFO
			if info["TYPE"]!="snp": # currently ignore anything that is not a SNP
				continue
			scaffold = lsplits[0]
			snptotal += 1
			if excludedict and excludedict.get(scaffold, False):
				excludesnps += 1
				continue
			position = int(lsplits[1])
		#	refbase = lsplits[3]
			altbase = lsplits[4]
			qualscore = float(lsplits[5])
			if qualscore <= qualcut:
				lowqualsnps += 1
				continue
			if aocut and int(info["AO"]) < aocut:
				lowqualsnps += 1
				continue
			if dpcut and int(info["DP"]) < dpcut:
				lowqualsnps += 1
				continue
			keptsnps += 1
			vcfsnps[scaffold][position] = altbase
	print >> sys.stderr, "# Found {} total variant features".format(featuretotal)
	print >> sys.stderr, "# Found {} total SNPs".format(snptotal)
	print >> sys.stderr, "# Discarded {} SNPs on excluded contigs".format(excludesnps)
	print >> sys.stderr, "# Discarded {} SNPs for low quality".format(lowqualsnps)
	print >> sys.stderr, "# Kept {} total SNPs".format(keptsnps)
	return vcfsnps

def compare_to_reference(referencesnps, testvcf, excludedict, qualcut, aocut, dpcut):
	featuretotal = 0
	snptotal = 0
	excludesnps = 0
	lowqualsnps = 0
	samepossnps = 0 # same position, regardless of letter
	samebasesnps = 0 # same position, same letter
	diffbasesnps = 0 # same position, different letter
	newposition = 0 # position not covered by reference
	print >> sys.stderr, "# Parsing comparison SNPs from {}".format(testvcf), time.asctime()
	for line in open(testvcf,'r').readlines():
		line = line.strip()
		if line and not line[0]=="#": # ignore empty and comment lines
			featuretotal += 1
			lsplits = line.split("\t")
			info = dict([(field.split("=")) for field in lsplits[7].split(";")]) # make fast dict of INFO
			if info["TYPE"]!="snp": # currently ignore anything that is not a SNP
				continue
			scaffold = lsplits[0]
			snptotal += 1
			if excludedict and excludedict.get(scaffold, False):
				excludesnps += 1
				continue
			position = int(lsplits[1])
			altbase = lsplits[4]
			qualscore = float(lsplits[5])
			if qualscore <= qualcut:
				lowqualsnps += 1
				continue
			if aocut and int(info["AO"]) < aocut:
				lowqualsnps += 1
				continue
			if dpcut and int(info["DP"]) < dpcut:
				lowqualsnps += 1
				continue
			if info["AO"]==info["DP"]: # if all reads mapping to a base are SNPs
				lowqualsnps += 1
				continue
			refvalue = referencesnps[scaffold].get(position, None)
			if refvalue:
				samepossnps += 1
				if refvalue==altbase:
					samebasesnps += 1
				else:
					diffbasesnps += 1
			else:
				newposition += 1
	print >> sys.stderr, "# Found {} total variant features in {}".format(featuretotal,testvcf)
	print >> sys.stderr, "# Found {} total SNPs".format(snptotal)
	print >> sys.stderr, "# Discarded {} SNPs on excluded contigs".format(excludesnps)
	print >> sys.stderr, "# Discarded {} SNPs for low quality".format(lowqualsnps)
	snpsum = (samebasesnps + diffbasesnps + newposition)/100.0
	print >> sys.stderr, "Same position, same letter: {}, {:.2f}%".format(samebasesnps, samebasesnps/snpsum)
	print >> sys.stderr, "Same position, different letter: {}, {:.2f}%".format(diffbasesnps, diffbasesnps/snpsum)
	print >> sys.stderr, "SNPs at new positions: {}, {:.2f}%".format(newposition, newposition/snpsum)

def get_excluded_contigs(exclusionlist):
	if exclusionlist:
		print >> sys.stderr, "# Reading exclusion list {}".format(exclusionlist), time.asctime()
		exclusiondict = {}
		for term in open(exclusionlist,'r'):
			term = term.strip()
			if term[0] == ">":
				term = term[1:]
			exclusiondict[term] = True
		print >> sys.stderr, "# Found {} contigs to exclude".format(len(exclusiondict) ), time.asctime()
	else:
		exclusiondict = None

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', nargs='*', help="vcf files to compare to reference", required=True)
	parser.add_argument('-r','--reference', help="reference vcf of SNPs")
	parser.add_argument('-E','--exclude', help="file of list of contigs to exclude")
	parser.add_argument('-a','--AO', type=int, help="minimum AO value - occurrence of SNP")
	parser.add_argument('-d','--DP', type=int, help="minimum DP value - total reads mapped at that locus")
	parser.add_argument('-q','--qual', type=float, default=0.0, help="QUAL value must be greater than [0.0]")
	args = parser.parse_args(argv)

	exclusiondict = get_excluded_contigs(args.exclude)
	vcflist = args.input

	if args.reference:
		referencevcfdict = get_reference_snps(args.reference, exclusiondict, args.qual, args.AO, args.DP)
	else:
		referencevcfdict = get_reference_snps(vcflist[0], exclusiondict, args.qual, args.AO, args.DP)
		vcflist.pop(0)

	for vcffile in vcflist:
		compare_to_reference(referencevcfdict, vcffile, exclusiondict, args.qual, args.AO, args.DP)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
