#! /usr/bin/env python
#
# v1.7 filetype changed to rt instead of rU 2024-12-09
# v1.6 add feature for starting motif requirement, for leader sequence reversal 2022-10-19
# maybe python3 update
# v1.5 forked with translator to simplify functions 25/may/2012
# v1.4 corrected minor formatting errors 2/apr/2012
# v1.3 converted to argparse, added functions 9/feb/2012
# v1.2 added 6-frame translation 7/feb/2012
# v1.1 added translation tool 3/feb/2012
# v1.0 simple reverse complementer of a fasta file 4/jan/2012

'''revcomp.py  last modified 2024-12-09

    revcomp.py sequences.fasta > rc_sequences.fasta

  to only reverse those with a particular starting motif

    revcomp.py -r -s CAAA leader_seqs.fasta > leader_seqs.rc.fasta


'''

import sys
import time
import argparse
from Bio.Seq import Seq
from Bio import SeqIO

def main(argv, wayout):
	#print argv
	if not len(argv):
		argv.append('-h')

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('r'), default = '-', help="fasta format file")
	parser.add_argument('-r', '--rename', action='store_true', help="rename sequences with _rc")
	parser.add_argument('-s', '--starting-motif', help="only reverse those with starting motif, e.g '-s ATCG' ")
	args = parser.parse_args(argv)
	
	input_file = args.input_file

	counter = 0
	base_sum = 0
	nullcount = 0

	starttime= time.time()
	
	for seq_record in SeqIO.parse(input_file, "fasta"):
		counter += 1
		base_sum += len(seq_record.seq)

		if args.starting_motif is None or seq_record.seq.find(args.starting_motif) == 0: # meaning sequence begins with motif
			seq_record.seq = seq_record.seq.reverse_complement()

			if args.rename:
				seq_record.id = seq_record.id + "_rc"
				seq_record.description = ""

		wayout.write(seq_record.format("fasta"))

	print("Counted {} records in {:.1f} minutes".format(counter, (time.time()-starttime)/60), file=sys.stderr )
	print("Total counted bases: {}".format(base_sum), file=sys.stderr )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

