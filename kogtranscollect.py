#!/usr/bin/env python
#
# v1.0 - take kog results and full seqs files and compile all positive matches into one file 19/sep/2012
# for use with fpaligner.py and kogcollector.py

import sys
import argparse
import subprocess
import os
from Bio import SeqIO
from Bio.Seq import Seq

def getSixFrame(seq_record,code):
	dna_seq = seq_record.seq
	prot_seqs = []
	prot_seqs.append(dna_seq[0:].translate(table=code))
	prot_seqs.append(dna_seq[1:].translate(table=code))
	prot_seqs.append(dna_seq[2:].translate(table=code))
	prot_seqs.append(dna_seq.reverse_complement()[0:].translate(table=code))
	prot_seqs.append(dna_seq.reverse_complement()[1:].translate(table=code))
	prot_seqs.append(dna_seq.reverse_complement()[2:].translate(table=code))
	return prot_seqs

"""
kogtranscollect.py -n all_kog_nucl.fasta -f fasta1.faa fasta2.faa -o allresults.fa

typically in format of:
-n kraken_all_kog_nucleotides.fasta
-f kraken_h01_transcripts_kog_seqs.faa
-o kraken_all_kog_proteins.fasta
"""

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-n','--nucleotides', help="all kog nucleotides", required=True)
	parser.add_argument('-f','--full-seqs', nargs='*', help="fasta sequence files, listed")
	parser.add_argument('-o','--output', help="name of file for all sequences", required=True)
	parser.add_argument('-c','--code', type=int, default=1, help="use alternate genetic code")
	parser.add_argument('-v','--verbose', action="store_true", help="provide names of each sequence and kog")
	args = parser.parse_args(argv)

	writeoutfile = open(args.output, 'w')
	protcount = 0

	print >> sys.stderr, "Reading sequences from %s" % (args.nucleotides)
	kog_nucls = list(SeqIO.parse(open(args.nucleotides, 'r'), 'fasta'))
	print >> sys.stderr, "Found %d sequences" % (len(kog_nucls))
	
	# read in all protein sequences from all files into one dictionary
	all_prots = {}
	for seqsfile in args.full_seqs: 
		print >> sys.stderr, "Reading proteins from %s" % (str(seqsfile))
		all_prots.update(SeqIO.to_dict(SeqIO.parse(open(seqsfile, 'r'), 'fasta')))

	# for each nucleotide sequence in the 'all kog nucleotides'
	# translate it and pick the frame that contains the sequence as indicated from the protein dictionary
	# the protein sequence will then contain the ORF from * to *, as opposed to starting at the first M as in the kog_seqs.faa
	best_seqs = []
	skip_list = []
	print >> sys.stderr, "Translating sequences based on protein results"
	for nucl_seq in kog_nucls:
		if len(nucl_seq.id.split("___"))>2:
			print >> sys.stderr, "Skipping redundant seq: %s" % (str(nucl_seq.id))
			skip_list.append(nucl_seq.id.split("___")[1])
		else:
			if args.verbose:
				print >> sys.stderr, "%s" % (str(nucl_seq.id))
			# translate each protein in six frames
			prot_seqs = getSixFrame(nucl_seq,args.code)
			all_orfs = []
			for frame in prot_seqs:
				preorfs = frame.split('*')
				all_orfs.extend(preorfs)
			# keep all ORFs that contain the translated protein as from the -f protein file
			good_orfs = [orf for orf in all_orfs if str(all_prots[nucl_seq.id].seq) in orf]
			# change the sequence to the translated ORF
			if good_orfs:
				nucl_seq.seq = good_orfs[0]
			# otherwise change to X to flag the sequence as an error
			else:
				nucl_seq.seq = "XXX"
			best_seqs.append(nucl_seq)
	# check if that kog has already been added to the list
	for outseq in best_seqs:
		# if that kog is already in the list, remove both proteins from the final set
		if outseq.id.split("___")[1] in skip_list:
			print >> sys.stderr, "Skipping redundant seq: %s" % (str(outseq.id))
		# otherwise write to file as usual
		else:
			writeoutfile.write("%s" % outseq.format("fasta"))
			protcount += 1
	if args.verbose:
		print >> sys.stderr, skip_list
	print >> sys.stderr, "Wrote %d sequences to %s" % (protcount,str(args.output))
	writeoutfile.close()

	return args.output
if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

