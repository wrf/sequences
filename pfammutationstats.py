#!/usr/bin/env python
#
# pfammutationstats.py v1.0 created 2015-11-04

'''
pfammutationstats.py  last modified 2015-11-05

    EXAMPLE USAGE:
pfammutationstats.py -t transcripts.fasta -p transcripts.pfam.tab -d transcript_dNdS.tab

    GENERATE PFAM TABULAR BY:
hmmscan --cpu 4 --domtblout transcripts.pfam.tab ~/PfamScan/data/Pfam-A.hmm transcripts.transdecoder.pep > transcripts.pfam.log
'''

import sys
import time
import argparse
from collections import Counter,defaultdict
from Bio import SeqIO

def get_size_dict(transcriptfasta):
	'''from a fasta file, return dict where keys are seq IDs and values are lengths'''
	sizedict = {}
	print >> sys.stderr, "# Reading sequence lengths from {}".format(transcriptfasta), time.asctime()
	for seqrec in SeqIO.parse(transcriptfasta, "fasta"):
		sizedict[seqrec.id] = len(seqrec.seq)
	return sizedict

def parse_transcript_dnds(dndsstats, positives):
	dndsbytrans = {}
	print >> sys.stderr, "# Reading dN/dS stats from {}".format(dndsstats), time.asctime()
	for line in open(dndsstats, 'r').readlines():
		# transcript  dS   dN-pos   dN-null   dN-neg  dN/dS  dN/dS%   mutations
		line = line.strip()
		lsplits = line.split("\t")
		transid = lsplits[0]
		dncount = sum(int(x) for x in lsplits[3:5])
		if positives:
			dncount += int(lsplits[2])
		dndsbytrans[transid] = dncount
	print >> sys.stderr, "# Found dN/dS stats for {} transcripts".format(len(dndsbytrans) ), time.asctime()
	return dndsbytrans

def parse_pfam_domains(pfamtabular, evaluecutoff):
	print >> sys.stderr, "# Parsing hmmscan PFAM tabular {}".format(pfamtabular), time.asctime()
	PFAMbyTransdict = defaultdict(dict)
	evalueRemovals = 0
	for line in open(pfamtabular, 'r').readlines():
		line = line.strip()
		if not line or line[0]=="#": # skip comment lines
			continue # also catch for empty line, which would cause IndexError
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
		lsplits = line.split()
		targetname = lsplits[0]
		pfamacc = lsplits[1].rsplit('.',1)[0] # accs as PF00530.13, so chop off .13
		queryid = lsplits[3].rsplit('|',1)[0] # chop transcript from transdecoder peptide
		evalue = float(lsplits[11]) # or [6] for full seq or [12]
		accdesc = "{}_{}".format(pfamacc,targetname)
		if evalue >= evaluecutoff: # then skip sequence
			evalueRemovals += 1
			continue
		if evalue < PFAMbyTransdict[queryid].get(accdesc, 1.0 ):
			PFAMbyTransdict[queryid][accdesc] = evalue
	print >> sys.stderr, "# Found domains for {} transcripts".format(len(PFAMbyTransdict)), time.asctime()
	print >> sys.stderr, "# Removed {} hits by evalue".format(evalueRemovals), time.asctime()
	return PFAMbyTransdict

def read_pfam_tabular(pfambytrans, evaluecutoff):
	print >> sys.stderr, "# Parsing pre formatted PFAM domains {}".format(pfambytrans), time.asctime()
	PFAMbyTransdict = defaultdict(dict)
	evalueRemovals = 0
	for line in open(pfambytrans, 'r').readlines():
		if line[0]=="#": # skip comment lines
			continue
		line = line.strip()
		lsplits = line.split("\t")
		queryid = lsplits[0]
		accessions = lsplits[1].split("`") # assume Trinotate format
		if accessions[0]=="0": # empty entry from no matches, so skip
			continue
		# Aqu2.40121_001	PF00644.15^PARP^Poly(ADP-ribose) polymerase catalytic domain^85-159^E:5.9e-10
		for pfam in accessions:
			pfamterms = pfam.split("^")
			evalue = float(pfamterms[-1].split(":")[1]) # should be last term
			accdesc = "{}_{}".format(*pfamterms[0:2])
			if evalue >= evaluecutoff: # then skip sequence
				evalueRemovals += 1
				continue
			if evalue < PFAMbyTransdict[queryid].get(accdesc, 1.0 ):
				PFAMbyTransdict[queryid][accdesc] = evalue
	print >> sys.stderr, "# Found domains for {} transcripts".format(len(PFAMbyTransdict)), time.asctime()
	print >> sys.stderr, "# Removed {} hits by evalue".format(evalueRemovals), time.asctime()
	return PFAMbyTransdict

def get_most_mutant_trans(fastalengthdict, dndsbytrans, numcutoff):
	print >> sys.stderr, "# Finding {} transcripts with highest dN per base".format(numcutoff), time.asctime()
	dnbylength = {}
	for k,v in dndsbytrans.iteritems(): # keys should be transcripts, values should be dN
		dnbylength[k] = v*1.0/fastalengthdict[k]
	return sorted(dnbylength, key=lambda x: x[1], reverse=True)[:numcutoff]

def get_common_pfams(transmutants, domainsbytrans, domainstoprint):
	print >> sys.stderr, "# Writing {} most common domains".format(domainstoprint), time.asctime()
	pfamcounter = Counter()
	for k in transmutants:
		pfamcounter.update(domainsbytrans[k].keys())
	for dm,n in pfamcounter.most_common(domainstoprint):
		print >> sys.stdout, dm,n

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-d','--dnds', help="dN/dS from classifymutationstats")
	parser.add_argument('-P','--pfam', help="PFAM domain information as hmmscan tabular")
	parser.add_argument('-t','--transcripts', help="fasta format transcripts")
	parser.add_argument('-e','--evalue', type=float, default=1e-1, help="evalue cutoff for domain filtering [1e-1]")
	parser.add_argument('-m','--mutations', type=int, default=1000, help="number of most polymorphic sequences [1000]")
	parser.add_argument('-n','--num-domains', type=int, default=150, help="number of most common mutated domains [150]")
	parser.add_argument('-l','--list-form', action="store_true", help="PFAM domains are in list for from Trinotate")
	parser.add_argument('-p','--positive', action="store_true", help="include positive nonsynonymous mutations in dN/dS value")
	args = parser.parse_args(argv)

	fastalengthdict = get_size_dict(args.transcripts)
	dndsbytrans = parse_transcript_dnds(args.dnds, args.positive)
	if args.list_form:
		domainsbytrans = read_pfam_tabular(args.pfam, args.evalue)
	else:
		domainsbytrans = parse_pfam_domains(args.pfam, args.evalue)

	transmutants = get_most_mutant_trans(fastalengthdict, dndsbytrans, args.mutations)

	get_common_pfams(transmutants, domainsbytrans, args.num_domains)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
