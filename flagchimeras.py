#! /usr/bin/env python
#
# v1 created 2016-01-28

'''flagchimeras.py  v1.1 last modified 2017-03-22
    check for non-overlapping 100+ AA proteins in a transcript
    requires Bio python

flagchimeras.py transcriptome.fasta

  to show the list of genetic codes and quit
flagchimeras.py -c 0 -

    output is space separated of:
>sequenceID (forward span) (reverse span) total-protein-frame-length
    so might appear as:
>seq1234 (123,567) (890,1024) 1100
    spans are in coordinates of amino acids, not nucleotides
    meaning forward protein is predicted from 123 to 567
    and reverse protein from 890 to 1024
    from a transcript where the total possible frame was 1100 amino acids

    -v verbose mode adds three columns
  forward-frame  reverse-frame  mod3-of-bases
'''

import sys
import time
import argparse
import re
from Bio import SeqIO
from collections import deque

codelist='''List of Genetic Codes

1    The Standard Code
2    The Vertebrate Mitochondrial Code
3    The Yeast Mitochondrial Code
4    The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code
5    The Invertebrate Mitochondrial Code
6    The Ciliate, Dasycladacean and Hexamita Nuclear Code
9    The Echinoderm and Flatworm Mitochondrial Code
10   The Euplotid Nuclear Code
11   The Bacterial, Archaeal and Plant Plastid Code
12   The Alternative Yeast Nuclear Code
13   The Ascidian Mitochondrial Code
14   The Alternative Flatworm Mitochondrial Code
15   Blepharisma Nuclear Code
16   Chlorophycean Mitochondrial Code
21   Trematode Mitochondrial Code
22   Scenedesmus Obliquus Mitochondrial Code
23   Thraustochytrium Mitochondrial Code
24   Pterobranchia Mitochondrial Code'''

def get_three_frame(seq_obj,code):
	allprotframes = []
	intronphase = deque([None,-2,-1])
	# some reason there is a problem subsetting 0:0 if entire seq should be translated
	intronphase.rotate(len(seq_obj) % 3)
	allprotframes.append(seq_obj[0:intronphase[0]].translate(table=code))
	allprotframes.append(seq_obj[1:intronphase[1]].translate(table=code))
	allprotframes.append(seq_obj[2:intronphase[2]].translate(table=code))
	return allprotframes

def colorize_string(sequencestring, frameoffset, span, seqcolor, coloroff, revoffset, rfirst):
	seqtolist = list(sequencestring)
	offposition = span[1]*3
	onposition = span[0]*3
	# correct for translation frame
	if rfirst:
		if not frameoffset: # 1st frame should be offset of 3 in reverse, not 0
			frameoffset = 3
		offposition -= frameoffset
		onposition -= frameoffset
		if revoffset: # correct for frame from reverse strand, ignore frame 0
			offposition -= (3-revoffset)
			onposition -= (3-revoffset)
	else:
		offposition += frameoffset
		onposition += frameoffset
	# insert at position defined from regex plus the wrap buffer
	# wrap buffer is effectively the number of lines to that position
	seqtolist.insert(offposition, coloroff)
	seqtolist.insert(onposition, seqcolor)
	# rejoin the list as a string, and write
	joinedseq = ''.join(seqtolist)
	return joinedseq

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('rU'), default = '-', help="fasta format file, or - for stdin")
	parser.add_argument('-a', '--above', type=float, metavar='N', default=1.0, help="only translate sequences longer than N")
	parser.add_argument('-b', '--below', type=float, metavar='N', default=1000000000.0, help="only translate sequences shorter than N")
	parser.add_argument('-c', '--code', type=int, default=1, help="use alternate genetic code, type 0 to print code list and exit")
	#parser.add_argument('-i', '--interval-overlap', type=int, default=10, help="maximum allowed overlap of chimeric intervals [10]")
	parser.add_argument('-m', '--minimum', type=int, default=100, help="minimum protein length [100]")
	parser.add_argument('-t', '--color-transcript', action='store_true', help="display translated frames on the transcript")
	parser.add_argument('-u', '--blue', action='store_true', help="use yellow and blue colors")
	parser.add_argument('-v', '--verbose', action='store_true', help="display more output")
	args = parser.parse_args(argv)

	if args.code==0: # print genetic codes and exit
		sys.exit(codelist)

	print >> sys.stderr, "# Translating sequences of {}".format(args.input_file.name), time.asctime()
	startclock=time.asctime()
	starttime= time.time()

	RED = '\x1b[31m'
	GREEN = '\x1b[32m'
	YELLOW = '\x1b[33m'
	BLUE = '\x1b[34m'
	PURPLE = '\x1b[35m'
	CYAN = '\x1b[36m'
	coloroff = '\x1b[0m'

	if args.blue:
		forwardcolor = YELLOW
		reversecolor = BLUE
	else:
		forwardcolor = GREEN
		reversecolor = RED

	# basic counts
	readcount = 0
	bothlong = 0
	nuclsum = 0
	overlaps = 0
	nooverlap = 0

	for seq_record in SeqIO.parse(args.input_file, "fasta"):
		readcount += 1
		transcriptlen = len(seq_record.seq)
		nuclsum += transcriptlen

		# in all cases, start with 3-frame translation
		longestforward = 0
		forwardframe = -1
		forwardprot = ""
		forwardspan = (0,0)
		longestreverse = 0
		reverseframe = -1
		reverseprot = ""
		reversespan = (0,0)

		for i, transframe in enumerate( get_three_frame(seq_record.seq, args.code) ):
			for mo in re.finditer("M\w+",str(transframe)):
				momatch = mo.group()
				molen = len(momatch)
				if molen >= args.minimum:
					if molen > longestforward:
						forwardframe = i
						longestforward = molen
						forwardprot = momatch
						forwardspan = mo.span()
		for i, transframe in enumerate( get_three_frame(seq_record.seq.reverse_complement(), args.code) ):
			framelen = len(transframe)
			for mo in re.finditer("M\w+",str(transframe)):
				momatch = mo.group()
				molen = len(momatch)
				if molen >= args.minimum:
					if molen > longestreverse:
						reverseframe = i
						longestreverse = molen
						reverseprot = momatch
						reversespan = [ framelen-x for x in reversed(mo.span()) ] ### TODO is this needed?
						# add 1AA/3bp correction for stop codon
						reversespan = tuple( x+reversespan.index(x) for x in reversespan )
		if longestforward and longestreverse: # protein matches found both directions
			bothlong += 1
			if reversespan[0]>forwardspan[0]: # reverse is ordinal first
				if forwardspan[1]>reversespan[0]>=forwardspan[0]: # spans overlap
					overlaps += 1
					continue
				elif forwardspan[1]>reversespan[1] and forwardspan[0]<reversespan[0]:
					overlaps += 1
					continue
			else:
				if reversespan[1]>forwardspan[0]>=reversespan[0]:
					overlaps += 1
					continue
				elif reversespan[1]>forwardspan[1] and reversespan[0]<forwardspan[0]:
					overlaps += 1
					continue
			# otherwise assume no overlap
			nooverlap += 1
			if args.verbose:
				print >> sys.stdout, ">{} {} {} {} {} {} {}".format(seq_record.id, forwardspan, reversespan, framelen, forwardframe, reverseframe, transcriptlen%3 )
			else:
				print >> sys.stdout, ">{} {} {} {}".format(seq_record.id, forwardspan, reversespan, framelen)
			if args.color_transcript:
				revoffset = transcriptlen%3
				sequencestring = str(seq_record.seq)
				if reversespan[0]>forwardspan[0]: # forward is ordinal first, so do reverse first
					sequencestring = colorize_string(sequencestring, reverseframe, reversespan, reversecolor, coloroff, revoffset, rfirst=True)
					sequencestring = colorize_string(sequencestring, forwardframe, forwardspan, forwardcolor, coloroff, revoffset, rfirst=False)
				else:
					sequencestring = colorize_string(sequencestring, forwardframe, forwardspan, forwardcolor, coloroff, revoffset, rfirst=False)
					sequencestring = colorize_string(sequencestring, reverseframe, reversespan, reversecolor, coloroff, revoffset, rfirst=True)
				print >> sys.stdout, sequencestring
	print >> sys.stderr, "# Counted %d records in %.2f minutes" % (readcount, (time.time()-starttime)/60)
	print >> sys.stderr, "# Total counted bases: {}".format(nuclsum)
	print >> sys.stderr, "# Sequences with long proteins on both strands: {}".format(bothlong)
	print >> sys.stderr, "# Long proteins overlap: {}".format(overlaps)
	print >> sys.stderr, "# Non-overlapping potential chimeras: {} ({:.2f}% of total)".format(nooverlap, nooverlap*100.0/readcount)

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

