#!/usr/bin/env python
#
# pfamgff2cddgff.py v1.0 created 2016-03-18

'''
pfamgff2cddgff.py  last modified 2016-04-06

pfamgff2cddgff.py -i proteins.pfam.gtf -f family_superfamily_links -a cdd.versions > proteins.cdd.gtf

    GENERATE PFAM GFF BY:
pfam2gff.py -i proteins.pfam.tab > proteins.pfam.gtf

    download NCBI CDD family to superfamily links at:
ftp://ftp.ncbi.nih.gov/pub/mmdb/cdd/family_superfamily_links

    download NCBI CDD versions at:
ftp://ftp.ncbi.nih.gov/pub/mmdb/cdd/cdd.versions
'''

import sys
import time
import argparse
import re
from itertools import groupby
from collections import defaultdict

def parse_family_links(familylinks):
	'''read in PFAM to CDD table and make a dict where keys are PFAM accessions and values are cl accessions'''
	pfamtocdd = {}
	print >> sys.stderr, "# Parsing CDD protein family links from {}".format(familylinks), time.asctime()
	for line in open(familylinks, 'r').readlines():
		line = line.strip()
		if line:
			lsplits = line.split("\t")
			pfamacc = lsplits[0] # not always pfam
			clacc = lsplits[2]
			pfamtocdd[pfamacc] = clacc
	print >> sys.stderr, "# Found {} family links".format(len(pfamtocdd)), time.asctime()
	return pfamtocdd

def parse_version_names(cddversions):
	'''read in CDD version table and make a dict where keys are cl accessions and values are domain names'''
	cltoannot = {}
	clversion = {}
	linecounter = 0
	print >> sys.stderr, "# Parsing CDD versions from {}".format(cddversions), time.asctime()
	for line in open(cddversions, 'r').readlines():
		line = line.strip()
		if line:
			linecounter += 1
			# Acc          ShortName      PssmId  Parent   Root     Ver  Lv Rl ER Time
			lsplits = line.split()
			clacc = lsplits[0]
			cddversion = lsplits[5]
			shortname = lsplits[1]
			if cddversion > clversion.get(clacc,0):
				cltoannot[clacc] = shortname
				clversion[clacc] = cddversion	
	print >> sys.stderr, "# Counted {} annotations lines".format(linecounter), time.asctime()
	print >> sys.stderr, "# Found {} domain superfamily annotations".format(len(cltoannot)), time.asctime()
	return cltoannot

def parse_pfam_gtf(pfamgtf, pfamtocldict, removenull):
	'''read PFAM GTF, merge identical annotations, and print the domain-merged GTF'''
	cldomainsbyprot = defaultdict( lambda: defaultdict(list) ) # keys are prot ID, then domain, then boundaries
	domcount = 0
	print >> sys.stderr, "# Parsing GTF from {}".format(pfamgtf), time.asctime()
	for line in open(pfamgtf,'r').readlines():
		line = line.strip()
		if line and not line[0]=="#":
			lsplits = line.split("\t")
			protid = lsplits[0]
			boundaries = ( int(lsplits[3]), int(lsplits[4]) )
			attributes = lsplits[8]
			hitid = re.search('ID=([\w.|-]+)', attributes).group(1)
			pfamid, targetname, domnumber = hitid.split('.') # ID should appear as ID={6}.{7}.{8};
			pfamid = pfamid.replace("PF","pfam") # to correct for CDD terminology
			cldomain = pfamtocldict.get(pfamid, None if removenull else pfamid)
			if cldomain is None: # meaning removenull
				continue # skip this entry
			cldomainsbyprot[protid][cldomain].append(boundaries)
			domcount += 1
	print >> sys.stderr, "# Found {} domains for {} proteins".format(domcount, len(cldomainsbyprot) ), time.asctime()
	return cldomainsbyprot

def combine_invervals(rangelist):
	'''convert list of tuples to non redundant invervals'''
	# sl = [(1,10), (1,6), (15,20), (1,10), (19,29), (30,35), (6,13), (40,48), (15,21), (42,50)]
	nrintervallist = []
	srtrangelist = sorted(rangelist) # sort list now, to only do this once
	interval = srtrangelist[0] # need to start with first time, which will be the same for the first bounds
	for bounds in srtrangelist:
		# since it is sorted bounds[0] should always be >= interval[0]
		if bounds[0] > interval[1]+1: # if the next interval starts past the end of the first + 1
			nrintervallist.append(interval) # add to the nr list, and continue with the next
			interval = bounds
		else:
			if bounds[1] > interval[1]: # bounds[1] <= interval[1] means do not extend
				interval = (interval[0], bounds[1]) # otherwise extend the interval
	else: # append last interval
		nrintervallist.append(interval)
	# should return [(3, 13), (15, 35), (40, 50)]
	return nrintervallist

def merge_domains(cldomainsbyprot, programname, outputtype, wayout, annotdict=None):
	print >> sys.stderr, "# Merging identical domains", time.asctime()
	writecount = 0
	for protid, listbydomain in cldomainsbyprot.iteritems():
		outprotlist = []
		for domain, coordlist in listbydomain.iteritems():
			#setofcoords = ranges_to_sets(coordlist)
			domainranges = combine_invervals(coordlist)
			for i, domcoords in enumerate(domainranges):
				domainlength = domcoords[1]-domcoords[0]+1 # adjust for GFF coordinates
				if annotdict:
					# some names end with ..., should be removed
					outputid = "{}.{}.{}".format(domain, annotdict.get(domain,"NO_INFO").replace("...",""), i)
				else:
					outputid = "{}.{}".format(domain, i)
				outputtup = (protid, programname, outputtype, domcoords[0], domcoords[1], domainlength, outputid)
				outprotlist.append(outputtup)
		for ot in sorted(outprotlist, key=lambda x: x[3]):
			writecount += 1
			print >> wayout, "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t.\t.\tID={6}".format(*ot)
	print >> sys.stderr, "# Wrote {} domains".format(writecount), time.asctime()
	# NO RETURN

def get_prot_lengths(sequences):
	'''from a fasta file, return a dictionary where protein ID is the key and length is the value'''
	seqlendict = {}
	print >> sys.stderr, "# Parsing proteins from {}".format(sequences), time.asctime()
	for seqrec in SeqIO.parse(sequences,'fasta'):
		seqlendict[seqrec.id] = len(seqrec.seq)
	return seqlendict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="PFAM protein gff")
	parser.add_argument('-a','--annotation', help="NCBI CDD version annotation file")
	parser.add_argument('-f','--family', help="NCBI CDD protein family to superfamily list")
	parser.add_argument('-n','--remove-null', action="store_true", help="remove domains that have no annotation")
	parser.add_argument('-p','--program', help="program for 2nd column in output [hmmscan]", default="hmmscan")
	parser.add_argument('-s','--sequences', help="fasta format file of protein sequences")
	parser.add_argument('-t','--type', help="gff type [CDD]", default="CDD")
	args = parser.parse_args(argv)

	pfamtocldict = parse_family_links(args.family)

	cltoannot = parse_version_names(args.annotation) if args.annotation else None
	seqlens = get_prot_lengths(args.sequences) if args.sequences else None

	domainsbyprot = parse_pfam_gtf(args.input, pfamtocldict, args.remove_null)
	merge_domains(domainsbyprot, args.program, args.type, wayout, cltoannot, seqlens)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
