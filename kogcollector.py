#!/usr/bin/env python
#
# v1.3 - added $32 long sequences for comparison 2014-02-18
# v1.2 - switched to detect $64 21/mar/2013
# v1.1 - uses evalue to pick "best" sequence for each kog 04/feb/2013
# v1.01- updated doc information with our common naming conventions 01/feb/2013
# v1.0 - take kog results and full seqs files and compile all positive matches into one file 19/sep/2012

import sys
import argparse
import os
from Bio import SeqIO
from Bio.Seq import Seq

"""
kogcollector.py -r resultsfile1 resultsfile2 -f fasta1.fa fasta2.fa -o allresults.fa

typically in format of:
-r kraken_h01_transcripts_kog_results.txt
-f kraken_h01_transcripts.fa
-o kraken_all_kog_nucleotides.fasta
"""

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-r','--results', nargs='*', help="results format files, listed", default="-")
	parser.add_argument('-f','--full-seqs', nargs='*', help="fasta sequence files, listed")
	parser.add_argument('-o','--output', help="name of file for all sequences", required=True)
	parser.add_argument('-p','--protein', action="store_true", help="fasta files are of protein sequences")
	parser.add_argument('-v','--verbose', action="store_true", help="provide names of each sequence and kog")
	parser.add_argument('-d','--descriptions', help="use description file", default=os.path.expanduser("~/db/Core_genes_Parra_desc.txt"))
	args = parser.parse_args(argv)

	if not len(args.results)==len(args.full_seqs):
		print >> sys.stderr, "Number of results and full seqs files do not match"
		print >> sys.stderr, ",".join(args.results)
		print >> sys.stderr, ",".join(args.full_seqs)
		print >> sys.stderr, "Exiting"
		sys.exit()

	writeoutfile = open(args.output, 'w')
	# program assumes that results and seqs are in pairs, indicated by fileset
	fileset = 0
	# getseq is the bool for flagging whether to grab the next fasta seq ID
	getseq = False
	# goodmatch will check the evalue and determine if a seq is better than the previous
	goodmatch = False
	# getnterm will get the first line of the protein output for comparison to the fasta
	getnterm = False
	# list of sequences to search for redundant seqs
	allseqs = []
	best_kogs = {}
	kog_list = []
	# get kog list from descriptions file, list entries are stored as numbers after "KOG", so "KOG0182" will be as "0182"
	if os.path.isfile(args.descriptions):
		for line in open(args.descriptions, 'r'):
			kog_num_desc = line.split("KOG")[1]
			descriptions = kog_num_desc.split(" ",1)
			kogid = descriptions[0].rstrip()
			kog_list.append(kogid)
			best_kogs[kogid] = ["",10,0]
	else:
		print >> sys.stderr, "KOG descriptions file not found"

	# cycle through all pairs of files, and compile the unique sequences in the output file
	for resultsfile in args.results:
		print >> sys.stderr, "Getting sequences from %s" % (str(args.full_seqs[fileset]))
		refseqs = SeqIO.to_dict(SeqIO.parse(open(args.full_seqs[fileset], 'r'), 'fasta'))
		print >> sys.stderr, "Reading results from %s" % (str(args.results[fileset]))
		alllines = open(resultsfile, 'r').readlines()
		for line in alllines:
			# read through results file, get the current KOG number
			if line[0]=="#":
				active_kog = line.split("KOG")[1].split(" ")[0].rstrip()
				if args.verbose:
					print >> sys.stderr, line.rstrip()
			# get the match value (good, long, short, nonsense, etc) for the best hit
			elif line[0]=="$":
				matchvalue = int(line.split(" ")[0][1:])
				if matchvalue==1 or matchvalue==2 or matchvalue==32 or matchvalue==64:
					goodmatch = True
			# if the match is good, then get the evalue and frame
			elif line[1]==":" and goodmatch:
				goodmatch = False
				evalue = float(line.split(" ")[3].split(":")[1])
				kogframe = int(line.split(" ")[4].split(":")[1])
				if evalue < best_kogs[active_kog][1]:
					getseq = True
			# if the evalue is the best so far, then get the sequence
			elif line[0]==">" and getseq:
				if args.verbose:
					print >> sys.stderr, line.rstrip()[1:]
				kogandid = line.split("___")
				seqid = kogandid[0].strip()[1:]
				kog = kogandid[1].strip()
				# removed redundant sequences when collecting each assembly
				#if not any(str(outseq.seq)==str(s.seq) for s in allseqs):
					#allseqs.append(outseq)
				#outseq.id = seqid+"___"+kog
				#outseq.description = outseq.id
				outseq = refseqs[seqid]
				if not args.protein:
				# reverse complement if frame is negative
					if kogframe < 0:
						outseq.seq = outseq.seq.reverse_complement()
				best_kogs[active_kog] = [outseq,evalue,kogframe]
				getseq = False
		fileset+=1
	for k in kog_list:
		if best_kogs[k][0]:
			best_kogs[k][0].id = best_kogs[k][0].id+"___KOG"+k
			best_kogs[k][0].description = best_kogs[k][0].id
			writeoutfile.write("%s" % best_kogs[k][0].format("fasta"))
			allseqs.append(best_kogs[k][0])

	print >> sys.stderr, "Wrote %d sequences to %s" % (len(allseqs),str(args.output))
	writeoutfile.close()

	return args.output
if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
