#!/usr/bin/env python
# created on 2015-09-23
'''
fastaqual2fastq.py seqs.fasta seqs.qual > seqs.fastq
'''

import sys
from Bio import SeqIO
from Bio.SeqIO.QualityIO import PairedFastaQualIterator

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	handle = sys.stdout
	print >> sys.stderr, "Merging {} and {}".format(*sys.argv[1:3])
	records = PairedFastaQualIterator(open(sys.argv[1],'r'), open(sys.argv[2],'r'))
	count = SeqIO.write(records, handle, "fastq")
	print >> sys.stderr, "Converted {} records".format(count)
